package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.network.TMB_network;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.showToast;

public class TeamMemberActivity extends BaseActivity {

    ImageButton backButton;
    ImageView profileImage;

    TextView titleText, emailText, nameText, dateText, roleText;
    LinearLayout removeLayout, deleteLayout, leaveLayout, manageTeamLayout, reSendLayout;

    //    TeamItem teamItem;
    String teamId;
    MemberItem memberItem;
    Dialog progress;

    boolean me = false;
    boolean isMine = false;
    boolean isLeader = false;
    boolean isGuest = false;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_team_member);

        teamId = getIntent().getStringExtra("team_id");
        memberItem = getIntent().getParcelableExtra("member_data");
        isMine = getIntent().getBooleanExtra("team_leader", false);
        if (memberItem != null && !TextUtils.isEmpty(teamId)) {
//            for (MemberItem item : teamItem.getMemberItems()) {
//                if (item.getTeamId().equalsIgnoreCase(memberId)) {
//                    memberItem = item;
//                    break;
//                }
//            }
            isGuest = memberItem.getStatus() == 0;
            isLeader = memberItem.getRole().equalsIgnoreCase("leader");
            me = sp.getLoginEmail().equalsIgnoreCase(memberItem.getEmail());
            initView();
        } else {
            finish();
        }
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> onBackPressed());
        backButton.setVisibility(View.VISIBLE);

        titleText = fv(R.id.text_title);
        titleText.setVisibility(View.VISIBLE);
        titleText.setText(r.s(R.string.title_member_detail));

        emailText = fv(R.id.text_email);
        emailText.setText(memberItem.getEmail());

        nameText = fv(R.id.text_user_name);
        nameText.setText(isGuest ? r.s(R.string.text_guest) : memberItem.getName());
        nameText.setTextColor(ContextCompat.getColor(this, isGuest ? R.color.red_a100 : R.color.black_a100));

        reSendLayout = fv(R.id.layout_re_send);
        reSendLayout.setOnClickListener(v -> {
            requestResendEmail(teamId, memberItem.getTeamId());
        });
        reSendLayout.setVisibility(isMine && isGuest ? View.VISIBLE : View.GONE);

        dateText = fv(R.id.text_user_date);
        dateText.setText(memberItem.getRegDate());
        profileImage = fv(R.id.image_user_profile);
        if (memberItem != null && !TextUtils.isEmpty(memberItem.getProfileUrl())) {
            Glide.with(this).load(memberItem.getProfileUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_group_peacock_64).into(new BitmapImageViewTarget(profileImage) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    profileImage.setImageDrawable(circularBitmapDrawable);
                }
            });
        }

        roleText = fv(R.id.text_user_role);
        roleText.setText(r.s(isLeader ? R.string.text_leader : R.string.text_member));

        manageTeamLayout = fv(R.id.layout_team_manage);
        manageTeamLayout.setVisibility(isMine || me ? View.VISIBLE : View.GONE);
        removeLayout = fv(R.id.layout_remove_team);
        removeLayout.setOnClickListener(v -> {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_team_remove));
                dlg.setPositiveLabel(r.s(R.string.button_remove));
                dlg.setPositiveListener((dialog, tag) -> requestMemberRemove(teamId, memberItem.getTeamId()));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                sdf(dlg);
            }
        });
        removeLayout.setVisibility(isLeader || me ? View.GONE : View.VISIBLE);

        deleteLayout = fv(R.id.layout_delete_team);
        deleteLayout.setOnClickListener(v -> {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_team_delete));
                dlg.setPositiveLabel(r.s(R.string.delete));
                dlg.setPositiveListener((dialog, tag) -> requestTeamDelete(teamId));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                sdf(dlg);
            }
        });
        deleteLayout.setVisibility(isLeader && me ? View.VISIBLE : View.GONE);

        leaveLayout = fv(R.id.layout_leave_team);
        leaveLayout.setOnClickListener(v -> {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_team_leave));
                dlg.setPositiveLabel(r.s(R.string.leave));
                dlg.setPositiveListener((dialog, tag) -> requestMemberLeave(teamId, memberItem.getTeamId()));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                sdf(dlg);
            }
        });
        leaveLayout.setVisibility(!isLeader && me ? View.VISIBLE : View.GONE);


    }

    public void requestResendEmail(String team_id, String member_id) {
        showProgress();
        try {
            showProgress();
            TMB_network.reSendJoinMail(team_id, member_id)
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    if (fdf(MessageDialog.TAG) == null) {
                                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                        dlg.setMessage(r.s(R.string.message_team_invite_re_send));
                                        dlg.setTitle(r.s(R.string.title_success));
                                        dlg.setPositiveLabel(r.s(R.string.ok));
                                        dlg.setPositiveListener((dialog, tag) -> {
//                                            Intent intent = new Intent();
//                                            setResult(RESULT_OK, intent);
//                                            finish();
                                        });
                                        sdf(dlg);
                                    }
                                } else {
                                    showToast(TeamMemberActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
            dismissProgress();
        }
    }

    private void requestTeamDelete(String teamId) {
        try {
            showProgress();
            TMB_network.deleteTeam(teamId)
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    if (fdf(MessageDialog.TAG) == null) {
                                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                        dlg.setMessage(r.s(R.string.message_team_delete_complete));
                                        dlg.setTitle(r.s(R.string.title_success));
                                        dlg.setPositiveLabel(r.s(R.string.ok));
                                        dlg.setPositiveListener((dialog, tag) -> {
                                            Intent intent = new Intent();
                                            intent.putExtra("team_deleted", true);
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        });
                                        sdf(dlg);
                                    }
                                } else {
                                    showToast(TeamMemberActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
            dismissProgress();
        }
    }

    private void requestMemberRemove(String teamId, String memberId) {
        try {
            showProgress();
            TMB_network.deleteTeamMember(teamId, memberId)
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    if (fdf(MessageDialog.TAG) == null) {
                                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                        dlg.setMessage(r.s(R.string.message_team_remove_complete));
                                        dlg.setTitle(r.s(R.string.title_success));
                                        dlg.setPositiveLabel(r.s(R.string.ok));
                                        dlg.setPositiveListener((dialog, tag) -> {
                                            Intent intent = new Intent();
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        });
                                        sdf(dlg);
                                    }
                                } else {
                                    showToast(TeamMemberActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
            dismissProgress();
        }
    }

    private void requestMemberLeave(String teamId, String memberId) {
        try {
            showProgress();
            TMB_network.leaveTeamMember(teamId, memberId)
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                    dlg.setMessage(r.s(R.string.message_team_leave_complete));
                                    dlg.setTitle(r.s(R.string.title_success));
                                    dlg.setPositiveLabel(r.s(R.string.ok));
                                    dlg.setPositiveListener((dialog, tag) -> {
                                        Intent intent = new Intent();
                                        intent.putExtra("team_deleted", true);
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    });
                                    sdf(dlg);
                                } else {
                                    showToast(TeamMemberActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
            dismissProgress();
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.title_member_detail), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }
}
