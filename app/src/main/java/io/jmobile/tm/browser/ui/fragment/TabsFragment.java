package io.jmobile.tm.browser.ui.fragment;

import android.app.Activity;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.BrowserAdapter;
import io.jmobile.tm.browser.adapter.HistoryAdapter;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.BrowserItem;
import io.jmobile.tm.browser.data.HistoryItem;

public class TabsFragment extends BaseFragment {

    private static TabsFragment fragment;

    ScrollView tabsScroll;

    RecyclerView tabsLv;
    LinearLayout historyEmptyLayout;
    ExpandableListView historyLv;
    ArrayList<HistoryItem> historyList;
    Map<String, List<HistoryItem>> map;
    ArrayList<String> historyDateList;
    HistoryAdapter historyAdapter;
    ArrayList<BrowserItem> browserList;
    LinearLayoutManager browserManager;
    BrowserAdapter browserAdapter;

    LinearLayout newLayout, clearLayout, clearTabsButton;
    FrameLayout tabsButton, historyButton;
    ImageView tabsImage, historyImage;
    TextView tabsText;
    View tabsLine, historyLine;

    TabsFragmentListener listener;


    public static TabsFragment newInstance() {
        if (fragment == null)
            fragment = new TabsFragment();
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_tabs;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();

        refreshList();
    }

    @Override
    public void handleApiMessage(Message m) {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void setInitialSavedState(SavedState state) {
        super.setInitialSavedState(state);
    }

    private void initView() {
        browserList = new ArrayList<>();
        browserList.addAll(db.getBrowserItems());

        tabsScroll = fv(R.id.scroll_tabs);
        clearTabsButton = fv(R.id.layout_clear_all_tabs);
        clearTabsButton.setOnClickListener(v -> {
            db.deleteAllBrowserItems();
            browserList.clear();

            BrowserItem item = new BrowserItem();

            item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setBrowserTitle("HOME");
            item.setBrowserUrl("HOME");
            item.setBrowserPrivacy(false);
            item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
            item.setBrowserAt(System.currentTimeMillis());
            item.setBrowserTeamId(sp.getCurrentTeamId());
            if (sp.isPrivateMode()) {
                item.setBrowserTeamName("Private");
            } else
                item.setBrowserTeamName(app.getMyTeam(sp.getCurrentTeamId()).getName());
            browserList.add(item);
            sp.setBrowserTabCount(browserList.size());
            sp.setCurrentBrowserIndex(0);
            browserAdapter.setSelectedIndex(0);

            db.insertOrUpdateBrowserItem(item);
            tabsText.setText("" + browserList.size());
            if (listener != null) {
                listener.clearTabButtonClicked();
            }
        });

        tabsLv = fv(R.id.lv_tab);
        browserManager = new GridLayoutManager(getActivity(), 1);
        tabsLv.setLayoutManager(browserManager);
        browserList = new ArrayList<>();
        browserAdapter = new BrowserAdapter(getActivity(), R.layout.item_browser, browserList, new BrowserAdapter.BrowserAdapterListener() {
            @Override
            public void onCloseButtonClicked(int position, BrowserItem item) {
                deleteBrowserTabItem(position);
            }

            @Override
            public void OnItemClick(int position, BrowserItem item) {
                browserAdapter.setSelectedIndex(position);
                sp.setCurrentBrowserIndex(position);
                sp.setBrowserTabCount(browserList.size());
                tabsText.setText("" + browserList.size());

                if (listener != null)
                    listener.selectBrowserTab(position);
            }

            @Override
            public void OnItemLongClick(int position, BrowserItem item) {

            }
        });
        tabsLv.setAdapter(browserAdapter);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(tabsLv);

        historyEmptyLayout = fv(R.id.layout_history_empty);
        historyEmptyLayout.setVisibility(View.GONE);

        historyLv = fv(R.id.lv_history);
        historyLv.setOnGroupClickListener((parent, v, groupPosition, id) -> false);
        historyLv.setOnItemClickListener((parent, view, position, id) -> {
        });

        historyLv.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            String group = historyDateList.get(groupPosition);
            String url = map.get(group).get(childPosition).getHistoryUrl();
            if (listener != null) {
                listener.goToBrowser(url);
                listener.visibleNewTabLayout(false, false);
            }
            return false;
        });
        historyList = new ArrayList<>();
        map = new HashMap<>();
        historyDateList = new ArrayList<>();

        historyAdapter = new HistoryAdapter(getActivity(), map, historyDateList);
        historyLv.setAdapter(historyAdapter);

        newLayout = fv(R.id.layout_new);
        newLayout.setOnClickListener(v -> {
            BrowserItem item = new BrowserItem();

            item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setBrowserTitle("HOME");
            item.setBrowserUrl("HOME");
            item.setBrowserPrivacy(false);
            item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
            item.setBrowserAt(System.currentTimeMillis());
            item.setBrowserTeamId(sp.getCurrentTeamId());
            if (sp.isPrivateMode()) {
                item.setBrowserTeamName("Private");
            } else
                item.setBrowserTeamName(app.getMyTeam(sp.getCurrentTeamId()).getName());
            browserList.add(item);
            browserAdapter.notifyDataSetChanged();
            sp.setBrowserTabCount(browserList.size());
            sp.setCurrentBrowserIndex(browserList.size() - 1);

            db.insertOrUpdateBrowserItem(item);
            if (listener != null) {
                listener.newTabButtonClicked(item);
            }
        });

        clearLayout = fv(R.id.layout_clear);
        clearLayout.setOnClickListener(v -> {
            db.deleteAllHistoryItems();
            refreshList();
            historyLv.setVisibility(View.GONE);
            historyEmptyLayout.setVisibility(View.VISIBLE);
        });

        tabsButton = fv(R.id.layout_tabs);
        tabsButton.setOnClickListener(v -> {
            setTabMode(true);
        });
        tabsImage = fv(R.id.image_tabs);
        tabsText = fv(R.id.text_tabs);
        tabsLine = fv(R.id.view_sel_tabs);

        historyButton = fv(R.id.layout_history);
        historyButton.setOnClickListener(v -> {
            setTabMode(false);
        });
        historyImage = fv(R.id.image_history);
        historyLine = fv(R.id.view_sel_history);

        refreshList();
    }

    private void refreshList() {
        if (browserList == null)
            browserList = new ArrayList<>();
        browserList.clear();
        browserList.addAll(db.getBrowserItems());
        sp.setBrowserTabCount(browserList.size());

        if (browserAdapter != null)
            browserAdapter.setSelectedIndex(sp.getCurrentBrowserIndex());
        tabsText.setText("" + sp.getBrowserTabCount());

        if (historyList == null)
            historyList = new ArrayList<>();

        historyList.clear();
        historyList.addAll(db.getHistoryItems());

        historyDateList.clear();
        map.clear();

        for (HistoryItem item : historyList) {
            long temp = item.getHistoryAt();
            String date = Util.getDateString(temp);
            if (historyDateList.indexOf(date) < 0) {
                historyDateList.add(date);
                map.put(date, new ArrayList<HistoryItem>());
            }
            map.get(date).add(item);
        }

        historyAdapter.notifyDataSetChanged();

        for (int i = 0; i < historyDateList.size(); i++)
            historyLv.expandGroup(i);
    }

    public void setListener(TabsFragmentListener listener) {
        this.listener = listener;
    }

    public void setTabMode(boolean isTabs) {
        if (listener != null)
            listener.showHistory(!isTabs);
        tabsScroll.setVisibility(isTabs ? View.VISIBLE : View.GONE);
        newLayout.setVisibility(isTabs ? View.VISIBLE : View.GONE);
        tabsImage.setImageResource(isTabs ? R.drawable.ic_crop_din_peacock_24 : R.drawable.ic_crop_din_gray_4_24);
        tabsText.setTextColor(ContextCompat.getColor(getActivity(), isTabs ? R.color.main : R.color.gray6_100));
        tabsLine.setVisibility(isTabs ? View.VISIBLE : View.GONE);
        historyEmptyLayout.setVisibility(isTabs ? View.GONE : View.VISIBLE);
        historyLv.setVisibility(isTabs ? View.GONE : View.GONE);
        clearLayout.setVisibility(isTabs ? View.GONE : View.VISIBLE);
        historyImage.setImageResource(isTabs ? R.drawable.ic_history_gray_4_24 : R.drawable.ic_history_peacock_24);
        historyLine.setVisibility(isTabs ? View.GONE : View.VISIBLE);
        refreshList();

        if (!isTabs) {
            boolean empty = historyList.size() <= 0;
            historyLv.setVisibility(empty ? View.GONE : View.VISIBLE);
            historyEmptyLayout.setVisibility(empty ? View.VISIBLE : View.GONE);
        }
    }

    private void deleteBrowserTabItem(int position) {
        BrowserItem item = browserList.get(position);
        if (item == null)
            return;

        browserAdapter.removeBrowserItem(item.getBrowserId());

        if (sp.getCurrentBrowserIndex() >= position)
            sp.setCurrentBrowserIndex(sp.getCurrentBrowserIndex() == 0 ? 0 : sp.getCurrentBrowserIndex() - 1);

        db.deleteBrowserItem(item);
//        browserAdapter.setSelectedIndex(sp.getCurrentBrowserIndex());
//        browserList.remove(position);
//        browserAdapter.notifyItemRemoved(position);
//
//        sp.setBrowserTabCount(browserList.size());
//        tabsText.setText("" + sp.getBrowserTabCount());
        if (listener != null)
            listener.deleteBrowserItem(item);

//        if (browserList.size() <= 0) {
//
//        }
        browserList.clear();
        browserList.addAll(db.getBrowserItems());
        sp.setBrowserTabCount(browserList.size());
        browserAdapter.setSelectedIndex(sp.getCurrentBrowserIndex());
        tabsLv.setAdapter(browserAdapter);
        tabsText.setText("" + sp.getBrowserTabCount());
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                deleteBrowserTabItem(position);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;

                    LogUtil.log("" + Math.abs(1.0 - (Math.abs(dX) / itemView.getWidth())));
                    itemView.setAlpha(Math.abs(1f - (Math.abs(dX) / itemView.getWidth())));
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        };

        return simpleCallback;
    }

    public static interface TabsFragmentListener {
        public void goToBrowser(String url);

        public void visibleBrowser(boolean show);

        public void visibleNewTabLayout(boolean show, boolean history);

        public void selectBrowserTab(int position);

        public void newTabButtonClicked(BrowserItem item);

        public void clearTabButtonClicked();

        public void deleteBrowserItem(BrowserItem item);

        public void showHistory(boolean show);
    }

}
