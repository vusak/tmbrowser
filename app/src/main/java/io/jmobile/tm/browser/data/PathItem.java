package io.jmobile.tm.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.tm.browser.common.LogUtil;

public class PathItem extends TableObject {
    public static final String TABLE_NAME = "path";

    public static final String ROW_ID = "_id";
    public static final String PATH_ID = "path_id";
    public static final String PATH_NAME = "path_name";
    public static final String PATH_DEPTH = "path_depth";
    public static final String PATH_PARENT_ID = "path_parent_id";

    public static final Creator<PathItem> CREATOR = new Creator<PathItem>() {
        @Override
        public PathItem createFromParcel(Parcel source) {
            return new PathItem(source);
        }

        @Override
        public PathItem[] newArray(int size) {
            return new PathItem[size];
        }
    };

    private long id;
    private String pathId;
    private String name;
    private int depth;
    private String parentId;

    public PathItem() {
        super();
    }

//    public AppItem(JSONObject o) {
//        this.categoryId = Util.optString(o, Common.TAG_IDX);
//        this.name = Util.optString(o, Common.TAG_NAME);
//        this.thumbnailUrl = Util.optString(o, Common.TAG_THUMBNAIL);
//        this.position = Integer.valueOf(Util.optString(o, Common.TAG_POSITION));
//        this.type = Integer.valueOf(Util.optString(o, Common.TAG_LIVE_BROADCAST));
//    }


    public PathItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.pathId = in.readString();
        this.name = in.readString();
        this.depth = in.readInt();
        this.parentId = in.readString();
    }

    public static void createTablePath(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(PATH_ID + " text not null unique, ")
                .append(PATH_NAME + " text, ")
                .append(PATH_DEPTH + " integer, ")
                .append(PATH_PARENT_ID + " text) ")
                .toString());
    }

    public static PathItem getPathItem(SQLiteDatabase db, String id) {
        List<PathItem> list = getPathItmes(db, null, PATH_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<PathItem> getPathItmes(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<PathItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                PathItem item = new PathItem()
                        .setRowId(c)
                        .setPathId(c)
                        .setName(c)
                        .setDepth(c)
                        .setParentId(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdatePathItem(SQLiteDatabase db, PathItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putPathId(v)
                    .putName(v)
                    .putDepth(v)
                    .putParentId(v);

            int rowAffected = db.update(TABLE_NAME, v, PATH_ID + " =? ", new String[]{item.getPathId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getName());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deletePathItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(pathId);
        out.writeString(name);
        out.writeString(parentId);
        out.writeInt(depth);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof PathItem && pathId == ((PathItem) obj).pathId;
    }

    public long getRowId() {
        return this.id;
    }

    public PathItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public PathItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public PathItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getPathId() {
        return this.pathId;
    }

    public PathItem setPathId(String id) {
        this.pathId = id;

        return this;
    }

    public PathItem setPathId(Cursor c) {
        this.pathId = s(c, PATH_ID);

        return this;
    }

    public PathItem putPathId(ContentValues v) {
        v.put(PATH_ID, this.pathId);

        return this;
    }

    public String getName() {
        return this.name;
    }

    public PathItem setName(String name) {
        this.name = name;

        return this;
    }

    public PathItem setName(Cursor c) {
        this.name = s(c, PATH_NAME);

        return this;
    }

    public PathItem putName(ContentValues v) {
        v.put(PATH_NAME, name);

        return this;
    }

    public String getParentId() {
        return this.parentId;
    }

    public PathItem setParentId(String parentId) {
        this.parentId = parentId;

        return this;
    }

    public PathItem setParentId(Cursor c) {
        this.parentId = s(c, PATH_PARENT_ID);

        return this;
    }

    public PathItem putParentId(ContentValues v) {
        v.put(PATH_PARENT_ID, parentId);

        return this;
    }

    public int getDepth() {
        return this.depth;
    }

    public PathItem setDepth(int depth) {
        this.depth = depth;

        return this;
    }

    public PathItem setDepth(Cursor c) {
        this.depth = i(c, PATH_DEPTH);

        return this;
    }

    public PathItem putDepth(ContentValues v) {
        v.put(PATH_DEPTH, depth);

        return this;
    }
}
