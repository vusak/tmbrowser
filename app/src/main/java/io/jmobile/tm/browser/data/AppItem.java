package io.jmobile.tm.browser.data;

import android.os.Parcel;
import android.text.TextUtils;

import org.json.JSONObject;

import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.Util;

public class AppItem extends TableObject {

    public static final Creator<AppItem> CREATOR = new Creator<AppItem>() {
        @Override
        public AppItem createFromParcel(Parcel source) {
            return new AppItem(source);
        }

        @Override
        public AppItem[] newArray(int size) {
            return new AppItem[size];
        }
    };

    private int appId;
    private String name;
    private String packageName;
    private String teamId;
    private String teamName;
    private String userId;
    private String userName;
    private String regDate;
    private String initial;


    public AppItem() {
        super();
    }

    public AppItem(JSONObject o) {
        String temp = Util.optString(o, Common.TAG_IDX);
        this.appId = TextUtils.isEmpty(temp) ? -1 : Integer.parseInt(temp);
        this.teamId = Util.optString(o, Common.TAG_GROUP_IDX);
        this.teamName = Util.optString(o, Common.TAG_GROUP_NAME);
        this.name = Util.optString(o, Common.TAG_APP_NAME);
        this.packageName = Util.optString(o, Common.TAG_TARGET_URL);
        this.userId = Util.optString(o, Common.TAG_CREATOR_ID);
        this.userName = Util.optString(o, Common.TAG_CREATOR_NAME);
        this.regDate = Util.optString(o, Common.TAG_CREATE_DATE);
    }


    public AppItem(Parcel in) {
        super(in);

        this.appId = in.readInt();
        this.name = in.readString();
        this.packageName = in.readString();
        this.teamId = in.readString();
        this.teamName = in.readString();
        this.userId = in.readString();
        this.userName = in.readString();
        this.regDate = in.readString();
        this.initial = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(appId);
        out.writeString(name);
        out.writeString(packageName);
        out.writeString(teamId);
        out.writeString(teamName);
        out.writeString(userId);
        out.writeString(userName);
        out.writeString(regDate);
        out.writeString(initial);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof AppItem && appId == ((AppItem) obj).appId;
    }

    public int getAppId() {
        return this.appId;
    }

    public AppItem setAppId(int id) {
        this.appId = id;

        return this;
    }

    public String getName() {
        return this.name;
    }

    public AppItem setName(String name) {
        this.name = name;

        return this;
    }

    public String getPackagename() {
        return this.packageName;
    }

    public AppItem setPackagename(String packageName) {
        this.packageName = packageName;

        return this;
    }


    public String getTeamId() {
        return this.teamId;
    }

    public AppItem setTeamId(String teamId) {
        this.teamId = teamId;

        return this;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public AppItem setTeamName(String teamName) {
        this.teamName = teamName;

        return this;
    }

    public String getUserId() {
        return this.userId;
    }

    public AppItem setUserId(String userId) {
        this.userId = userId;

        return this;
    }

    public String getUserName() {
        return this.userName;
    }

    public AppItem setUserName(String name) {
        this.userName = name;

        return this;
    }

    public String getRegDate() {
        return this.regDate;
    }

    public AppItem setRegDate(String regDate) {
        this.regDate = regDate;

        return this;
    }

    public String getInitial() {
        return this.initial;
    }

    public AppItem setInitial(String initial) {
        this.initial = initial;

        return this;
    }
}
