package io.jmobile.tm.browser.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.AppAdapter;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.data.AppItem;
import io.jmobile.tm.browser.network.Api;

public class AppsFragment extends BaseFragment {

    LinearLayout emptyLayout;
    RecyclerView lv;
    LinearLayoutManager manager;
    AppAdapter adapter;
    ArrayList<AppItem> list;

    Api api;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_apps;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();
        api.getAppList();
    }


    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.API_CODE_APP_SELECT) {
            boolean result = m.getData().getBoolean(Common.TAG_RESULT);
            if (result) {
                list.clear();
                ArrayList<AppItem> temp = m.getData().getParcelableArrayList(Common.TAG_APPS);
                list.addAll(temp);
                adapter.notifyDataSetChanged();
                emptyLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
            }
        }
    }

    private void initView() {
        emptyLayout = fv(R.id.layout_empty);
        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new AppAdapter(getActivity(), R.layout.item_app, list, new AppAdapter.AppAdapterListener() {
            @Override
            public void OnItemClick(int position, AppItem item) {

            }

            @Override
            public void OnItemLongClick(int position, AppItem item) {

            }
        });
        lv.setAdapter(adapter);
    }

}
