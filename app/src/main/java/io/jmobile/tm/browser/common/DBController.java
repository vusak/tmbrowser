package io.jmobile.tm.browser.common;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

import io.jmobile.tm.browser.data.BookmarkItem;
import io.jmobile.tm.browser.data.BrowserItem;
import io.jmobile.tm.browser.data.FavoriteItem;
import io.jmobile.tm.browser.data.HistoryItem;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.data.ReadingItem;

public class DBController {
    public static final String DB_ID = "_id";

    private static final String DB_NAME = "tmbr.db";
    private static final int DB_VERSION = 1;
    private static DBHelper helper;
    private static SQLiteDatabase db;
    private Context context;

    public DBController(Context context) {
        this.context = context;

        if (helper == null) {
            helper = new DBHelper(context.getApplicationContext());
            db = helper.getWritableDatabase();
        }
    }

    public synchronized long getNextDBId(String tableName) {
        long id = -1;

        Cursor cursor = db.rawQuery("select seq from SQLITE_SEQUENCE where name = ?", new String[]{tableName});
        if (cursor.moveToFirst())
            id = cursor.getLong(cursor.getColumnIndex("seq"));
        cursor.close();

        return id + 1;
    }

    /**
     * Member Items
     **/
    public synchronized List<MemberItem> getMemberItems() {
        return MemberItem.getMemberItems(db, null, null, null, null, null, null, null);
    }

    public synchronized MemberItem getMyInfo() {
        return MemberItem.getMemberItem(db, Common.TMBR_PRIVATE_TEAM_ID);
    }

    public synchronized MemberItem getMemberItem(String id) {
        return MemberItem.getMemberItem(db, id);
    }

    public synchronized boolean insertOrUpdateMemberItem(MemberItem item) {
        return MemberItem.insertOrUpdateMemberItem(db, item);
    }

    public synchronized boolean deleteAllMemberItem() {
        return MemberItem.deleteAllMemberItems(db);
    }

    /**
     * Browser Items
     **/
    public synchronized BrowserItem getBrowserItem(String id) {
        return BrowserItem.getBrowserItem(db, id);
    }

    public synchronized List<BrowserItem> getBrowserItemsByTeam(String teamId) {
        return BrowserItem.getBrowserItesByTeam(db, teamId);
    }

    public synchronized List<BrowserItem> getBrowserItems() {
        return BrowserItem.getBrowserItems(db, null, null, null, null, null, null, null);
    }

    public synchronized boolean insertOrUpdateBrowserItem(BrowserItem item) {
        return BrowserItem.insertOrUpdateBrowserItem(db, item);
    }

    public synchronized boolean updateBrowser(BrowserItem item) {
        return BrowserItem.updateBrowser(db, item);
    }

    public synchronized boolean deleteBrowserItem(BrowserItem item) {
        return BrowserItem.deleteBrowserItem(db, item);
    }

    public synchronized boolean deleteAllBrowserItems() {
        return BrowserItem.deleteAllBrowserItems(db);
    }

    /**
     * History Items
     **/
    public synchronized List<HistoryItem> getHistoryItems() {
        return HistoryItem.getHistoryItems(db, null, null, null, null, null, HistoryItem.HISTORY_AT + " DESC", null);
    }

    public synchronized List<HistoryItem> getHistoryItemsByTeam(String teamId) {
        return HistoryItem.getHistoryItemsByTeam(db, teamId);
    }

    public synchronized HistoryItem getHistoryItem(String id) {
        return HistoryItem.getHistoryItem(db, id);
    }

    public synchronized boolean insertOrUpdateHistoryItem(HistoryItem item) {
        return HistoryItem.insertOrUpdateHistoryItem(db, item);
    }

    public synchronized boolean deleteHistoryItem(HistoryItem item) {
        return HistoryItem.deleteHistoryItem(db, item);
    }

    public synchronized boolean deleteAllHistoryItems() {
        return HistoryItem.deleteAllHistoryItems(db);
    }

    /**
     * Favorite Items
     **/
    public synchronized FavoriteItem getFavoriteItem(String url) {
        return FavoriteItem.getFavoriteItem(db, url);
    }

    public synchronized List<FavoriteItem> getFavoriteItems() {
        return FavoriteItem.getFavoriteItems(db, null, null, null, null, null, FavoriteItem.FAVORITE_COUNT + " DESC", "10");
    }

    public synchronized boolean insertOrUpdateFavoriteItem(FavoriteItem item) {
        return FavoriteItem.insertOrUpdateFavoriteItem(db, item);
    }

    public synchronized boolean deleteFavoriteItem(FavoriteItem item) {
        return FavoriteItem.deleteFavoriteItem(db, item);
    }

    public synchronized boolean deleteAllFavoriteItems() {
        return FavoriteItem.deleteAllFavoriteItems(db);
    }

    /**
     * Bookmark Items
     **/
    public synchronized BookmarkItem getBookmarkItem(String bookmarkId) {
        return BookmarkItem.getBookmarkItem(db, bookmarkId);
    }

    public synchronized List<BookmarkItem> getQuickmarkItems() {
        return BookmarkItem.getQuickmarkItems(db);
    }

    public synchronized List<BookmarkItem> getBookmarkItemsByFolders(String parentIdx) {
        return BookmarkItem.getBookmarkItemsByFolders(db, parentIdx);
    }

    public synchronized List<BookmarkItem> getBookmarkItems() {
        return BookmarkItem.getBookmarkItems(db, null, null, null, null, null, null, null);
    }

    public synchronized boolean insertOrUpdateBookmarkItem(BookmarkItem item) {
        return BookmarkItem.insertOrUpdateBookmarkItem(db, item);
    }

    public synchronized boolean deleteBookmarkItem(BookmarkItem item) {
        return BookmarkItem.deleteBookmarkItem(db, item);
    }

    public synchronized boolean deleteAllBookmarkItems() {
        return BookmarkItem.deleteAllBookmarkItems(db);
    }

    public synchronized boolean updateQuickItem(BookmarkItem item) {
        return BookmarkItem.insertOrUpdateBookmarkItem(db, item);
    }

    /**
     * Reading Items
     **/
    public synchronized ReadingItem getReadingItem(String readingId) {
        return ReadingItem.getReadingItem(db, readingId);
    }

    public synchronized List<ReadingItem> getReadingItems() {
        return ReadingItem.getReadingItems(db, null, null, null, null, null, null, null);
    }

    public synchronized boolean insertOrUpdateReadingItem(ReadingItem item) {
        return ReadingItem.insertOrUpdateReadingItem(db, item);
    }

    public synchronized boolean deleteReadingItem(ReadingItem item) {
        return ReadingItem.deleteReadingItem(db, item);
    }

    public synchronized boolean deleteDoneReadingItem() {
        return ReadingItem.deleteDoneReadingItem(db);
    }

    public synchronized boolean deleteAllReadingItems() {
        return ReadingItem.deleteAllReadingItems(db);
    }

    private static class DBHelper extends SQLiteOpenHelper {
        DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // create tables
            createAllTables(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            switch (newVersion) {
            }
        }

        private void dropAllTables(SQLiteDatabase db) {

        }

        private void createAllTables(SQLiteDatabase db) {
            MemberItem.createTableMember(db);
            BrowserItem.createTableBrowserItems(db);
            HistoryItem.createTableHistory(db);
            FavoriteItem.createTableFavorite(db);
            BookmarkItem.createTableBookmarks(db);
            ReadingItem.createTableReadings(db);
        }
    }
}
