package io.jmobile.tm.browser.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.ReadingAdapter;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.data.ReadingItem;
import io.jmobile.tm.browser.network.Api;
import io.jmobile.tm.browser.ui.MainActivity;

public class ReadingFragment extends BaseFragment {
    ExpandableListView lv;
    ReadingAdapter adapter;
    ArrayList<ReadingItem> list;
    Map<String, List<ReadingItem>> map;
    ArrayList<String> groupNameList;

    LinearLayout emptyLayout;

    Api api;

    private ReadingFragmentListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_reading;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);
        emptyLayout = fv(R.id.layout_empty);

        lv = fv(R.id.lv);
        lv.setOnGroupClickListener((parent, v, groupPosition, id) -> false);
        lv.setOnItemClickListener((parent, view, position, id) -> {
        });

        lv.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            String group = groupNameList.get(groupPosition);
            if (adapter.isCheckMode()) {
                adapter.select(group, childPosition);
                if (listener != null)
                    listener.onCheckChanged(adapter.getSelectedItem().size(), adapter.isSelectedAll());
            } else {
                ReadingItem item = map.get(group).get(childPosition);
                if (!item.isReadingDone()) {
                    item.setReadingDone(true);
                    if (sp.isPrivateMode()) {
                        db.insertOrUpdateReadingItem(item);
                    } else {
                        api.addOrEditReaded(item);
                    }
                }

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra(Common.ARG_BROWSER_OPEN, item.getReadingUrl());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            return false;
        });

        list = new ArrayList<>();
        map = new HashMap<>();
        groupNameList = new ArrayList<>();

        adapter = new ReadingAdapter(getActivity(), map, groupNameList);
        lv.setAdapter(adapter);

        refreshList();
    }

    @Override
    public void handleApiMessage(Message m) {
        super.handleApiMessage(m);
        if (listener != null)
            listener.onDismissProgress();
        if (m.what == Common.API_CODE_READING_SELECT || m.what == Common.API_CODE_READING_DELETE) {
            boolean result = m.getData().getBoolean(Common.TAG_RESULT);
            if (result) {
                list.clear();
                ArrayList<ReadingItem> temp = m.getData().getParcelableArrayList(Common.TAG_READINGS);
                list.addAll(temp);
                setupItems();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser)
            refreshList();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void refreshList() {
        if (list == null)
            return;

        if (sp.getCurrentTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID)) {
            list.clear();
            list.addAll(db.getReadingItems());
            setupItems();
        } else {
            if (listener != null)
                listener.onShowProgress();
            api.getReadingList();
        }
    }

    private void setupItems() {
        groupNameList.clear();
        map.clear();

        for (ReadingItem item : list) {
            if (item.isReadingDone()) {
                String read = r.s(R.string.group_reading_read);
                if (groupNameList.indexOf(read) < 0) {
                    groupNameList.add(read);
                    map.put(read, new ArrayList<ReadingItem>());
                }
                map.get(read).add(item);
            } else {
                String unRead = r.s(R.string.group_reading_unread);
                if (groupNameList.indexOf(unRead) < 0) {
                    groupNameList.add(0, unRead);
                    map.put(unRead, new ArrayList<ReadingItem>());
                }

                map.get(unRead).add(item);
            }
        }
        adapter.notifyDataSetChanged();

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);
        emptyLayout.setVisibility(empty ? View.VISIBLE : View.GONE);

        for (int i = 0; i < groupNameList.size(); i++)
            lv.expandGroup(i);

//        if (listener != null)
//            listener.notiEmptyReadingList(empty);
    }

    public void selectModeClick(boolean mode) {
        if (adapter != null)
            adapter.setCheckMode(mode);
    }

    public void setListener(ReadingFragmentListener listener) {
        this.listener = listener;
    }

    public void markAllReadStatus(boolean read) {
        if (getActivity() == null)
            return;

        if (sp.isPrivateMode() && listener != null)
            listener.onShowProgress();

        for (ReadingItem item : list) {
            if (item.isReadingDone() == read)
                continue;

            item.setReadingDone(read);
            if (sp.isPrivateMode()) {
                db.insertOrUpdateReadingItem(item);
            } else {
                if (listener != null)
                    listener.onShowProgress();
                api.addOrEditReaded(item);
            }
        }
        if (sp.isPrivateMode()) {
            lv.postDelayed((Runnable) () -> {
                refreshList();
                if (listener != null)
                    listener.onDismissProgress();
            }, 100);
        }
    }

    public void setDeleteMode(boolean mode) {
        if (getActivity() == null || adapter == null)
            return;
        adapter.setCheckMode(mode);
    }

    public void setDeleteAllSelected(boolean all) {
        if (getActivity() == null || adapter == null || !adapter.isCheckMode())
            return;

        if (all)
            adapter.setAllSelection();
        else
            adapter.clearSelection();

        if (listener != null)
            listener.onCheckChanged(adapter.getSelectedItem().size(), adapter.isSelectedAll());
    }

    public void deleteSelectedItem() {
        if (getActivity() == null || adapter == null || !adapter.isCheckMode())
            return;

        if (sp.isPrivateMode() && listener != null)
            listener.onShowProgress();

        for (ReadingItem item : adapter.getSelectedItem()) {
            if (sp.isPrivateMode()) {
                db.deleteReadingItem(item);
            } else {
                if (listener != null)
                    listener.onShowProgress();
                api.deleteReading(item);
            }
        }

        if (sp.isPrivateMode()) {
            lv.postDelayed(() -> {
                if (listener != null)
                    listener.onDismissProgress();
                refreshList();
            }, 100);

        }
    }

    public static interface ReadingFragmentListener {
        public void onCheckChanged(int count, boolean all);

        public void onShowProgress();

        public void onDismissProgress();
    }


}
