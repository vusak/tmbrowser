package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.TeamAdapter;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.base.ExpandLayout;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.data.BookmarkItem;
import io.jmobile.tm.browser.data.TeamItem;
import io.jmobile.tm.browser.network.Api;
import io.jmobile.tm.browser.ui.dialog.ContentMenuPopup;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import io.jmobile.tm.browser.ui.fragment.BookmarksFragment;
import io.jmobile.tm.browser.ui.fragment.MemosFragment;
import io.jmobile.tm.browser.ui.fragment.ReadingFragment;

import static io.jmobile.tm.browser.common.Util.Progress;

public class ContentActivity extends BaseActivity implements ReadingFragment.ReadingFragmentListener,
        BookmarksFragment.BookmarkFragmentListener,
        MemosFragment.MemoFragmentListener {

    private static final String KEY_TEAM_LIST = "team_list";
    private static final String KEY_CURRENT_TEAM = "current_team";
    private static final String KEY_SELECTED_MODE = "selected_mode";
    private static final String KEY_SELECTED_COUNT = "selected_count";
    private static final String KEY_SELECTED_ALL = "selected_all";

    ImageButton backButton, expandButton, moreButton, selectButton, trashButton;
    TextView titleText;
    ExpandLayout teamExpandLayout;
    TeamAdapter teamAdapter;
    LinearLayoutManager manager;
    ArrayList<TeamItem> teamList;
    RecyclerView teamLv;

    TabLayout tabLayout;
    ViewPager viewPager;

    BookmarksFragment bookmarksFragment;
    ReadingFragment readingFragment;
    MemosFragment memosFragment;

    Api api;
    TeamItem currentTeam;
    int currentType = Common.TYPE_BOOKMARKS;

    Dialog progress;
    int requestCount = 0;
    boolean selectedMode = false;
    int selectedCount = 0;
    boolean selectedAll = false;
    ViewPagerAdapter pagerAdapter;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_content);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        if (getIntent() != null) {
            currentType = getIntent().getIntExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_BOOKMARKS);
        }

        if (savedInstanceState == null) {
            teamList = app.getMyTeams();
            if (teamList == null)
                teamList = new ArrayList<>();
            teamList.add(0, new TeamItem().setName("me (private)")
                    .setMyRole("leader")
                    .setTeamId(Common.TMBR_PRIVATE_TEAM_ID));
            currentTeam = app.getMyTeam(sp.getCurrentTeamId());
            if (currentTeam == null) {
                TeamItem item = new TeamItem()
                        .setName("me (private)")
                        .setMyRole("leader")
                        .setTeamId(Common.TMBR_PRIVATE_TEAM_ID);
                currentTeam = item;
            }
        } else {
            teamList = savedInstanceState.getParcelableArrayList(KEY_TEAM_LIST);
            currentTeam = savedInstanceState.getParcelable(KEY_CURRENT_TEAM);
            selectedMode = savedInstanceState.getBoolean(KEY_SELECTED_MODE);
            selectedCount = savedInstanceState.getInt(KEY_SELECTED_COUNT);
            selectedAll = savedInstanceState.getBoolean(KEY_SELECTED_ALL);
        }

        initView();
        initFragment();
        setupTitle(false, false);

        if (currentType == Common.TYPE_QUICKMARK)
            bookmarksFragment.setCurrentType(currentType);
        if (currentType == Common.TYPE_FOLDER) {
            BookmarkItem folder = (BookmarkItem) getIntent().getParcelableExtra(Common.ARG_INTENT_PARENT_IDX);

            viewPager.postDelayed(() -> {
                if (bookmarksFragment != null)
                    bookmarksFragment.openFolder(folder);
            }, 100);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(KEY_TEAM_LIST, teamList);
        outState.putParcelable(KEY_CURRENT_TEAM, currentTeam);
        outState.putBoolean(KEY_SELECTED_MODE, selectedMode);
        outState.putInt(KEY_SELECTED_COUNT, selectedCount);
        outState.putBoolean(KEY_SELECTED_ALL, selectedAll);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (viewPager.getCurrentItem() == Common.TYPE_BOOKMARKS && bookmarksFragment != null)
            bookmarksFragment.refreshList(false);
        else if (viewPager.getCurrentItem() == Common.TYPE_READINGS && readingFragment != null)
            readingFragment.setUserVisibleHint(true);
        else if (viewPager.getCurrentItem() == Common.TYPE_MEMOS && memosFragment != null)
            memosFragment.setUserVisibleHint(true);
    }

    @Override
    public void onCheckChanged(int count, boolean all) {
        this.selectedCount = count;
        this.selectedAll = all;

//        showToast(this, String.format(r.s(R.string.title_selected), count) + " " + (all ? "TRUE" : "FALSE"), true);
        setupTitle(true, false);
    }

    @Override
    public void onShowProgress() {
        requestCount++;
        showProgress();
    }

    @Override
    public void onDismissProgress() {
        requestCount--;
        if (requestCount >= 0)
            dismissProgress();
    }

    private void setupTitle(boolean deleteMode, boolean init) {
        if (init) {
            selectedMode = deleteMode;
            selectedCount = 0;
            selectedAll = false;

            viewPager.postDelayed(() -> {
                if (viewPager.getCurrentItem() == Common.TYPE_BOOKMARKS && bookmarksFragment != null)
                    bookmarksFragment.setDeleteMode(deleteMode);
                else if (viewPager.getCurrentItem() == Common.TYPE_READINGS && readingFragment != null)
                    readingFragment.setDeleteMode(deleteMode);
                else if (viewPager.getCurrentItem() == Common.TYPE_MEMOS && memosFragment != null)
                    memosFragment.setDeleteMode(deleteMode);
            }, 100);

        }
        tabLayout.setVisibility(selectedMode || currentType == Common.TYPE_QUICKMARK ? View.GONE : View.VISIBLE);
        titleText.setVisibility(View.VISIBLE);
        expandButton.setVisibility(selectedMode || currentType == Common.TYPE_QUICKMARK || !sp.isLogined() ? View.GONE : View.VISIBLE);
        boolean show = true;
        if (viewPager.getCurrentItem() == Common.TYPE_READINGS && (!sp.isPrivateMode() && !currentTeam.isTeamLeader()))
//        if (viewPager.getCurrentItem() == Common.TYPE_READINGS && !sp.isPrivateMode())
            show = false;
        moreButton.setVisibility(selectedMode || !show ? View.GONE : View.VISIBLE);
        selectButton.setVisibility(selectedMode ? View.VISIBLE : View.GONE);
        trashButton.setVisibility(selectedMode ? View.VISIBLE : View.GONE);

        if (selectedMode) {
            titleText.setText(selectedCount > 0 ? String.format(r.s(R.string.title_selected), selectedCount) : r.s(R.string.title_selected_items));
            selectButton.setImageResource(selectedAll ? R.drawable.ic_playlist_unchecked_gray_4_24 : R.drawable.ic_playlist_checked_gray_4_24);
        } else {
            if (currentTeam != null)
                titleText.setText(currentType == Common.TYPE_QUICKMARK ? r.s(R.string.title_add_quickmark) : currentTeam.getName());
            else
                titleText.setText(r.s(R.string.tm_browser));
        }
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(v -> {
            if (selectedMode)
                setupTitle(false, true);
            else
                onBackPressed();
        });

        titleText = fv(R.id.text_title);

        selectButton = fv(R.id.button_select);
        selectButton.setOnClickListener(v -> {
            if (viewPager.getCurrentItem() == Common.TYPE_BOOKMARKS && bookmarksFragment != null)
                bookmarksFragment.setDeleteAllSelected(!selectedAll);
            else if (viewPager.getCurrentItem() == Common.TYPE_READINGS && readingFragment != null)
                readingFragment.setDeleteAllSelected(!selectedAll);
            else if (viewPager.getCurrentItem() == Common.TYPE_MEMOS && memosFragment != null)
                memosFragment.setDeleteAllSelected(!selectedAll);
        });
        trashButton = fv(R.id.button_trash);
        trashButton.setOnClickListener(v -> {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            if (viewPager.getCurrentItem() == Common.TYPE_BOOKMARKS && bookmarksFragment != null)
                dlg.setMessage(selectedCount > 0 ? r.s(R.string.message_delete_bookmark) : r.s(R.string.message_no_selected_bookmark));
            else if (viewPager.getCurrentItem() == Common.TYPE_READINGS && readingFragment != null)
                dlg.setMessage(selectedCount > 0 ? r.s(R.string.message_delete_reading) : r.s(R.string.message_no_selected_reading));
            else if (viewPager.getCurrentItem() == Common.TYPE_MEMOS && memosFragment != null)
                dlg.setMessage(selectedCount > 0 ? r.s(R.string.message_delete_memo) : r.s(R.string.message_no_selected_memo));

            if (selectedCount > 0) {
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveLabel(r.s(R.string.delete));
            } else {
                dlg.setNegativeLabel(r.s(R.string.ok));
            }
            dlg.setPositiveListener((dialog, tag) -> {
                if (viewPager.getCurrentItem() == Common.TYPE_BOOKMARKS && bookmarksFragment != null)
                    bookmarksFragment.deleteSelectedItem();
                else if (viewPager.getCurrentItem() == Common.TYPE_READINGS && readingFragment != null)
                    readingFragment.deleteSelectedItem();
                else if (viewPager.getCurrentItem() == Common.TYPE_MEMOS && memosFragment != null)
                    memosFragment.deleteSelectedItem();
                setupTitle(false, false);
            });
            sdf(dlg);
        });

        expandButton = fv(R.id.button_expand);
        expandButton.setOnClickListener(v -> {
            if (teamExpandLayout.isExpanded()) {
                teamExpandLayout.collapse(true);
                expandButton.setImageResource(R.drawable.ic_round_expand_more_gray_4_24);
            } else {
                teamExpandLayout.expand(true);
                expandButton.setImageResource(R.drawable.ic_round_expand_less_gray_4_24);
            }
        });
        expandButton.setImageResource(R.drawable.ic_round_expand_more_gray_4_24);

        teamLv = fv(R.id.lv_team);
        manager = new GridLayoutManager(this, 1);
        teamLv.setLayoutManager(manager);
        teamAdapter = new TeamAdapter(this, R.layout.item_team_content, teamList, new TeamAdapter.TeamAdapterListener() {
            @Override
            public void onInfoButtonClicked(int position, TeamItem item) {

            }

            @Override
            public void OnItemClick(int position, TeamItem item) {
                if (!item.getTeamId().equalsIgnoreCase(currentTeam.getTeamId())) {
                    currentTeam = item;
                    sp.setCurrentTeamId(item.getTeamId());
                    teamAdapter.setSelectedId(item.getTeamId());
                    teamExpandLayout.collapse(true);
                    expandButton.setImageResource(R.drawable.ic_round_expand_more_gray_4_24);
                    titleText.setText(item.getName());

                    int index = viewPager.getCurrentItem();
                    if (bookmarksFragment != null)
                        bookmarksFragment.refreshList(true);
                    if (readingFragment != null)
                        readingFragment.refreshList();
                    if (!currentTeam.getTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID)) {
                        if (pagerAdapter.getCount() < 3) {
                            if (memosFragment == null) {
                                memosFragment = new MemosFragment();
                                memosFragment.setListener(ContentActivity.this);
                            }
                            pagerAdapter.addFrag(memosFragment, r.s(R.string.tab_memos));
                            pagerAdapter.notifyDataSetChanged();
                        }
                        if (memosFragment != null)
                            memosFragment.refreshList();
                        viewPager.setCurrentItem(index);
                    } else {
                        if (pagerAdapter.getCount() > 2) {
                            pagerAdapter.removeFrag(2);
                            pagerAdapter.notifyDataSetChanged();
                        }
                        viewPager.setCurrentItem(index > 1 ? 0 : index);
                    }
                    boolean show = true;
                    if (viewPager.getCurrentItem() == Common.TYPE_READINGS && !sp.isPrivateMode() && !currentTeam.isTeamLeader())
//                    if (viewPager.getCurrentItem() == Common.TYPE_READINGS && !sp.isPrivateMode())
                        show = false;
                    moreButton.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            }

            @Override
            public void OnItemLongClick(int position, TeamItem item) {

            }
        });
        teamLv.setAdapter(teamAdapter);
        teamAdapter.setSelectedId(currentTeam.getTeamId());

        moreButton = fv(R.id.button_more);
        moreButton.setOnClickListener(v -> {
            if (fdf(ContentMenuPopup.TAG) == null) {
                if (teamExpandLayout.isExpanded()) {
                    teamExpandLayout.collapse(true);
                    expandButton.setImageResource(R.drawable.ic_round_expand_more_gray_4_24);
                }

                boolean edit = viewPager.getCurrentItem() == Common.TYPE_BOOKMARKS && !bookmarksFragment.isEmpty();
                ContentMenuPopup popup = ContentMenuPopup.newInstance(ContentMenuPopup.TAG, currentType, edit);
                popup.setListener(new ContentMenuPopup.ContentMenuPopupListener() {

                    @Override
                    public void onNewBookmarkButtonClicked() {
                        Intent intent = new Intent(ContentActivity.this, ContentAddActivity.class);
                        intent.putExtra(Common.ARG_INTENT_PARENT_IDX, bookmarksFragment.getParentIdx());
                        intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_BOOKMARKS);
                        intent.putExtra(Common.ARG_INTENT_ADD_QUICK, currentType == Common.TYPE_QUICKMARK);
                        intent.putExtra(Common.ARG_INTENT_TEAM_IDX, currentTeam.getTeamId());
                        startActivityForResult(intent, 2001);
                    }

                    @Override
                    public void onNewFolderButtonClicked() {
                        Intent intent = new Intent(ContentActivity.this, ContentAddActivity.class);
                        intent.putExtra(Common.ARG_INTENT_PARENT_IDX, bookmarksFragment.getParentIdx());
                        intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_FOLDER);
                        intent.putExtra(Common.ARG_INTENT_TEAM_IDX, currentTeam.getTeamId());
                        startActivityForResult(intent, 2002);
                    }

                    @Override
                    public void onNewMemoButtonClicked() {
                        Intent intent = new Intent(ContentActivity.this, ContentAddActivity.class);
                        intent.putExtra(Common.ARG_INTENT_PARENT_IDX, bookmarksFragment.getParentIdx());
                        intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_MEMOS);
                        intent.putExtra(Common.ARG_INTENT_TEAM_IDX, currentTeam.getTeamId());
                        startActivityForResult(intent, 2001);
                    }

                    @Override
                    public void onEditButtonClicked() {

                    }

                    @Override
                    public void onSelectDeleteButtonClicked() {
                        setupTitle(true, true);
                    }

                    @Override
                    public void onAllDeleteButtonClicked() {

                    }

                    @Override
                    public void onAllReadButtonClicked() {
                        if (readingFragment != null)
                            readingFragment.markAllReadStatus(true);
                    }

                    @Override
                    public void onAllUnreadButtonClicked() {
                        if (readingFragment != null)
                            readingFragment.markAllReadStatus(false);
                    }
                });
                sdf(popup);
            }
        });

        teamExpandLayout = fv(R.id.layout_expend_team);
        teamExpandLayout.collapse(false);
    }

    private void initFragment() {
        viewPager = fv(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        setupViewPager(viewPager);
        tabLayout = fv(R.id.tabs_content);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setVisibility(selectedMode || currentType == Common.TYPE_QUICKMARK ? View.GONE : View.VISIBLE);
        if (currentType == Common.TYPE_FOLDER)
            viewPager.setCurrentItem(Common.TYPE_BOOKMARKS);
        else
            viewPager.setCurrentItem(currentType);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == Common.TYPE_BOOKMARKS && bookmarksFragment != null)
                    bookmarksFragment.refreshList(false);
                currentType = position;
                boolean show = true;
                if (position == Common.TYPE_READINGS && (!sp.isPrivateMode() && !currentTeam.isTeamLeader()))
//                if (position == Common.TYPE_READINGS && !sp.isPrivateMode())
                    show = false;

                moreButton.setVisibility(show ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
        });
    }

    private void setupViewPager(ViewPager pager) {
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        bookmarksFragment = new BookmarksFragment();
        bookmarksFragment.setListener(this);
        pagerAdapter.addFrag(bookmarksFragment, r.s(R.string.tab_bookmarks));
        if (currentType != Common.TYPE_QUICKMARK) {
            readingFragment = new ReadingFragment();
            readingFragment.setListener(this);
            pagerAdapter.addFrag(readingFragment, r.s(R.string.tab_readings));
            if (!currentTeam.getTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID)) {
                memosFragment = new MemosFragment();
                memosFragment.setListener(this);
                pagerAdapter.addFrag(memosFragment, r.s(R.string.tab_memos));
            }
        }
        pager.setAdapter(pagerAdapter);
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.tm_browser), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void removeFrag(Fragment fragment, String title) {
            mFragmentList.remove(fragment);
            mFragmentTitleList.remove(title);
        }

        public void removeFrag(int index) {
            mFragmentList.remove(index);
            mFragmentTitleList.remove(index);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
