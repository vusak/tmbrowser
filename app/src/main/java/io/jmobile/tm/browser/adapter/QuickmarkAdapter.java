package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.data.BookmarkItem;

public class QuickmarkAdapter extends ReSelectableAdapter<BookmarkItem, QuickmarkAdapter.QuickmarkHolder> {

    private Context context;
    private ArrayList<BookmarkItem> items = new ArrayList<>();

    public QuickmarkAdapter(Context context, int layoutId, ArrayList<BookmarkItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(QuickmarkHolder holder, int position) {

        BookmarkItem item = items.get(position);
        boolean folder = item.isFolder();

        if (item.getBookmarkId().equalsIgnoreCase(Common.TMBR_BOOKMAKR_ADD_ID)) {
            holder.title.setText(context.getString(R.string.add));
            holder.frame.setBackgroundResource(R.color.transparent);
            holder.icon.setBackgroundResource(R.drawable.circle_gray_fill);
            holder.icon.setImageResource(R.drawable.ic_add_peacock_24);
            holder.icon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            holder.initial.setVisibility(View.GONE);
        } else {
            holder.title.setText(item.getName());
            holder.frame.setBackgroundResource(R.color.transparent);
            holder.icon.setBackgroundResource(R.color.transparent);
            holder.initial.setVisibility(View.VISIBLE);
            holder.initial.setBackgroundResource(R.drawable.circle_main);
            holder.icon.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.initial.setText(item.getInitial());

            if (!folder) {
                if (!TextUtils.isEmpty(item.getThumbnailUrl())) {
                    Glide.with(context).load(item.getThumbnailUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_tm_avator_color_48).into(new BitmapImageViewTarget(holder.icon) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.icon.setImageDrawable(circularBitmapDrawable);
                        }
                    });
                    holder.icon.setVisibility(View.VISIBLE);
                    holder.initial.setVisibility(View.GONE);
                } else {
                    holder.icon.setVisibility(View.GONE);
                    holder.initial.setVisibility(View.VISIBLE);
                }
            } else {
                holder.icon.setImageResource(R.drawable.ic_round_folder_preacock_24);
                holder.icon.setVisibility(View.VISIBLE);
                holder.initial.setVisibility(View.GONE);
            }
        }
//        Glide.with(context).load(member.getProfileUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_tm_avator_color_48).into(new BitmapImageViewTarget(holder.profiles[i]) {
//            @Override
//            protected void setResource(Bitmap resource) {
//                RoundedBitmapDrawable circularBitmapDrawable =
//                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
//                circularBitmapDrawable.setCircular(true);
//                holder.profiles[index].setImageDrawable(circularBitmapDrawable);
//            }
//        });
    }

    @Override
    public QuickmarkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        QuickmarkHolder holder = new QuickmarkHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.frame = holder.fv(R.id.layout_frame);
        holder.icon = holder.fv(R.id.image_icon);
        holder.title = holder.fv(R.id.text_title);
        holder.initial = holder.fv(R.id.text_initial);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        return holder;
    }

    public class QuickmarkHolder extends ReAbstractViewHolder {
        LinearLayout root, frame;
        ImageView icon;
        TextView title, initial;

        public QuickmarkHolder(View itemView) {
            super(itemView);
        }
    }


}
