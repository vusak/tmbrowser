package io.jmobile.tm.browser.base;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class SquareFrameLayout extends FrameLayout {
    Context context1;

    public SquareFrameLayout(Context context) {
        super(context);
        context1 = context;
    }

    public SquareFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        context1 = context;
    }

    public SquareFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        context1 = context;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
