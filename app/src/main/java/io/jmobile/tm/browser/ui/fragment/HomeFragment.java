package io.jmobile.tm.browser.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.FavoriteAdapter;
import io.jmobile.tm.browser.adapter.MemoAdapter;
import io.jmobile.tm.browser.adapter.QuickmarkAdapter;
import io.jmobile.tm.browser.adapter.ReAdapter;
import io.jmobile.tm.browser.adapter.TeamAdapter;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.base.ExpandLayout;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.BookmarkItem;
import io.jmobile.tm.browser.data.FavoriteItem;
import io.jmobile.tm.browser.data.MemoItem;
import io.jmobile.tm.browser.data.TeamItem;
import io.jmobile.tm.browser.network.Api;
import io.jmobile.tm.browser.ui.ContentActivity;
import io.jmobile.tm.browser.ui.ContentAddActivity;
import io.jmobile.tm.browser.ui.LoginActivity;
import io.jmobile.tm.browser.ui.TeamActivity;
import io.jmobile.tm.browser.ui.TeamSearchActivity;
import io.jmobile.tm.browser.ui.dialog.CreateTeamDialog;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;

public class HomeFragment extends BaseFragment {

    private static HomeFragment fragment;
    ImageButton menuButton;
    Button searchButton;
    TextView teamNameText, teamCountText;
    LinearLayout teamSettingLayout;
    LinearLayout teamInfoLayout, teamMemoLayout, myTeamLayout, mostVisitLayout;

    ExpandLayout teamExpand, visitExpand, memoExpand;
    ImageButton createTeamTopButton, searchTeamButton, refreshButton, teamHandleButton, visitHandleButton, visitDeleteButton, memoHandleButton, newMemoButton;
    LinearLayout createTeamLayout, createTeamButton;

    RecyclerView teamLv, bookmarkLv, visitLv, memoLv;
    LinearLayoutManager teamManager, bookmarkManager, visitManager, memoManager;

    TeamAdapter teamAdapter;
    ArrayList<TeamItem> list;

    QuickmarkAdapter bookmarkAdapter;
    ArrayList<BookmarkItem> bookmarkList;

    ArrayList<FavoriteItem> visitList;
    FavoriteAdapter visitAdapter;
    LinearLayout emptyVisitLayout;

    ArrayList<MemoItem> memoList;
    MemoAdapter memoAdapter;
    LinearLayout emptyMemoLayout;

    Api api;
    HomeFragmentListener listener;

    // TM FileManager banner
    CardView bannerCardView;
    RelativeLayout bannerLayout;
    ImageButton bannerCloseButton;
    Button bannerTryButton;

    public static HomeFragment newInstance() {
        if (fragment == null)
            fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);
        initView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            refreshMainList();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);

        if (createTeamButton != null)
            createTeamButton.post(() -> {
                ViewGroup.LayoutParams params = createTeamButton.getLayoutParams();
                params.width = dm.widthPixels / r.i(R.integer.quickmark_count);
                createTeamButton.setLayoutParams(params);
            });
    }

    @Override
    public void handleApiMessage(Message m) {
        super.handleApiMessage(m);
        TeamItem teamItem = app.getMyTeam(sp.getCurrentTeamId());
        if (m.what == Common.API_CODE_BOOKMARK_SELECT) {
            boolean result = m.getData().getBoolean(Common.TAG_RESULT);
            if (result) {
                bookmarkList.clear();
                ArrayList<BookmarkItem> temp = m.getData().getParcelableArrayList(Common.TAG_BOOKMARKS);
                if (temp != null && temp.size() > 0) {
                    for (BookmarkItem item : temp) {
                        if (item.isQuick())
                            bookmarkList.add(item);
                    }
                }
                if (teamItem.isTeamLeader())
                    bookmarkList.add(new BookmarkItem().setBookmarkId(Common.TMBR_BOOKMAKR_ADD_ID));
                bookmarkAdapter.notifyDataSetChanged();
            }
        } else if (m.what == Common.API_CODE_TODAY_MEMO_SELECT) {
            boolean result = m.getData().getBoolean(Common.TAG_RESULT);
            if (result) {
                memoList.clear();
                ArrayList<MemoItem> temp = m.getData().getParcelableArrayList(Common.TAG_TODAY_MEMOS);
                if (temp != null && temp.size() > 0)
                    memoList.addAll(temp);
                memoAdapter.notifyDataSetChanged();

                emptyMemoLayout.setVisibility(memoList.size() > 0 ? View.GONE : View.VISIBLE);
                memoLv.setVisibility(memoList.size() > 0 ? View.VISIBLE : View.GONE);
            }
        }
    }

    private void initView() {
        // TM Browser Banner
        bannerCardView = fv(R.id.cardview_banner);
        implementSwipeDismiss();
        bannerLayout = fv(R.id.layout_banner);

        bannerTryButton = fv(R.id.button_banner_try);
        bannerTryButton.setOnClickListener(v -> {
            Util.launchApp(getActivity(), Common.PACKAGE_TM_FILEMANAGER);
        });
        bannerCloseButton = fv(R.id.button_banner_close);
        bannerCloseButton.setOnClickListener(v -> {
            bannerClose();
        });

        menuButton = fv(R.id.button_menu);
        menuButton.setVisibility(View.VISIBLE);
        menuButton.setOnClickListener(v -> {
            if (listener != null)
                listener.onMenuButtonClicked();
        });

        searchButton = fv(R.id.button_search);
        searchButton.setOnClickListener(v -> {
            if (listener != null)
                listener.visibleUrlLayout(true, "");
        });

        teamInfoLayout = fv(R.id.layout_team_info);
        teamInfoLayout.setVisibility(View.GONE);

        teamNameText = fv(R.id.text_team_name);
        teamCountText = fv(R.id.text_team_member_count);

        teamExpand = fv(R.id.expand_team);
        teamExpand.expand(false);

        teamSettingLayout = fv(R.id.layout_team_setting);
        teamSettingLayout.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), TeamActivity.class);
            intent.putExtra("team_id", sp.getCurrentTeamId());
            getActivity().startActivityForResult(intent, 1003);
        });

        myTeamLayout = fv(R.id.layout_my_teams);

        createTeamTopButton = fv(R.id.button_team_top_create);
        createTeamTopButton.setOnClickListener(v -> {
            if (sp.isLogined()) {
                if (fdf(CreateTeamDialog.TAG) == null) {
                    CreateTeamDialog dlg = CreateTeamDialog.newInstance(CreateTeamDialog.TAG);
                    dlg.setPositiveListener((dialog, tag) -> {
                        if (listener != null)
                            listener.onRefreshButtonClicked();
                    });
                    sdf(dlg);
                }
            } else {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        searchTeamButton = fv(R.id.button_team_search);
        searchTeamButton.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), TeamSearchActivity.class);
            getActivity().startActivityForResult(intent, 1002);
        });

        refreshButton = fv(R.id.button_team_refresh);
        refreshButton.setOnClickListener(v -> {
            if (listener != null)
                listener.onRefreshButtonClicked();
        });

        teamHandleButton = fv(R.id.button_team_handle);
        teamHandleButton.setOnClickListener(v -> {
            if (teamExpand.isExpanded())
                teamExpand.collapse(true);
            else
                teamExpand.expand(true);
            teamHandleButton.setImageResource(teamExpand.isExpanded() ? R.drawable.ic_round_expand_more_gray_4_24 : R.drawable.ic_round_expand_less_gray_4_24);
        });

        createTeamLayout = fv(R.id.layout_create_team);


        createTeamButton = fv(R.id.button_team_create);
        createTeamButton.setOnClickListener(v -> {
            if (sp.isLogined()) {
                if (fdf(CreateTeamDialog.TAG) == null) {
                    CreateTeamDialog dlg = CreateTeamDialog.newInstance(CreateTeamDialog.TAG);
                    dlg.setPositiveListener((dialog, tag) -> {
                        if (listener != null)
                            listener.onRefreshButtonClicked();
                    });
                    sdf(dlg);
                }
            } else {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        mostVisitLayout = fv(R.id.layout_most_visit);

        visitExpand = fv(R.id.expand_visit);
        visitExpand.expand(false);

        visitDeleteButton = fv(R.id.button_delete_visit);
        visitDeleteButton.setOnClickListener(v -> {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setTitle(r.s(R.string.title_confirm_delete));
                dlg.setMessage(r.s(R.string.message_delete_all));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveLabel(r.s(R.string.delete));
                dlg.setPositiveListener((dialog, tag) -> {
                    db.deleteAllFavoriteItems();
                    refreshFavoriteList();
                });
                sdf(dlg);
            }

        });

        visitHandleButton = fv(R.id.button_visit_handle);
        visitHandleButton.setOnClickListener(v -> {
            if (visitExpand.isExpanded())
                visitExpand.collapse(true);
            else
                visitExpand.expand(true);
            visitHandleButton.setImageResource(visitExpand.isExpanded() ? R.drawable.ic_round_expand_more_gray_4_24 : R.drawable.ic_round_expand_less_gray_4_24);
        });

        emptyVisitLayout = fv(R.id.layout_visit_empty);
        visitLv = fv(R.id.lv_most_visit);
        visitManager = new GridLayoutManager(getActivity(), 1);
        visitLv.setLayoutManager(visitManager);
        visitList = new ArrayList<>();
        visitAdapter = new FavoriteAdapter(getActivity(), R.layout.item_favorite, visitList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                if (listener != null)
                    listener.goToBrowser(((FavoriteItem) item).getFavoriteSite());
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        visitLv.setAdapter(visitAdapter);

        teamLv = fv(R.id.lv_team_home);
        teamManager = new GridLayoutManager(getActivity(), 1);
        teamLv.setLayoutManager(teamManager);
        list = new ArrayList<>();
        teamAdapter = new TeamAdapter(getActivity(), R.layout.item_team, list, new TeamAdapter.TeamAdapterListener() {
            @Override
            public void onInfoButtonClicked(int position, TeamItem item) {

            }

            @Override
            public void OnItemClick(int position, TeamItem item) {
                sp.setCurrentTeamId(item.getTeamId());
                if (listener != null)
                    listener.onHomeSelected(item.getTeamId());
            }

            @Override
            public void OnItemLongClick(int position, TeamItem item) {

            }
        });
        teamAdapter.setEmail(sp.getLoginEmail());
        teamLv.setAdapter(teamAdapter);
        teamLv.setHasFixedSize(true);

        bookmarkLv = fv(R.id.lv_bookmark_private);
        bookmarkList = new ArrayList<>();
        bookmarkManager = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.quickmark_count));
        bookmarkLv.setLayoutManager(bookmarkManager);
        bookmarkAdapter = new QuickmarkAdapter(getActivity(), R.layout.item_quickmark, bookmarkList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                BookmarkItem bookmark = (BookmarkItem) item;
                if (bookmark.getBookmarkId().equalsIgnoreCase(Common.TMBR_BOOKMAKR_ADD_ID)) {
                    Intent intent = new Intent(getActivity(), ContentActivity.class);
                    intent.putExtra(Common.ARG_INTENT_PARENT_IDX, Common.TMBR_BOOKMARK_FOLDER_ROOT_ID);
                    intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_QUICKMARK);
                    intent.putExtra(Common.ARG_INTENT_TEAM_IDX, sp.getCurrentTeamId());
                    getActivity().startActivityForResult(intent, 1005);
                } else {
                    if (bookmark.isFolder()) {

                    } else {
                        if (listener != null)
                            listener.goToBrowser(bookmark.getUrl());
                    }
                }
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        bookmarkLv.setAdapter(bookmarkAdapter);

        teamMemoLayout = fv(R.id.layout_team_memo);
        teamMemoLayout.setVisibility(View.GONE);

        memoLv = fv(R.id.lv_team_memo);
        memoList = new ArrayList<>();
        memoManager = new GridLayoutManager(getActivity(), 1);
        memoLv.setLayoutManager(memoManager);
        memoAdapter = new MemoAdapter(getActivity(), R.layout.item_memo, sp.getLoginEmail(), memoList, new MemoAdapter.MemoAdapterListener() {
            @Override
            public void OnItemClick(int position, MemoItem item) {

            }

            @Override
            public void OnItemLongClick(int position, MemoItem item) {

            }

            @Override
            public void onMemoClicked(int position, MemoItem item) {

            }
        });
        memoLv.setAdapter(memoAdapter);
        emptyMemoLayout = fv(R.id.layout_memo_empty);

        newMemoButton = fv(R.id.button_new_memo);
        newMemoButton.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ContentAddActivity.class);
            intent.putExtra(Common.ARG_INTENT_PARENT_IDX, "0");
            intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_MEMOS);
            intent.putExtra(Common.ARG_INTENT_TEAM_IDX, sp.getCurrentTeamId());
            getActivity().startActivityForResult(intent, 1005);
        });

        memoExpand = fv(R.id.expand_team_memo);
        memoExpand.expand(false);

        memoHandleButton = fv(R.id.button_memo_handle);
        memoHandleButton.setOnClickListener(v -> {
            if (memoExpand.isExpanded())
                memoExpand.collapse(true);
            else
                memoExpand.expand(true);
            memoHandleButton.setImageResource(memoExpand.isExpanded() ? R.drawable.ic_round_expand_more_gray_4_24 : R.drawable.ic_round_expand_less_gray_4_24);
        });

        refreshMainList();
    }

    public void setMainTeam(ArrayList<TeamItem> items) {
        if (items == null || items.size() <= 0) {
            teamLv.setVisibility(View.GONE);
            createTeamLayout.setVisibility(View.VISIBLE);
            createTeamTopButton.setVisibility(View.GONE);
        } else {
            teamLv.setVisibility(View.VISIBLE);
            createTeamLayout.setVisibility(View.GONE);

            boolean create = true;
            for (TeamItem item : items) {
                if (item.isTeamLeader()) {
                    create = false;
                    break;
                }
            }
            createTeamTopButton.setVisibility(create ? View.VISIBLE : View.GONE);
        }
        searchTeamButton.setVisibility(sp.isLogined() ? View.VISIBLE : View.GONE);
        refreshButton.setVisibility(sp.isLogined() ? View.VISIBLE : View.GONE);

        if (list == null)
            list = new ArrayList<>();
        else
            list.clear();

        if (items != null)
            list.addAll(items);
        teamAdapter.notifyDataSetChanged();
    }

    public void refreshMainList() {
        if (Util.getPackageList(getActivity(), Common.PACKAGE_TM_FILEMANAGER) || !sp.isDelayedShowADTMFileManager() || !sp.isPrivateMode())
            bannerLayout.setVisibility(View.GONE);
        else
            bannerLayout.setVisibility(View.VISIBLE);
        if (sp.isPrivateMode()) {
            if (bookmarkList == null)
                bookmarkList = new ArrayList<>();
            bookmarkList.clear();
            bookmarkList.addAll(db.getQuickmarkItems());
            bookmarkList.add(new BookmarkItem().setBookmarkId(Common.TMBR_BOOKMAKR_ADD_ID));
            bookmarkAdapter.notifyDataSetChanged();
            refreshFavoriteList();
            if (!sp.isLogined())
                setMainTeam(null);
        } else {
            TeamItem item = app.getMyTeam(sp.getCurrentTeamId());
            api.getBookmarkList("", item.getTeamId());
            api.getTodayMemo();

            if (teamNameText != null) {
                teamNameText.setVisibility(View.VISIBLE);
                teamNameText.setText(item.getName());
            }

            if (teamCountText != null)
                teamCountText.setText(String.format(r.s(R.string.format_members), item.getMemberCount()));
        }

        teamInfoLayout.setVisibility(sp.isPrivateMode() ? View.GONE : View.VISIBLE);
        myTeamLayout.setVisibility(sp.isPrivateMode() ? View.VISIBLE : View.GONE);
        mostVisitLayout.setVisibility(sp.isPrivateMode() ? View.VISIBLE : View.GONE);
        teamMemoLayout.setVisibility(sp.isPrivateMode() ? View.GONE : View.VISIBLE);
    }

    public void refreshFavoriteList() {
        visitList.clear();
        visitList.addAll(db.getFavoriteItems());
        visitDeleteButton.setVisibility(visitList.size() > 0 ? View.VISIBLE : View.GONE);
        emptyVisitLayout.setVisibility(visitList.size() > 0 ? View.GONE : View.VISIBLE);
        if (visitAdapter != null)
            visitAdapter.notifyDataSetChanged();
    }

    // TM Browser banner
    private void implementSwipeDismiss() {
        SwipeDismissBehavior swipeDismissBehavior = new SwipeDismissBehavior();
        swipeDismissBehavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_START_TO_END);//Swipe direction i.e any direction, here you can put any direction LEFT or RIGHT
        swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
            @Override
            public void onDismiss(View view) {
                sp.setDelayShowAdTMFileManager();
                bannerCardView.setVisibility(View.GONE);
            }

            @Override
            public void onDragStateChanged(int state) {

            }
        });
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) bannerCardView.getLayoutParams();
        layoutParams.setBehavior(swipeDismissBehavior);//set swipe behaviour to Coordinator layout
    }

    private void bannerClose() {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_right);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                sp.setDelayShowAdTMFileManager();
                bannerCardView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        bannerCardView.startAnimation(anim);
    }

    public void setListener(HomeFragmentListener listener) {
        this.listener = listener;
    }

    public static interface HomeFragmentListener {
        public void visibleUrlLayout(boolean show, String url);

        public void onRefreshButtonClicked();

        public void onMenuButtonClicked();

        public void onHomeSelected(String teamId);

        public void goToBrowser(String url);
    }

}
