package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.MemberAdapter;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.data.TeamItem;
import io.jmobile.tm.browser.network.TMB_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.showToast;

public class TeamActivity extends BaseActivity {

    ImageButton backButton, addMemberButton;
    LinearLayout privacyLayout, settingLayout, advancedLayout, waitMemberLayout;
    TextView titleText, privacyText;
    Button leaveButton, deleteButton;

    boolean mine = false;
    RecyclerView lv, waitLv;
    ArrayList<MemberItem> list, memberList, waitList;
    LinearLayoutManager manager, waitManager;
    MemberAdapter adapter, waitAdapter;

    String teamId;
    TeamItem myTeam;
    private Dialog progress;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_team);

        teamId = getIntent().getStringExtra("team_id");
        if (!TextUtils.isEmpty(teamId))
            getTeamData();

        initView();
        setTeamData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001 || requestCode == 1002 || requestCode == 1003) {
            if (resultCode == Activity.RESULT_OK) {
//                String rule = data.getStringExtra("team_privacy");
//                setPrivacyText(rule);
                boolean delete = data.getBooleanExtra("team_deleted", false);
                if (delete) {
                    Intent intent = new Intent(TeamActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else
                    getTeamData();
            }
        }
    }

    private void setTeamData() {
        if (myTeam == null)
            return;

        titleText.setText(myTeam.getName());
        setPrivacyText(myTeam.getJoinRule());
        if (list == null)
            list = new ArrayList<>();
        list.clear();
        list.addAll(myTeam.getMemberItems());

        if (memberList == null)
            memberList = new ArrayList<>();
        memberList.clear();
        if (waitList == null)
            waitList = new ArrayList<>();
        waitList.clear();

        for (MemberItem item : list) {
            if (item.isWaitMember())
                waitList.add(item);
            else
                memberList.add(item);
        }

        if (adapter != null)
            adapter.notifyDataSetChanged();
        if (waitAdapter != null)
            waitAdapter.notifyDataSetChanged();
        waitMemberLayout.setVisibility(waitList.size() > 0 ? View.VISIBLE : View.GONE);

        addMemberButton.setVisibility(myTeam.isTeamLeader() ? View.VISIBLE : View.GONE);
        settingLayout.setVisibility(myTeam.isTeamLeader() ? View.VISIBLE : View.GONE);
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(v -> {
            onBackPressed();
        });

        titleText = fv(R.id.text_title);
        titleText.setVisibility(View.VISIBLE);
        titleText.setText(r.s(R.string.team_management));

//        nameText = fv(R.id.text_team_name);
//        nameText.setText(myTeam.getName());

        privacyLayout = fv(R.id.layout_team_privacy);
        privacyLayout.setOnClickListener(v -> {
            Intent intent = new Intent(TeamActivity.this, TeamSettingActivity.class);
            intent.putExtra("team_id", myTeam.getTeamId());
            intent.putExtra("team_privacy", myTeam.getJoinRule());
            intent.putExtra("team_privacy_domain", myTeam.getJoinDomain());
            startActivityForResult(intent, 1001);

        });

        privacyText = fv(R.id.text_team_privacy);

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        memberList = new ArrayList<>();
        manager = new GridLayoutManager(this, 1);
        lv.setLayoutManager(manager);
        adapter = new MemberAdapter(this, R.layout.item_member, memberList, new MemberAdapter.MemberAdapterListener() {
            @Override
            public void onDeleteButtonClick(int positon, MemberItem item) {

            }

            @Override
            public void OnItemClick(int position, MemberItem item) {
                MemberItem _item = item;
                Intent intent = new Intent(TeamActivity.this, TeamMemberActivity.class);
//                intent.putExtra("team_id", myTeam.getTeamId());
                intent.putExtra("team_id", myTeam.getTeamId());
                intent.putExtra("team_leader", myTeam.isTeamLeader());
                intent.putExtra("member_data", _item);
                startActivityForResult(intent, 1002);
            }

            @Override
            public void OnItemLongClick(int position, MemberItem item) {

            }
        });
        lv.setAdapter(adapter);

        waitMemberLayout = fv(R.id.layout_team_wait_member);
        waitMemberLayout.setVisibility(View.GONE);
        waitLv = fv(R.id.lv_wait_member);
        waitList = new ArrayList<>();
        waitManager = new GridLayoutManager(this, 1);
        waitLv.setLayoutManager(waitManager);
        waitAdapter = new MemberAdapter(this, R.layout.item_wait_member, waitList, new MemberAdapter.MemberAdapterListener() {
            @Override
            public void onDeleteButtonClick(int positon, MemberItem item) {

            }

            @Override
            public void OnItemClick(int position, MemberItem item) {
                waitingMemberConfirm(item);
            }

            @Override
            public void OnItemLongClick(int position, MemberItem item) {

            }
        });
        waitLv.setAdapter(waitAdapter);

        addMemberButton = fv(R.id.button_add_member);
        addMemberButton.setOnClickListener(v -> {
            Intent intent = new Intent(TeamActivity.this, TeamAddMemberActivity.class);
            intent.putParcelableArrayListExtra("team_members", myTeam.getMemberItems());
            startActivityForResult(intent, 1003);
        });

        settingLayout = fv(R.id.layout_team_setting);
        advancedLayout = fv(R.id.layout_team_advanced);
        advancedLayout.setVisibility(View.GONE);
    }

    private void setPrivacyText(String privacy) {
        String rule = r.s(R.string.text_private);
        if (privacy.equalsIgnoreCase(TeamItem.TEAM_RULE_PUBLIC))
            rule = r.s(R.string.text_open);
        else if (privacy.equalsIgnoreCase(TeamItem.TEAM_RULE_PROTECT))
            rule = r.s(R.string.text_join_immediately);

        privacyText.setText(rule);
    }

    private void getTeamData() {
        try {
            showProgress();
            TMB_network.getTeamInfo(teamId)
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
                                    myTeam = new TeamItem(result);
                                    if (myTeam != null)
                                        setTeamData();
//                                    app.setMyTeam(myTeam);
                                } else {
                                    showToast(TeamActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void waitingMemberConfirm(MemberItem mItem) {
        try {
            showProgress();
            TMB_network.waitMemberConfirm(myTeam.getTeamId(), mItem.getTeamId())
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    getTeamData();
                                } else {
                                    showToast(TeamActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.title_my_team), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

}
