package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.data.BookmarkItem;

public class FolderAdapter extends ReSelectableAdapter<BookmarkItem, FolderAdapter.FolderHolder> {

    private Context context;
    private ArrayList<BookmarkItem> items = new ArrayList<>();

    public FolderAdapter(Context context, int layoutId, ArrayList<BookmarkItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(FolderHolder holder, int position) {

        BookmarkItem item = items.get(position);

        holder.name.setText(item.getName());
        holder.name.setTextColor(ContextCompat.getColor(context, items.size() - 1 == position ? R.color.main : R.color.black_a100));
        holder.icon.setVisibility(items.size() - 1 == position ? View.INVISIBLE : View.VISIBLE);

    }

    @Override
    public FolderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FolderHolder holder = new FolderHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.name = holder.fv(R.id.text_name);
        holder.icon = holder.fv(R.id.image_icon);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }


    public class FolderHolder extends ReAbstractViewHolder {
        TextView name;
        ImageView icon;

        public FolderHolder(View itemView) {
            super(itemView);
        }
    }
}
