package io.jmobile.tm.browser.ui.fragment;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.Parcel;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.base.ExpandLayout;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.ImageUtil;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.BookmarkItem;
import io.jmobile.tm.browser.data.BrowserItem;
import io.jmobile.tm.browser.data.FavoriteItem;
import io.jmobile.tm.browser.data.HistoryItem;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.data.ReadingItem;
import io.jmobile.tm.browser.data.TeamItem;
import io.jmobile.tm.browser.network.Api;

public class BrowserFragment extends BaseFragment implements GestureDetector.OnGestureListener {

    public static final String MAIL_TO = "mailto:";
    public static final String INTENT_URI_START = "intent:";
    public static final String INTENT_FALLBACK_URL = "browser_fallback_url";
    public static final String URI_SCHEME_MARKET = "market://details?id=";
    private static BrowserFragment fragment;
    public WebView webView;
    TextView urlEdit; //, newTabButton;
    boolean bookmark = false;
    ImageButton refreshButton, bookmarkButton;
    ProgressBar loadingProgress;
    ExpandLayout urlExpand;
    String currentTitle, currentSite, addHistoryUrl;
    Bitmap currentIcon;
    String readingTitle, readingUrl, readingDescription, readingImage;
    ArrayList<String> readingTags;
    GestureDetector detector;
    float initialY = -1;
    String checkUrl = "";
    BrowserItem item;
    LinearLayout rootLayout;
    BrowserFragmentListener listener;
    Api api;
    LinearLayout mainTopLayout;
    private RelativeLayout childLayout;
    private RelativeLayout browserLayout;
    private ImageButton mainCloseButton;
    private TextView titleText;

    public static BrowserFragment newInstance() {
        if (fragment == null)
            fragment = new BrowserFragment();
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_browser;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (webView != null) {
            webView.resumeTimers();
            webView.onResume();
            webView.reload();
        }

        if (urlExpand != null)
            urlExpand.collapse(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (webView != null) {
            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (webView != null) {

            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }
    }

    @Override
    public void onDestroy() {
        if (webView != null) {
            webView.stopLoading();
            webView.onPause();
            webView.destroy();
        }

        super.onDestroy();
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (urlExpand.isExpanded() && distanceY > 0 && Math.abs(initialY - distanceY) > 100) {
            urlExpand.collapse(true);
            initialY = distanceY;
            return true;
        } else if (!urlExpand.isExpanded() && distanceY < 0 && Math.abs(initialY - distanceY) > 100) {
            urlExpand.expand(true);
            initialY = distanceY;
            return true;
        }
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    public boolean onPrevButtonClicked() {

        if (childLayout != null && childLayout.getVisibility() == View.VISIBLE) {
            WebView view = (WebView) browserLayout.getChildAt(0);
            if (view != null && view.canGoBack())
                view.goBack();
            else
                closeChild();
            return true;
        } else if (webView != null && webView.canGoBack()) {
            webView.goBack();
            return true;
        }

        return false;
    }

    public void onNextButtonClicked() {
        if (webView != null)
            webView.goForward();
    }

    public void setURLString(String url) {
        urlEdit.setText(url);
    }

    public void setListener(BrowserFragmentListener listener) {
        this.listener = listener;
    }

    public BrowserItem getBrowserItem() {
        return this.item;
    }

    public void setBrowserItem(BrowserItem item) {
        if (item == null)
            return;

        this.item = item;

    }

    public void goUrl(final String url) {
        if (urlExpand != null && !urlExpand.isExpanded())
            urlExpand.expand(false);

        if (urlEdit != null)
            urlEdit.post(() -> urlEdit.setText(url));

        if (webView != null)
            webView.post(() -> {
                webView.onResume();
                webView.loadUrl(url);
            });

    }

    public void saveBrowserItem(boolean update) {
        Bundle bundle = new Bundle();
        webView.saveState(bundle);
        final Parcel parcel = Parcel.obtain();
        bundle.writeToParcel(parcel, 0);
        byte[] bundleBytes = parcel.marshall();
        parcel.recycle();

        if (bundleBytes != null)
            item.setBrowserHistory(bundleBytes);
        if (update)
            new UpdateBrowserItem().execute(item);
        else
            new UnderUpdateBrowserItem().execute(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void handleApiMessage(Message m) {
        boolean result = m.getData().getBoolean(Common.TAG_RESULT);
        if (m.what == Common.API_CODE_BOOKMARK_ADD) {
            Util.showToast(getActivity(), result ? r.s(R.string.toast_message_added_bookmark) : r.s(R.string.toast_message_failed_bookmark), false);
        } else if (m.what == Common.API_CODE_READING_ADD) {
            Util.showToast(getActivity(), result ? r.s(R.string.toast_message_added_reading) : r.s(R.string.toast_message_failed_reading), false);
        }
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();

        detector = new GestureDetector(getActivity(), this);
        if (webView != null) {

            if ((webView.copyBackForwardList() == null || (webView.copyBackForwardList().getSize() <= 0) && (item != null && item.getBrowserHistory() != null))) {
                if (webView.getOriginalUrl() != null && webView.getOriginalUrl().equalsIgnoreCase(item.getBrowserUrl()))
                    return;

                byte[] bundleBytes = item.getBrowserHistory();
                final Parcel parcel = Parcel.obtain();
                parcel.unmarshall(bundleBytes, 0, bundleBytes.length);
                parcel.setDataPosition(0);
                Bundle bundle = (Bundle) parcel.readBundle();
                parcel.recycle();

                webView.restoreState(bundle);

                if (webView.getUrl() != null)
                    urlEdit.setText(webView.getUrl());
            }
        }

//        if (getArguments() != null) {
////            String url = getArguments().getString(Common.INTENT_GO_TO_URL);
////            if (!TextUtils.isEmpty(url))
////                goUrl(url);
//        }
    }

    private void initView() {
        if (LogUtil.isDebugMode() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            WebView.setWebContentsDebuggingEnabled(true);

        rootLayout = fv(R.id.layout_root);

        urlExpand = fv(R.id.layout_expend_url);
        urlExpand.expand(false);

        browserLayout = fv(R.id.mainBrowserLayout);
        childLayout = fv(R.id.mainAdChildLayout);
        titleText = fv(R.id.mainTitleText);
        mainTopLayout = fv(R.id.mainLinearLayout);
        mainCloseButton = fv(R.id.mainCloseButton);
        mainCloseButton.setOnClickListener(v -> {
            closeChild();
        });

        webView = fv(R.id.webview);

        if (!sp.isSavePasswords()) {
            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(webView.getContext());
            android.webkit.CookieManager cookieManager = android.webkit.CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.removeSessionCookies(null);
                cookieManager.removeAllCookies(null);
            } else {
                cookieManager.removeSessionCookie();
                cookieManager.removeAllCookie();
            }
            cookieSyncManager.sync();
        }

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDatabaseEnabled(sp.isSavePasswords());
        webView.getSettings().setAppCacheEnabled(sp.isSavePasswords());
        webView.getSettings().setDomStorageEnabled(sp.isSavePasswords());
        webView.getSettings().setSavePassword(sp.isSavePasswords());
        webView.getSettings().setSaveFormData(sp.isSavePasswords());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportMultipleWindows(true);

        webView.setWebViewClient(new MyWebViewClient());
        webView.setOnTouchListener((v, event) -> detector.onTouchEvent(event));
        webView.requestFocus();

        webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");
        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                currentTitle = title;
                currentSite = view.getOriginalUrl();
                item.setBrowserTitle(title);
                item.setBrowserUrl(currentSite);
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);
//                item.setBrowserIcon(ImageUtil.getImageBytes(icon));
                currentIcon = icon;
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                loadingProgress.setProgress(newProgress);
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                WebView.HitTestResult result = view.getHitTestResult();
                String url = result.getExtra();

                if (TextUtils.isEmpty(url)) {
                    Message href = view.getHandler().obtainMessage();
                    view.requestFocusNodeHref(href);

                    url = href.getData().getString("url");
                    if (TextUtils.isEmpty(url)) {
                        openChild(resultMsg);
                        return true;
                    }
                }
                switch (result.getType()) {
                    case WebView.HitTestResult.EMAIL_TYPE:
                        openEmail(url);
                        return false;
                }
                if (Uri.parse(url).getScheme().equals("market") || url.contains("https://play.google.com/store/apps/")) {
                    if (childLayout != null && childLayout.getVisibility() == View.VISIBLE) {
                        closeChild();
                    }
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return false;
                }
                // 구글 마켓 이동 doFallback
                else if (url.toLowerCase().startsWith(INTENT_URI_START)) {
                    Intent parsedIntent = null;
                    try {
                        parsedIntent = Intent.parseUri(url, 0);
                        startActivity(parsedIntent);
                    } catch (ActivityNotFoundException | URISyntaxException e) {
                        return doFallback(view, parsedIntent);
                    }
                } else {
                    openChild(resultMsg);
                }
                return true;
            }

        });

        urlEdit = fv(R.id.text_url);
        urlEdit.setOnTouchListener((v, event) -> {
            if (webView != null)
                webView.onPause();
            if (listener != null)
                listener.visibleUrlLayout(true, webView.getUrl());
            return false;
        });

        refreshButton = fv(R.id.button_url_refresh);
        refreshButton.setOnClickListener(v -> webView.reload());

        loadingProgress = fv(R.id.progress_url);
    }

    private void addHistoryList(String url) {
        if (TextUtils.isEmpty(url))
            return;

        addHistoryUrl = url;
        HistoryItem item = new HistoryItem();
        item.setHistoryId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setHistoryUrl(url);
        item.setHistoryTitle(currentTitle);
        item.setHistoryAt(System.currentTimeMillis());
        item.setHistoryTeamId(sp.getCurrentTeamId());

        new AddHistoryItem().execute(item);
    }

    public void addBookmarkItem(String teamId) {
        if (currentSite == null || currentTitle == null || currentSite.isEmpty() || currentTitle.isEmpty()) {
            Util.showToast(getActivity(), r.s(R.string.toast_message_failed_bookmark), false);
            return;
        }

        BookmarkItem item = new BookmarkItem();
        item.setUrl(currentSite);
        item.setName(currentTitle);
        item.setFolder(false);
        item.setRegDate("" + System.currentTimeMillis());
        item.setTeamId(teamId);
        String iconUrl = Uri.parse(currentSite).getHost() + "/favicon.ico";
        if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
            iconUrl = "http://" + iconUrl;
        item.setThumbnailUrl(iconUrl);
        item.setInitial(currentTitle.substring(0, 1));

        if (teamId.equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID)) {
            item.setBookmarkId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setParentIdx(Common.TMBR_BOOKMARK_FOLDER_ROOT_ID);
            boolean result = db.insertOrUpdateBookmarkItem(item);
            if (item == null || !result)
                Util.showToast(getActivity(), r.s(R.string.toast_message_failed_bookmark), false);
            else
                Util.showToast(getActivity(), r.s(R.string.toast_message_added_bookmark), false);
        } else {
            TeamItem team = app.getMyTeam(teamId);
            item.setTeamName(team.getName());
            MemberItem myInfo = db.getMyInfo();
            item.setUserId(myInfo.getEmail());
            item.setUserName(myInfo.getName());
            api.addOrEditBookmark(item, Common.TMBR_BOOKMARK_FOLDER_ROOT_ID, false);
        }
//        new AddBookmarkItem().execute(item);
    }

    public void addReadingItem(String teamId) {
        if (readingTitle == null || currentSite == null || readingTitle.isEmpty() || currentSite.isEmpty()) {
            Util.showToast(getActivity(), r.s(R.string.toast_message_failed_reading), false);
            return;
        }

        ReadingItem item = new ReadingItem();
        item.setReadingUrl(currentSite);
        item.setReadingTitle(readingTitle);
        item.setReadingContents(readingDescription);
        item.setReadingThumbnailPath(readingImage);
        item.setReadingAt(System.currentTimeMillis());

        if (teamId.equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID)) {
            item.setReadingId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            new AddReadingItem().execute(item);
        } else {
            TeamItem team = app.getMyTeam(teamId);
            item.setTeamId(team.getTeamId());
            item.setTeamName(team.getName());
            MemberItem myInfo = db.getMyInfo();
            item.setUserId(myInfo.getEmail());
            item.setUserName(myInfo.getName());
            api.addOrEditReading(item);
        }
    }

    public void setHomeData() {
        if (webView != null) {
            webView.clearHistory();
            item.setBrowserUrl("HOME");
            item.setBrowserTitle("HOME");
            item.setBrowserIcon((byte[]) null);
            item.setBrowserThumbnail((byte[]) null);
            item.setBrowserHistory((byte[]) null);
            db.updateBrowser(item);
        }
    }

    public void clearCurrData() {

        readingTitle = "";
        readingUrl = "";
        readingDescription = "";
        readingImage = "";
        currentIcon = null;
        currentTitle = "";
        currentSite = "";
        addHistoryUrl = "";
    }

    public void stopWebViewLoading() {
        if (webView != null) {
            webView.onPause();
        }
    }

    public String getCurrentUrl() {
        String url = "";
        if (urlEdit != null)
            url = urlEdit.getText().toString();

        return url;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    // 구글 마켓 이동
    private boolean doFallback(WebView view, Intent parsedIntent) {
        if (parsedIntent == null) {
            return false;
        }

        String fallbackUrl = parsedIntent.getStringExtra(INTENT_FALLBACK_URL);
        if (fallbackUrl != null) {
            view.loadUrl(fallbackUrl);
            return true;
        }

        String packageName = parsedIntent.getPackage();
        if (packageName != null) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URI_SCHEME_MARKET + packageName)));
            return true;
        }
        return false;
    }

    private void closeChild() {
        int colorStart = r.c(getActivity(), R.color.black_a80);
        int colorEnd = Color.TRANSPARENT;

        ValueAnimator colorAnim = ObjectAnimator.ofInt((View) mainTopLayout, "backgroundColor", colorStart, colorEnd);
        colorAnim.setDuration(100);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(0);
        colorAnim.start();

        Animation slideClose = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_close);
        childLayout.startAnimation(slideClose);
        slideClose.setAnimationListener(new

                                                Animation.AnimationListener() {
                                                    @Override
                                                    public void onAnimationStart(Animation animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animation animation) {
                                                        titleText.setText("");
                                                        childLayout.setVisibility(View.INVISIBLE);
                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animation animation) {

                                                    }
                                                });
    }

    private void openChild(Message resultMsg) {
        // remove any current child views
        browserLayout.removeAllViews();
        // make the child web view's layout visible
        childLayout.setVisibility(View.VISIBLE);

        // now create a new web view
        WebView newView = new WebView(getActivity());
        WebSettings settings = newView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSupportMultipleWindows(true);
        settings.setUseWideViewPort(false);
        newView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                // once the view has loaded, display its title
                titleText.setText(view.getTitle());
            }
        });

        newView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                openChild(resultMsg);
                return true;
            }
        });
        // add the new web view to the layout
        newView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        browserLayout.addView(newView);
        // tell the transport about the new view
        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
        transport.setWebView(newView);
        resultMsg.sendToTarget();

        // let's be cool and slide the new web view up into view
        Animation slideOpen = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_open);
        childLayout.startAnimation(slideOpen);

        if (urlExpand != null && urlExpand.isExpanded())
            urlExpand.collapse(true);

        int colorStart = Color.TRANSPARENT;
        int colorEnd = r.c(getActivity(), R.color.black_a80);

        ValueAnimator colorAnim = ObjectAnimator.ofInt((View) mainTopLayout, "backgroundColor", colorStart, colorEnd);
        colorAnim.setDuration(4000);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(0);
        colorAnim.start();
    }

    private void openEmail(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + email));
        intent.putExtra(Intent.EXTRA_EMAIL, email);

        startActivity(intent);
    }

    public static interface BrowserFragmentListener {
        public void visibleUrlLayout(boolean show, String url);

        public void savedBrowserItem();
    }

    private class MyWebViewClient extends WebViewClient {

        String compareText = "";

        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            resend.sendToTarget();
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            if (url.equals("about:blank")) {
                urlEdit.setText("");
                clearCurrData();
            }
            super.onPageCommitVisible(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            loadingProgress.setVisibility(View.VISIBLE);
            urlExpand.collapse(false);
            clearCurrData();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            view.loadUrl("javascript:window.Android.getHeader(document.getElementsByTagName('head')[0].innerHTML);");

            compareText = url;
            loadingProgress.setVisibility(View.INVISIBLE);

            if (url.equals("about:blank") || Common.SITE.equals(compareText)) {
                return;
            }

            Common.SITE = url;
            sp.setBrowserUrl(url);
            item.setBrowserUrl(url);
            urlEdit.setText(url);
            currentSite = url;
            urlExpand.collapse(false);
            super.onPageFinished(view, url);
        }

        @Override
        public void onLoadResource(WebView view, final String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            super.doUpdateVisitedHistory(view, url, isReload);
            addHistoryList(currentSite);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getScheme().equals("market") || url.contains("https://play.google.com/store/apps/")) {
                if (childLayout != null && childLayout.getVisibility() == View.VISIBLE) {
                    closeChild();
                }
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            }
            // 구글 마켓 이동 doFallback
            else if (url.toLowerCase().startsWith(INTENT_URI_START)) {
                Intent parsedIntent = null;
                try {
                    parsedIntent = Intent.parseUri(url, 0);
                    startActivity(parsedIntent);
                } catch (ActivityNotFoundException | URISyntaxException e) {
                    return doFallback(view, parsedIntent);
                }
            } else if (url.startsWith("mailto:")) {
                return true;
            } else {
                view.loadUrl(url);
            }
            return false;
        }
    }

    private class MyJavascriptInterface {

        @JavascriptInterface
        public void getHeader(String header) {
            readingTitle = "";
            readingUrl = "";
            readingDescription = "";
            readingImage = "";
            readingTags = new ArrayList<>();

            Pattern p = Pattern.compile("(<meta.*?property=\")(.*?)(\".*?content=\")(.*?)(\".*?>)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);
            Matcher m = p.matcher(header);
            while (m.find()) {
                String name = m.group(2);
                String content = m.group(4).replace("&quot;", "\"");
                if (name.equalsIgnoreCase(Common.META_NAME_NAME))
                    readingTitle = content;

                if (name.equalsIgnoreCase(Common.META_NAME_URL))
                    readingUrl = content;

                if (name.equalsIgnoreCase(Common.META_NAME_DESCRIPTION))
                    readingDescription = content;

                if (name.equalsIgnoreCase(Common.META_NAME_IMAGE))
                    readingImage = content;

                if (name.equalsIgnoreCase(Common.META_NAME_TAG))
                    readingTags.add(content);

                LogUtil.log("Reading Item name : " + name + "  content : " + content);
            }
        }
    }

    /**
     * Browser List
     */
    class UpdateBrowserItem extends AsyncTask<BrowserItem, Void, BrowserItem> {
        @Override
        protected BrowserItem doInBackground(BrowserItem... params) {
            BrowserItem item = params[0];
            if (webView != null) {
                item.setBrowserThumbnail(ImageUtil.getScreenShotByteArray(getActivity(), webView));
            }

            if (currentIcon != null) {
                item.setBrowserIcon(ImageUtil.getImageBytes(currentIcon));
            }

            return item;
        }

        @Override
        protected void onPostExecute(BrowserItem browserItem) {
            item.setBrowserAt(System.currentTimeMillis());
            if (sp.getCurrentTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID))
                item.setBrowserTeamName("Private");
            else
                item.setBrowserTeamName(app.getMyTeam(sp.getCurrentTeamId()).getName());
            item.setBrowserTeamId(sp.getCurrentTeamId());
            db.insertOrUpdateBrowserItem(browserItem);
            if (listener != null)
                listener.savedBrowserItem();
        }
    }

    class UnderUpdateBrowserItem extends AsyncTask<BrowserItem, Void, BrowserItem> {
        @Override
        protected BrowserItem doInBackground(BrowserItem... params) {
            BrowserItem item = params[0];
            if (webView != null) {
                item.setBrowserThumbnail(ImageUtil.getScreenShotByteArray(getActivity(), webView));
            }

            if (currentIcon != null) {
                item.setBrowserIcon(ImageUtil.getImageBytes(currentIcon));
            }

            return item;
        }

        @Override
        protected void onPostExecute(BrowserItem browserItem) {
            item.setBrowserAt(System.currentTimeMillis());
            if (sp.getCurrentTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID))
                item.setBrowserTeamName("Private");
            else
                item.setBrowserTeamName(app.getMyTeam(sp.getCurrentTeamId()).getName());
            item.setBrowserTeamId(sp.getCurrentTeamId());
            db.insertOrUpdateBrowserItem(browserItem);
        }
    }

    /**
     * Reading Item
     **/
    class AddReadingItem extends AsyncTask<ReadingItem, Void, ReadingItem> {
        @Override
        protected ReadingItem doInBackground(ReadingItem... params) {
            ReadingItem item = params[0];
            Bitmap bitmap = null;
            if (!readingImage.isEmpty()) {
                bitmap = ImageUtil.getImageFromURLNonTask(readingImage);
                if (bitmap != null)
                    item.setReadingThumbnail(ImageUtil.getImageBytes(bitmap));
            }
            return item;
        }

        @Override
        protected void onPostExecute(ReadingItem item) {
            boolean result = db.insertOrUpdateReadingItem(item);
            if (listener != null) {
                if (item == null || !result)
                    Util.showToast(getActivity(), r.s(R.string.toast_message_failed_reading), false);
                else
                    Util.showToast(getActivity(), r.s(R.string.toast_message_added_reading), false);
            }
        }
    }

    /**
     * Bookmark Item
     **/
    class AddBookmarkItem extends AsyncTask<BookmarkItem, Void, BookmarkItem> {
        @Override
        protected BookmarkItem doInBackground(BookmarkItem... params) {
            BookmarkItem item = params[0];
            String path = item.getUrl();
            if (TextUtils.isEmpty(path))
                return null;

            String iconUrl = Uri.parse(path).getHost() + "/favicon.ico";
            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                iconUrl = "http://" + iconUrl;
            item.setThumbnailUrl(iconUrl);
            Bitmap bitmap = null;
            bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);

            return item;
        }

        @Override
        protected void onPostExecute(BookmarkItem item) {

            boolean result = db.insertOrUpdateBookmarkItem(item);
            if (listener != null) {
                if (item == null || !result)
                    Util.showToast(getActivity(), r.s(R.string.toast_message_failed_bookmark), false);
                else
                    Util.showToast(getActivity(), r.s(R.string.toast_message_added_bookmark), false);
            }
        }
    }

    /**
     * History Item
     **/
    class AddHistoryItem extends AsyncTask<HistoryItem, Void, HistoryItem> {
        @Override
        protected HistoryItem doInBackground(HistoryItem... params) {
            HistoryItem item = params[0];
            String path = item.getHistoryUrl();
            if (TextUtils.isEmpty(path))
                return null;

            String iconUrl = Uri.parse(path).getHost() + "/favicon.ico";
            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                iconUrl = "http://" + iconUrl;
            Bitmap bitmap = null;
            bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);

            if (bitmap != null)
                item.setHistoryIcon(ImageUtil.getImageBytes(bitmap));
            else if (currentIcon != null)
                item.setHistoryIcon(ImageUtil.getImageBytes(currentIcon));

            return item;
        }

        @Override
        protected void onPostExecute(HistoryItem item) {
            String temp = item.getHistoryUrl().replace("http://", "");
            temp = temp.replace("https://", "");
            temp = temp.replace("www.", "");
            temp = temp.replace("m.", "");
            String[] temps = temp.split("/");
            String name = temp;
            if (temps != null && temps.length > 0) {
                name = temps[0];
            }
            if (TextUtils.isEmpty(item.getHistoryTitle()))
                item.setHistoryTitle(name);
            item.setHistoryInitial(name.substring(0, 1));
            db.insertOrUpdateHistoryItem(item);
            FavoriteItem favorite = db.getFavoriteItem(temp);
            if (favorite != null) {
                favorite.setFavoriteCount(favorite.getFavoriteCount() + 1);
            } else
                favorite = new FavoriteItem();
            favorite.setFavoriteUrl(temp);
            favorite.setFavoriteSite(item.getHistoryUrl());
            favorite.setFavoriteInitial(item.getHistoryInitial());
            favorite.setFavoriteTitle(item.getHistoryTitle());
            favorite.setFavoriteIcon(item.getHistoryIcon());
            favorite.setFavoriteAt(item.getHistoryAt());
            db.insertOrUpdateFavoriteItem(favorite);
        }
    }
}
