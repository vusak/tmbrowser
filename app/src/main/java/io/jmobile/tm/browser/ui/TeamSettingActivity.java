package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.data.TeamItem;
import io.jmobile.tm.browser.network.TMB_network;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.hideKeyBoard;
import static io.jmobile.tm.browser.common.Util.showKeyBoard;
import static io.jmobile.tm.browser.common.Util.showToast;

public class TeamSettingActivity extends BaseActivity {

    TextView titleText;
    ImageButton backButton, domainDelButton;
    Button saveButton;

    LinearLayout privateLayout, publicLayout, protectedLayout;
    ImageView privateImage, publicImage, protectedImage;
    TextView domainText;
    EditText domainEdit;
    View domainLine;

    String teamId;
    String curPrivacy, changedPrivacy;
    String curDomain;
    boolean changed = false;

    Dialog progress;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_team_setting);

        Intent intent = getIntent();
        teamId = intent.getStringExtra("team_id");
        curPrivacy = intent.getStringExtra("team_privacy");
        curDomain = intent.getStringExtra("team_privacy_domain");
        initView();
        setPrivacy(curPrivacy);
    }

    private void initView() {
        titleText = fv(R.id.text_title);
        titleText.setText(r.s(R.string.title_privacy_settings));

        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> {
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, resultIntent);
            finish();
        });

        saveButton = fv(R.id.button_save);
        saveButton.setVisibility(View.VISIBLE);
        saveButton.setOnClickListener(v -> {
            hideKeyBoard(domainEdit);
//            if (!changed) {
//                Intent resultIntent = new Intent();
//                setResult(Activity.RESULT_CANCELED, resultIntent);
//                finish();
//            }
//            else {
            String domain = "0";
            if (changedPrivacy.equalsIgnoreCase(TeamItem.TEAM_RULE_PROTECT)) {
                if (!checkEmpty())
                    domain = domainEdit.getText().toString();
                else
                    return;
            }
            setTeamInfo(changedPrivacy, domain);
//            }
        });

        privateLayout = fv(R.id.layout_private);
        privateLayout.setOnClickListener(v -> setPrivacy(TeamItem.TEAM_RULE_PRIVATE));
        privateImage = fv(R.id.image_private);

        publicLayout = fv(R.id.layout_public);
        publicLayout.setOnClickListener(v -> setPrivacy(TeamItem.TEAM_RULE_PUBLIC));
        publicImage = fv(R.id.image_public);

        protectedLayout = fv(R.id.layout_protected);
        protectedLayout.setOnClickListener(v -> {
            setPrivacy(TeamItem.TEAM_RULE_PROTECT);
        });
        protectedImage = fv(R.id.image_protected);

        domainText = fv(R.id.text_domain);
        domainDelButton = fv(R.id.button_domain_del);
        domainDelButton.setOnClickListener(v -> {
            domainEdit.setText("");
        });
        domainEdit = fv(R.id.edit_domain);
        domainEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                domainDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });
        if (!TextUtils.isEmpty(curDomain))
            domainEdit.setText(curDomain);
        else {
            String[] domain = sp.getLoginEmail().split("@");
            if (domain.length > 1) {
                domainEdit.setText(domain[1]);
            }
        }
        domainLine = fv(R.id.view_domain_line);
    }

    private boolean checkEmpty() {
        if (TextUtils.isEmpty(domainEdit.getText().toString())) {
            showToast(this, r.s(R.string.message_empty_domain), true);
            showKeyBoard(domainEdit);
            return true;
        }

        return false;
    }

    private void setPrivacy(String privacy) {
        changedPrivacy = privacy;
        if (privacy.equalsIgnoreCase(TeamItem.TEAM_RULE_PROTECT)) {
            privateImage.setImageResource(R.drawable.ic_radio_button_unchecked_gray_4_24);
            publicImage.setImageResource(R.drawable.ic_radio_button_unchecked_gray_4_24);
            protectedImage.setImageResource(R.drawable.ic_radio_button_checked_gray_4_24);

            domainEdit.setEnabled(true);
            domainText.setTextColor(ContextCompat.getColor(this, R.color.main));
            domainDelButton.setVisibility(TextUtils.isEmpty(domainEdit.getText().toString()) ? View.GONE : View.VISIBLE);
            domainLine.setVisibility(View.VISIBLE);
            showKeyBoard(domainEdit);
        } else {
            domainEdit.setEnabled(false);
            domainText.setTextColor(ContextCompat.getColor(this, R.color.gray5_a100));
            domainDelButton.setVisibility(View.GONE);
            domainLine.setVisibility(View.GONE);
            hideKeyBoard(domainEdit);
            if (privacy.equalsIgnoreCase(TeamItem.TEAM_RULE_PRIVATE)) {
                privateImage.setImageResource(R.drawable.ic_radio_button_checked_gray_4_24);
                publicImage.setImageResource(R.drawable.ic_radio_button_unchecked_gray_4_24);
                protectedImage.setImageResource(R.drawable.ic_radio_button_unchecked_gray_4_24);
            } else if (privacy.equalsIgnoreCase(TeamItem.TEAM_RULE_PUBLIC)) {
                privateImage.setImageResource(R.drawable.ic_radio_button_unchecked_gray_4_24);
                publicImage.setImageResource(R.drawable.ic_radio_button_checked_gray_4_24);
                protectedImage.setImageResource(R.drawable.ic_radio_button_unchecked_gray_4_24);
            }
        }

        changed = !privacy.equalsIgnoreCase(curPrivacy);
        changed = changed || !domainEdit.getText().toString().equalsIgnoreCase(curDomain);
    }

    private void setTeamInfo(String join_rule, String join_domain) {
        try {
            showProgress();
            TMB_network.ModifyTeamInfo(teamId, -1, join_rule, join_domain, "0", 0)
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
//                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
                                    if (fdf(MessageDialog.TAG) == null) {
                                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                        dlg.setMessage(r.s(R.string.message_setting_changed));
                                        dlg.setTitle(r.s(R.string.title_success));
                                        dlg.setPositiveLabel(r.s(R.string.ok));
                                        dlg.setPositiveListener((dialog, tag) -> {
                                            Intent intent = new Intent();
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        });
                                        sdf(dlg);
                                    }
                                } else {
                                    showToast(TeamSettingActivity.this, ((String) tmBrowserPair.second), false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.title_privacy_settings), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

}
