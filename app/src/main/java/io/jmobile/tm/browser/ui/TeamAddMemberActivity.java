package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Stack;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.MemberAdapter;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.network.TMB_network;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.hideKeyBoard;
import static io.jmobile.tm.browser.common.Util.showToast;

public class TeamAddMemberActivity extends BaseActivity {

    TextView titleText;
    ImageButton backButton, emailDelButton, addButton;
    Button sendButton;

    TextView emailText;
    EditText emailEdit;
    View emailLine;

    ArrayList<MemberItem> teamMember;
    ArrayList<MemberItem> newMember;

    RecyclerView lv;
    LinearLayoutManager manager;
    MemberAdapter adapter;

    Stack<String> addUserStack;
    Dialog progress;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_team_add_member);


        initView();
//        teamMember = app.getMyTeam().getMemberItems();
        teamMember = getIntent().getParcelableArrayListExtra("team_members");
    }

    private void initView() {
        titleText = fv(R.id.text_title);
        titleText.setText(r.s(R.string.title_add_new_member));

        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> {
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, resultIntent);
            finish();
        });

        sendButton = fv(R.id.button_save);
        sendButton.setVisibility(View.VISIBLE);
        sendButton.setText(r.s(R.string.button_send));
        sendButton.setOnClickListener(v -> {
            hideKeyBoard(emailEdit);
            addUserStack = new Stack<>();
            if (newMember == null || newMember.size() <= 0) {
                showToast(this, r.s(R.string.message_team_add_member_add_email), true);
            } else {
                for (MemberItem item : newMember) {
                    addUserStack.add(item.getEmail());
                }
                sendEmail();
            }
        });

        emailText = fv(R.id.text_email);
        emailDelButton = fv(R.id.button_email_del);
        emailDelButton.setOnClickListener(v -> {
            emailEdit.setText("");
        });
        emailEdit = fv(R.id.edit_email);
        emailEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                emailDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
                addButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        emailLine = fv(R.id.view_email_line);

        addButton = fv(R.id.button_add);
        addButton.setOnClickListener(v -> {
            if (!checkInvalidEmail()) {
                newMember.add(new MemberItem().setEmail(emailEdit.getText().toString()));
                adapter.notifyDataSetChanged();
                emailEdit.setText("");
            }
        });

        lv = fv(R.id.lv);
        newMember = new ArrayList<>();
        manager = new GridLayoutManager(this, 1);
        lv.setLayoutManager(manager);
        adapter = new MemberAdapter(this, R.layout.item_new_member, newMember, new MemberAdapter.MemberAdapterListener() {
            @Override
            public void onDeleteButtonClick(int positon, MemberItem item) {
                newMember.remove(positon);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void OnItemClick(int position, MemberItem item) {

            }

            @Override
            public void OnItemLongClick(int position, MemberItem item) {

            }
        });
        lv.setAdapter(adapter);
    }

    private boolean checkInvalidEmail() {
        if (TextUtils.isEmpty(emailEdit.getText().toString())) {
            showToast(this, r.s(R.string.message_enter_add_email), true);
            return true;
        }

        if (!Util.emailvalidate(emailEdit.getText().toString())) {
            showToast(this, r.s(R.string.message_error_email_pattern), true);
            return true;
        }

        for (MemberItem item : teamMember) {
            if (item.getEmail().equalsIgnoreCase(emailEdit.getText().toString())) {
                showToast(this, r.s(R.string.message_duplicated_member_email), true);
                return true;
            }
        }

        for (MemberItem item : newMember) {
            if (item.getEmail().equalsIgnoreCase(emailEdit.getText().toString())) {
                showToast(this, r.s(R.string.message_already_exists_email), true);
                return true;
            }
        }
        return false;
    }

    private void sendEmail() {
        try {
            if (addUserStack.size() > 0) {
                showProgress();
                String email = addUserStack.get(addUserStack.size() - 1);
                addUserStack.pop();
                TMB_network.addTeamMember(sp.getCurrentTeamId(), email)
                        .flatMap(tmBrowserPair -> {
                            return Observable.just(tmBrowserPair);
                        })
                        .doOnError(throwable -> {
                            dismissProgress();
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(tmBrowserPair -> {
                                    if (tmBrowserPair.first) {

                                    }
                                    sendEmail();
                                },
                                throwable -> {
                                    dismissProgress();
                                    throwable.printStackTrace();
                                });
            } else {
                dismissProgress();
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.message_team_add_member_complete));
                    dlg.setPositiveLabel(r.s(R.string.ok));
                    dlg.setPositiveListener((dialog, tag) -> {
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    });
                    sdf(dlg);
                }
            }
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.title_add_new_member), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

}
