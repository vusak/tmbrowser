package io.jmobile.tm.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.text.TextUtils;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;

public class MemberItem extends TableObject {
    public static final String TABLE_NAME = "member";

    public static final String ROW_ID = "_id";
    public static final String MEMBER_ID = "member_id";
    public static final String MEMBER_TEAM_ID = "member_team_id";
    public static final String MEMBER_NAME = "member_name";
    public static final String MEMBER_EMAIL = "member_email";
    public static final String MEMBER_PROFILE = "member_profile";
    public static final String MEMBER_PROFILE_URL = "member_profile_url";
    public static final String MEMBER_GRADE = "member_grade";
    public static final String MEMBER_STATUS = "member_status";
    public static final String MEMBER_REG_DATE = "member_reg_date";
    public static final String MEMBER_MODI_DATE = "member_modi_date";
    public static final String MEMBER_MASTER = "member_master";
    public static final String MEMBER_WAIT = "member_wait";

    public static final Creator<MemberItem> CREATOR = new Creator<MemberItem>() {
        @Override
        public MemberItem createFromParcel(Parcel source) {
            return new MemberItem(source);
        }

        @Override
        public MemberItem[] newArray(int size) {
            return new MemberItem[size];
        }
    };

    private long id;
    private String memberId;
    private String teamId;
    private String role;
    private String name;
    private String email;
    private String profileUrl;
    private byte[] profile;
    private String grade;
    private int status;
    private String regDate;
    private String modiDate;
    private boolean master;
    private boolean waitMember = false;

    public MemberItem() {
        super();
    }

    public MemberItem(JsonObject o, boolean team) {
        try {
            JSONObject obj = new JSONObject(o.toString());
            this.email = Util.optString(obj, "email");
            this.name = Util.optString(obj, "name");
            this.profileUrl = Util.optString(obj, "profile_thumb");
            this.status = Integer.parseInt(Util.optString(obj, "status"));
            if (team) {
                this.memberId = Util.optString(obj, "user_id");
                this.teamId = Util.optString(obj, "team_member_id");
                this.role = Util.optString(obj, "role");
            } else {
                this.memberId = Common.TMBR_PRIVATE_TEAM_ID;
                this.teamId = Common.TMBR_PRIVATE_TEAM_ID;
            }
            String date = Util.optString(obj, "regdate");
            if (!TextUtils.isEmpty(date))
                this.regDate = Util.formatDate(date);
            this.grade = Util.optString(obj, "grade");
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

    public MemberItem(Parcel in) {
        this.id = in.readLong();
        this.memberId = in.readString();
        this.name = in.readString();
        this.email = in.readString();
        this.profileUrl = in.readString();
        this.status = in.readInt();
        this.regDate = in.readString();
        this.modiDate = in.readString();
        this.master = in.readInt() == 1;
        this.grade = in.readString();
        this.teamId = in.readString();
        this.role = in.readString();
        this.waitMember = in.readInt() == 1;
    }

    public static void createTableMember(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(MEMBER_ID + " text not null unique, ")
                .append(MEMBER_NAME + " text, ")
                .append(MEMBER_EMAIL + " text, ")
                .append(MEMBER_GRADE + " text, ")
                .append(MEMBER_PROFILE_URL + " text, ")
                .append(MEMBER_STATUS + " integer, ")
                .append(MEMBER_REG_DATE + " text, ")
                .append(MEMBER_MODI_DATE + " text, ")
                .append(MEMBER_MASTER + " integer, ")
                .append(MEMBER_WAIT + " integer, ")
                .append(MEMBER_PROFILE + " BLOB) ")
                .toString());
    }

    public static MemberItem getMemberItem(SQLiteDatabase db, String id) {
        List<MemberItem> list = getMemberItems(db, null, MEMBER_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<MemberItem> getMemberItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<MemberItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                MemberItem item = new MemberItem()
                        .setRowId(c)
                        .setMemberId(c)
                        .setName(c)
                        .setEmail(c)
                        .setProfile(c)
                        .setProfileUrl(c)
                        .setStatus(c)
                        .setMaster(c)
                        .setWaitMember(c)
                        .setRegDate(c)
                        .setGrade(c)
                        .setModiDate(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateMemberItem(SQLiteDatabase db, MemberItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putMemberId(v)
                    .putName(v)
                    .putEmail(v)
                    .putProfile(v)
                    .putProfileUrl(v)
                    .putStatus(v)
                    .putMaster(v)
                    .putRegDate(v)
                    .putGrade(v)
                    .putWaitMember(v)
                    .putModiDate(v);

            int rowAffected = db.update(TABLE_NAME, v, MEMBER_ID + " =? ", new String[]{item.getMemberId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getName());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteAllMemberItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(id);
        out.writeString(memberId);
        out.writeString(name);
        out.writeString(email);
        out.writeString(profileUrl);
        out.writeInt(status);
        out.writeString(regDate);
        out.writeString(modiDate);
        out.writeInt(master ? 1 : 0);
        out.writeString(grade);
        out.writeString(teamId);
        out.writeString(role);
        out.writeInt(waitMember ? 1 : 0);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MemberItem && memberId == ((MemberItem) obj).memberId;
    }

    public long getRowId() {
        return this.id;
    }

    public MemberItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public MemberItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public MemberItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getMemberId() {
        return this.memberId;
    }

    public MemberItem setMemberId(String id) {
        this.memberId = id;

        return this;
    }

    public MemberItem setMemberId(Cursor c) {
        this.memberId = s(c, MEMBER_ID);

        return this;
    }

    public MemberItem putMemberId(ContentValues v) {
        v.put(MEMBER_ID, this.memberId);

        return this;
    }

    public String getName() {
        return this.name;
    }

    public MemberItem setName(String name) {
        this.name = name;

        return this;
    }

    public MemberItem setName(Cursor c) {
        this.name = s(c, MEMBER_NAME);

        return this;
    }

    public MemberItem putName(ContentValues v) {
        v.put(MEMBER_NAME, name);

        return this;
    }

    public String getEmail() {
        return this.email;
    }

    public MemberItem setEmail(String email) {
        this.email = email;

        return this;
    }

    public MemberItem setEmail(Cursor c) {
        this.email = s(c, MEMBER_EMAIL);

        return this;
    }

    public MemberItem putEmail(ContentValues v) {
        v.put(MEMBER_EMAIL, email);

        return this;
    }

    public String getGrade() {
        return this.grade;
    }

    public MemberItem setGrade(Cursor c) {
        this.grade = s(c, MEMBER_GRADE);

        return this;
    }

    public MemberItem setGrade(String grade) {
        this.grade = grade;

        return this;
    }

    public MemberItem putGrade(ContentValues v) {
        v.put(MEMBER_GRADE, this.grade);

        return this;
    }

    public int getStatus() {
        return this.status;
    }

    public MemberItem setStatus(int status) {
        this.status = status;

        return this;
    }

    public MemberItem setStatus(Cursor c) {
        this.status = i(c, MEMBER_STATUS);

        return this;
    }

    public MemberItem putStatus(ContentValues v) {
        v.put(MEMBER_STATUS, status);

        return this;
    }

    public byte[] getProfile() {
        return this.profile;
    }

    public MemberItem setProfile(byte[] profile) {
        this.profile = profile;

        return this;
    }

    public MemberItem setProfile(Cursor c) {
        this.profile = blob(c, MEMBER_PROFILE);

        return this;
    }

    public MemberItem putProfile(ContentValues v) {
        v.put(MEMBER_PROFILE, this.profile);

        return this;
    }

    public String getProfileUrl() {
        return this.profileUrl;
    }

    public MemberItem setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;

        return this;
    }

    public MemberItem setProfileUrl(Cursor c) {
        this.profileUrl = s(c, MEMBER_PROFILE_URL);

        return this;
    }

    public MemberItem putProfileUrl(ContentValues v) {
        v.put(MEMBER_PROFILE_URL, this.profileUrl);

        return this;
    }

    public boolean isMaster() {
        return this.master;
    }

    public MemberItem setMaster(boolean master) {
        this.master = master;

        return this;
    }

    public MemberItem setMaster(Cursor c) {
        this.master = i(c, MEMBER_MASTER) == 1;

        return this;
    }

    public MemberItem putMaster(ContentValues v) {
        v.put(MEMBER_MASTER, this.master ? 1 : 0);

        return this;
    }

    public String getRegDate() {
        return this.regDate;
    }

    public MemberItem setRegDate(String regDate) {
        this.regDate = regDate;

        return this;
    }

    public MemberItem setRegDate(Cursor c) {
        this.regDate = s(c, MEMBER_REG_DATE);

        return this;
    }

    public MemberItem putRegDate(ContentValues v) {
        v.put(MEMBER_REG_DATE, this.regDate);

        return this;
    }

    public String getModiDate() {
        return this.modiDate;
    }

    public MemberItem setModiDate(String modiDate) {
        this.modiDate = modiDate;

        return this;
    }

    public MemberItem setModiDate(Cursor c) {
        this.modiDate = s(c, MEMBER_MODI_DATE);

        return this;
    }

    public MemberItem putModiDate(ContentValues v) {
        v.put(MEMBER_MODI_DATE, this.modiDate);

        return this;
    }

    public String getTeamId() {
        return this.teamId;
    }

    public MemberItem setTeamId(String teamId) {
        this.teamId = teamId;

        return this;
    }

    public String getRole() {
        return this.role;
    }

    public MemberItem setRole(String role) {
        this.role = role;

        return this;
    }

    public boolean isWaitMember() {
        return this.waitMember;
    }

    public MemberItem setWaitMember(boolean wait) {
        this.waitMember = wait;

        return this;
    }

    public MemberItem setWaitMember(Cursor c) {
        this.waitMember = i(c, MEMBER_WAIT) == 1;

        return this;
    }

    public MemberItem putWaitMember(ContentValues v) {
        v.put(MEMBER_WAIT, this.waitMember ? 1 : 0);

        return this;
    }

}
