package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.data.BookmarkItem;

public class BookmarkAdapter extends ReSelectableAdapter<BookmarkItem, BookmarkAdapter.BookmarkHolder> {

    private Context context;
    private ArrayList<BookmarkItem> items = new ArrayList<>();
    private boolean editMode = false;
    private boolean deleteMode = false;

    public BookmarkAdapter(Context context, int layoutId, ArrayList<BookmarkItem> items, BookmarkAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(BookmarkHolder holder, int position) {

        BookmarkItem item = items.get(position);

        holder.name.setText(item.getName());
        holder.initial.setText(item.getInitial());
        boolean folder = item.isFolder();
        holder.thumbnail.setVisibility(folder ? View.GONE : View.VISIBLE);
        holder.initial.setVisibility(folder ? View.GONE : View.VISIBLE);
        holder.folder.setVisibility(folder ? View.VISIBLE : View.GONE);
        if (holder.quickCheck != null) {
            holder.quickCheck.setVisibility(editMode && !item.isFolder() ? View.VISIBLE : View.GONE);
            holder.quickCheck.setChecked(item.isQuick());
        }
        if (holder.deleteCheck != null) {
            holder.deleteCheck.setChecked(item.isSelected());
            holder.deleteCheck.setVisibility(deleteMode ? View.VISIBLE : View.GONE);
        }

        if (!folder) {
            if (!TextUtils.isEmpty(item.getThumbnailUrl())) {
                Glide.with(context).load(item.getThumbnailUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_tm_avator_color_48).into(new BitmapImageViewTarget(holder.thumbnail) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        if (resource == null) {
                            holder.thumbnail.setVisibility(View.GONE);
                            holder.initial.setVisibility(View.VISIBLE);
                        } else {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.thumbnail.setImageDrawable(circularBitmapDrawable);
                            holder.thumbnail.setVisibility(View.VISIBLE);
                            holder.initial.setVisibility(View.GONE);
                        }
//                        holder.thumbnail.setImageBitmap(resource);
                    }
                });
            } else {
                holder.thumbnail.setVisibility(View.GONE);
                holder.initial.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public BookmarkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BookmarkHolder holder = new BookmarkHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.folder = holder.fv(R.id.image_folder);
        holder.thumbnail = holder.fv(R.id.image_thumbnail);
        holder.name = holder.fv(R.id.text_name);
        holder.initial = holder.fv(R.id.text_initial);
        holder.line = holder.fv(R.id.view_line);
        holder.quickCheck = holder.fv(R.id.check_quick);
        holder.deleteCheck = holder.fv(R.id.check_delete);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public boolean isEditMode() {
        return this.editMode;
    }

    public void setEditMode(boolean mode) {
        this.editMode = mode;
        notifyDataSetChanged();
    }

    public boolean isDeleteMode() {
        return this.deleteMode;
    }

    public void setDeleteMode(boolean mode) {
        this.deleteMode = mode;
        clearSelection();
        notifyDataSetChanged();
    }

    public static interface BookmarkAdapterListener extends ReOnItemClickListener<BookmarkItem> {
//        public void onInfoButtonClicked(int position, TeamItem item);
//
//        public void onDeleteButtonClick(int positon, TeamItem item);
    }

    public class BookmarkHolder extends ReAbstractViewHolder {
        LinearLayout root;
        ImageView thumbnail, folder;
        TextView name, initial;
        View line;
        CheckBox quickCheck, deleteCheck;
//        private OnItemViewClickListener settingButtonClickListener, deleteButtonClickListener;

        public BookmarkHolder(View itemView) {
            super(itemView);
        }

//        public void setOnInfoButtonClickListener(OnItemViewClickListener listener) {
//            settingButtonClickListener = listener;
//            if (listener != null && infoButton != null) {
//                infoButton.setOnClickListener(v -> settingButtonClickListener.onItemViewClick(getPosition(), AppHolder.this));
//            }
//        }
//
//        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
//            deleteButtonClickListener = listener;
//            if (listener != null && deleteButton != null) {
//                deleteButton.setOnClickListener(v ->deleteButtonClickListener.onItemViewClick(getAdapterPosition(), AppHolder.this));
//            }
//        }
    }
}
