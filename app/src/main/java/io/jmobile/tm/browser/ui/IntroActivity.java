package io.jmobile.tm.browser.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.google.gson.JsonObject;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.network.TMB_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class IntroActivity extends BaseActivity {

    Button startButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_intro);

        initView();

        if (TextUtils.isEmpty(sp.getLoginToken())) {
            new Handler().postDelayed(() -> {
                appearStartButton();
//            Intent intent = new Intent(IntroActivity.this, MainActivity.class);
//            startActivity(intent);
//            finish();
//            if (LogUtil.DEBUG_MODE) {
//                if (TextUtils.isEmpty(sp.getLoginToken())) {
//                    Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                    finish();
//                } else {
//                    Intent intent = new Intent(IntroActivity.this, MainActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
//            }
            }, 500);
        } else
            getUserInfo();
    }

    private void initView() {
        startButton = fv(R.id.button_start);
        startButton.setVisibility(View.GONE);
        startButton.setOnClickListener(v -> {
            Intent intent = new Intent(IntroActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
//            if (TextUtils.isEmpty(sp.getLoginToken())) {
//                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
//                startActivity(intent);
//                finish();
//            } else {
//                Intent intent = new Intent(IntroActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//            }
        });
    }

    private void appearStartButton() {
        startButton.clearAnimation();
        Animation appear = AnimationUtils.loadAnimation(this, R.anim.fadein);
        appear.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                startButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startButton.startAnimation(appear);
    }

    private void getUserInfo() {
        try {
            TMB_network.getUserInfo()
                    .flatMap(tmBrowserPair -> Observable.just(tmBrowserPair))
                    .doOnError(throwable -> {
                        sp.setLoginToken("");
                        appearStartButton();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                if (tmBrowserPair.first) {
                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
                                    String name = result.get("name").getAsString();
                                    sp.setLoginName(name);
                                    MemberItem item = new MemberItem(result, false);
                                    if (item == null) {
                                        sp.setLoginToken("");
                                    } else {
                                        item.setMaster(true);
                                        db.insertOrUpdateMemberItem(item);
                                    }
                                } else {
                                    sp.setLoginToken("");
                                }
                                appearStartButton();
                            },
                            throwable -> {
                                sp.setLoginToken("");
                                throwable.printStackTrace();
                                appearStartButton();
                            });
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

}
