package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.network.TMB_network;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.showToast;

public class AccountDeleteActivity extends BaseActivity {

    TextView titleText;
    ImageButton backButton;

    CheckBox agreeCheck;
    Button keepButton, deleteButton;

    Dialog progress;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_account_delete);

        initView();
    }

    private void initView() {
        titleText = fv(R.id.text_title);
        titleText.setText(r.s(R.string.title_delete_account));

        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> {
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, resultIntent);
            finish();
        });

        agreeCheck = fv(R.id.check_agree);

        keepButton = fv(R.id.button_keep_account);
        keepButton.setOnClickListener(v -> {
            finish();
        });

        deleteButton = fv(R.id.button_delete_account);
        deleteButton.setOnClickListener(v -> {
            if (agreeCheck != null && agreeCheck.isChecked()) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_delete_account1));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveLabel(r.s(R.string.delete));
                dlg.setPositiveListener((dialog, tag) -> deleteMyAccount());
                sdf(dlg);

            } else {
                showToast(this, r.s(R.string.message_delete_account_agree_require), true);
            }
        });
    }

    private void deleteMyAccount() {
        try {
            showProgress();
            TMB_network.leaveMember()
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                        sp.setLoginToken("");
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    sp.clearLoginUser(true);

                                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                    dlg.setMessage(r.s(R.string.message_delete_account_confirm));
                                    dlg.setPositiveLabel(r.s(R.string.ok));
                                    dlg.setPositiveListener((dialog, tag) -> {
                                        Intent intent = new Intent(AccountDeleteActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    });
                                    sdf(dlg);
                                } else {
                                    showToast(AccountDeleteActivity.this, ((String) tmBrowserPair.second), false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                sp.setLoginToken("");
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.title_delete_account), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

}
