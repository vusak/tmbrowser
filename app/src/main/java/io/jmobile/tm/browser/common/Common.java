package io.jmobile.tm.browser.common;

public class Common {

    // HEADER_**
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_NO_CACHE = "no-cache";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_APPLICATION_VERSION = "APPLICATION_VERSION";
    public static final String HEADER_ACCEPT_LANGUAGE = "Accept-Language";
    public static final String HEADER_OS = "OS";
    public static final int TIMEOUT = 20000;
    //    public static final String BASE_AUTH_URL = "https://auth.teammobile.io/v01a/";
    public static final String BASE_URL = "http://api.jmobile.io/g/";
    public static final String API_BOOKMARK = "bookmark";
    public static final String API_MEMO = "memo";
    public static final String API_TEAM_MEMO = "memo/team";
    public static final String API_APP = "app";
    public static final String API_READING = "reading";
    public static final String API_READED = "readed";
    public static final String API_DELETE = "/d";

    public static final int API_CODE_BOOKMARK_ADD = 10001;
    public static final int API_CODE_BOOKMARK_FOLDER = 10002;
    public static final int API_CODE_BOOKMARK_SELECT = 10003;
    public static final int API_CODE_BOOKMARK_DELETE = 10004;
    public static final int API_CODE_MEMO_ADD = 10005;
    public static final int API_CODE_MEMO_SELECT = 10006;
    public static final int API_CODE_MEMO_DELETE = 10007;
    public static final int API_CODE_APP_ADD = 10008;
    public static final int API_CODE_APP_SELECT = 10009;
    public static final int API_CODE_APP_DELETE = 10010;
    public static final int API_CODE_BOOLMARK_FOLDER = 10011;
    public static final int API_CODE_TEAM_MEMO_SELECT = 10012;
    public static final int API_CODE_TODAY_MEMO_SELECT = 10013;
    public static final int API_CODE_READING_ADD = 10014;
    public static final int API_CODE_READING_SELECT = 10015;
    public static final int API_CODE_READING_DELETE = 10016;
    public static final int API_CODE_READED_ADD = 10017;

    // RESULT_CODE_**
    public static final int RESULT_CODE_ERROR_OK = 0;

    //    public static final String API_AUTH_TOKEN = "auth/token";
//    public static final String API_AUTH_RESOURCE = "auth/resource";
//    public static final String API_AUTH_REFRESH = "auth/refresh";
//    public static final String API_USER_JOIN = "user/join";
//    public static final String API_USER_JOIN_RESEND = "user/join/resend";
//    public static final String API_USER = "user";
//    public static final String API_USER_REPASSWORD = "user/repassword";
//    public static final String API_USER_PROFILE = "user/profile";
//
//    public static final int API_CODE_AUTH_TOKEN = 20001;
//    public static final int API_CODE_AUTH_RESOURCE = 20002;
//    public static final int API_CODE_AUTH_REFRESH = 20003;
//    public static final int API_CODE_USER_JOIN_POST = 20004;    // 회원 가입 이메일 코드발송
//    public static final int API_CODE_USER_JOIN_PUT = 20005;     // 회원 가입
//    public static final int API_CODE_USER_JOIN_RESEND = 20006;  // 인증 메일 재전송
//    public static final int API_CODE_USER_GET = 20007;          // 회원 정보 조회
//    public static final int API_CODE_USER_PUT = 20008;          // 회원 정보 수정
//    public static final int API_CODE_USER_DELETE = 20009;       // 회원 탈퇴
//    public static final int API_CODE_USER_REPASSWORD = 20010;   // 비밀번호 찾기
//    public static final int API_CODE_USER_PROFILE = 20011;
    public static final int RESULT_CODE_ERROR_NO_RESPONSE = -1;
    public static final int RESULT_CODE_ERROR_UNKNOWN = -2;
    public static final int RESULT_CODE_ERROR_CANCELED = -3;
    public static final String PARAM_PROGRESS_MESSAGE = "progressMessage";
    public static final String PARAM_SHOW_ERROR_MESSAGE = "showErrorMessage";
    public static final String PARAM_IDX = "idx";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_PARENT_IDX = "parentidx";
    public static final String PARAM_GROUP_IDX = "groupidx";
    public static final String PARAM_GROUP_NAME = "groupname";
    public static final String PARAM_TARGET_URL = "targeturl";
    public static final String PARAM_THUMBNAIL_URL = "thumbnailurl";
    public static final String PARAM_FOLDER_YN = "folderyn";
    public static final String PARAM_DEPTH = "dapth";
    public static final String PARAM_POSITION = "position";
    public static final String PARAM_USER_ID = "userid";
    public static final String PARAM_USER_NAME = "username";
    public static final String PARAM_AUTHKEY = "authkey";
    public static final String PARAM_QUICK_YN = "quickyn";
    public static final String PARAM_LIMIT = "limit";
    public static final String PARAM_ORDER = "order";
    public static final String PARAM_MEMO = "memo";
    public static final String PARAM_PROFILE_URL = "profileurl";
    public static final String PARAM_APP_NAME = "appname";
    public static final String PARAM_CONTENTS = "contents";
    public static final String TAG_RESULT = "result";
    public static final String TAG_LIST = "list";
    public static final String TAG_BOOKMARKS = "bookmarks";
    public static final String TAG_IDX = "idx";
    public static final String TAG_NAME = "name";
    public static final String TAG_PARENT_IDX = "parentidx";
    public static final String TAG_GROUP_IDX = "groupidx";
    public static final String TAG_GROUP_NAME = "groupname";
    public static final String TAG_TARGET_URL = "targeturl";
    public static final String TAG_THUMBNAIL_URL = "thumbnailurl";
    public static final String TAG_QUICK_YN = "quickyn";
    public static final String TAG_FOLDER_YN = "folderyn";
    public static final String TAG_DEPTH = "depth";
    public static final String TAG_POSITION = "position";
    public static final String TAG_CREATE_DATE = "createdate";
    public static final String TAG_CREAT_DATE = "creatdate";
    public static final String TAG_CREATOR_ID = "creatorid";
    public static final String TAG_CREATOR_NAME = "creatorname";
    public static final String TAG_MEMO = "memo";
    public static final String TAG_APP_NAME = "appname";
    public static final String TAG_PROFILE_URL = "profileurl";
    public static final String TAG_APPS = "apps";
    public static final String TAG_MEMOS = "memos";
    public static final String TAG_READINGS = "readings";
    public static final String TAG_ALL_MEMOS = "allMemos";
    public static final String TAG_ORDER_ASC = "asc";
    public static final String TAG_ORDER_DESC = "desc";
    public static final String TAG_TODAY_MEMOS = "today_memos";
    public static final String TAG_CONTENTS = "contents";
    public static final String TAG_READ_YN = "readyn";

    public static final String ARG_BROWSER_OPEN = "browser_open";
    public static final String ARG_INTENT_CONTENT_TYPE = "content_type";
    public static final String ARG_INTENT_PARENT_IDX = "parent_idx";
    public static final String ARG_INTENT_TEAM_IDX = "team_idx";
    public static final String ARG_INTENT_ADD_QUICK = "add_quick";
    public static final String URL_JMOBILE_PRIVACY = "http://jmobile.io/privacy/";
    public static final String URL_JMOBILE_TERMS = "http://jmobile.io/terms/";
    public static final int TYPE_BOOKMARKS = 0;
    public static final int TYPE_READINGS = 1;
    public static final int TYPE_MEMOS = 2;
    public static final int TYPE_FOLDER = 3;
    public static final int TYPE_QUICKMARK = 4;
    public static String SITE = "";
    // Readinglist
    public static String META_NAME_NAME = "og:title";
    public static String META_NAME_DESCRIPTION = "og:description";
    public static String META_NAME_URL = "og:url";
    public static String META_NAME_IMAGE = "og:image";
    public static String META_NAME_TAG = "og:video:tag";
    public static String META_NAME_TYPE = "og:type";
    public static String TMBR_PRIVATE_TEAM_ID = "tmbr_private_team_id";
    public static String TMBR_BOOKMAKR_ADD_ID = "tmbr_add";
    public static String TMBR_BOOKMARK_FOLDER_ROOT_ID = "0";

    public static String PLAYSTORE_URL = "https://play.google.com/store/apps/details?id=";
    public static String PACKAGE_TM_FILEMANAGER = "jiran.com.tmfilemanager";
}
