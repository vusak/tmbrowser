package io.jmobile.tm.browser.ui.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.TeamAdapter;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.data.TeamItem;

public class DrawerFragment extends BaseFragment implements AppBarLayout.OnOffsetChangedListener {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.7f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private static DrawerFragment fragment;
    // Appbar
    AppBarLayout appBarLayout;
    CollapsingToolbarLayout collapsing;
    ImageView profileImage, userProfileImage;
    TextView nameText, emailText, userNameText;
    ImageButton accountLoginButton, accountManagerButton; //, accountHandleButton;
    LinearLayout userInfoLayout; //, accountLayout, mainLayout;
    Toolbar toolbar;
    LinearLayout homeLyout, settingLayout; //, logoutLayout, manageAccountLayout;
    LinearLayout teamLayout;
    ImageButton searchTeamButton;
    RecyclerView lv;
    ArrayList<TeamItem> teamItems;
    LinearLayoutManager manager;
    TeamAdapter adapter;
    DrawerFragmentListener listener;
    // banner
    FrameLayout bannerLayout;
    TextView bannerText;
    LottieAnimationView lottie;
    private boolean titleVisible = false;
    private boolean containerVisible = true;

    public static DrawerFragment newInstance() {
        if (fragment == null)
            fragment = new DrawerFragment();
        return fragment;
    }

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @Override
    public int getLayoutId() {
        return R.layout.layout_drawer;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        initView();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void handleApiMessage(Message m) {
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            boolean login = !TextUtils.isEmpty(sp.getLoginToken());
            nameText.setText(login ? sp.getLoginName() : r.s(R.string.tm_browser));
            emailText.setText(login ? sp.getLoginEmail() : r.s(R.string.button_sign_in));
            userNameText.setText(login ? sp.getLoginName() : r.s(R.string.tm_browser));
            accountLoginButton.setVisibility(login ? View.GONE : View.VISIBLE);
            accountManagerButton.setVisibility(login ? View.VISIBLE : View.GONE);
            teamLayout.setVisibility(login ? View.VISIBLE : View.GONE);

            boolean exist = Util.getPackageList(getActivity(), Common.PACKAGE_TM_FILEMANAGER);
            bannerLayout.setVisibility(exist ? View.GONE : View.VISIBLE);
            lottie = fv(R.id.lottie_banner);
            lottie.setRepeatCount(10000);
            lottie.playAnimation();

            if (login) {
                MemberItem item = db.getMyInfo();
                if (item != null && item.getProfile() == null) {
                    Glide.with(this).load(item.getProfileUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_group_peacock_64).into(new BitmapImageViewTarget(profileImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            profileImage.setImageDrawable(circularBitmapDrawable);
                        }
                    });
                    Glide.with(this).load(item.getProfileUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_group_peacock_64).into(new BitmapImageViewTarget(userProfileImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            userProfileImage.setImageDrawable(circularBitmapDrawable);
                        }
                    });
                }
            } else {
                profileImage.setImageResource(R.drawable.ic_tm_avator_color_48);
                userProfileImage.setImageResource(R.drawable.ic_tm_avator_color_48);
            }
        }
    }

    private void initView() {
        homeLyout = fv(R.id.layout_home);
        homeLyout.setOnClickListener(v -> {
            if (listener != null)
                listener.onHomeButtonClicked();
        });

        settingLayout = fv(R.id.layout_setting);
        settingLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onSettingButtonClicked();
        });

        appBarLayout = fv(R.id.appbar_layout);
        collapsing = fv(R.id.collapsing_toolbar_android_layout);
        profileImage = fv(R.id.image_profile);
        userProfileImage = fv(R.id.image_user_profile);
        nameText = fv(R.id.text_name);
        emailText = fv(R.id.text_email);
        userNameText = fv(R.id.text_user_name);
        toolbar = fv(R.id.toolbar);

        userInfoLayout = fv(R.id.layout_user_info);

        accountLoginButton = fv(R.id.button_account_login);
        accountLoginButton.setOnClickListener(v -> {
            if (listener != null)
                listener.onLoginButtonClicked();
        });

        accountManagerButton = fv(R.id.button_account_info);
        accountManagerButton.setOnClickListener(v -> {
            if (listener != null)
                listener.onAccountManagerButtonClicked();
        });

        teamLayout = fv(R.id.layout_team_drawer);
        teamLayout.setVisibility(View.GONE);

        searchTeamButton = fv(R.id.button_team_search);
        searchTeamButton.setOnClickListener(v -> {
            if (listener != null)
                listener.onTeamSearchButtonClicked();
        });

        appBarLayout.addOnOffsetChangedListener(this);
        startAlphaAnimation(toolbar, 0, View.INVISIBLE);

        lv = fv(R.id.lv_team_drawer);
        teamItems = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new TeamAdapter(getActivity(), R.layout.item_team_drawer, teamItems, new TeamAdapter.TeamAdapterListener() {
            @Override
            public void onInfoButtonClicked(int position, TeamItem item) {
                if (listener != null)
                    listener.onTeamManagerButtonClicked(item.getTeamId());
            }

            @Override
            public void OnItemClick(int position, TeamItem item) {
                sp.setCurrentTeamId(item.getTeamId());
                if (listener != null)
                    listener.onHomeSelected(item.getTeamId());
            }

            @Override
            public void OnItemLongClick(int position, TeamItem item) {

            }
        });
        lv.setAdapter(adapter);

        bannerLayout = fv(R.id.layout_banner_tmfm);
        bannerLayout.setOnClickListener(v -> {
            Util.launchApp(getActivity(), Common.PACKAGE_TM_FILEMANAGER);
        });

//        bannerText = fv(R.id.text_banner);
//        bannerText.setText(Html.fromHtml(r.s(R.string.banner_content) + " " + r.s(R.string.banner_link)));
    }

    //----------------------------------------------------------------------------------------------
    // Side Appbar
    //----------------------------------------------------------------------------------------------M
    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int max = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) max;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {
            if (!titleVisible) {
                startAlphaAnimation(toolbar, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                titleVisible = true;
            }
        } else {
            if (titleVisible) {
                startAlphaAnimation(toolbar, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                titleVisible = false;
            }
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (containerVisible) {
                startAlphaAnimation(userInfoLayout, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                containerVisible = false;
            }

        } else {

            if (!containerVisible) {
                startAlphaAnimation(userInfoLayout, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                containerVisible = true;
            }
        }
    }

    public void setTeamItem(String id) {
        sp.setCurrentTeamId(id);
        adapter.notifyDataSetChanged();
    }

    public void setTeamItems(ArrayList<TeamItem> items) {
        if (adapter == null)
            return;

        if (teamItems == null)
            teamItems = new ArrayList<>();
        teamItems.clear();
        teamItems.addAll(items);
        adapter.notifyDataSetChanged();
    }


    public void setListener(DrawerFragmentListener listener) {
        this.listener = listener;
    }

    public static interface DrawerFragmentListener {
        public void onLoginButtonClicked();

        public void onAccountManagerButtonClicked();

        public void onTeamManagerButtonClicked(String teamId);

        public void onTeamSearchButtonClicked();

        public void onSettingButtonClicked();

        public void onHomeButtonClicked();

        public void onHomeSelected(String teamId);
    }

}
