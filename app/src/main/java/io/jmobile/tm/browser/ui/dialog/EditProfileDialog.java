package io.jmobile.tm.browser.ui.dialog;


import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.LinearLayout;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseDialogFragment;

public class EditProfileDialog extends BaseDialogFragment {

    public static final String TAG = EditProfileDialog.class.getSimpleName();
    public static EditProfileDialogListener listener;
    LinearLayout albumLayout, cameraLayout, removeLayout;

    public static EditProfileDialog newInstance(String tag) {
        EditProfileDialog d = new EditProfileDialog();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_edit_profile;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

//        if (!Util.isTablet(getActivity())) {
        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        float rate = 0.8f;
        params.width = (int) (dm.widthPixels * rate);
        getDialog().getWindow().setAttributes(params);
//        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initView() {
        albumLayout = fv(R.id.layout_album);
        albumLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onAlbumButtonClicked();
            dismiss();
        });

        cameraLayout = fv(R.id.layout_camera);
        cameraLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onCameraButtonClicked();
            dismiss();
        });

        removeLayout = fv(R.id.layout_remove_photo);
        removeLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onRemovePhotoButtonClicked();
            dismiss();
        });
    }

    public EditProfileDialog setListener(EditProfileDialogListener listener) {
        this.listener = listener;

        return this;
    }

    public static interface EditProfileDialogListener {
        public void onAlbumButtonClicked();

        public void onCameraButtonClicked();

        public void onRemovePhotoButtonClicked();
    }

}
