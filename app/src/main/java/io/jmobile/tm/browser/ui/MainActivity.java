package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.UUID;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.data.BrowserItem;
import io.jmobile.tm.browser.data.TeamItem;
import io.jmobile.tm.browser.network.TMB_network;
import io.jmobile.tm.browser.ui.dialog.HomeMenuPopup;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import io.jmobile.tm.browser.ui.dialog.ShareWithDialog;
import io.jmobile.tm.browser.ui.fragment.BrowserFragment;
import io.jmobile.tm.browser.ui.fragment.DrawerFragment;
import io.jmobile.tm.browser.ui.fragment.HomeFragment;
import io.jmobile.tm.browser.ui.fragment.TabsFragment;
import io.jmobile.tm.browser.ui.fragment.UrlFragment;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.showToast;

public class MainActivity extends BaseActivity implements HomeFragment.HomeFragmentListener
        , UrlFragment.UrlFragmentListener
        , TabsFragment.TabsFragmentListener
        , BrowserFragment.BrowserFragmentListener
        , DrawerFragment.DrawerFragmentListener {

    private static final String KEY_CURRENT_URL = "current_url";
    private static final String KEY_BROWSER_URL = "browser_url";
    private static final String KEY_SHOW_LAYOUT_URL = "show_layout_url";
    private static final String KEY_SHOW_LAYOUT_TABS = "show_layout_tabs";
    private static final String KEY_SHOW_LAYOUT_BROWSER = "show_layout_browser";
    private static final String KEY_SHOW_LAYOUT_HISTORY = "show_layout_history";

    protected OnBackPressedListener onBackPressedListener;
    ArrayList<TeamItem> teamItems;
    DrawerLayout drawerLayout;
    View drawer;
    DrawerFragment drawerFragment;

    HomeFragment homeFragment;
    Dialog progress;

    // Bottom Menu layout
    ImageButton prevButton, nextButton, homeButton, moreButton, tabButton;
    TextView tabsText;

    // Url layout
    LinearLayout urlLayout, tabsLayout, browserLayout;
    UrlFragment urlFragment;
    TabsFragment tabsFragment;

    ArrayList<BrowserFragment> browserFragments;
    BrowserFragment currentBrowserFragment;

    TeamItem currentTeam;
    boolean showUrlLayout = false;
    boolean showTabsLayout = false;
    boolean showBrowserLayout = false;
    boolean showHistoryLayout = false;
    String currentUrl = "";
    String browserUrl = "";

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            sp.setCurrentTeamId(Common.TMBR_PRIVATE_TEAM_ID);
        }

        initView();
        initViewSideAppbar();

        if (savedInstanceState != null) {
            showUrlLayout = savedInstanceState.getBoolean(KEY_SHOW_LAYOUT_URL);
            showBrowserLayout = savedInstanceState.getBoolean(KEY_SHOW_LAYOUT_BROWSER);
            showTabsLayout = savedInstanceState.getBoolean(KEY_SHOW_LAYOUT_TABS);
            showHistoryLayout = savedInstanceState.getBoolean(KEY_SHOW_LAYOUT_HISTORY);
            currentUrl = savedInstanceState.getString(KEY_CURRENT_URL);
            browserUrl = savedInstanceState.getString(KEY_BROWSER_URL);
        } else
            showHistoryLayout = false;

        if (getIntent() != null) {
            String url = getIntent().getStringExtra(Common.ARG_BROWSER_OPEN);
            if (!TextUtils.isEmpty(url)) {
//                initHome(FRAGMENT_HOME);
                browserLayout.postDelayed(() -> goToBrowser(url), 100);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (drawerFragment != null)
            drawerFragment.setUserVisibleHint(true);
        if (homeFragment != null)
            homeFragment.setUserVisibleHint(true);
        if (tabsFragment != null)
            tabsFragment.setUserVisibleHint(true);
        if (urlFragment != null)
            urlFragment.setUserVisibleHint(true);

        if (sp.isLogined())
            getMyTeamList();
        else {

        }

        visibleNewTabLayout(showTabsLayout, showHistoryLayout);
        if (showBrowserLayout) {
            goToBrowser(browserUrl);
        } else
            visibleBrowser(showBrowserLayout);
        visibleUrlLayout(showUrlLayout, currentUrl);
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null)
            onBackPressedListener.doBack();
        else if (drawerLayout.isDrawerOpen(drawer))
            drawerLayout.closeDrawers();
        else if (isUrlVisibility())
            visibleUrlLayout(false, "");
        else if (isTabsVisibility())
            visibleNewTabLayout(false, false);
        else if (isBrowserVisibility()) {
            if (!currentBrowserFragment.onPrevButtonClicked())
                visibleBrowser(false);
        } else {
            if (!sp.isPrivateMode())
                onHomeButtonClicked();
            else {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_exit));
                dlg.setPositiveListener((dialog, tag) -> finish());
                dlg.setPositiveLabel(r.s(R.string.quit));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                sdf(dlg);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(KEY_SHOW_LAYOUT_URL, showUrlLayout);
        outState.putBoolean(KEY_SHOW_LAYOUT_TABS, showTabsLayout);
        outState.putBoolean(KEY_SHOW_LAYOUT_BROWSER, showBrowserLayout);
        outState.putBoolean(KEY_SHOW_LAYOUT_HISTORY, showHistoryLayout);
        outState.putString(KEY_CURRENT_URL, urlFragment.getCurrentUrl());
        outState.putString(KEY_BROWSER_URL, currentBrowserFragment.getCurrentUrl());
    }

    private void initView() {
        initHome();

        teamItems = new ArrayList<>();

        prevButton = fv(R.id.button_menu_back);
        prevButton.setOnClickListener(v -> {
            if (currentBrowserFragment != null && browserLayout != null && isBrowserVisibility()
                    && urlLayout != null && !isUrlVisibility())
                currentBrowserFragment.onPrevButtonClicked();
        });

        nextButton = fv(R.id.button_menu_next);
        nextButton.setOnClickListener(v -> {
            if (currentBrowserFragment != null && browserLayout != null && isBrowserVisibility()
                    && urlLayout != null && !isUrlVisibility())
                currentBrowserFragment.onNextButtonClicked();
        });

        homeButton = fv(R.id.button_menu_home);
        homeButton.setOnClickListener(v -> {

            if (isBrowserVisibility()) {
                currentBrowserFragment.setHomeData();
                visibleBrowser(false);
            }
            visibleUrlLayout(false, "");
            onHomeSelected(sp.getCurrentTeamId());
        });

        tabButton = fv(R.id.button_menu_tabs);
        tabButton.setOnClickListener(v -> {
            if (isBrowserVisibility() && currentBrowserFragment != null) {
                currentBrowserFragment.saveBrowserItem(true);
            } else
                visibleNewTabLayout(true, false);
        });

        tabsText = fv(R.id.text_menu_tabs);
        tabsText.setText("" + sp.getBrowserTabCount());

        moreButton = fv(R.id.button_menu_more);
        moreButton.setOnClickListener(v -> {
            if (fdf(HomeMenuPopup.TAG) == null) {
                HomeMenuPopup popup = HomeMenuPopup.newInstance(HomeMenuPopup.TAG, this, isBrowserVisibility());
                popup.setListener(new HomeMenuPopup.HomeMenuPopupListener() {
                    @Override
                    public void onNewTabButtonClicked() {
                        if (isBrowserVisibility() && currentBrowserFragment != null) {
                            currentBrowserFragment.saveBrowserItem(false);
                        }

                        BrowserItem item = new BrowserItem();

                        item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                        item.setBrowserTitle("HOME");
                        item.setBrowserUrl("HOME");
                        item.setBrowserPrivacy(false);
                        item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
                        item.setBrowserAt(System.currentTimeMillis());
                        item.setBrowserTeamId(sp.getCurrentTeamId());
                        if (sp.isPrivateMode()) {
                            item.setBrowserTeamName("Private");
                        } else
                            item.setBrowserTeamName(app.getMyTeam(sp.getCurrentTeamId()).getName());
                        int count = sp.getBrowserTabCount() + 1;
                        sp.setBrowserTabCount(count);
                        sp.setCurrentBrowserIndex(count - 1);

                        db.insertOrUpdateBrowserItem(item);
                        newTabButtonClicked(item);
                    }

                    @Override
                    public void onAddBookmarkButtonClicked() {
                        if (currentBrowserFragment == null)
                            return;

                        if (!sp.isLogined())
                            currentBrowserFragment.addBookmarkItem(Common.TMBR_PRIVATE_TEAM_ID);
                        else {
                            if (fdf(ShareWithDialog.TAG) == null) {
                                ShareWithDialog dlg = ShareWithDialog.newInstance(ShareWithDialog.TAG);
                                dlg.setPositiveListener((dialog, teamId) -> currentBrowserFragment.addBookmarkItem(teamId));
                                sdf(dlg);
                            }
                        }
                    }

                    @Override
                    public void onAddReadingButtonClicked() {
                        if (currentBrowserFragment == null)
                            return;

                        if (!sp.isLogined())
                            currentBrowserFragment.addReadingItem(Common.TMBR_PRIVATE_TEAM_ID);
                        else {
                            if (fdf(ShareWithDialog.TAG) == null) {
                                ShareWithDialog dlg = ShareWithDialog.newInstance(ShareWithDialog.TAG);
                                dlg.setPositiveListener((dialog, teamId) -> currentBrowserFragment.addReadingItem(teamId));
                                sdf(dlg);

                            }
                        }
                    }

                    @Override
                    public void onBookmarksButtonClicked() {
                        Intent intent = new Intent(MainActivity.this, ContentActivity.class);
                        intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_BOOKMARKS);
                        startActivity(intent);
                    }

                    @Override
                    public void onReadingButtonClicked() {
                        Intent intent = new Intent(MainActivity.this, ContentActivity.class);
                        intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_READINGS);
                        startActivity(intent);
                    }

                    @Override
                    public void onMemosButtonClicked() {
                        Intent intent = new Intent(MainActivity.this, ContentActivity.class);
                        intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_MEMOS);
                        startActivity(intent);
                    }
                });
                sdf(popup);
            }
        });

        tabsText = fv(R.id.text_menu_tabs);
    }

    private void initViewSideAppbar() {
        drawerLayout = fv(R.id.layout_drawer);
        drawer = fv(R.id.drawer);

        drawerFragment = new DrawerFragment();
        drawerFragment.setListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.drawer, drawerFragment).commit();

        urlLayout = fv(R.id.layout_url);
        urlFragment = new UrlFragment();
        urlFragment.setListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_url, urlFragment).commit();

        tabsLayout = fv(R.id.layout_tabs);
        tabsFragment = new TabsFragment();
        tabsFragment.setListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_tabs, tabsFragment).commit();

        browserLayout = fv(R.id.layout_fragment_browser);
        browserFragments = new ArrayList<>();
        currentBrowserFragment = new BrowserFragment();
        currentBrowserFragment.setListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_fragment_browser, currentBrowserFragment).commit();

        refreshBrowserList();
    }

    private void initHome() {
        homeFragment = new HomeFragment();
        homeFragment.setListener(this);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.layout_content, homeFragment).commit();
    }


    //----------------------------------------------------------------------------------------------


    private void refreshTeamList(ArrayList<TeamItem> items) {
        if (items != null && items.size() > 0) {
            if (teamItems != null)
                teamItems.clear();
            else
                teamItems = new ArrayList<>();
            teamItems.addAll(items);
//            DrawerFragment.newInstance().setTeamItems(items);
//            adapter.notifyDataSetChanged();

            if (drawerFragment != null)
                drawerFragment.setTeamItems(items);
            if (homeFragment != null)
                homeFragment.setMainTeam(items);
//            HomeFragment.newInstance().setMainTeam(items);
        }
    }

    private void getMyTeamList() {
        ArrayList<TeamItem> items = new ArrayList<>();
        try {
            showProgress();
            TMB_network.getHomeData()
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    JsonArray result = ((JsonObject) tmBrowserPair.second).getAsJsonArray("result");
                                    for (int i = 0; i < result.size(); i++) {
                                        TeamItem item = new TeamItem((JsonObject) result.get(i));
                                        if (item != null)
                                            items.add(item);
                                    }

                                    refreshTeamList(items);
                                    app.setMyTeams(items);
                                } else {
                                    showToast(MainActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void refreshBrowserList() {
        if (browserFragments == null)
            browserFragments = new ArrayList<>();
        browserFragments.clear();

        ArrayList<BrowserItem> browserItems = new ArrayList<>();
        browserItems.addAll(db.getBrowserItems());

        if (browserItems == null || browserItems.size() <= 0) {
            BrowserItem item = new BrowserItem();
            item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setBrowserTitle("HOME");
            item.setBrowserUrl("HOME");
            item.setBrowserPrivacy(false);
            item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
            item.setBrowserAt(System.currentTimeMillis());
            if (sp.isPrivateMode()) {
                item.setBrowserTeamName("Private");
            } else
                item.setBrowserTeamName(app.getMyTeam(sp.getCurrentTeamId()).getName());
            browserItems.add(item);

            currentBrowserFragment = new BrowserFragment();
            currentBrowserFragment.setListener(this);
            currentBrowserFragment.setBrowserItem(item);
            browserFragments.add(currentBrowserFragment);
            sp.setCurrentBrowserIndex(0);

            db.insertOrUpdateBrowserItem(item);
        } else {
            for (BrowserItem item : browserItems) {
                BrowserFragment bFragment = new BrowserFragment();
                bFragment.setBrowserItem(item);
                bFragment.setListener(this);
                browserFragments.add(bFragment);
            }
            int index = sp.getCurrentBrowserIndex();
            if (index < 0 || index >= browserItems.size())
                sp.setCurrentBrowserIndex(0);

            currentBrowserFragment = browserFragments.get(sp.getCurrentBrowserIndex());
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.layout_fragment_browser, currentBrowserFragment, browserItems.get(sp.getCurrentBrowserIndex()).getBrowserId());
        transaction.commit();

        sp.setBrowserTabCount(browserItems.size());
        tabsText.setText("" + sp.getBrowserTabCount());
    }

    @Override
    public void selectBrowserTab(int position) {
        sp.setCurrentBrowserIndex(position);

        currentBrowserFragment = browserFragments.get(sp.getCurrentBrowserIndex());
        BrowserItem item = currentBrowserFragment.getBrowserItem();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.layout_fragment_browser, currentBrowserFragment, item.getBrowserId());
        transaction.commit();

        tabsText.setText("" + sp.getBrowserTabCount());

        visibleNewTabLayout(false, false);
        BrowserItem bItem = currentBrowserFragment.getBrowserItem();
        if (bItem != null && bItem.getBrowserTitle().equals("HOME"))
            visibleBrowser(false);
        else
            visibleBrowser(true);
    }

    @Override
    public void deleteBrowserItem(BrowserItem item) {
        refreshBrowserList();
    }

    @Override
    public void showHistory(boolean show) {
        showHistoryLayout = show;
    }

    @Override
    public void onMenuButtonClicked() {
        drawerLayout.openDrawer(drawer);
    }

    @Override
    public void onHomeSelected(String teamId) {
        if (drawerLayout != null)
            drawerLayout.closeDrawers();

        if (drawerFragment != null)
            drawerFragment.setTeamItem(teamId);

        if (homeFragment != null)
            homeFragment.refreshMainList();
    }

    @Override
    public void visibleUrlLayout(boolean show, String url) {
        if (urlLayout == null)
            return;

        showUrlLayout = show;
        urlLayout.setVisibility(show ? View.VISIBLE : View.GONE);

        if (urlFragment != null && show)
            urlFragment.visible(show, url);
    }

    @Override
    public void savedBrowserItem() {
        visibleNewTabLayout(true, false);
    }

    @Override
    public void visibleNewTabLayout(boolean show, boolean history) {
        showTabsLayout = show;
        showHistoryLayout = history;
        tabsLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        if (show) {
            tabsFragment.setTabMode(!history);
        }
    }

    @Override
    public void onRefreshButtonClicked() {
        getMyTeamList();
    }

    @Override
    public void onHomeButtonClicked() {
        sp.setCurrentTeamId(Common.TMBR_PRIVATE_TEAM_ID);
        onHomeSelected(Common.TMBR_PRIVATE_TEAM_ID);

        if (sp.isLogined())
            getMyTeamList();
    }

    @Override
    public void onLoginButtonClicked() {
        if (drawerLayout != null)
            drawerLayout.closeDrawers();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onAccountManagerButtonClicked() {
        if (drawerLayout != null)
            drawerLayout.closeDrawers();
        Intent intent = new Intent(MainActivity.this, AccountManageActivity.class);
        startActivityForResult(intent, 1001);
    }

    @Override
    public void onTeamManagerButtonClicked(String teamId) {
        if (drawerLayout != null)
            drawerLayout.closeDrawers();
        Intent intent = new Intent(MainActivity.this, TeamActivity.class);
        intent.putExtra("team_id", teamId);
        startActivity(intent);
    }

    @Override
    public void onTeamSearchButtonClicked() {
        if (drawerLayout != null)
            drawerLayout.closeDrawers();
        Intent intent = new Intent(this, TeamSearchActivity.class);
        startActivityForResult(intent, 1002);
    }

    @Override
    public void onSettingButtonClicked() {
        if (drawerLayout != null)
            drawerLayout.closeDrawers();
        Intent intent = new Intent(MainActivity.this, SettingActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToBrowser(String url) {
        if (!url.contains("http://") && !url.contains("https://"))
            url = "http://" + url;

        visibleUrlLayout(false, "");
        visibleBrowser(true);
        currentBrowserFragment.goUrl(url);
    }

    public void visibleBrowser(boolean show) {
        showBrowserLayout = show;
        browserLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        if (!show) {
            if (currentBrowserFragment != null) {
                currentBrowserFragment.stopWebViewLoading();
                currentBrowserFragment.clearCurrData();
            }
//            refreshMainList();
        } else {
            if (currentBrowserFragment != null) {
                currentBrowserFragment.onResume();
            }
        }
    }

    @Override
    public void newTabButtonClicked(BrowserItem item) {
        refreshBrowserList();

        visibleBrowser(false);
        visibleNewTabLayout(false, false);
    }

    @Override
    public void clearTabButtonClicked() {
        refreshBrowserList();
    }

    public void setOnBackPressedListener(OnBackPressedListener listener) {
        this.onBackPressedListener = listener;
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.title_my_team), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

    private boolean isBrowserVisibility() {
        return browserLayout.getVisibility() == View.VISIBLE;
    }

    private boolean isUrlVisibility() {
        return urlLayout.getVisibility() == View.VISIBLE;
    }

    private boolean isTabsVisibility() {
        return tabsLayout.getVisibility() == View.VISIBLE;
    }

    public interface OnBackPressedListener {
        void doBack();
    }
}
