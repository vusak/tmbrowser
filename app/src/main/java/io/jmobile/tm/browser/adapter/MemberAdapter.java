package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.data.MemberItem;

public class MemberAdapter extends ReSelectableAdapter<MemberItem, MemberAdapter.MemberHolder> {

    private static final int MAX_PORT = 5;
    private static final int MAX_LAND = 8;
    private Context context;
    private ArrayList<MemberItem> items = new ArrayList<>();
    private int index = -1;
    private int checkedIndex = -1;

    public MemberAdapter(Context context, int layoutId, ArrayList<MemberItem> items, MemberAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(MemberHolder holder, int position) {

        MemberItem item = items.get(position);

        if (holder.name != null)
            holder.name.setText(item.getName());

        if (holder.email != null)
            holder.email.setText(item.getEmail());

        if (holder.profile != null) {
            if (!TextUtils.isEmpty(item.getProfileUrl()))
                Glide.with(context).load(item.getProfileUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_tm_avator_color_48).into(new BitmapImageViewTarget(holder.profile) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        holder.profile.setImageDrawable(circularBitmapDrawable);
                    }
                });
            else
                holder.profile.setImageResource(R.drawable.ic_tm_avator_color_48);
        }

        if (holder.line != null) {
            holder.line.setVisibility(items.size() - 1 == position ? View.GONE : View.VISIBLE);
        }

    }

    @Override
    public MemberHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MemberHolder holder = new MemberHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.profile = holder.fv(R.id.image_profile);
        holder.name = holder.fv(R.id.text_name);
        holder.email = holder.fv(R.id.text_email);
        holder.deleteButton = holder.fv(R.id.button_delete);
        holder.line = holder.fv(R.id.view_line);

        holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    ((MemberAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public static interface MemberAdapterListener extends ReOnItemClickListener<MemberItem> {
        public void onDeleteButtonClick(int positon, MemberItem item);
    }

    public class MemberHolder extends ReAbstractViewHolder {
        ImageView profile;
        TextView name, email;
        ImageButton deleteButton;
        View line;
        private OnItemViewClickListener deleteButtonClickListener;

        public MemberHolder(View itemView) {
            super(itemView);
        }

        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
            deleteButtonClickListener = listener;
            if (listener != null && deleteButton != null) {
                deleteButton.setOnClickListener(v -> deleteButtonClickListener.onItemViewClick(getAdapterPosition(), MemberHolder.this));
            }
        }
    }

}
