package io.jmobile.tm.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.tm.browser.common.LogUtil;

public class BrowserItem extends TableObject {
    public static final String TABLE_NAME = "browser";

    public static final String ROW_ID = "_id";
    public static final String BROWSER_ID = "browser_id";
    public static final String BROWSER_TITLE = "browser_title";
    public static final String BROWSER_PRIVACY = "browser_privacy";
    public static final String BROWSER_ICON = "browser_icon";
    public static final String BROWSER_THUMBNAIL = "browser_thumbnail";
    public static final String BROWSER_URL = "browser_url";
    public static final String BROWSER_HISTORY = "browser_history";
    public static final String BROWSER_ORDER = "browser_order";
    public static final String BROWSER_AT = "browser_at";
    public static final String BROWSER_TEAM_ID = "browser_team_id";
    public static final String BROWSER_TEAM_NAME = "browser_team_name";

    public static final Creator<BrowserItem> CREATOR = new Creator<BrowserItem>() {
        @Override
        public BrowserItem createFromParcel(Parcel source) {
            return new BrowserItem(source);
        }

        @Override
        public BrowserItem[] newArray(int size) {
            return new BrowserItem[size];
        }
    };
    private long id;
    private String browserId;
    private String url;
    private String title;
    private byte[] icon;
    private byte[] thumbnail;
    private boolean privacy;
    private long order;
    private byte[] history;
    private long browserAt;
    private String teamId;
    private String teamName;

    public BrowserItem() {
        super();
    }

    public BrowserItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.browserId = in.readString();
        this.title = in.readString();
        this.url = in.readString();
        in.readByteArray(this.icon);
        in.readByteArray(this.thumbnail);
        this.privacy = in.readInt() == 1;
        in.readByteArray(this.history);
        this.order = in.readLong();
        this.browserAt = in.readLong();
        this.teamId = in.readString();
        this.teamName = in.readString();
    }

    public static void createTableBrowserItems(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(BROWSER_ID + " text not null unique, ")
                .append(BROWSER_TITLE + " text, ")
                .append(BROWSER_PRIVACY + " integer not null, ")
                .append(BROWSER_ICON + " BLOB, ")
                .append(BROWSER_THUMBNAIL + " BLOB, ")
                .append(BROWSER_URL + " text, ")
                .append(BROWSER_HISTORY + " BLOB, ")
                .append(BROWSER_ORDER + " integer not null, ")
                .append(BROWSER_TEAM_ID + " text, ")
                .append(BROWSER_TEAM_NAME + " text, ")
                .append(BROWSER_AT + " integer not null) ")
                .toString());
    }

    public static BrowserItem getBrowserItem(SQLiteDatabase db, String id) {
        List<BrowserItem> list = getBrowserItems(db, null, BROWSER_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<BrowserItem> getBrowserItesByTeam(SQLiteDatabase db, String teamId) {
        List<BrowserItem> list = getBrowserItems(db, null, BROWSER_TEAM_ID + " = ? ", new String[]{teamId}, null, null, null, null);

        return list;
    }

    public static List<BrowserItem> getBrowserItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<BrowserItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                BrowserItem item = new BrowserItem()
                        .setRowId(c)
                        .setBrowserId(c)
                        .setBrowserTitle(c)
                        .setBrowserPrivacy(c)
                        .setBrowserIcon(c)
                        .setBrowserThumbnail(c)
                        .setBrowserUrl(c)
                        .setBrowserHistory(c)
                        .setBrowserOrder(c)
                        .setBrowserTeamId(c)
                        .setBrowserTeamName(c)
                        .setBrowserAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateBrowserItem(SQLiteDatabase db, BrowserItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBrowserId(v)
                    .putBrowserTitle(v)
                    .putBrowserPrivacy(v)
                    .putBrowserIcon(v)
                    .putBrowserThumbnail(v)
                    .putBrowserUrl(v)
                    .putBrowserHistory(v)
                    .putBrowserOrder(v)
                    .putBrowserTeamId(v)
                    .putBrowserTeamName(v)
                    .putBrowserAt(v);

            int rowAffected = db.update(TABLE_NAME, v, BROWSER_ID + " =? ", new String[]{item.getBrowserId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getBrowserTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean updateBrowserOrder(SQLiteDatabase db, BrowserItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBrowserOrder(v);

            db.update(TABLE_NAME, v, BROWSER_ID + " = ?", new String[]{item.getBrowserId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateBrowserPrivacy(SQLiteDatabase db, BrowserItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBrowserPrivacy(v);

            db.update(TABLE_NAME, v, BROWSER_ID + " = ?", new String[]{item.getBrowserId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateBrowser(SQLiteDatabase db, BrowserItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBrowserTitle(v);
            item.putBrowserUrl(v);
            item.putBrowserIcon(v);
            item.putBrowserThumbnail(v);
            item.putBrowserAt(v);
            item.putBrowserHistory(v);
            item.putBrowserOrder(v);

            db.update(TABLE_NAME, v, BROWSER_ID + " = ?", new String[]{item.getBrowserId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteBrowserItem(SQLiteDatabase db, BrowserItem item) {
        try {
            db.delete(TABLE_NAME, BROWSER_ID + " = ? ", new String[]{item.getBrowserId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteBrowserItemsByTeam(SQLiteDatabase db, String teamId) {
        try {
            db.delete(TABLE_NAME, BROWSER_TEAM_ID + " = ? ", new String[]{teamId});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllBrowserItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(browserId);
        out.writeString(title);
        out.writeString(url);
        out.writeByteArray(icon);
        out.writeByteArray(thumbnail);
        out.writeInt(privacy ? 1 : 0);
        out.writeByteArray(history);
        out.writeLong(order);
        out.writeLong(browserAt);
        out.writeString(teamName);
        out.writeString(teamId);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BrowserItem && browserId == ((BrowserItem) obj).browserId;
    }

    public long getRowId() {
        return this.id;
    }

    public BrowserItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public BrowserItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public BrowserItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getBrowserId() {
        return this.browserId;
    }

    public BrowserItem setBrowserId(String id) {
        this.browserId = id;

        return this;
    }

    public BrowserItem setBrowserId(Cursor c) {
        this.browserId = s(c, BROWSER_ID);

        return this;
    }

    public BrowserItem putBrowserId(ContentValues v) {
        v.put(BROWSER_ID, browserId);

        return this;
    }

    public String getBrowserUrl() {
        return this.url;
    }

    public BrowserItem setBrowserUrl(String url) {
        this.url = url;

        return this;
    }

    public BrowserItem setBrowserUrl(Cursor c) {
        this.url = s(c, BROWSER_URL);

        return this;
    }

    public BrowserItem putBrowserUrl(ContentValues v) {
        v.put(BROWSER_URL, url);

        return this;
    }

    public String getBrowserTitle() {
        return this.title;
    }

    public BrowserItem setBrowserTitle(String title) {
        this.title = title;

        return this;
    }

    public BrowserItem setBrowserTitle(Cursor c) {
        this.title = s(c, BROWSER_TITLE);

        return this;
    }

    public BrowserItem putBrowserTitle(ContentValues v) {
        v.put(BROWSER_TITLE, title);

        return this;
    }

    public byte[] getBrowserIcon() {
        return this.icon;
    }

    public BrowserItem setBrowserIcon(byte[] icon) {
        this.icon = icon;
        return this;
    }

    public BrowserItem setBrowserIcon(Cursor c) {
        this.icon = blob(c, BROWSER_ICON);

        return this;
    }

    public BrowserItem putBrowserIcon(ContentValues v) {
        v.put(BROWSER_ICON, icon);

        return this;
    }

    public byte[] getBrowserThumbnail() {
        return this.thumbnail;
    }

    public BrowserItem setBrowserThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public BrowserItem setBrowserThumbnail(Cursor c) {
        this.thumbnail = blob(c, BROWSER_THUMBNAIL);

        return this;
    }

    public BrowserItem putBrowserThumbnail(ContentValues v) {
        v.put(BROWSER_THUMBNAIL, thumbnail);

        return this;
    }

    public boolean isBrowserPrivacy() {
        return this.privacy;
    }

    public BrowserItem setBrowserPrivacy(boolean privacy) {
        this.privacy = privacy;

        return this;
    }

    public BrowserItem setBrowserPrivacy(Cursor c) {
        this.privacy = i(c, BROWSER_PRIVACY) == 1;

        return this;
    }

    public BrowserItem putBrowserPrivacy(ContentValues v) {
        v.put(BROWSER_PRIVACY, privacy ? 1 : 0);

        return this;
    }


    public byte[] getBrowserHistory() {
        return this.history;
    }

    public BrowserItem setBrowserHistory(byte[] history) {
        this.history = history;
        return this;
    }

    public BrowserItem setBrowserHistory(Cursor c) {
        this.history = blob(c, BROWSER_HISTORY);

        return this;
    }

    public BrowserItem putBrowserHistory(ContentValues v) {
        v.put(BROWSER_HISTORY, history);

        return this;
    }

    public long getBrowserOrder() {
        return this.order;
    }

    public BrowserItem setBrowserOrder(long order) {
        this.order = order;

        return this;
    }

    public BrowserItem setBrowserOrder(Cursor c) {
        this.order = l(c, BROWSER_ORDER);

        return this;
    }

    public BrowserItem putBrowserOrder(ContentValues v) {
        v.put(BROWSER_ORDER, order);

        return this;
    }

    public long getBrowserAt() {
        return this.browserAt;
    }

    public BrowserItem setBrowserAt(long browserAt) {
        this.browserAt = browserAt;

        return this;
    }

    public BrowserItem setBrowserAt(Cursor c) {
        this.browserAt = l(c, BROWSER_AT);

        return this;
    }

    public BrowserItem putBrowserAt(ContentValues v) {
        v.put(BROWSER_AT, browserAt);

        return this;
    }

    public String getBrowserTeamId() {
        return this.teamId;
    }

    public BrowserItem setBrowserTeamId(String teamId) {
        this.teamId = teamId;

        return this;
    }

    public BrowserItem setBrowserTeamId(Cursor c) {
        this.teamId = s(c, BROWSER_TEAM_ID);

        return this;
    }

    public BrowserItem putBrowserTeamId(ContentValues v) {
        v.put(BROWSER_TEAM_ID, this.teamId);

        return this;
    }

    public String getBrowserTeamName() {
        return this.teamName;
    }

    public BrowserItem setBrowserTeamName(Cursor c) {
        this.teamName = s(c, BROWSER_TEAM_NAME);

        return this;
    }

    public BrowserItem setBrowserTeamName(String teamName) {
        this.teamName = teamName;

        return this;
    }

    public BrowserItem putBrowserTeamName(ContentValues v) {
        v.put(BROWSER_TEAM_NAME, this.teamName);

        return this;
    }
}
