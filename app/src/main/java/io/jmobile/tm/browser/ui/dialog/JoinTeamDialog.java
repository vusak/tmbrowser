package io.jmobile.tm.browser.ui.dialog;


import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseDialogFragment;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.network.TMB_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.showToast;

public class JoinTeamDialog extends BaseDialogFragment {

    public static final String TAG = JoinTeamDialog.class.getSimpleName();

    TextView messageText;
    EditText messageEdit, senderEdit;
    View messageLine;
    ImageButton messageDelButton;
    String teamId;

    Button okButton, cancelButton;
    Dialog progress;

    public static JoinTeamDialog newInstance(String tag, String teamId) {
        JoinTeamDialog d = new JoinTeamDialog();
        d.teamId = teamId;
        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_request_join_team;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initView() {
        senderEdit = fv(R.id.edit_sender);
        senderEdit.setText(sp.getLoginEmail());
        senderEdit.setEnabled(false);
        messageText = fv(R.id.text_message);
        messageLine = fv(R.id.view_message_line);
        messageEdit = fv(R.id.edit_message);
        messageEdit.setOnFocusChangeListener((v, hasFocus) -> {
            messageText.setTextColor(r.c(getActivity(), hasFocus ? R.color.main : R.color.main_tm));
            messageLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            messageDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(messageEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });
        messageEdit.setText(r.s(R.string.hint_request_message));
        messageEdit.setSelection(messageEdit.getText().toString().length());

        messageEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                messageDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        messageDelButton = fv(R.id.button_message_del);
        messageDelButton.setOnClickListener(v -> {
            if (messageEdit != null)
                messageEdit.setText("");
        });

        okButton = fv(R.id.button_positive);
        okButton.setOnClickListener(v -> {
//            if (checkEmpty())
//                return;
            requestJoinEmail();
        });

        cancelButton = fv(R.id.button_negative);
        cancelButton.setOnClickListener(v -> dismiss());
    }

//    private boolean checkEmpty() {
//        if (TextUtils.isEmpty(messageEdit.getText().toString())) {
//            showToast(getActivity(), r.s(R.string.message_empty_name), true);
//            return true;
//        }
//
//        return false;
//    }

    private void requestJoinEmail() {
        final String message = messageEdit.getText().toString().trim();
        showProgress();
        try {
            TMB_network.joinTeamSendEmail(teamId, message)
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    if (positiveListener != null)
                                        positiveListener.onDialogPositive(JoinTeamDialog.this, tag);
                                    dismiss();
                                } else {
                                    showToast(getActivity(), (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(getActivity(), r.s(R.string.title_request_join_team), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }
}
