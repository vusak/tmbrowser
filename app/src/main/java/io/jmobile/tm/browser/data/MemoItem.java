package io.jmobile.tm.browser.data;

import android.os.Parcel;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.Comparator;

import io.jmobile.tm.browser.base.BaseComparator;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.Util;

public class MemoItem extends TableObject {

    public static final Creator<MemoItem> CREATOR = new Creator<MemoItem>() {
        @Override
        public MemoItem createFromParcel(Parcel source) {
            return new MemoItem(source);
        }

        @Override
        public MemoItem[] newArray(int size) {
            return new MemoItem[size];
        }
    };

    public static MemoComparator COMPARATOR_DATE_ASC = new MemoDateComparator(true);
    public static MemoComparator COMPARATOR_DATE_DESC = new MemoDateComparator(false);

    private int memoId;
    private String memo;
    private String teamId;
    private String teamName;
    private String userId;
    private String userName;
    private String userProfile;
    private String regDate;


    public MemoItem() {
        super();
    }

    public MemoItem(JSONObject o) {
        String temp = Util.optString(o, Common.TAG_IDX);
        this.memoId = TextUtils.isEmpty(temp) ? -1 : Integer.parseInt(temp);
        this.teamId = Util.optString(o, Common.TAG_GROUP_IDX);
        this.teamName = Util.optString(o, Common.TAG_GROUP_NAME);
        this.memo = Util.optString(o, Common.TAG_MEMO);
        this.userProfile = Util.optString(o, Common.TAG_PROFILE_URL);
        this.userId = Util.optString(o, Common.TAG_CREATOR_ID);
        this.userName = Util.optString(o, Common.TAG_CREATOR_NAME);
        this.regDate = Util.optString(o, Common.TAG_CREATE_DATE);
    }


    public MemoItem(Parcel in) {
        super(in);

        this.memoId = in.readInt();
        this.memo = in.readString();
        this.teamId = in.readString();
        this.teamName = in.readString();
        this.userId = in.readString();
        this.userName = in.readString();
        this.userProfile = in.readString();
        this.regDate = in.readString();
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(memoId);
        out.writeString(memo);
        out.writeString(teamId);
        out.writeString(teamName);
        out.writeString(userId);
        out.writeString(userName);
        out.writeString(userProfile);
        out.writeString(regDate);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MemoItem && memoId == ((MemoItem) obj).memoId;
    }

    public int getMemoId() {
        return this.memoId;
    }

    public MemoItem setMemoId(int id) {
        this.memoId = id;

        return this;
    }

    public String getMemo() {
        return this.memo;
    }

    public MemoItem setMemo(String memo) {
        this.memo = memo;

        return this;
    }

    public String getTeamId() {
        return this.teamId;
    }

    public MemoItem setTeamId(String teamId) {
        this.teamId = teamId;

        return this;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public MemoItem setTeamName(String name) {
        this.teamName = name;

        return this;
    }


    public String getUserId() {
        return this.userId;
    }

    public MemoItem setUserId(String userId) {
        this.userId = userId;

        return this;
    }

    public String getUserName() {
        return this.userName;
    }

    public MemoItem setUserName(String name) {
        this.userName = name;

        return this;
    }

    public String getUserProfile() {
        return this.userProfile;
    }

    public MemoItem setUserProfile(String profile) {
        this.userProfile = profile;

        return this;
    }

    public String getRegDate() {
        return this.regDate;
    }


    public MemoItem setRegDate(String regDate) {
        this.regDate = regDate;

        return this;
    }

    private static class MemoComparator extends BaseComparator implements Comparator<MemoItem> {
        public MemoComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(MemoItem lhs, MemoItem rhs) {
            if (lhs == null && rhs != null)
                return 1;
            else if (rhs == null && lhs != null)
                return -1;
            else
                return 0;
        }

        protected final int compareId(MemoItem lhs, MemoItem rhs) {
            return compareInt(lhs.getMemoId(), rhs.getMemoId(), false);
        }
    }

    private static class MemoDateComparator extends MemoComparator {
        public MemoDateComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(MemoItem lhs, MemoItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            return compareInt(lhs.getMemoId(), rhs.getMemoId(), false);
        }
    }
}
