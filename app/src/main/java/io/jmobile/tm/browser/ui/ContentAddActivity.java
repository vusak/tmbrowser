package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.UUID;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.data.AppItem;
import io.jmobile.tm.browser.data.BookmarkItem;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.data.MemoItem;
import io.jmobile.tm.browser.data.TeamItem;
import io.jmobile.tm.browser.network.Api;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.hideKeyBoard;
import static io.jmobile.tm.browser.common.Util.showKeyBoard;
import static io.jmobile.tm.browser.common.Util.showToast;

public class ContentAddActivity extends BaseActivity {
    int currentType = Common.TYPE_BOOKMARKS;
    String parentIdx = Common.TMBR_BOOKMARK_FOLDER_ROOT_ID;
    String teamIdx = Common.TMBR_PRIVATE_TEAM_ID;
    boolean quick = false;
    TeamItem currentTeam;

    AppItem appItem = null;
    BookmarkItem bookmarkItem = null;
    MemoItem memoItem = null;

    ImageButton backButton;
    TextView titleText;
    Button saveButton;

    FrameLayout nameLayout, webLayout, memoLayout;
    TextView nameText, webText, memoText;
    EditText nameEdit, webEdit, memoEdit;
    View nameLine, webLine, memoLine;
    ImageButton nameDelButton, webDelButton, memoDelButton;

//    RecyclerView lv;
//    ArrayList<AppItem> appItems;
//    LinearLayoutManager appManager;
//    AppAdapter appAdapter;

    Api api;
    Dialog progress;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_content_add);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        if (getIntent() != null) {
            currentType = getIntent().getIntExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_BOOKMARKS);
            parentIdx = getIntent().getStringExtra(Common.ARG_INTENT_PARENT_IDX);
            teamIdx = getIntent().getStringExtra(Common.ARG_INTENT_TEAM_IDX);
            quick = getIntent().getBooleanExtra(Common.ARG_INTENT_ADD_QUICK, false);
            if (!TextUtils.isEmpty(teamIdx) && !teamIdx.equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID))
                currentTeam = app.getMyTeam(teamIdx);
            else {
                currentTeam = new TeamItem()
                        .setName("me (private)")
                        .setMyRole("leader")
                        .setTeamId(Common.TMBR_PRIVATE_TEAM_ID);
            }
        }
        initView();
    }

    @Override
    public void handleApiMessage(Message m) {
        dismissProgress();
        boolean result = m.getData().getBoolean(Common.TAG_RESULT);
        if (m.what == Common.API_CODE_APP_ADD) {
//            if (result)
//                showToast(this, "ADD APP - " + appItem.getName(), false);
        } else if (m.what == Common.API_CODE_BOOKMARK_ADD) {
            if (result) {
                showToast(this, r.s(R.string.toast_message_added_bookmark), false);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        } else if (m.what == Common.API_CODE_BOOKMARK_FOLDER) {
            if (result) {
                showToast(this, r.s(R.string.toast_message_added_bookmark_folder), false);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        } else if (m.what == Common.API_CODE_MEMO_ADD) {
            if (result) {
                showToast(this, r.s(R.string.toast_message_added_memo), false);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> onBackPressed());

        titleText = fv(R.id.text_title);
        saveButton = fv(R.id.button_save);
        saveButton.setOnClickListener(v -> {
            if (!checkEmpty()) {
                saveContent();
            }
        });
        saveButton.setVisibility(View.VISIBLE);

        nameLayout = fv(R.id.layout_name);
        nameText = fv(R.id.text_name);
        nameLine = fv(R.id.view_name_line);
        nameEdit = fv(R.id.edit_name);
        nameEdit.setOnFocusChangeListener((v, hasFocus) -> {
            nameText.setTextColor(r.c(ContentAddActivity.this, hasFocus ? R.color.main : R.color.main_tm));
            nameLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            nameDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(nameEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });

        nameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                nameDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });
        nameDelButton = fv(R.id.button_name_del);
        nameDelButton.setOnClickListener(v -> {
            if (nameEdit != null)
                nameEdit.setText("");
        });

        webLayout = fv(R.id.layout_web);
        webText = fv(R.id.text_web);
        webLine = fv(R.id.view_web_line);
        webEdit = fv(R.id.edit_web);
        webEdit.setOnFocusChangeListener((v, hasFocus) -> {
            webText.setTextColor(r.c(ContentAddActivity.this, hasFocus ? R.color.main : R.color.main_tm));
            webLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            webDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(webEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });

        webEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                webDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });
        webDelButton = fv(R.id.button_web_del);
        webDelButton.setOnClickListener(v -> {
            if (webEdit != null)
                webEdit.setText("");
        });

        memoLayout = fv(R.id.layout_memo);
        memoText = fv(R.id.text_memo);
        memoLine = fv(R.id.view_memo_line);
        memoEdit = fv(R.id.edit_memo);
        memoEdit.setOnFocusChangeListener((v, hasFocus) -> {
            memoText.setTextColor(r.c(ContentAddActivity.this, hasFocus ? R.color.main : R.color.main_tm));
            memoLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            memoDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(memoEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });

        memoEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                memoDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });
        memoDelButton = fv(R.id.button_memo_del);
        memoDelButton.setOnClickListener(v -> {
            if (memoEdit != null)
                memoEdit.setText("");
        });

//        lv = fv(R.id.lv);
//        appItems = new ArrayList<>();
//        appManager = new GridLayoutManager(this, 5);
//        lv.setLayoutManager(appManager);
//        appAdapter = new AppAdapter(this, R.layout.item_app_add, appItems, new AppAdapter.AppAdapterListener() {
//            @Override
//            public void OnItemClick(int position, AppItem item) {
//                appAdapter.setSelectedIndex(position);
//                appItem = item;
//                if (nameEdit.getText().toString().isEmpty()) {
//                    nameEdit.setText(item.getName());
//                    nameEdit.selectAll();
//                }
//            }
//
//            @Override
//            public void OnItemLongClick(int position, AppItem item) {
//
//            }
//        });
//        lv.setAdapter(appAdapter);

        changeType();
    }

    private void changeType() {
        String title = r.s(R.string.menu_add_app);
        switch (currentType) {
//            case TYPE_APPS :
//                getPackageList();
//                title = r.s(R.string.menu_add_app);
//                nameEdit.setHint(r.s(R.string.hint_app_name));
//                break;

            case Common.TYPE_BOOKMARKS:
                nameEdit.setHint(r.s(R.string.hint_bookmark_name));
                title = r.s(R.string.menu_add_bookmark);
                break;

            case Common.TYPE_FOLDER:
                nameEdit.setHint(r.s(R.string.hint_bookmark_folder_name));
                title = r.s(R.string.menu_new_folder);
                break;

            case Common.TYPE_MEMOS:
                title = r.s(R.string.menu_add_memo);

                break;
        }
        titleText.setText(title);
        nameLayout.setVisibility(currentType != Common.TYPE_MEMOS ? View.VISIBLE : View.GONE);
        webLayout.setVisibility(currentType == Common.TYPE_BOOKMARKS ? View.VISIBLE : View.GONE);
        memoLayout.setVisibility(currentType == Common.TYPE_MEMOS ? View.VISIBLE : View.GONE);
//        lv.setVisibility(currentType == TYPE_APPS ? View.VISIBLE : View.GONE);
    }

//    private void getPackageList() {
//        appItems.clear();
//
//        PackageManager manager = getPackageManager();
//        List<ResolveInfo> apps;
//        Intent intent = new Intent(Intent.ACTION_MAIN, null);
//        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//        apps = manager.queryIntentActivities(intent, 0);
//
//        try {
//            for (int i = 0; i < apps.size(); i++) {
//                ActivityInfo info = apps.get(i).activityInfo;
//                if ((info.flags & ApplicationInfo.FLAG_SYSTEM) != 1) {
//                    AppItem item = new AppItem();
//                    item.setName(manager.getApplicationLabel(info.applicationInfo).toString());
//                    item.setPackagename(info.packageName);
//                    if (!TextUtils.isEmpty(item.getName()))
//                        item.setInitial(item.getName().substring(0, 1));
//                    appItems.add(item);
//                }
//            }
//            appAdapter.notifyDataSetChanged();
//        }
//        catch (Exception e) {
//            LogUtil.log(e.getMessage());
//        }
//    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, titleText.getText().toString(), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

    private boolean checkEmpty() {
        hideKeyBoard(this);
        switch (currentType) {
//            case TYPE_APPS:
//                if (TextUtils.isEmpty(nameEdit.getText().toString())) {
//                    showToast(this, r.s(R.string.message_empty_app_name));
//                    showKeyBoard(nameEdit);
//                    return true;
//                }
//
//                if (appItem == null) {
//                    showToast(this, r.s(R.string.message_empty_app));
//                    return true;
//                }
//                break;

            case Common.TYPE_BOOKMARKS:
                if (TextUtils.isEmpty(nameEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_empty_bookmark_name), false);
                    showKeyBoard(nameEdit);
                    return true;
                }

                if (TextUtils.isEmpty(webEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_empty_bookmark_url), false);
                    showKeyBoard(webEdit);
                    return true;
                }
                break;

            case Common.TYPE_MEMOS:
                if (TextUtils.isEmpty(memoEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_empty_memo), false);
                    showKeyBoard(memoEdit);
                    return true;
                }
                break;

            case Common.TYPE_FOLDER:
                if (TextUtils.isEmpty(nameEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_empty_bookmark_folder_name), false);
                    showKeyBoard(nameEdit);
                    return true;
                }
                break;
        }

        return false;
    }

    private void saveContent() {
        showProgress();
        MemberItem myInfo = db.getMyInfo();
        switch (currentType) {
//            case TYPE_APPS:
//                AppItem item = new AppItem();
//                item.setName(nameEdit.getText().toString());
//                item.setPackagename(appItem.getPackagename());
//                item.setInitial(appItem.getInitial());
//                item.setTeamId(app.getMyTeam().getTeamId());
//                item.setTeamName(app.getMyTeam().getName());
//                item.setUserId(app.getMyMember().getEmail());
//                item.setUserName(app.getMyMember().getName());
//                api.addOrEditApp(item);
//                break;

            case Common.TYPE_BOOKMARKS:
                BookmarkItem bItem = new BookmarkItem();
                bItem.setName(nameEdit.getText().toString());
                bItem.setUrl(webEdit.getText().toString());
                bItem.setTeamId(currentTeam.getTeamId());
                bItem.setFolder(false);
                bItem.setQuick(quick);
                if (currentTeam.getTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID)) {
                    dismissProgress();

                    String iconUrl = bItem.getUrl() + "/favicon.ico";
                    if (Uri.parse(bItem.getUrl()).getHost() != null)
                        iconUrl = Uri.parse(bItem.getUrl()).getHost() + "/favicon.ico";
                    if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                        iconUrl = "http://" + iconUrl;
                    bItem.setThumbnailUrl(iconUrl);
                    bItem.setBookmarkId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                    bItem.setParentIdx(parentIdx);
                    boolean result = db.insertOrUpdateBookmarkItem(bItem);
                    if (result) {
                        showToast(this, r.s(R.string.toast_message_added_bookmark), false);
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                } else {
                    bItem.setTeamName(currentTeam.getName());
                    bItem.setUserId(myInfo.getEmail());
                    bItem.setUserName(myInfo.getName());

                    bookmarkItem = bItem;
                    api.addOrEditBookmark(bItem, parentIdx, false);
                }
                break;

            case Common.TYPE_MEMOS:
                MemoItem mItem = new MemoItem();
                mItem.setMemo(memoEdit.getText().toString());
                mItem.setTeamId(currentTeam.getTeamId());
                mItem.setTeamName(currentTeam.getName());
                mItem.setUserId(myInfo.getEmail());
                mItem.setUserName(myInfo.getName());
                mItem.setUserProfile(myInfo.getProfileUrl());
                memoItem = mItem;
                api.addOrEditMemo(mItem);
                break;

            case Common.TYPE_FOLDER:
                BookmarkItem fItem = new BookmarkItem();
                fItem.setName(nameEdit.getText().toString());
                fItem.setTeamId(currentTeam.getTeamId());
                fItem.setFolder(true);
                fItem.setQuick(false);
                if (currentTeam.getTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID)) {
                    dismissProgress();
                    fItem.setBookmarkId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                    fItem.setParentIdx(parentIdx);
                    boolean result = db.insertOrUpdateBookmarkItem(fItem);
                    if (result) {
                        showToast(this, r.s(R.string.toast_message_added_bookmark_folder), false);
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                } else {
                    fItem.setTeamName(currentTeam.getName());
                    fItem.setUserId(myInfo.getEmail());
                    fItem.setUserName(myInfo.getName());
                    bookmarkItem = fItem;
                    api.addOrEditBookmarkFolder(fItem, parentIdx);
                }
                break;
        }
    }
}
