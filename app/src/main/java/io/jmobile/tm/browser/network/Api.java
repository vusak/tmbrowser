package io.jmobile.tm.browser.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import io.jmobile.tm.browser.BaseApplication;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.DBController;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.SPController;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.AppItem;
import io.jmobile.tm.browser.data.BookmarkItem;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.data.MemoItem;
import io.jmobile.tm.browser.data.ReadingItem;

public class Api extends Handler {

    private static RequestQueue requestQueue;
    private static Queue<Message> messageQueue = new LinkedList<Message>();

    private WeakReference<ApiListener> ref;
    private BaseApplication app;
    private SPController sp;
    private DBController db;
    private BaseApplication.ResourceWrapper r;

    public Api(BaseApplication application, ApiListener target) {
        app = application;
        ref = new WeakReference<ApiListener>(target);
        sp = application.getSPController();
        db = application.getDBController();
        r = application.getResourceWrapper();

        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(app.getApplicationContext());
    }

    public static void cancelApprequest() {
        requestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }

    public void setTarget(ApiListener target) {
        if (ref != null)
            ref.clear();

        ref = new WeakReference<ApiListener>(target);
    }

    public void getAllMemoList(int limit) {
        Message m = newMessage(Common.API_CODE_TEAM_MEMO_SELECT, null, false);
        Map<String, String> params = new HashMap<>();
        try {
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
            params.put(Common.PARAM_LIMIT, String.valueOf(limit));
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_TEAM_MEMO, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<MemoItem> memos = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            memos.add(new MemoItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_ALL_MEMOS, memos);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }
                .ajax();
    }

    public void addOrEditApp(AppItem item) {
        Message m = newMessage(Common.API_CODE_APP_ADD, null, false);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_APP_NAME, item.getName());
            params.put(Common.PARAM_TARGET_URL, item.getPackagename());
            params.put(Common.PARAM_GROUP_IDX, item.getTeamId());
            params.put(Common.PARAM_GROUP_NAME, item.getTeamName());
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_USER_NAME, sp.getLoginName());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.PUT, Common.BASE_URL + Common.API_APP, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<AppItem> appItems = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            appItems.add(new AppItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_APPS, appItems);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getAppList() {
        Message m = newMessage(Common.API_CODE_APP_SELECT, null, false);
        Map<String, String> params = new HashMap<>();
        try {
            params.put(Common.PARAM_GROUP_IDX, sp.getCurrentTeamId());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_APP, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<AppItem> apps = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            apps.add(new AppItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_APPS, apps);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void addOrEditBookmark(BookmarkItem item, String parentIdx, boolean edit) {
        Message m = newMessage(Common.API_CODE_BOOKMARK_ADD, null, false);
        Map<String, String> params = new HashMap<String, String>();
        try {
            if (edit)
                params.put(Common.PARAM_IDX, item.getBookmarkId());
            params.put(Common.PARAM_NAME, item.getName());
            params.put(Common.PARAM_PARENT_IDX, parentIdx);
            params.put(Common.PARAM_GROUP_IDX, item.getTeamId());
            params.put(Common.PARAM_GROUP_NAME, item.getTeamName());
            params.put(Common.PARAM_TARGET_URL, URLEncoder.encode(item.getUrl(), "utf-8"));

            String iconUrl = item.getUrl();
            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                iconUrl = "http://" + iconUrl;
            URL url = new URL(iconUrl);
            iconUrl = url.getHost() + "/favicon.ico";
            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                iconUrl = "http://" + iconUrl;
//            iconUrl = Uri.parse(item.getUrl()).getHost() + "/favicon.ico";

            params.put(Common.PARAM_THUMBNAIL_URL, URLEncoder.encode(iconUrl, "utf-8"));
            params.put(Common.PARAM_FOLDER_YN, "N");
            params.put(Common.PARAM_QUICK_YN, item.isQuick() ? "Y" : "N");
            params.put(Common.PARAM_DEPTH, "0");
            params.put(Common.PARAM_POSITION, "0");
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_USER_NAME, sp.getLoginName());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.PUT, Common.BASE_URL + Common.API_BOOKMARK, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<BookmarkItem> bookmarks = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            bookmarks.add(new BookmarkItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_BOOKMARKS, bookmarks);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void addOrEditBookmarkFolder(BookmarkItem item, String parentIdx) {
        Message m = newMessage(Common.API_CODE_BOOKMARK_FOLDER, null, false);
        Map<String, String> params = new HashMap<>();
        try {
            params.put(Common.PARAM_NAME, item.getName());
            params.put(Common.PARAM_PARENT_IDX, parentIdx);
            params.put(Common.PARAM_GROUP_IDX, item.getTeamId());
            params.put(Common.PARAM_GROUP_NAME, item.getTeamName());
            params.put(Common.PARAM_FOLDER_YN, "Y");
            params.put(Common.PARAM_QUICK_YN, item.isQuick() ? "Y" : "N");
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_USER_NAME, sp.getLoginName());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.PUT, Common.BASE_URL + Common.API_BOOKMARK, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<BookmarkItem> bookmarks = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            bookmarks.add(new BookmarkItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_BOOKMARKS, bookmarks);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }
                .ajax();
    }

    public void deleteBookmarkItem(BookmarkItem item) {
        Message m = newMessage(Common.API_CODE_BOOKMARK_DELETE, null, false);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_IDX, item.getBookmarkId());
            params.put(Common.PARAM_PARENT_IDX, item.getParentIdx());
            params.put(Common.PARAM_GROUP_IDX, item.getTeamId());
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_USER_NAME, sp.getLoginName());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_BOOKMARK + Common.API_DELETE, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<BookmarkItem> bookmarks = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            bookmarks.add(new BookmarkItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_BOOKMARKS, bookmarks);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getBookmarkList(String parentIdx, String groupIdx) {
        Message m = newMessage(Common.API_CODE_BOOKMARK_SELECT, null, false);
        Map<String, String> params = new HashMap<>();
        try {
            params.put(Common.PARAM_PARENT_IDX, parentIdx);
            params.put(Common.PARAM_GROUP_IDX, groupIdx);
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_BOOKMARK, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<BookmarkItem> bookmarks = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            bookmarks.add(new BookmarkItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_BOOKMARKS, bookmarks);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }
                .ajax();
    }

    public void addOrEditReading(ReadingItem item) {
        Message m = newMessage(Common.API_CODE_READING_ADD, null, false);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_NAME, item.getReadingTitle());
            params.put(Common.PARAM_GROUP_IDX, item.getTeamId());
            params.put(Common.PARAM_GROUP_NAME, item.getTeamName());
            params.put(Common.PARAM_TARGET_URL, item.getReadingUrl());
            params.put(Common.PARAM_THUMBNAIL_URL, item.getReadingThumbnailPath());
            params.put(Common.PARAM_CONTENTS, item.getReadingContents());
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_USER_NAME, sp.getLoginName());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.PUT, Common.BASE_URL + Common.API_READING, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<ReadingItem> readingItems = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            readingItems.add(new ReadingItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_READINGS, readingItems);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void deleteReading(ReadingItem item) {
        Message m = newMessage(Common.API_CODE_READING_DELETE, null, false);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_IDX, item.getReadingId());
            params.put(Common.PARAM_GROUP_IDX, item.getTeamId());
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_USER_NAME, sp.getLoginName());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_READING + Common.API_DELETE, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<ReadingItem> readingItems = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            readingItems.add(new ReadingItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_READINGS, readingItems);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void addOrEditReaded(ReadingItem item) {
        Message m = newMessage(Common.API_CODE_READED_ADD, null, false);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_IDX, item.getReadingId());
            params.put(Common.PARAM_GROUP_IDX, item.getTeamId());
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_USER_NAME, sp.getLoginName());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.PUT, Common.BASE_URL + Common.API_READED, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getReadingList() {
        Message m = newMessage(Common.API_CODE_READING_SELECT, null, false);
        Map<String, String> params = new HashMap<>();
        try {
            params.put(Common.PARAM_GROUP_IDX, sp.getCurrentTeamId());
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_READING, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<ReadingItem> readingItems = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            readingItems.add(new ReadingItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_READINGS, readingItems);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void addOrEditMemo(MemoItem item) {
        Message m = newMessage(Common.API_CODE_MEMO_ADD, null, false);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_MEMO, item.getMemo());
            params.put(Common.PARAM_GROUP_IDX, item.getTeamId());
            params.put(Common.PARAM_GROUP_NAME, item.getTeamName());
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_USER_NAME, sp.getLoginName());
            MemberItem member = db.getMyInfo();
            if (member != null)
                params.put(Common.PARAM_PROFILE_URL, member.getProfileUrl());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.PUT, Common.BASE_URL + Common.API_MEMO, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<MemoItem> memoItems = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            memoItems.add(new MemoItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_MEMOS, memoItems);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void deleteMemo(MemoItem item) {
        Message m = newMessage(Common.API_CODE_MEMO_DELETE, null, false);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put(Common.PARAM_IDX, "" + item.getMemoId());
            params.put(Common.PARAM_GROUP_IDX, item.getTeamId());
            params.put(Common.PARAM_USER_ID, sp.getLoginEmail());
            params.put(Common.PARAM_USER_NAME, sp.getLoginName());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_MEMO + Common.API_DELETE, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<MemoItem> memoItems = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            memoItems.add(new MemoItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_MEMOS, memoItems);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getMemoList() {
        Message m = newMessage(Common.API_CODE_MEMO_SELECT, null, false);
        Map<String, String> params = new HashMap<>();
        try {
            params.put(Common.PARAM_GROUP_IDX, sp.getCurrentTeamId());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_MEMO, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<MemoItem> memos = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            memos.add(new MemoItem(obj));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_MEMOS, memos);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }
                .ajax();
    }

    public void getTodayMemo() {
        Message m = newMessage(Common.API_CODE_TODAY_MEMO_SELECT, null, false);
        Map<String, String> params = new HashMap<>();
        try {
            params.put(Common.PARAM_GROUP_IDX, sp.getCurrentTeamId());
            params.put(Common.PARAM_AUTHKEY, sp.getLoginToken());
            params.put(Common.PARAM_ORDER, Common.TAG_ORDER_DESC);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_MEMO, params, true) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<MemoItem> memos = new ArrayList<>();
                boolean result = false;
                try {
                    result = j_result.getBoolean(Common.TAG_RESULT);
                    if (result) {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            MemoItem memo = new MemoItem(obj);
                            if (Util.isToday2(memo.getRegDate()))
                                memos.add(memo);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                m.getData().putBoolean(Common.TAG_RESULT, result);
                m.getData().putParcelableArrayList(Common.TAG_TODAY_MEMOS, memos);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }
                .ajax();
    }


    private Message newMessage(int apiCode, String progressMessage, boolean showErrorMessage) {
        Message m = Message.obtain(this, apiCode);
        Bundle b = new Bundle();
        b.putString(Common.PARAM_PROGRESS_MESSAGE, progressMessage);
        b.putBoolean(Common.PARAM_SHOW_ERROR_MESSAGE, showErrorMessage);
        m.setData(b);

        return m;
    }

    @Override
    public void handleMessage(Message msg) {
        if (ref == null || ref.get() == null)
            return;

        ref.get().handleApiMessage(msg);
    }

    public static interface ApiListener {
        public void handleApiMessage(Message m);

        public BaseActivity getApiActivity();

        public FragmentManager getApiFragmentManager();
    }

    private abstract static class ApiRequest implements Response.Listener<String>, Response.ErrorListener {
        protected Api api;
        protected Message m;
        protected boolean addAuthorization;
        protected StringRequest request;
        protected String paramString;

        public ApiRequest(Api _api, Message _m, int method, String _url, final Map<String, String> params, final boolean addAuthorizationToken) {
            this.api = _api;
            this.m = _m;
            this.addAuthorization = addAuthorizationToken;

            JSONObject j_params = null;
            if (method == Request.Method.GET) {
                if (params != null && params.size() > 0) {
                    Object[] temp = new Object[params.size()];
                    Set<String> keySet = params.keySet();
                    int i = 0;
                    for (String key : keySet) {
                        try {
                            temp[i] = key + "=" + URLEncoder.encode(String.valueOf(params.get(key)), "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            temp[i] = key + "=" + String.valueOf(params.get(key));
                        }
                        i++;
                    }

                    _url += "?" + TextUtils.join("&", temp);
                }
            }
            request = new StringRequest(method, _url, this, this) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = super.getHeaders();
                    if (headers == null || headers.equals(Collections.emptyMap()))
                        headers = new Hashtable<String, String>();

                    headers.put(Common.HEADER_CACHE_CONTROL, Common.HEADER_NO_CACHE);
                    headers.put(Common.HEADER_APPLICATION_VERSION, api.app.getVersion());
                    headers.put(Common.HEADER_ACCEPT_LANGUAGE, api.app.getLocale());
                    headers.put(Common.HEADER_OS, api.app.getOS());
                    if (addAuthorizationToken)
                        headers.put(Common.HEADER_AUTHORIZATION, BaseApplication.getInstance().getSPController().getLoginToken());
                    headers.put("Content-type", "application/x-www-form-urlencoded");
//                    headers.put(Common.HEADER_DEVICE_IDENTIFIER, api.app.getDeviceIdentifier());
//                    if (addAuthorizationToken)
//                        headers.put(Common.HEADER_ACCESS_TOKEN, api.sp.getAccessToken());

                    if (LogUtil.DEBUG_MODE) {
                        StringBuilder sb = new StringBuilder();
                        for (String key : headers.keySet()) {
                            sb.append(key)
                                    .append(": ")
                                    .append(headers.get(key))
                                    .append("\n");
                        }
                        LogUtil.log("API_REQUEST", "HEADER===\n" + sb.toString());
                    }
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
//                    if (TextUtils.isEmpty(paramString))
//                        return super.getParams();
//                    else {
//                        Map<String, String> params = new HashMap<>();
//                        params.put("param", paramString);
//                        return params;
//                    }
                    Map<String, String> _params = super.getParams();
                    if (_params == null || _params.equals(Collections.emptyMap()))
                        _params = new Hashtable<String, String>();

                    _params.putAll(params);

                    return _params;
                }
            };

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(Common.TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            if (LogUtil.DEBUG_MODE) {
                LogUtil.log("API_REQUEST", "URL : " + request.getUrl());
                try {
                    if (request.getBody() != null)
                        LogUtil.log("API_REQUEST", "BODY : " + new String(request.getBody()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        abstract public void onSuccess(String result);

        abstract public void onSuccess(JSONObject j_result);

        abstract public void onError();

        @Override
        public void onResponse(String response) {
            if (response == null) {
                m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                onReceiveResponse(null, null, "NO_RESPONSE");
                return;
            }

            if (LogUtil.DEBUG_MODE && response != null)
                LogUtil.log("API_REQUEST", "RESPONSE : " + response.toString());
            JSONObject j_response = null;
            try {
                j_response = new JSONObject(response);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (j_response == null) {
                m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                onReceiveResponse(null, null, "NO_RESPONSE");
                return;
            }
            onReceiveResponse(j_response, null, null);
//            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;

            if (error.networkResponse == null)
                m.arg2 = Common.RESULT_CODE_ERROR_UNKNOWN;
            else
                m.arg2 = error.networkResponse.statusCode;

            onReceiveResponse(null, null, String.valueOf(m.arg2));
        }

        public void ajax() {
            Api.requestQueue.add(request);
            synchronized (messageQueue) {
                messageQueue.add(m);
            }
        }

        private void onReceiveResponse(JSONObject j_result, String result, String errorCode) {
            synchronized (messageQueue) {
                messageQueue.remove(m);
            }

            new ResponseTask(this, j_result, result, errorCode).run();
        }
    }

    private static class ResponseTask extends AsyncTask<Void, Void, Void> {
        protected ApiRequest request;
        protected JSONObject j_result;
        protected String result;
        protected String errorCode;

        ResponseTask(ApiRequest request, JSONObject j_result, String result, String errorCode) {
            this.request = request;
            this.j_result = j_result;
            this.result = result;
            this.errorCode = errorCode;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (j_result != null)
                request.onSuccess(j_result);
            else if (result != null)
                request.onSuccess(result);
            else
                request.onError();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            request.api.sendMessage(request.m);
        }

        public void run() {
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        }
    }
}
