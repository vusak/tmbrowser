package io.jmobile.tm.browser.ui.dialog;


import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseDialogFragment;
import io.jmobile.tm.browser.common.Util;

public class HomeMenuPopup extends BaseDialogFragment {

    public static final String TAG = HomeMenuPopup.class.getSimpleName();
    public static HomeMenuPopupListener listener;
    LinearLayout addBookmarkLayout, historyLayout, shareLayout, myAppsLayout, bookmarksLayout, memosLayout;
    LinearLayout newTabLayout, addReadingLayout, readingLayout;

    private boolean browser;
    private Context context;

    public static HomeMenuPopup newInstance(String tag, Context context, boolean browser) {
        HomeMenuPopup d = new HomeMenuPopup();

        d.context = context;
        d.browser = browser;
        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.pop_menu_home;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();
        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
//        float rate = 0.93f;
//        params.width = (int) (dm.widthPixels * rate);
//        params.height = (int) (dm.heightPixels * rate);
        params.width = dm.widthPixels - (int) Util.convertDpToPixel(12f, getActivity());
        params.height = dm.heightPixels - (int) Util.convertDpToPixel(36f, getActivity());
        getDialog().getWindow().setAttributes(params);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initView() {
        newTabLayout = fv(R.id.layout_new_tab);
        newTabLayout.setVisibility(browser ? View.VISIBLE : View.GONE);
        newTabLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onNewTabButtonClicked();
            dismiss();
        });

        addBookmarkLayout = fv(R.id.layout_add_bookmark);
        addBookmarkLayout.setVisibility(browser ? View.VISIBLE : View.GONE);
        addBookmarkLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onAddBookmarkButtonClicked();
            dismiss();
        });

        addReadingLayout = fv(R.id.layout_add_readinglist);
        addReadingLayout.setVisibility(browser ? View.VISIBLE : View.GONE);
        addReadingLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onAddReadingButtonClicked();
            dismiss();
        });

        bookmarksLayout = fv(R.id.layout_bookmarks);
        bookmarksLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onBookmarksButtonClicked();
            dismiss();
        });

        readingLayout = fv(R.id.layout_readinglist);
        readingLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onReadingButtonClicked();
            dismiss();
        });

        memosLayout = fv(R.id.layout_memos);
        memosLayout.setVisibility(sp.isPrivateMode() ? View.GONE : View.VISIBLE);
        memosLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onMemosButtonClicked();
            dismiss();
        });

        (fv(R.id.button_close)).setOnClickListener(v -> dismiss());
        (fv(R.id.button_close2)).setOnClickListener(v -> dismiss());


    }

    public HomeMenuPopup setListener(HomeMenuPopupListener listener) {
        this.listener = listener;

        return this;
    }

    public static interface HomeMenuPopupListener {
        public void onNewTabButtonClicked();

        public void onAddBookmarkButtonClicked();

        public void onAddReadingButtonClicked();

        public void onBookmarksButtonClicked();

        public void onReadingButtonClicked();

        public void onMemosButtonClicked();

//        public void onHistoryButtonClicked();
//
//        public void onShareButtonClicked();
//
//        public void onMyAppsButtonClicked();


    }

}
