package io.jmobile.tm.browser.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.BookmarkAdapter;
import io.jmobile.tm.browser.adapter.FolderAdapter;
import io.jmobile.tm.browser.adapter.ReAdapter;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.data.BookmarkItem;
import io.jmobile.tm.browser.network.Api;
import io.jmobile.tm.browser.ui.MainActivity;

public class BookmarksFragment extends BaseFragment {

    LinearLayout emptyLayout;
    RecyclerView lv, folderLv;
    LinearLayoutManager manager, folderManager;
    BookmarkAdapter adapter;
    FolderAdapter folderAdapter;
    ArrayList<BookmarkItem> list, folderList;

    Api api;
    String parentIdx = Common.TMBR_BOOKMARK_FOLDER_ROOT_ID;

    int currentType = Common.TYPE_BOOKMARKS;
    BookmarkFragmentListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_bookmarks;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();

//        if (sp.getCurrentTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID))
//            setPrivateList();
//        else {
//            showProgress();
//            api.getBookmarkList(parentIdx, sp.getCurrentTeamId());
//        }
        refreshList(true);
    }

    @Override
    public void handleApiMessage(Message m) {
        super.handleApiMessage(m);
        if (listener != null)
            listener.onDismissProgress();
        if (m.what == Common.API_CODE_BOOKMARK_SELECT || m.what == Common.API_CODE_BOOKMARK_ADD || m.what == Common.API_CODE_BOOKMARK_DELETE) {
            boolean result = m.getData().getBoolean(Common.TAG_RESULT);
            if (result) {
                list.clear();
                ArrayList<BookmarkItem> temp = m.getData().getParcelableArrayList(Common.TAG_BOOKMARKS);
                list.addAll(temp);
                Collections.sort(list, BookmarkItem.BOOKMARK_TEAM_DESC);
                adapter.notifyDataSetChanged();
                emptyLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);

                if (folderAdapter != null)
                    folderAdapter.notifyDataSetChanged();
            }
        }

    }

    private void initView() {
        emptyLayout = fv(R.id.layout_empty);
        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new BookmarkAdapter(getActivity(), R.layout.item_bookmark, list, new BookmarkAdapter.BookmarkAdapterListener() {
            @Override
            public void OnItemClick(int position, BookmarkItem item) {
                if (adapter.isDeleteMode()) {
                    adapter.select(position);
                    if (listener != null) {
                        listener.onCheckChanged(adapter.getSelectedList().size(), adapter.isSelectedAll());
                    }
                } else {
                    if (item.isFolder()) {
                        parentIdx = String.valueOf(item.getBookmarkId());
                        folderList.add(item);
                        if (sp.isPrivateMode()) {
                            setPrivateList();
                            if (folderAdapter != null)
                                folderAdapter.notifyDataSetChanged();
                        } else {
                            if (listener != null)
                                listener.onShowProgress();
                            api.getBookmarkList(String.valueOf(item.getBookmarkId()), sp.getCurrentTeamId());
                        }
                    } else {
                        if (adapter.isEditMode()) {
                            item.setQuick(!item.isQuick());
                            if (sp.isPrivateMode()) {
                                db.updateQuickItem(item);
                                adapter.notifyItemChanged(position);
                            } else {
                                if (listener != null)
                                    listener.onShowProgress();
                                api.addOrEditBookmark(item, parentIdx, true);
                            }
                        } else {
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            intent.putExtra(Common.ARG_BROWSER_OPEN, item.getUrl());
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                }
            }

            @Override
            public void OnItemLongClick(int position, BookmarkItem item) {

            }
        });
        adapter.setEditMode(currentType == Common.TYPE_QUICKMARK);
        lv.setAdapter(adapter);

        folderLv = fv(R.id.lv_folder);
        folderList = new ArrayList<>();
        folderList.add(new BookmarkItem().setName("Root").setBookmarkId(Common.TMBR_BOOKMARK_FOLDER_ROOT_ID));

        folderManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        folderLv.setLayoutManager(folderManager);
        folderAdapter = new FolderAdapter(getActivity(), R.layout.item_folder, folderList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                BookmarkItem bItem = (BookmarkItem) item;
                for (int i = folderList.size() - 1; i >= 0; i--) {
                    if (folderList.get(i).getBookmarkId() == bItem.getBookmarkId()) {
                        parentIdx = String.valueOf(bItem.getBookmarkId());
                        if (sp.isPrivateMode()) {
                            setPrivateList();
                        } else {
                            if (listener != null)
                                listener.onShowProgress();
                            api.getBookmarkList(String.valueOf(bItem.getBookmarkId()), sp.getCurrentTeamId());
                        }
                        break;
                    } else {
                        folderList.remove(i);
                    }
                }
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        folderLv.setAdapter(folderAdapter);
    }

    public void refreshList(boolean init) {
        if (getActivity() == null)
            return;

        if (init) {
            parentIdx = Common.TMBR_BOOKMARK_FOLDER_ROOT_ID;
            folderList.clear();
            folderList.add(new BookmarkItem().setName("Root").setBookmarkId(parentIdx));
            folderAdapter.notifyDataSetChanged();
        }

        if (sp.getCurrentTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID))
            setPrivateList();
        else {
            if (listener != null)
                listener.onShowProgress();
            api.getBookmarkList(parentIdx, sp.getCurrentTeamId());
        }
    }

    public void setPrivateList() {
        if (getActivity() == null)
            return;

        if (list == null)
            list = new ArrayList<>();
        list.clear();
        list.addAll(db.getBookmarkItemsByFolders(parentIdx));
        Collections.sort(list, BookmarkItem.BOOKMARK_PRIVATE_DESC);
        adapter.notifyDataSetChanged();
        emptyLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);

        if (folderAdapter != null)
            folderAdapter.notifyDataSetChanged();
    }

    public void openFolder(BookmarkItem item) {
        if (listener != null)
            listener.onShowProgress();
        folderList.clear();
        folderList.add(new BookmarkItem().setName("Root").setBookmarkId(Common.TMBR_BOOKMARK_FOLDER_ROOT_ID));
        folderList.add(item);

        parentIdx = String.valueOf(item.getBookmarkId());
        api.getBookmarkList(parentIdx, sp.getCurrentTeamId());
    }

    public void setDeleteMode(boolean mode) {
        if (getActivity() == null || adapter == null)
            return;
        adapter.setDeleteMode(mode);
    }

    public void setDeleteAllSelected(boolean all) {
        if (getActivity() == null || adapter == null || !adapter.isDeleteMode())
            return;

        if (all)
            adapter.setAllSelection();
        else
            adapter.clearSelection();

        if (listener != null)
            listener.onCheckChanged(adapter.getSelectedList().size(), adapter.isSelectedAll());
    }

    public void deleteSelectedItem() {
        if (getActivity() == null || adapter == null || !adapter.isDeleteMode())
            return;

        if (sp.isPrivateMode() && listener != null)
            listener.onShowProgress();

        for (BookmarkItem item : adapter.getSelectedList()) {
            if (sp.isPrivateMode()) {
                db.deleteBookmarkItem(item);
            } else {
                if (listener != null)
                    listener.onShowProgress();
                api.deleteBookmarkItem(item);
            }
        }

        if (sp.isPrivateMode()) {
            lv.postDelayed(() -> {
                if (listener != null)
                    listener.onDismissProgress();
                setPrivateList();
            }, 100);

        }
    }

    public int getCurrentType() {
        return this.currentType;
    }

    public void setCurrentType(int type) {
        this.currentType = type;
    }

    public String getParentIdx() {
        return this.parentIdx;
    }

    public boolean isEmpty() {
        return list == null || list.size() < 0;
    }

    public void setListener(BookmarkFragmentListener listener) {
        this.listener = listener;
    }

    public static interface BookmarkFragmentListener {
        public void onCheckChanged(int count, boolean all);

        public void onShowProgress();

        public void onDismissProgress();
    }

}
