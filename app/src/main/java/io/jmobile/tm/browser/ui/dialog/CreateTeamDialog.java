package io.jmobile.tm.browser.ui.dialog;


import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseDialogFragment;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.network.TMB_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.hideKeyBoard;
import static io.jmobile.tm.browser.common.Util.showToast;

public class CreateTeamDialog extends BaseDialogFragment {

    public static final String TAG = CreateTeamDialog.class.getSimpleName();

    EditText nameEdit;
    TextView nameText;
    View nameLine;
    ImageButton nameDelButton;
    Button cancelButton, okButton;

    Dialog progress;

    public static CreateTeamDialog newInstance(String tag) {
        CreateTeamDialog d = new CreateTeamDialog();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_create_team;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        nameText = fv(R.id.text_name);
        nameLine = fv(R.id.view_name_line);
        nameEdit = fv(R.id.edit_name);
        nameEdit.setPrivateImeOptions("defaultInputmode=english;");
        nameEdit.setOnFocusChangeListener((v, hasFocus) -> {
            nameText.setTextColor(r.c(getActivity(), hasFocus ? R.color.main : R.color.main_tm));
            nameLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            nameDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(nameEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });

        nameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                nameDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        nameDelButton = fv(R.id.button_name_del);
        nameDelButton.setOnClickListener(v -> {
            if (nameEdit != null)
                nameEdit.setText("");
        });
        cancelButton = fv(R.id.button_negative);
        cancelButton.setOnClickListener(v -> dismiss());

        okButton = fv(R.id.button_positive);
        okButton.setOnClickListener(v -> {
            if (checkEmpty())
                return;

            startCreateTeam();
        });
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private boolean checkEmpty() {
        if (TextUtils.isEmpty(nameEdit.getText().toString())) {
            showToast(getActivity(), r.s(R.string.message_empty_team_name), true);
            return true;
        }

        return false;
    }

    private void startCreateTeam() {
        if (nameEdit != null)
            hideKeyBoard(nameEdit);

        try {
            showProgress();
            TMB_network.createTeam(nameEdit.getText().toString().trim())
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
//                                ((MainActivity)mMainContext).dismissProgress();
                                if (tmBrowserPair.first) {
                                    showToast(getActivity(), r.s(R.string.message_team_create_complete), false);
                                    if (positiveListener != null)
                                        positiveListener.onDialogPositive(this, CreateTeamDialog.TAG);
                                    dismiss();

//                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
//                                    MultiFragment.teamItams = null;
//                                    ((MainActivity)mMainContext).initFragment();
//                                    Utils.notiAlert(mMainContext, getString(R.string.team_make_storage_complete));
//                                    showToast(getActivity(), r.s(R.string.complete));
                                } else {
                                    showToast(getActivity(), (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(getActivity(), r.s(R.string.title_create_team), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }
}
