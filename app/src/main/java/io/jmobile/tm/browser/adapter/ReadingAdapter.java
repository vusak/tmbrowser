package io.jmobile.tm.browser.adapter;


import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.ImageUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.ReadingItem;

public class ReadingAdapter extends BaseExpandableListAdapter {
    public static final int CHOICE_SINGLE = 0;
    public static final int CHOICE_SINGLE_ENABLE_OFF = 1;
    public static final int CHOICE_MULTIPLE = 2;

    private Context context;
    private Map<String, List<ReadingItem>> map;
    private List<String> groupNameList;
    private LayoutInflater li;

    private int selectMode = CHOICE_MULTIPLE;
    private boolean checkMode = false;
    private List<ReadingItem> selectedList;


    public ReadingAdapter(Activity context, Map<String, List<ReadingItem>> map, List<String> groupNameList) {
        this.context = context;
        this.map = map;
        this.groupNameList = groupNameList;
        this.selectedList = new ArrayList<>();
        li = context.getLayoutInflater();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        String name = groupNameList.get(groupPosition);
        return map.get(name).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = li.inflate(R.layout.item_reading, parent, false);

        ReadingItem item = (ReadingItem) getChild(groupPosition, childPosition);
        LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.layout_list);
//        layout.setBackgroundColor(ContextCompat.getColor(context, isCheckMode() && item.isSelected() ? R.color.bookmarks_list_background_sel : R.color.white));
        CheckBox check = (CheckBox) convertView.findViewById(R.id.check_delete);
        ImageView thumbnail = (ImageView) convertView.findViewById(R.id.image_thumbnail);
        TextView textTitle = (TextView) convertView.findViewById(R.id.text_title);
        TextView textDescription = (TextView) convertView.findViewById(R.id.text_contents);
        TextView textUrl = (TextView) convertView.findViewById(R.id.text_url);
        TextView textDate = (TextView) convertView.findViewById(R.id.text_date);
        View line = (View) convertView.findViewById(R.id.line_reading);
        line.setVisibility(isLastChild ? View.INVISIBLE : View.VISIBLE);

        check.setVisibility(isCheckMode() ? View.VISIBLE : View.GONE);
        check.setChecked(item.isSelected());
        textTitle.setText(item.getReadingTitle());
        String url = item.getReadingUrl().replace("http://", "");
        url = url.replace("https://", "");

        textUrl.setText(url);
        textDescription.setText(item.getReadingContents());

        if (item.getReadingThumbnail() != null)
            thumbnail.setImageBitmap(ImageUtil.getImage(item.getReadingThumbnail()));
        else {
            if (!TextUtils.isEmpty(item.getReadingThumbnailPath())) {
                Glide.with(context).load(item.getReadingThumbnailPath()).asBitmap().centerCrop().placeholder(R.drawable.ic_tm_browser_gray_4_64).into(new BitmapImageViewTarget(thumbnail));
            } else
                thumbnail.setImageResource(R.drawable.ic_tm_browser_gray_4_64);
        }

        if (TextUtils.isEmpty(item.getTeamId()) || item.getTeamId().equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID))
            textDate.setText(Util.getFullDateString(item.getReadingAt()));
        else
            textDate.setText(item.getRegDate());

        View finishLine = (View) convertView.findViewById(R.id.line);
        finishLine.setVisibility(isLastChild ? View.VISIBLE : View.GONE);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        String name = groupNameList.get(groupPosition);
        return map.get(name).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupNameList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groupNameList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = li.inflate(R.layout.group_read, parent, false);

        TextView date = (TextView) convertView.findViewById(R.id.text_group);
        String name = (String) getGroup(groupPosition);
        date.setText(name);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public List<ReadingItem> getSelectedItem() {
        return this.selectedList;
    }

    public void clearSelection() {
        synchronized (selectedList) {
            for (ReadingItem selectedItem : selectedList) {
                selectedItem.setSelected(false);
            }
            selectedList.clear();
        }
        notifyDataSetChanged();
    }

    public void setAllSelection() {
        selectedList.clear();
        for (String group : groupNameList) {
            for (ReadingItem item : map.get(group)) {
                item.setSelected(true);
                selectedList.add(item);
            }
        }
        notifyDataSetChanged();
    }

    public void select(String group, int position) {
        ReadingItem item = null;
        try {
            item = map.get(group).get(position);
        } catch (Exception e) {
            return;
        }

        synchronized (selectedList) {
            boolean contains = selectedList.contains(item);

            if (contains) {
                if (selectMode == CHOICE_SINGLE)
                    return;

                item.setSelected(false);
                selectedList.remove(item);
            } else {
                if (selectMode != CHOICE_MULTIPLE)
                    clearSelection();

                item.setSelected(true);
                selectedList.add(item);
            }
        }

        if (item == null || !item.isEnabled())
            return;

        notifyDataSetChanged();
    }

    public void updateSelectedItems() {
        for (String group : groupNameList) {
            for (ReadingItem item : map.get(group)) {
                updateSelectedItem(item);
            }
        }
        notifyDataSetChanged();
    }

    private void updateSelectedItem(ReadingItem item) {
        synchronized (selectedList) {
            boolean contains = selectedList.contains(item);

            if (item.isSelected() && !contains)
                selectedList.add(item);
            else if (!item.isSelected() && contains)
                selectedList.remove(item);
        }
    }

    public boolean isCheckMode() {
        return checkMode;
    }

    public void setCheckMode(boolean checkMode) {
        this.checkMode = checkMode;

        clearSelection();
        notifyDataSetChanged();
    }

    public boolean isSelectedAll() {
        for (String group : groupNameList) {
            for (ReadingItem item : map.get(group)) {
                if (item.isSelected() == false)
                    return false;
            }
        }
        return true;
    }

}
