package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.common.ImageUtil;
import io.jmobile.tm.browser.data.FavoriteItem;

public class FavoriteAdapter extends ReSelectableAdapter<FavoriteItem, FavoriteAdapter.ReHistoryHolder> {

    private Context context;
    private boolean deleteMode = false;

    public FavoriteAdapter(Context context, int layoutId, List<FavoriteItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);

        this.context = context;
        this.listener = listener;

    }

    @Override
    public void onBindViewHolder(ReHistoryHolder holder, int position) {
        FavoriteItem item = items.get(position);

        String url = item.getFavoriteUrl().replace("http://", "");
        url = url.replace("https://", "");
        holder.title.setText(item.getFavoriteTitle());
        holder.icon.setBackgroundResource(R.color.transparent);

        if (item.getFavoriteIcon() != null) {
            RoundedBitmapDrawable circularBitmapDrawable =
                    RoundedBitmapDrawableFactory.create(context.getResources(), ImageUtil.getImage(item.getFavoriteIcon()));
            circularBitmapDrawable.setCircular(true);
            holder.icon.setImageDrawable(circularBitmapDrawable);
            holder.initial.setVisibility(View.GONE);
//            holder.icon.setImageBitmap(ImageUtil.getImage(item.getFavoriteIcon()));
        } else {
            holder.icon.setImageResource(R.drawable.circle_main);
            holder.initial.setVisibility(View.VISIBLE);
            holder.initial.setText(item.getFavoriteInitial());

        }
        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position)
                    listener.OnItemClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
    }

    @Override
    public ReHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReHistoryHolder holder = new ReHistoryHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

//        holder.dim = holder.fv(R.id.view_dim);
        holder.icon = holder.fv(R.id.image_icon);
        holder.title = holder.fv(R.id.text_title);
        holder.initial = holder.fv(R.id.text_initial);
        return holder;
    }

    public boolean isDeleteMode() {
        return this.deleteMode;
    }

    public void setDeleteMode(boolean mode) {
        this.deleteMode = mode;
    }

    public class ReHistoryHolder extends ReAbstractViewHolder {

        View dim;
        ImageView icon;
        TextView title, initial;

        public ReHistoryHolder(View itemView) {
            super(itemView);
        }

    }
}
