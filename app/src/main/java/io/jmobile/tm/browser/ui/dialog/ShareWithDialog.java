package io.jmobile.tm.browser.ui.dialog;


import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Button;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.TeamAdapter;
import io.jmobile.tm.browser.base.BaseDialogFragment;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.TeamItem;


public class ShareWithDialog extends BaseDialogFragment {

    public static final String TAG = ShareWithDialog.class.getSimpleName();

    Button cancelButton, shareButton;

    RecyclerView lv;
    LinearLayoutManager manager;
    TeamAdapter adapter;
    ArrayList<TeamItem> list;


    public static ShareWithDialog newInstance(String tag) {
        ShareWithDialog d = new ShareWithDialog();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_share_with;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        cancelButton = fv(R.id.btn_negative);
        shareButton = fv(R.id.btn_positive);

        shareButton.setOnClickListener(v -> {
            LogUtil.log(ShareWithDialog.TAG + " :: " + adapter.getSelectedId());
            if (positiveListener != null)
                positiveListener.onDialogPositive(ShareWithDialog.this, adapter.getSelectedId());
            dismiss();
        });

        cancelButton.setOnClickListener(v -> {
            dismiss();
        });

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new TeamAdapter(getActivity(), R.layout.item_team_dialog, list, new TeamAdapter.TeamAdapterListener() {
            @Override
            public void onInfoButtonClicked(int position, TeamItem item) {

            }

            @Override
            public void OnItemClick(int position, TeamItem item) {
                adapter.setSelectedId(item.getTeamId());
            }

            @Override
            public void OnItemLongClick(int position, TeamItem item) {

            }
        });
        lv.setAdapter(adapter);
        adapter.setSelectedId(sp.getCurrentTeamId());

        refreshList();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private void refreshList() {
        if (list == null)
            list = new ArrayList<>();
        list.clear();

        TeamItem item = new TeamItem()
                .setName("me (private)")
                .setMyRole("leader")
                .setTeamId(Common.TMBR_PRIVATE_TEAM_ID);
        list.add(item);
        list.addAll(app.getMyTeams());
        adapter.notifyDataSetChanged();
    }
}
