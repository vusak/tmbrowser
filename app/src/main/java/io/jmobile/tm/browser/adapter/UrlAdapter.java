package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.data.HistoryItem;

public class UrlAdapter extends ReSelectableAdapter<HistoryItem, UrlAdapter.UrlHolder> {

    private Context context;
    private ArrayList<HistoryItem> items = new ArrayList<>();
    private String keyword = "";

    public UrlAdapter(Context context, int layoutId, ArrayList<HistoryItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(UrlHolder holder, int position) {

        HistoryItem item = items.get(position);


        if (TextUtils.isEmpty(keyword)) {
            holder.title.setText(item.getHistoryTitle());
            holder.url.setText(item.getHistoryUrl());
        } else {
            String title = item.getHistoryTitle();
            int idx = title.indexOf(keyword);
            if (idx < 0)
                holder.title.setText(title);
            else {
                SpannableStringBuilder sb = new SpannableStringBuilder(title);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.red_a100)), idx, idx + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.title.setText(sb);
            }

            String url = item.getHistoryUrl();
            int index = url.indexOf(keyword);
            if (index < 0) {
                holder.url.setText(url);
            } else {
                SpannableStringBuilder ssb = new SpannableStringBuilder(url);
                ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.red_a100)), index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.url.setText(ssb);
            }
        }

        holder.line.setVisibility(position == items.size() - 1 ? View.GONE : View.VISIBLE);
    }

    @Override
    public UrlHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UrlHolder holder = new UrlHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.title = holder.fv(R.id.text_name);
        holder.url = holder.fv(R.id.text_url);
        holder.line = holder.fv(R.id.view_url_line);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;

        notifyDataSetChanged();
    }

    public class UrlHolder extends ReAbstractViewHolder {
        View line;
        TextView title, url;

        public UrlHolder(View itemView) {
            super(itemView);
        }
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        final Holder holder;
//        String url = items.get(position);
//
//        if (convertView == null) {
//            holder = new Holder();
//
//            convertView = inflater.inflate(R.layout.item_url, null);
//            holder.urlText = (TextView) convertView.findViewById(R.id.text_url);
//            convertView.setTag(holder);
//        } else
//            holder = (Holder) convertView.getTag();
//
//
//        return convertView;
//    }
//
//    public void setItems(ArrayList<String> _items, String keyword) {
//        this.items = _items;
//        this.keyword = keyword;
//        notifyDataSetChanged();
//    }
//
//    private class Holder {
//        TextView urlText;
//    }

}
