package io.jmobile.tm.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.text.TextUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;

public class TeamItem extends TableObject {
    public static final String TABLE_NAME = "team";

    public static final String ROW_ID = "_id";
    public static final String TEAM_ID = "team_id";
    public static final String TEAM_NAME = "team_name";
    public static final String TEAM_MY_ROLE = "team_my_role";
    public static final String TEAM_ROLE = "team_role";
    public static final String TEAM_REG_DATE = "team_reg_date";
    public static final String TEAM_MODI_DATE = "team_modi_date";
    public static final String TEAM_MEMBER_COUNT = "team_member_count";
    public static final String TEAM_MEMBER_IDS = "team_member_ids";
    public static final String TEAM_MEMBER_WAIT_IDS = "team_member_wait_ids";
    public static final String TEAM_POSITION = "team_position";
    public static final String TEAM_STATUS = "team_status";
    public static final String TEAM_JOIN_RULE = "team_join_rule";
    public static final String TEAM_JOIN_DOMAIN = "team_join_domain";
    public static final String TEAM_CURRENT_BROWSER_IDX = "team_current_browser_idx";

    public static final String TEAM_RULE_PRIVATE = "private";
    public static final String TEAM_RULE_PUBLIC = "public";
    public static final String TEAM_RULE_PROTECT = "protect";

    public static final Creator<TeamItem> CREATOR = new Creator<TeamItem>() {
        @Override
        public TeamItem createFromParcel(Parcel source) {
            return new TeamItem(source);
        }

        @Override
        public TeamItem[] newArray(int size) {
            return new TeamItem[size];
        }
    };

    private long id;
    private String teamId;
    private String name;
    private String myRole;
    private String teamRole;
    private String regDate;
    private String modiDate;
    private int memberCount;
    private String memberIds;
    private String memberWaitIds;
    private int position;
    private int status;
    private String joinRule;
    private String joinDomain;
    private ArrayList<MemberItem> memberItems;
    private String browserIdx;


    public TeamItem() {
        super();
    }

    public TeamItem(JsonObject o) {
        try {
            JSONObject obj = new JSONObject(o.toString());
            myRole = Util.optString(obj, "my_role");
            JSONObject team = obj.getJSONObject("team");
            teamId = Util.optString(team, "team_id");
            name = Util.optString(team, "name");
            teamRole = Util.optString(team, "role");
            joinRule = Util.optString(team, "join_rule");
            joinDomain = Util.optString(team, "join_protect_domain");
            if (!TextUtils.isEmpty(joinDomain) && joinDomain.contains("@")) {
                joinDomain = joinDomain.substring(joinDomain.indexOf("@") + 1, joinDomain.length());
            }
            String date = Util.optString(team, "regdate");
            if (!TextUtils.isEmpty(date))
                regDate = Util.formatDate(date);
            date = Util.optString(team, "modified");
            if (!TextUtils.isEmpty(date))
                modiDate = Util.formatDate(date);
            status = Integer.parseInt(Util.optString(team, "status"));
            memberCount = Integer.parseInt(Util.optString(team, "member_count"));
            memberItems = new ArrayList<>();
            JsonArray team_member = o.getAsJsonArray("team_member");
            if (team_member != null)
                for (int i = 0; i < team_member.size(); i++) {
                    MemberItem memberItem = new MemberItem(team_member.get(i).getAsJsonObject(), true);
                    memberItem.setWaitMember(false);
                    memberItems.add(memberItem);
                }

            JsonArray wait_member = o.getAsJsonArray("wait_member");
            if (wait_member != null)
                for (int i = 0; i < wait_member.size(); i++) {
                    MemberItem memberItem = new MemberItem(wait_member.get(i).getAsJsonObject(), true);
                    memberItem.setWaitMember(true);
                    memberItems.add(memberItem);
                }
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
//        myRole = o.get("my_role").getAsString();
//        JsonObject team = o.getAsJsonObject("team");
//        teamId = team.get("team_id").getAsString();
//        try {
//            teamRole = team.get("role").getAsString();
//        }
//        catch (Exception e) {
//            LogUtil.log(e.getMessage());
//        }
//        name = team.get("name").getAsString();
////         = (int) team.get("volume").getAsLong();
////        item.setVolumeUsage(team.get("volume_usage").getAsLong();
//        joinRule = team.get("join_rule").getAsString();
//        joinDomain = team.get("join_protect_domain").getAsString();
////        item.setFlagPush(team.get("flag_push").getAsString();
////        item.setTrashOption(team.get("trash_option").getAsInt();
//        status = team.get("status").getAsInt();
////        item.setRegdate(Utils.formatDate(team.get("regdate").getAsString()));
////        item.setModified(Utils.formatDate(team.get("modified").getAsString()));
//        memberCount = team.get("member_count").getAsInt();

//        memberItems = new ArrayList<>();
//        JsonArray team_member = o.getAsJsonArray("team_member");
//        if (team_member != null)
//            for (int i = 0; i < team_member.size(); i++) {
//                MemberItem memberItem = new MemberItem(team_member.get(i).getAsJsonObject(), true);
//                memberItem.setWaitMember(false);
//                memberItems.add(memberItem);
//            }
//
//        JsonArray wait_member = o.getAsJsonArray("wait_member");
//        if (wait_member != null)
//            for (int i = 0; i < wait_member.size(); i++) {
//                MemberItem memberItem = new MemberItem(wait_member.get(i).getAsJsonObject(), true);
//                memberItem.setWaitMember(true);
//                memberItems.add(memberItem);
//            }
    }

    public TeamItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.teamId = in.readString();
        this.name = in.readString();
        this.myRole = in.readString();
        this.teamRole = in.readString();
        this.regDate = in.readString();
        this.modiDate = in.readString();
        this.memberCount = in.readInt();
        this.memberIds = in.readString();
        this.memberWaitIds = in.readString();
        this.position = in.readInt();
        this.status = in.readInt();
        this.joinRule = in.readString();
        this.joinDomain = in.readString();
        this.memberItems = in.readArrayList(MemberItem.class.getClassLoader());
        this.browserIdx = in.readString();
    }

    public static void createTableTeam(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(TEAM_ID + " text not null unique, ")
                .append(TEAM_NAME + " text, ")
                .append(TEAM_MY_ROLE + " text, ")
                .append(TEAM_ROLE + " text, ")
                .append(TEAM_MEMBER_COUNT + " integer, ")
                .append(TEAM_MEMBER_IDS + " text, ")
                .append(TEAM_MEMBER_WAIT_IDS + " text, ")
                .append(TEAM_POSITION + " integer, ")
                .append(TEAM_STATUS + " integer, ")
                .append(TEAM_JOIN_RULE + " text, ")
                .append(TEAM_JOIN_DOMAIN + " text, ")
                .append(TEAM_CURRENT_BROWSER_IDX + " text, ")
                .append(TEAM_REG_DATE + " text, ")
                .append(TEAM_MODI_DATE + " text) ")
                .toString());
    }

    public static TeamItem getTeamItem(SQLiteDatabase db, String id) {
        List<TeamItem> list = getTeamItems(db, null, TEAM_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<TeamItem> getTeamItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<TeamItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                TeamItem item = new TeamItem()
                        .setRowId(c)
                        .setTeamId(c)
                        .setName(c)
                        .setTeamRole(c)
                        .setMyRole(c)
                        .setMemberCount(c)
                        .setMemberIds(c)
                        .setMemberWaitIds(c)
                        .setPosition(c)
                        .setStatus(c)
                        .setJoinRule(c)
                        .setJoinDomain(c)
                        .setBrowserIdx(c)
                        .setRegDate(c)
                        .setModiDate(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateTeamItem(SQLiteDatabase db, TeamItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putTeamId(v)
                    .putName(v)
                    .putTeamRole(v)
                    .putMyRole(v)
                    .putMemberCount(v)
                    .putMemberIds(v)
                    .putMemberWaitIds(v)
                    .putPosition(v)
                    .putStatus(v)
                    .putJoinRule(v)
                    .putJoinDomain(v)
                    .putRegDate(v)
                    .putBrowserIdx(v)
                    .putModiDate(v);

            int rowAffected = db.update(TABLE_NAME, v, TEAM_ID + " =? ", new String[]{item.getTeamId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getName());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteTeamItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(teamId);
        out.writeString(name);
        out.writeString(myRole);
        out.writeString(teamRole);
        out.writeInt(memberCount);
        out.writeString(memberIds);
        out.writeString(memberWaitIds);
        out.writeInt(position);
        out.writeInt(status);
        out.writeString(joinRule);
        out.writeString(joinDomain);
        out.writeString(regDate);
        out.writeString(modiDate);
        out.writeList(memberItems);
        out.writeString(browserIdx);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof TeamItem && teamId == ((TeamItem) obj).teamId;
    }

    public long getRowId() {
        return this.id;
    }

    public TeamItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public TeamItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public TeamItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getTeamId() {
        return this.teamId;
    }

    public TeamItem setTeamId(String id) {
        this.teamId = id;

        return this;
    }

    public TeamItem setTeamId(Cursor c) {
        this.teamId = s(c, TEAM_ID);

        return this;
    }

    public TeamItem putTeamId(ContentValues v) {
        v.put(TEAM_ID, this.teamId);

        return this;
    }

    public String getName() {
        return this.name;
    }

    public TeamItem setName(String name) {
        this.name = name;

        return this;
    }

    public TeamItem setName(Cursor c) {
        this.name = s(c, TEAM_NAME);

        return this;
    }

    public TeamItem putName(ContentValues v) {
        v.put(TEAM_NAME, name);

        return this;
    }

    public String getMyRole() {
        return this.myRole;
    }

    public TeamItem setMyRole(String myRole) {
        this.myRole = myRole;

        return this;
    }

    public TeamItem setMyRole(Cursor c) {
        this.myRole = s(c, TEAM_MY_ROLE);

        return this;
    }

    public TeamItem putMyRole(ContentValues v) {
        v.put(TEAM_MY_ROLE, myRole);

        return this;
    }

    public boolean isTeamLeader() {
        return this.myRole.equals("leader");
    }

    public String getTeamRole() {
        return this.teamRole;
    }

    public TeamItem setTeamRole(String teamRole) {
        this.teamRole = teamRole;

        return this;
    }

    public TeamItem setTeamRole(Cursor c) {
        this.teamRole = s(c, TEAM_ROLE);

        return this;
    }

    public TeamItem putTeamRole(ContentValues v) {
        v.put(TEAM_ROLE, teamRole);

        return this;
    }

    public int getMemberCount() {
        return this.memberCount;
    }

    public TeamItem setMemberCount(int memberCount) {
        this.memberCount = memberCount;

        return this;
    }

    public TeamItem setMemberCount(Cursor c) {
        this.memberCount = i(c, TEAM_MEMBER_COUNT);

        return this;
    }

    public TeamItem putMemberCount(ContentValues v) {
        v.put(TEAM_MEMBER_COUNT, this.memberCount);

        return this;
    }

    public String getMemberIds() {
        return this.memberIds;
    }

    public TeamItem setMemberIds(String memberIds) {
        this.memberIds = memberIds;

        return this;
    }

    public TeamItem setMemberIds(Cursor c) {
        this.memberIds = s(c, TEAM_MEMBER_IDS);

        return this;
    }

    public TeamItem putMemberIds(ContentValues v) {
        v.put(TEAM_MEMBER_IDS, this.memberIds);

        return this;
    }

    public String getMemberWaitIds() {
        return this.memberWaitIds;
    }

    public TeamItem setMemberWaitIds(String memberWaitIds) {
        this.memberWaitIds = memberWaitIds;

        return this;
    }

    public TeamItem setMemberWaitIds(Cursor c) {
        this.memberWaitIds = s(c, TEAM_MEMBER_WAIT_IDS);

        return this;
    }

    public TeamItem putMemberWaitIds(ContentValues v) {
        v.put(TEAM_MEMBER_WAIT_IDS, this.memberWaitIds);

        return this;
    }

    public int getPosition() {
        return this.position;
    }

    public TeamItem setPosition(int position) {
        this.position = position;

        return this;
    }

    public TeamItem setPosition(Cursor c) {
        this.position = i(c, TEAM_POSITION);

        return this;
    }

    public TeamItem putPosition(ContentValues v) {
        v.put(TEAM_POSITION, this.position);

        return this;
    }

    public int getStatus() {
        return this.status;
    }

    public TeamItem setStatus(int status) {
        this.status = status;

        return this;
    }

    public TeamItem setStatus(Cursor c) {
        this.status = i(c, TEAM_STATUS);

        return this;
    }

    public TeamItem putStatus(ContentValues v) {
        v.put(TEAM_STATUS, this.status);

        return this;
    }

    public String getJoinRule() {
        return this.joinRule;
    }

    public TeamItem setJoinRule(String joinRule) {
        this.joinRule = joinRule;

        return this;
    }

    public TeamItem setJoinRule(Cursor c) {
        this.joinRule = s(c, TEAM_JOIN_RULE);

        return this;
    }

    public TeamItem putJoinRule(ContentValues v) {
        v.put(TEAM_JOIN_RULE, this.joinRule);

        return this;
    }

    public String getJoinDomain() {
        return this.joinDomain;
    }

    public TeamItem setJoinDomain(String joinDomain) {
        this.joinDomain = joinDomain;

        return this;
    }

    public TeamItem setJoinDomain(Cursor c) {
        this.joinDomain = s(c, TEAM_JOIN_DOMAIN);

        return this;
    }

    public TeamItem putJoinDomain(ContentValues v) {
        v.put(TEAM_JOIN_DOMAIN, this.joinDomain);

        return this;
    }

    public String getRegDate() {
        return this.regDate;
    }

    public TeamItem setRegDate(String regDate) {
        this.regDate = regDate;

        return this;
    }

    public TeamItem setRegDate(Cursor c) {
        this.regDate = s(c, TEAM_REG_DATE);

        return this;
    }

    public TeamItem putRegDate(ContentValues v) {
        v.put(TEAM_REG_DATE, this.regDate);

        return this;
    }

    public String getModiDate() {
        return this.regDate;
    }

    public TeamItem setModiDate(String modiDate) {
        this.modiDate = modiDate;

        return this;
    }

    public TeamItem setModiDate(Cursor c) {
        this.modiDate = s(c, TEAM_MODI_DATE);

        return this;
    }

    public TeamItem putModiDate(ContentValues v) {
        v.put(TEAM_MODI_DATE, this.regDate);

        return this;
    }

    public ArrayList<MemberItem> getMemberItems() {
        return memberItems;
    }

    public TeamItem setMemberItems(ArrayList<MemberItem> items) {
        this.memberItems = items;

        return this;
    }

    public String getBrowserIdx() {
        return this.browserIdx;
    }

    public TeamItem setBrowserIdx(Cursor c) {
        this.browserIdx = s(c, TEAM_CURRENT_BROWSER_IDX);

        return this;
    }

    public TeamItem setBrowserIdx(String idx) {
        this.browserIdx = idx;

        return this;
    }

    public TeamItem putBrowserIdx(ContentValues v) {
        v.put(TEAM_CURRENT_BROWSER_IDX, this.browserIdx);

        return this;
    }
}
