package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.common.ImageUtil;
import io.jmobile.tm.browser.data.BrowserItem;

public class BrowserAdapter extends ReSelectableAdapter<BrowserItem, BrowserAdapter.BrowserHolder> {

    private Context context;
    private ArrayList<BrowserItem> items = new ArrayList<>();
    private int selectedIndex = -1;

    public BrowserAdapter(Context context, int layoutId, ArrayList<BrowserItem> items, BrowserAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(BrowserHolder holder, int position) {

        BrowserItem item = items.get(position);

        holder.team.setText(item.getBrowserTeamName());
        holder.title.setText(item.getBrowserTitle());
        String url = item.getBrowserUrl();
        holder.url.setText(url);

        String temp = url.replace("http://", "");
        temp = temp.replace("https://", "");
        temp = temp.replace("www.", "");
        temp = temp.replace("m.", "");
        String[] temps = temp.split("/");
        url = temp;
        if (temps != null && temps.length > 0) {
            url = temps[0];
        }
        holder.initial.setText(url.substring(0, 1));

        holder.line.setBackgroundResource(position == selectedIndex ? R.color.main : R.color.gray3_a100);
        holder.title.setTextColor(ContextCompat.getColor(context, position == selectedIndex ? R.color.main : R.color.black_a100));
        holder.selView.setVisibility(position == selectedIndex ? View.VISIBLE : View.GONE);

        if (item.getBrowserThumbnail() != null) {
            holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getBrowserThumbnail()));
            holder.initial.setVisibility(View.GONE);
        } else {
            holder.thumbnail.setImageResource(R.color.transparent);
            holder.initial.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public BrowserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BrowserHolder holder = new BrowserHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.line = holder.fv(R.id.view_line_browser);

        holder.thumbnail = holder.fv(R.id.image_icon);
        holder.initial = holder.fv(R.id.text_initial);
        holder.selView = holder.fv(R.id.view_select);

        holder.team = holder.fv(R.id.text_team);
        holder.title = holder.fv(R.id.text_title);
        holder.url = holder.fv(R.id.text_url);
        holder.closeButton = holder.fv(R.id.button_close);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnCloseButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    ((BrowserAdapterListener) listener).onCloseButtonClicked(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

//    public void clearItems() {
//        notifyItemRemoved(0);
//    }

    public void removeBrowserItem(String id) {
        if (items == null)
            return;

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getBrowserId().equalsIgnoreCase(id)) {
                items.remove(i);
                notifyItemRemoved(i);
                if (i < items.size())
                    notifyItemChanged(i);
                return;
            }
        }
    }

    public void setSelectedIndex(int index) {
        this.selectedIndex = index;
        notifyDataSetChanged();
    }

    public static interface BrowserAdapterListener extends ReOnItemClickListener<BrowserItem> {
        public void onCloseButtonClicked(int position, BrowserItem item);
    }

    public class BrowserHolder extends ReAbstractViewHolder {
        ImageView thumbnail;
        View selView, line;
        TextView title, initial, team, url;
        ImageButton closeButton;

        private OnItemViewClickListener closeButtonClickListener;

        public BrowserHolder(View itemView) {
            super(itemView);
        }

        public void setOnCloseButtonClickListener(OnItemViewClickListener listener) {
            closeButtonClickListener = listener;
            if (listener != null && closeButton != null) {
                closeButton.setOnClickListener(v -> closeButtonClickListener.onItemViewClick(getAdapterPosition(), BrowserHolder.this));
            }
        }
    }

}
