package io.jmobile.tm.browser.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.MimeTypes;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.network.TMB_network;
import io.jmobile.tm.browser.ui.dialog.EditNameDialog;
import io.jmobile.tm.browser.ui.dialog.EditPasswordDialog;
import io.jmobile.tm.browser.ui.dialog.EditProfileDialog;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.getRealPathFromURI;
import static io.jmobile.tm.browser.common.Util.getStorageSaveFileFolderPath;
import static io.jmobile.tm.browser.common.Util.showToast;

public class AccountManageActivity extends BaseActivity {

    private static final int REQUEST_CODE_READ_EXTERNAL_STORAGE = 10001;
    private static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 10002;
    private static String[] PERMISSIONS = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    ImageButton backButton;
    ImageView profileImage, editProfileImage;

    TextView titleText, emailText, nameText;
    LinearLayout editProfileLayout, editNameLayout, editPWLayout;
    //    LinearLayout termsLayout, privacyLayout;
    LinearLayout signoutLayout, deleteLayout;
//    Button deleteButton;

    MemberItem myItem;
    Dialog progress;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_account_manage);

        initView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == 0) { // from Gallery
            Uri contentURI = data.getData();
            try {
                if (contentURI != null) {
                    File file = new File(getRealPathFromURI(AccountManageActivity.this, contentURI));
                    if (file.exists()) {
                        uploadProfile(file.getPath());
                    }
                }
                return;
            } catch (Exception e) {
                LogUtil.log(e.getMessage());
            }
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                String path = saveImage(bitmap);
                uploadProfile(path);
            } catch (IOException e) {
                LogUtil.log(e.getMessage());
            }
        } else if (requestCode == 1) { // take a photo
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            String path = saveImage(thumbnail);
            uploadProfile(path);
        }
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> onBackPressed());
        backButton.setVisibility(View.VISIBLE);

        titleText = fv(R.id.text_title);
        titleText.setVisibility(View.VISIBLE);
        titleText.setText(r.s(R.string.title_edit_profile));

        emailText = fv(R.id.text_email);
        emailText.setText(sp.getLoginEmail());

        nameText = fv(R.id.text_edit_name);
        nameText.setText(sp.getLoginName());

        profileImage = fv(R.id.image_user_profile);
        editProfileImage = fv(R.id.image_profile);

        myItem = db.getMyInfo();
        if (myItem != null) {
            Glide.with(this).load(myItem.getProfileUrl()).centerCrop().placeholder(R.drawable.ic_group_peacock_64).into(editProfileImage);
            Glide.with(this).load(myItem.getProfileUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_group_peacock_64).into(new BitmapImageViewTarget(profileImage) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    profileImage.setImageDrawable(circularBitmapDrawable);
                }
            });
        }

        editProfileLayout = fv(R.id.layout_edit_profile);
        editProfileLayout.setOnClickListener(v -> {
            if (!askForPermission())
                return;

            if (fdf(EditProfileDialog.TAG) == null) {
                EditProfileDialog dlg = EditProfileDialog.newInstance(EditProfileDialog.TAG);
                dlg.setListener(new EditProfileDialog.EditProfileDialogListener() {
                    @Override
                    public void onAlbumButtonClicked() {
                        choosePhotoFromGallery();
                    }

                    @Override
                    public void onCameraButtonClicked() {
                        takePhotoFromCamera();
                    }

                    @Override
                    public void onRemovePhotoButtonClicked() {

                    }
                });
                sdf(dlg);
            }
        });

        editNameLayout = fv(R.id.layout_edit_name);
        editNameLayout.setOnClickListener(v -> {
            if (fdf(EditNameDialog.TAG) == null) {
                EditNameDialog dlg = EditNameDialog.newInstance(EditNameDialog.TAG);
                dlg.setPositiveListener((dialog, tag) -> {
                    nameText.setText(sp.getLoginName());
                });
                sdf(dlg);
            }
        });

        editPWLayout = fv(R.id.layout_edit_password);
        editPWLayout.setOnClickListener(v -> {
            if (fdf(EditPasswordDialog.TAG) == null) {
                EditPasswordDialog dlg = EditPasswordDialog.newInstance(EditPasswordDialog.TAG);
                dlg.setPositiveListener((dialog, tag) -> showToast(AccountManageActivity.this, r.s(R.string.message_password_reset_completed), false));
                sdf(dlg);
            }
        });

//        privacyLayout = fv(R.id.layout_privacy);
//        privacyLayout.setOnClickListener(v -> {
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Common.URL_JMOBILE_PRIVACY));
//            startActivity(intent);
//        });
//
//        termsLayout = fv(R.id.layout_terms);
//        termsLayout.setOnClickListener(v -> {
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Common.URL_JMOBILE_TERMS));
//            startActivity(intent);
//        });

        signoutLayout = fv(R.id.layout_signout);
        signoutLayout.setOnClickListener(v -> {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.message_sign_out));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setTitle(r.s(R.string.sign_out));
                dlg.setPositiveLabel(r.s(R.string.sign_out));
                dlg.setPositiveListener((dialog, tag) -> {
                    if (!sp.isRememberLoginEmail())
                        sp.setLoginEmail("");
                    sp.setLoginToken("");
                    sp.setCurrentTeamId(Common.TMBR_PRIVATE_TEAM_ID);
                    db.deleteAllMemberItem();
                    finish();
                });
                sdf(dlg);
            }
        });

        deleteLayout = fv(R.id.layout_delete_account);
        deleteLayout.setOnClickListener(v -> {
            Intent intent = new Intent(AccountManageActivity.this, AccountDeleteActivity.class);
            startActivity(intent);
        });
    }


    public void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, 0);
    }


    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 1);
    }

    public void uploadProfile(String path) {
        showProgress();
        try {
            List<MultipartBody.Part> parts = new ArrayList();
            File file = new File(path);

            String mime = MimeTypes.getMimeType(file);
            RequestBody requestBody;
            if (mime != null) {
                requestBody = RequestBody.create(MediaType.parse(mime), file);
            } else {
                requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            }

            MultipartBody.Part part = MultipartBody.Part.createFormData("filedata", file.getName(), requestBody);
            parts.add(part);

            TMB_network.uploadProfile(parts)
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    getUserInfo();
                                    file.delete();
                                } else {
                                    showToast(AccountManageActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(AccountManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    private String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(getStorageSaveFileFolderPath());

        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            LogUtil.log("File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void getUserInfo() {
        try {
            TMB_network.getUserInfo()
                    .flatMap(tmBrowserPair -> Observable.just(tmBrowserPair))
                    .doOnError(throwable -> {
                        sp.setLoginToken("");
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {

                                if (tmBrowserPair.first) {
                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
                                    String name = result.get("name").getAsString();
                                    sp.setLoginName(name);
                                    MemberItem item = new MemberItem(result, false);
                                    if (item == null) {
                                        sp.setLoginToken("");
                                    } else {
                                        item.setMaster(true);
                                        db.insertOrUpdateMemberItem(item);
                                        Glide.with(this).load(item.getProfileUrl()).centerCrop().placeholder(R.drawable.ic_group_peacock_64).into(editProfileImage);
                                        Glide.with(this).load(item.getProfileUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_group_peacock_64).into(new BitmapImageViewTarget(profileImage) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                RoundedBitmapDrawable circularBitmapDrawable =
                                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                                circularBitmapDrawable.setCircular(true);
                                                profileImage.setImageDrawable(circularBitmapDrawable);
                                            }
                                        });

                                    }
                                } else {
                                    sp.setLoginToken("");
                                    showToast(AccountManageActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                sp.setLoginToken("");
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.title_edit_profile), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

    // permission
    private boolean askForPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.app_name) + " needs permission.\nGo to Settings > Permissions, then allow the following permissions and try again:");
                    dlg.setPositiveLabel("Settings");
                    dlg.setPositiveListener((dialog, tag) -> {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + getPackageName()));
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    });
                    dlg.setNegativeLabel("cancel");
                    sdf(dlg);
                }

                return false;
            } else {
                ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_CODE_WRITE_EXTERNAL_STORAGE);
                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case REQUEST_CODE_WRITE_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Util.showToast(this, "NEED PERMISSION CHECK", false);
                }
                return;
            default:
                break;
        }
    }
}
