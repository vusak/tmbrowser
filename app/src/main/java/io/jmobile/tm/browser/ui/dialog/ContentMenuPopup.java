package io.jmobile.tm.browser.ui.dialog;


import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseDialogFragment;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.data.TeamItem;

public class ContentMenuPopup extends BaseDialogFragment {
//    public static final int TYPE_BOOKMARKS = 0;
//    public static final int TYPE_READINGLIST = 1;
//    public static final int TYPE_MEMOS = 2;

    public static final String TAG = ContentMenuPopup.class.getSimpleName();
    public static ContentMenuPopupListener listener;
    LinearLayout editLayout, selDeleteLayout, allDeleteLayout;
    LinearLayout newBookmarkLayout, newFolderLayout;
    LinearLayout allReadLayout, allUnreadLayout;
    LinearLayout newMemoLayout;
    int type;
    boolean editMode;

    public static ContentMenuPopup newInstance(String tag, int type, boolean editMode) {
        ContentMenuPopup d = new ContentMenuPopup();

        d.createArguments(tag);
        d.type = type;
        d.editMode = editMode;
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.pop_content_menu;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();
        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        float rate = 0.9f;
        params.width = (int) (dm.widthPixels * rate);
        params.height = (int) (dm.heightPixels * rate);
        getDialog().getWindow().setAttributes(params);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initView() {
        newMemoLayout = fv(R.id.layout_new_memo);
        newMemoLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onNewMemoButtonClicked();
            dismiss();
        });
        newMemoLayout.setVisibility(type == Common.TYPE_MEMOS ? View.VISIBLE : View.GONE);

        newBookmarkLayout = fv(R.id.layout_new_bookmark);
        newBookmarkLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onNewBookmarkButtonClicked();
            dismiss();
        });
        newBookmarkLayout.setVisibility(type == Common.TYPE_BOOKMARKS || type == Common.TYPE_QUICKMARK ? View.VISIBLE : View.GONE);

        newFolderLayout = fv(R.id.layout_new_folder);
        newFolderLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onNewFolderButtonClicked();

            dismiss();
        });
        newFolderLayout.setVisibility(type == Common.TYPE_BOOKMARKS || type == Common.TYPE_QUICKMARK ? View.VISIBLE : View.GONE);

        editLayout = fv(R.id.layout_edit_content);
        editLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onEditButtonClicked();
            dismiss();
        });
        editLayout.setVisibility(View.GONE);

        TeamItem team = app.getMyTeam(sp.getCurrentTeamId());

        selDeleteLayout = fv(R.id.layout_sel_delete);
        selDeleteLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onSelectDeleteButtonClicked();
            dismiss();
        });
        selDeleteLayout.setVisibility((sp.isPrivateMode() || team.isTeamLeader()) && type != Common.TYPE_QUICKMARK ? View.VISIBLE : View.GONE);
//        selDeleteLayout.setVisibility(sp.isPrivateMode() ? View.VISIBLE : View.GONE);

        allDeleteLayout = fv(R.id.layout_all_delete);
        allDeleteLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onAllDeleteButtonClicked();
            dismiss();
        });
        allDeleteLayout.setVisibility(View.GONE);
//        allDeleteLayout.setVisibility(sp.isPrivateMode() || team.isTeamLeader() ? View.VISIBLE : View.GONE);

        allReadLayout = fv(R.id.layout_all_read);
        allReadLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onAllReadButtonClicked();
            dismiss();
        });
        allReadLayout.setVisibility(sp.isPrivateMode() && type == Common.TYPE_READINGS ? View.VISIBLE : View.GONE);

        allUnreadLayout = fv(R.id.layout_all_unread);
        allUnreadLayout.setOnClickListener(v -> {
            if (listener != null)
                listener.onAllUnreadButtonClicked();

            dismiss();
        });
        allUnreadLayout.setVisibility(sp.isPrivateMode() && type == Common.TYPE_READINGS ? View.VISIBLE : View.GONE);
//        allUnreadLayout.setVisibility(View.GONE);

        (fv(R.id.button_close)).setOnClickListener(v -> dismiss());
        (fv(R.id.button_close2)).setOnClickListener(v -> dismiss());
    }

    public ContentMenuPopup setListener(ContentMenuPopupListener listener) {
        this.listener = listener;

        return this;
    }

    public static interface ContentMenuPopupListener {
        public void onNewMemoButtonClicked();

        public void onEditButtonClicked();

        public void onSelectDeleteButtonClicked();

        public void onAllDeleteButtonClicked();

        public void onNewBookmarkButtonClicked();

        public void onNewFolderButtonClicked();

        public void onAllReadButtonClicked();

        public void onAllUnreadButtonClicked();
    }

}
