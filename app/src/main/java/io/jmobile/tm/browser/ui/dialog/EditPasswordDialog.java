package io.jmobile.tm.browser.ui.dialog;


import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseDialogFragment;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.network.TMB_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.showToast;

public class EditPasswordDialog extends BaseDialogFragment {

    public static final String TAG = EditPasswordDialog.class.getSimpleName();

    TextView passwordText, passwordNewText, passwordConfirmText;
    EditText passwordEdit, passwordNewEdit, passwordConfirmEdit;
    View passwordLine, passwordNewLine, passwordConfirmLine;
    ImageButton passwordDelButton, passwordNewDelButton, passwordConfirmDelButton;

    Button okButton, cancelButton;
    Dialog progress;

    public static EditPasswordDialog newInstance(String tag) {
        EditPasswordDialog d = new EditPasswordDialog();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_edit_password;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initView() {
        passwordText = fv(R.id.text_password);
        passwordLine = fv(R.id.view_password_line);
        passwordEdit = fv(R.id.edit_password);
        passwordEdit.setOnFocusChangeListener((v, hasFocus) -> {
            passwordText.setTextColor(r.c(getActivity(), hasFocus ? R.color.main : R.color.main_tm));
            passwordLine.setBackgroundResource(R.color.main);
            passwordLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            passwordDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(passwordEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });

        passwordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                passwordDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        passwordDelButton = fv(R.id.button_password_del);
        passwordDelButton.setOnClickListener(v -> {
            if (passwordEdit != null)
                passwordEdit.setText("");
        });

        passwordNewText = fv(R.id.text_password_new);
        passwordNewLine = fv(R.id.view_password_new_line);
        passwordNewEdit = fv(R.id.edit_password_new);
        passwordNewEdit.setOnFocusChangeListener((v, hasFocus) -> {
            passwordNewText.setTextColor(r.c(getActivity(), hasFocus ? R.color.main : R.color.main_tm));
            passwordNewLine.setBackgroundResource(R.color.main);
            passwordNewLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            passwordNewDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(passwordNewEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });

        passwordNewEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                passwordNewDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        passwordNewDelButton = fv(R.id.button_password_new_del);
        passwordNewDelButton.setOnClickListener(v -> {
            if (passwordNewEdit != null)
                passwordNewEdit.setText("");
        });

        passwordConfirmText = fv(R.id.text_password_confirm);
        passwordConfirmLine = fv(R.id.view_password_confirm_line);
        passwordConfirmEdit = fv(R.id.edit_password_confirm);
        passwordConfirmEdit.setOnFocusChangeListener((v, hasFocus) -> {
            passwordConfirmText.setTextColor(r.c(getActivity(), hasFocus ? R.color.main : R.color.main_tm));
            passwordConfirmLine.setBackgroundResource(R.color.main);
            passwordConfirmLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            passwordConfirmDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(passwordConfirmEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });

        passwordConfirmEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                passwordConfirmDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        passwordConfirmDelButton = fv(R.id.button_password_confirm_del);
        passwordConfirmDelButton.setOnClickListener(v -> {
            if (passwordConfirmEdit != null)
                passwordConfirmEdit.setText("");
        });

        okButton = fv(R.id.button_positive);
        okButton.setOnClickListener(v -> {
            if (checkEmpty())
                return;
            resetPassword();
        });

        cancelButton = fv(R.id.button_negative);
        cancelButton.setOnClickListener(v -> dismiss());
    }

    private boolean checkEmpty() {
        if (TextUtils.isEmpty(passwordEdit.getText().toString())) {
            passwordText.setTextColor(r.c(getActivity(), R.color.red_a100));
            passwordLine.setBackgroundResource(R.color.red_a100);
            passwordLine.setVisibility(View.VISIBLE);
            showToast(getActivity(), r.s(R.string.message_empty_current_password), true);
            return true;
        }

        if (TextUtils.isEmpty(passwordNewEdit.getText().toString())) {
            passwordNewText.setTextColor(r.c(getActivity(), R.color.red_a100));
            passwordNewLine.setBackgroundResource(R.color.red_a100);
            passwordNewLine.setVisibility(View.VISIBLE);
            showToast(getActivity(), r.s(R.string.message_empty_new_password), true);
            return true;
        }

        if (passwordNewEdit.getText().toString().length() < 6) {
            passwordNewText.setTextColor(r.c(getActivity(), R.color.red_a100));
            passwordNewLine.setBackgroundResource(R.color.red_a100);
            passwordNewLine.setVisibility(View.VISIBLE);
            showToast(getActivity(), r.s(R.string.hint_password), true);
            return true;
        }

        if (TextUtils.isEmpty(passwordConfirmEdit.getText().toString())) {
            passwordConfirmText.setTextColor(r.c(getActivity(), R.color.red_a100));
            passwordConfirmLine.setBackgroundResource(R.color.red_a100);
            passwordConfirmLine.setVisibility(View.VISIBLE);
            showToast(getActivity(), r.s(R.string.message_empty_confirm_password), true);
            return true;
        }

        if (!passwordNewEdit.getText().toString().equalsIgnoreCase(passwordConfirmEdit.getText().toString())) {
            passwordConfirmText.setTextColor(r.c(getActivity(), R.color.red_a100));
            passwordConfirmLine.setBackgroundResource(R.color.red_a100);
            passwordConfirmLine.setVisibility(View.VISIBLE);
            showToast(getActivity(), r.s(R.string.message_wrong_confirm_password), true);
            return true;
        }

        return false;
    }

    private void resetPassword() {
        showProgress();
        try {
            TMB_network.changePassword(passwordEdit.getText().toString().trim(),
                    passwordNewEdit.getText().toString().trim(),
                    passwordConfirmEdit.getText().toString().trim())
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    if (positiveListener != null)
                                        positiveListener.onDialogPositive(EditPasswordDialog.this, tag);
                                    dismiss();
                                } else {
                                    passwordText.setTextColor(r.c(getActivity(), R.color.red_a100));
                                    passwordLine.setBackgroundResource(R.color.red_a100);
                                    passwordLine.setVisibility(View.VISIBLE);
                                    showToast(getActivity(), (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(getActivity(), r.s(R.string.title_reset_password), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }
}
