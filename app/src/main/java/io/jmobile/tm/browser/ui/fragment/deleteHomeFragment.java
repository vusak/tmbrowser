package io.jmobile.tm.browser.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.BrowserAdapter;
import io.jmobile.tm.browser.adapter.HistoryAdapter;
import io.jmobile.tm.browser.adapter.MemoAdapter;
import io.jmobile.tm.browser.adapter.QuickmarkAdapter;
import io.jmobile.tm.browser.adapter.ReAdapter;
import io.jmobile.tm.browser.adapter.UrlAdapter;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.AppItem;
import io.jmobile.tm.browser.data.BookmarkItem;
import io.jmobile.tm.browser.data.BrowserItem;
import io.jmobile.tm.browser.data.HistoryItem;
import io.jmobile.tm.browser.data.MemoItem;
import io.jmobile.tm.browser.data.TeamItem;
import io.jmobile.tm.browser.network.Api;
import io.jmobile.tm.browser.ui.ContentActivity;
import io.jmobile.tm.browser.ui.ContentAddActivity;
import io.jmobile.tm.browser.ui.MainActivity;
import io.jmobile.tm.browser.ui.TeamActivity;
import io.jmobile.tm.browser.ui.dialog.HomeMenuPopup;

public class deleteHomeFragment extends BaseFragment implements BrowserFragment.BrowserFragmentListener, MainActivity.OnBackPressedListener {

    private static deleteHomeFragment fragment;
    ImageButton menuButton, searchButton, dashButton;
    TextView titleText, titleText2, tabText;
    ImageButton prevButton, nextButton, homeButton, moreButton, tabButton, newMemoButton;

    Api api;
    LinearLayout teamSettingLayout;
    TextView teamCountText;

    ArrayList<AppItem> appItems;
    ArrayList<BookmarkItem> bookmarkItems;
    ArrayList<MemoItem> memoItems;

    RecyclerView bookmarkLv, memoLv;
    QuickmarkAdapter bookmarkAdapter;
    MemoAdapter memoAdapter;
    LinearLayoutManager bookmarkManager, memoManager;
    LinearLayout memoEmptyLayout;

    LinearLayout urlLayout;
    ImageButton urlBackButton, urlSearchButton;
    EditText urlEdit;

    LinearLayout tabsLayout;
    ScrollView tabsScroll;

    RecyclerView tabsLv;
    LinearLayout historyEmptyLayout;
    ExpandableListView historyLv;
    ArrayList<HistoryItem> historyList;
    Map<String, List<HistoryItem>> map;
    ArrayList<String> historyDateList;
    HistoryAdapter historyAdapter;

    RecyclerView urlLv, recentLv;
    ArrayList<HistoryItem> urlList, recentList;
    LinearLayoutManager urlManager, recentManager;
    UrlAdapter urlAdapter, recentAdapter;
    LinearLayout emptyLayout, recentLayout, urlListLayout;

    LinearLayout newLayout, clearLayout, clearTabsButton;
    FrameLayout tabsButton, historyButton;
    ImageView tabsImage, historyImage;
    TextView tabsText;
    View tabsLine, historyLine;

    LinearLayout browserLayout;
    ArrayList<BrowserFragment> browserFragments;
    ArrayList<BrowserItem> browserList, teamBrowserList;
    LinearLayoutManager browserManager;
    BrowserAdapter browserAdapter;

    BrowserFragment currentBrowserFragment;
    TeamItem currentTeam;

//    public static deleteHomeFragment newInstance() {
//        if (fragment == null)
//            fragment = new deleteHomeFragment();
//        return fragment;
//    }
//    private Handler urlSearchHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            if (msg.what == 0) {
//                if (urlList == null)
//                    urlList = new ArrayList<>();
//                urlList.clear();
//                String keyword = urlEdit.getText().toString();
//                for (HistoryItem item : historyList) {
//                    if (item.getHistoryUrl().contains(keyword)
//                            || item.getHistoryTitle().contains(keyword))
//                        urlList.add(item);
//                }
//
//                urlAdapter.setKeyword(keyword);
//            }
//        }
//    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_delete_home;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((MainActivity) context).setOnBackPressedListener(this);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();
        initTabsView();

        if (api != null && currentTeam != null) {
            api.getBookmarkList("", currentTeam.getTeamId());
            api.getTodayMemo();
        }
    }

    @Override
    public void savedBrowserItem() {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void doBack() {
        if (urlLayout.getVisibility() == View.VISIBLE) {
            visibleUrl(false, "");
        } else if (tabsLayout.getVisibility() == View.VISIBLE) {
            visibleNewTabLayout(false);
        } else if (browserLayout.getVisibility() == View.VISIBLE) {
            if (!currentBrowserFragment.onPrevButtonClicked())
                visibleBrowser(false);
        } else {
            ((MainActivity) getActivity()).setOnBackPressedListener(null);
            ((MainActivity) getActivity()).onHomeButtonClicked();
        }

    }

    @Override
    public void onResume() {
        if (currentBrowserFragment != null && isBrowserVisibility())
            currentBrowserFragment.onResume();
//        refreshMainList();
        super.onResume();
    }

    @Override
    public void onPause() {
        if (currentBrowserFragment != null)
            currentBrowserFragment.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void handleApiMessage(Message m) {
        super.handleApiMessage(m);
        if (m.what == Common.API_CODE_BOOKMARK_SELECT) {
            boolean result = m.getData().getBoolean(Common.TAG_RESULT);
            if (result) {
                bookmarkItems.clear();
                ArrayList<BookmarkItem> temp = m.getData().getParcelableArrayList(Common.TAG_BOOKMARKS);
                if (temp != null && temp.size() > 0)
                    bookmarkItems.addAll(temp);
                bookmarkItems.add(new BookmarkItem().setBookmarkId(Common.TMBR_BOOKMAKR_ADD_ID));
                bookmarkAdapter.notifyDataSetChanged();
//                emptyLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
            }
        } else if (m.what == Common.API_CODE_TODAY_MEMO_SELECT) {
            boolean result = m.getData().getBoolean(Common.TAG_RESULT);
            if (result) {
                memoItems.clear();
                ArrayList<MemoItem> temp = m.getData().getParcelableArrayList(Common.TAG_TODAY_MEMOS);
                if (temp != null && temp.size() > 0)
                    memoItems.addAll(temp);
                memoAdapter.notifyDataSetChanged();

                memoEmptyLayout.setVisibility(memoItems.size() > 0 ? View.GONE : View.VISIBLE);
                memoLv.setVisibility(memoItems.size() > 0 ? View.VISIBLE : View.GONE);

            }
        }
    }

    private void initView() {
        menuButton = fv(R.id.button_menu);
        menuButton.setOnClickListener(v -> {
            ((MainActivity) getActivity()).onMenuButtonClicked();
//            if (listener != null)
//                listener.onMenuButtonClicked();
        });
        menuButton.setVisibility(View.VISIBLE);

        titleText = fv(R.id.text_title);
        titleText2 = fv(R.id.text_title_2);
        if (currentTeam != null) {
            titleText.setVisibility(View.VISIBLE);
            titleText.setText(currentTeam.getName());
        }

        dashButton = fv(R.id.button_dash);
        dashButton.setOnClickListener(v -> {
            ((MainActivity) getActivity()).onHomeButtonClicked();
//            if (listener != null)
//                listener.onHomeButtonClicked();
        });
        dashButton.setVisibility(View.VISIBLE);

        searchButton = fv(R.id.button_search);
        searchButton.setVisibility(View.VISIBLE);
        searchButton.setOnClickListener(v -> {
            visibleUrl(true, "");
        });

        urlLayout = fv(R.id.layout_url);
//        urlBackButton = fv(R.id.button_url_back);
//        urlBackButton.setOnClickListener(v -> {
//            visibleUrlLayout(false, "");
//        });
//
//        urlEdit = fv(R.id.edit_url);
//        urlEdit.setOnFocusChangeListener((v, hasFocus) -> urlEdit.selectAll());
//        urlEdit.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                urlSearchHandler.removeCallbacksAndMessages(null);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                boolean empty = TextUtils.isEmpty(s.toString());
//                urlListLayout.setVisibility(empty ? View.GONE : View.VISIBLE);
//                if (!empty)
//                    urlSearchHandler.sendEmptyMessage(0);
//            }
//        });
//        urlEdit.setOnEditorActionListener((v, actionId, event) -> {
//            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                Util.hideKeyBoard(getActivity());
//
//                String url = urlEdit.getText().toString().trim();
//                String SCHEME = "http://";
//                if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
//                    sp.setBrowserUrl(getSearchEnging() + url);
//                } else if (url.matches("http.*")) { // http로 시작
//                    sp.setBrowserUrl(url);
//                } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
//                        || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
//                        || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
//                        || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
//                        || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
//                    sp.setBrowserUrl(SCHEME + url);
//                } else {
//                    sp.setBrowserUrl(getSearchEnging() + url);
//                }
//                goToBrowser(sp.getBrowserUrl());
//            }
//            return false;
//        });
//
//        urlSearchButton = fv(R.id.button_url_search);
//        urlSearchButton.setOnClickListener(v -> {
//            if (!urlEdit.getText().toString().isEmpty()) {
//                Util.hideKeyBoard(getActivity());
//
//                String url = urlEdit.getText().toString().trim();
//                String SCHEME = "http://";
//                if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
//                    sp.setBrowserUrl(getSearchEnging() + url);
//                } else if (url.matches("http.*")) { // http로 시작
//                    sp.setBrowserUrl(url);
//                } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
//                        || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
//                        || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
//                        || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
//                        || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
//                    sp.setBrowserUrl(SCHEME + url);
//                } else {
//                    sp.setBrowserUrl(getSearchEnging() + url);
//                }
//                goToBrowser(sp.getBrowserUrl());
//            }
//        });

        teamSettingLayout = fv(R.id.layout_team_setting);
        teamSettingLayout.setOnClickListener(v -> {
            if (currentTeam != null) {
                Intent intent = new Intent(getActivity(), TeamActivity.class);
                intent.putExtra("team_id", currentTeam.getTeamId());
                getActivity().startActivityForResult(intent, 1003);
            }
        });

        teamCountText = fv(R.id.text_team_member_count);
        if (currentTeam != null)
            teamCountText.setText(String.format(r.s(R.string.text_members), currentTeam.getMemberCount()));

        bookmarkLv = fv(R.id.lv_bookmark);
        bookmarkItems = new ArrayList<>();
        bookmarkManager = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.quickmark_count));
        bookmarkLv.setLayoutManager(bookmarkManager);
        bookmarkAdapter = new QuickmarkAdapter(getActivity(), R.layout.item_quickmark, bookmarkItems, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                BookmarkItem bookmark = (BookmarkItem) item;
                if (bookmark.getBookmarkId().equalsIgnoreCase(Common.TMBR_BOOKMARK_FOLDER_ROOT_ID)) {
                    Intent intent = new Intent(getActivity(), ContentAddActivity.class);
                    intent.putExtra("PARENT_IDX", "0");
                    intent.putExtra("TYPE", Common.TYPE_BOOKMARKS);
                    intent.putExtra("TEAM_IDX", currentTeam.getTeamId());
                    getActivity().startActivityForResult(intent, 1005);
                } else {
                    if (bookmark.isFolder()) {
                        Intent intent = new Intent(getActivity(), ContentActivity.class);
                        intent.putExtra(Common.ARG_INTENT_PARENT_IDX, bookmark);
                        intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_FOLDER);
                        startActivity(intent);
                    } else if (!TextUtils.isEmpty(bookmark.getUrl()))
                        goToBrowser(bookmark.getUrl());
                }
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        bookmarkLv.setAdapter(bookmarkAdapter);

        memoLv = fv(R.id.lv_home_memo);
        memoItems = new ArrayList<>();
        memoManager = new GridLayoutManager(getActivity(), 1);
        memoLv.setLayoutManager(memoManager);
        memoAdapter = new MemoAdapter(getActivity(), R.layout.item_memo, sp.getLoginEmail(), memoItems, new MemoAdapter.MemoAdapterListener() {
            @Override
            public void OnItemClick(int position, MemoItem item) {

            }

            @Override
            public void OnItemLongClick(int position, MemoItem item) {

            }

            @Override
            public void onMemoClicked(int position, MemoItem item) {

            }
        });
        memoLv.setAdapter(memoAdapter);
        memoEmptyLayout = fv(R.id.layout_memo_empty);
        newMemoButton = fv(R.id.button_new_memo);
        newMemoButton.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ContentAddActivity.class);
            intent.putExtra("PARENT_IDX", "0");
            intent.putExtra("TYPE", Common.TYPE_MEMOS);
            getActivity().startActivityForResult(intent, 1005);
        });

        currentBrowserFragment = new BrowserFragment();
        currentBrowserFragment.setListener(this);

        browserLayout = fv(R.id.layout_fragment_browser);
        browserLayout.setVisibility(View.INVISIBLE);

        browserList = new ArrayList<>();
        browserList.addAll(db.getBrowserItems());
        teamBrowserList = new ArrayList<>();
        teamBrowserList.addAll(db.getBrowserItemsByTeam(sp.getCurrentTeamId()));

        browserFragments = new ArrayList<>();

        prevButton = fv(R.id.button_menu_back);
        prevButton.setOnClickListener(v -> {
            if (currentBrowserFragment != null && browserLayout != null && isBrowserVisibility()
                    && urlLayout != null && !isUrlVisibility())
                currentBrowserFragment.onPrevButtonClicked();
        });

        nextButton = fv(R.id.button_menu_next);
        nextButton.setOnClickListener(v -> {
            if (currentBrowserFragment != null && browserLayout != null && isBrowserVisibility()
                    && urlLayout != null && !isUrlVisibility())
                currentBrowserFragment.onNextButtonClicked();
        });

        homeButton = fv(R.id.button_menu_home);
        homeButton.setOnClickListener(v -> {
            currentBrowserFragment.setHomeData();
            visibleBrowser(false);
            visibleUrlLayout(false, "");
        });

        tabButton = fv(R.id.button_menu_tabs);
        tabButton.setOnClickListener(v -> {
            visibleNewTabLayout(true);
        });

        tabText = fv(R.id.text_menu_tabs);
        tabText.setText("" + sp.getBrowserTabCount());

        moreButton = fv(R.id.button_menu_more);
        moreButton.setOnClickListener(v -> {
            if (fdf(HomeMenuPopup.TAG) == null) {
                HomeMenuPopup popup = HomeMenuPopup.newInstance(HomeMenuPopup.TAG, getActivity(), false);
                popup.setListener(new HomeMenuPopup.HomeMenuPopupListener() {
                    @Override
                    public void onNewTabButtonClicked() {

                    }

                    @Override
                    public void onAddBookmarkButtonClicked() {

                    }

                    @Override
                    public void onAddReadingButtonClicked() {

                    }

                    @Override
                    public void onBookmarksButtonClicked() {
                        Intent intent = new Intent(getActivity(), ContentActivity.class);
                        intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_BOOKMARKS);
                        startActivity(intent);
                    }

                    @Override
                    public void onReadingButtonClicked() {

                    }

                    @Override
                    public void onMemosButtonClicked() {
                        Intent intent = new Intent(getActivity(), ContentActivity.class);
                        intent.putExtra(Common.ARG_INTENT_CONTENT_TYPE, Common.TYPE_MEMOS);
                        startActivity(intent);
                    }
                });
                sdf(popup);
            }
        });

        emptyLayout = fv(R.id.layout_url_empty);
        recentLayout = fv(R.id.layout_url_history);
        urlListLayout = fv(R.id.layout_url_list);
        urlListLayout.setVisibility(View.GONE);

        recentLv = fv(R.id.lv_url_history);
        recentManager = new GridLayoutManager(getActivity(), 1);
        recentLv.setLayoutManager(recentManager);
        recentList = new ArrayList<>();
        recentAdapter = new UrlAdapter(getActivity(), R.layout.item_url, recentList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                goToBrowser(((HistoryItem) item).getHistoryUrl());
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        recentLv.setAdapter(recentAdapter);

        urlLv = fv(R.id.lv_url);
        urlManager = new GridLayoutManager(getActivity(), 1);
        urlLv.setLayoutManager(urlManager);
        urlList = new ArrayList<>();
        urlAdapter = new UrlAdapter(getActivity(), R.layout.item_url, urlList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                goToBrowser(((HistoryItem) item).getHistoryUrl());
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        urlLv.setAdapter(urlAdapter);
    }

    private void initTabsView() {
        tabsLayout = fv(R.id.layout_new_tab);
        tabsLayout.setVisibility(View.GONE);
        tabsScroll = fv(R.id.scroll_tabs);
        clearTabsButton = fv(R.id.layout_clear_all_tabs);
        clearTabsButton.setOnClickListener(v -> {
            db.deleteAllBrowserItems();
//            browserAdapter.clearItems();
            refreshBrowserList();
        });

        tabsLv = fv(R.id.lv_tab);
        browserManager = new GridLayoutManager(getActivity(), 1);
        tabsLv.setLayoutManager(browserManager);
        browserList = new ArrayList<>();
        browserAdapter = new BrowserAdapter(getActivity(), R.layout.item_browser, browserList, new BrowserAdapter.BrowserAdapterListener() {
            @Override
            public void onCloseButtonClicked(int position, BrowserItem item) {

            }

            @Override
            public void OnItemClick(int position, BrowserItem item) {
                selectTab(position);
//                if (sp.getCurrentBrowserIndex() == position) {
//
//                }
//                else
//                    selectTab(position);
//                visibleNewTabLayout(false);
//                visibleBrowser(true);
            }

            @Override
            public void OnItemLongClick(int position, BrowserItem item) {

            }
        });
        tabsLv.setAdapter(browserAdapter);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(tabsLv);

        historyEmptyLayout = fv(R.id.layout_history_empty);
        historyEmptyLayout.setVisibility(View.GONE);

        historyLv = fv(R.id.lv_history);
        historyLv.setOnGroupClickListener((parent, v, groupPosition, id) -> false);
        historyLv.setOnItemClickListener((parent, view, position, id) -> {

        });

        historyLv.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            String group = historyDateList.get(groupPosition);
            String url = map.get(group).get(childPosition).getHistoryUrl();
            goToBrowser(url);
            visibleNewTabLayout(false);
            return false;
        });
        historyList = new ArrayList<>();
        map = new HashMap<>();
        historyDateList = new ArrayList<>();

        historyAdapter = new HistoryAdapter(getActivity(), map, historyDateList);
        historyLv.setAdapter(historyAdapter);

        newLayout = fv(R.id.layout_new);
        newLayout.setOnClickListener(v -> {
            BrowserItem item = new BrowserItem();

            item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setBrowserTitle("HOME");
            item.setBrowserUrl("HOME");
            item.setBrowserPrivacy(false);
            item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
            item.setBrowserAt(System.currentTimeMillis());
            if (currentTeam != null) {
                item.setBrowserTeamId(currentTeam.getTeamId());
                item.setBrowserTeamName(currentTeam.getName());
            }
            item.setBrowserTeamId(sp.getCurrentTeamId());
            currentBrowserFragment = new BrowserFragment();
            currentBrowserFragment.setListener(deleteHomeFragment.this);
            currentBrowserFragment.setBrowserItem(item);
            browserFragments.add(currentBrowserFragment);

//                browserViewAdapter.addFrag(currBrowserFragment, item.getBrowserId());
//                browserViewAdapter.notifyDataSetChanged();
            browserList.add(item);
            teamBrowserList.add(item);
            browserAdapter.notifyDataSetChanged();
            sp.setCurrentBrowserIndex(browserList.size() - 1);

            db.insertOrUpdateBrowserItem(item);
            visibleBrowser(false);
            visibleNewTabLayout(false);
        });

        clearLayout = fv(R.id.layout_clear);
        clearLayout.setOnClickListener(v -> {
            db.deleteAllHistoryItems();
            refreshHistoryList();
            historyLv.setVisibility(View.GONE);
            historyEmptyLayout.setVisibility(View.VISIBLE);
        });

        tabsButton = fv(R.id.layout_tabs);
        tabsButton.setOnClickListener(v -> {
            setTabMode(true);
        });
        tabsImage = fv(R.id.image_tabs);
        tabsText = fv(R.id.text_tabs);
        tabsLine = fv(R.id.view_sel_tabs);

        historyButton = fv(R.id.layout_history);
        historyButton.setOnClickListener(v -> {
            setTabMode(false);
        });
        historyImage = fv(R.id.image_history);
        historyLine = fv(R.id.view_sel_history);

        refreshHistoryList();
        refreshBrowserList();
    }

    private void visibleNewTabLayout(boolean show) {
        tabsLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        if (show)
            setTabMode(true);
    }

    private void setTabMode(boolean isTabs) {
        tabsScroll.setVisibility(isTabs ? View.VISIBLE : View.GONE);
        newLayout.setVisibility(isTabs ? View.VISIBLE : View.GONE);
        tabsImage.setImageResource(isTabs ? R.drawable.ic_crop_din_peacock_24 : R.drawable.ic_crop_din_gray_4_24);
        tabsText.setTextColor(ContextCompat.getColor(getActivity(), isTabs ? R.color.main : R.color.gray6_100));
        tabsLine.setVisibility(isTabs ? View.VISIBLE : View.GONE);
        historyEmptyLayout.setVisibility(isTabs ? View.GONE : View.VISIBLE);
        historyLv.setVisibility(isTabs ? View.GONE : View.GONE);
        clearLayout.setVisibility(isTabs ? View.GONE : View.VISIBLE);
        historyImage.setImageResource(isTabs ? R.drawable.ic_history_gray_4_24 : R.drawable.ic_history_peacock_24);
        historyLine.setVisibility(isTabs ? View.GONE : View.VISIBLE);


        if (!isTabs) {
            refreshHistoryList();
            boolean empty = historyList.size() <= 0;
            historyLv.setVisibility(empty ? View.GONE : View.VISIBLE);
            historyEmptyLayout.setVisibility(empty ? View.VISIBLE : View.GONE);
        }
    }

    private void visibleUrl(boolean show, String url) {
        if (urlLayout == null)
            return;

        urlLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        if (show) {
            if (url != null && !TextUtils.isEmpty(url)) {
                urlEdit.setText(url);
                urlEdit.setSelection(0, url.length());
            }
            Util.showKeyBoard(urlEdit);
        } else
            Util.hideKeyBoard(getActivity());
    }

    public void goToBrowser(String url) {
        if (!url.contains("http://") && !url.contains("https://"))
            url = "http://" + url;
        Util.hideKeyBoard(urlEdit);
        urlLayout.setVisibility(View.GONE);

        visibleBrowser(true);
        currentBrowserFragment.goUrl(url);
    }

    private void visibleBrowser(boolean show) {
        browserLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        if (!show) {
            if (currentBrowserFragment != null) {
                currentBrowserFragment.stopWebViewLoading();
            }
            currentBrowserFragment.clearCurrData();
//            refreshMainList();
        } else {
            if (currentBrowserFragment != null) {
                currentBrowserFragment.onResume();
            }
        }
    }

    private boolean isBrowserVisibility() {
        return browserLayout.getVisibility() == View.VISIBLE;
    }

    private boolean isUrlVisibility() {
        return urlLayout.getVisibility() == View.VISIBLE;
    }

//    public void pagerDelete(BrowserItem item, final int position) {
//        if (sp.getCurrentBrowserIndex() == position)
//            sp.setCurrentBrowserIndex(position == 0 ? 0 : position - 1);
//        db.deleteBrowserItem(item);
//
//        browserList.remove(position);
//        browserFragments.remove(position);
////        browserAdapter.notifyDataSetChanged();
//        if (browserList.size() > 0) {
////            pager.setCurrentItem(position == 0 ? 0 : position - 1);
//            sp.setBrowserTabCount(browserList.size());
//            tabText.setText("" + browserList.size());
//            tabsText.setText("" + browserList.size());
////            bottomNewText.setText("" + browserList.size());
////            newText.setText("" + browserList.size());
////            returnText.setText("" + browserList.size());
//        } else {
//            refreshBrowserList();
//            visibleNewTabLayout(false);
//        }
//    }

//    private String getSearchEnging() {
//        String engine = "http://www.google.com/m?q=";
//        switch (sp.getSearchEngine()) {
//            case 0: // Google
//                engine = "http://www.google.com/m?q=";
//                break;
//
//            case 1: // Naver
//                engine = "http://m.search.naver.com/search.naver?query=";
//                break;
//
//            case 2: // bing
//                engine = "http://www.bing.com/search?q=";
//                break;
//        }
//
//        return engine;
//    }
//
//    public void selectedTab(int position) {
//        sp.setBrowserTabCount(browserList.size());
//        tabText.setText("" + browserList.size());
//        tabsText.setText("" + browserList.size());
////        bottomNewText.setText("" + browserList.size());
////        newText.setText("" + browserList.size());
////        returnText.setText("" + browserList.size());
////
////        if (pager.getCurrentItem() == sp.getCurrentBrowserIndex())
////            pager.setCurrentItem(pager.getCurrentItem() == 0 ? 1 : pager.getCurrentItem() - 1);
//        visibleNewTabLayout(false);
//        BrowserItem item = currentBrowserFragment.getBrowserItem();
//        if (item != null && item.getBrowserTitle().equals("HOME"))
//            visibleBrowser(false);
//        else
//            visibleBrowser(true);
//    }

    public void selectTab(int position) {
        sp.setCurrentBrowserIndex(position);
        browserAdapter.setSelectedIndex(position);
        currentBrowserFragment = browserFragments.get(sp.getCurrentBrowserIndex());
        BrowserItem item = currentBrowserFragment.getBrowserItem();
        currentTeam.setBrowserIdx(item.getBrowserId());
        app.setMyTeamBrowserIdx(currentTeam.getTeamId(), item.getBrowserId());
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.layout_fragment_browser, currentBrowserFragment, item.getBrowserId());
        transaction.commit();

        sp.setBrowserTabCount(browserList.size());
        tabText.setText("" + browserList.size());
        tabsText.setText("" + browserList.size());
//        bottomNewText.setText("" + browserList.size());
//        newText.setText("" + browserList.size());
//        returnText.setText("" + browserList.size());
//
//        if (pager.getCurrentItem() == sp.getCurrentBrowserIndex())
//            pager.setCurrentItem(pager.getCurrentItem() == 0 ? 1 : pager.getCurrentItem() - 1);
        visibleNewTabLayout(false);
        BrowserItem bItem = currentBrowserFragment.getBrowserItem();
        if (bItem != null && bItem.getBrowserTitle().equals("HOME"))
            visibleBrowser(false);
        else
            visibleBrowser(true);
    }

    public void refreshMainList(TeamItem team) {
        this.currentTeam = team;

        if (titleText != null) {
            titleText.setVisibility(View.VISIBLE);
            titleText.setText(currentTeam.getName());
        }

        if (teamCountText != null)
            teamCountText.setText(String.format(r.s(R.string.text_members), currentTeam.getMemberCount()));

        if (api != null) {
            api.getBookmarkList("", currentTeam.getTeamId());
            api.getTodayMemo();
        }


    }

    private void refreshHistoryList() {
        if (historyList == null)
            return;

        historyList.clear();
        historyList.addAll(db.getHistoryItems());

        historyDateList.clear();
        map.clear();

        for (HistoryItem item : historyList) {
            long temp = item.getHistoryAt();
            String date = Util.getDateString(temp);
            if (historyDateList.indexOf(date) < 0) {
                historyDateList.add(date);
                map.put(date, new ArrayList<HistoryItem>());
            }
            map.get(date).add(item);
        }

        historyAdapter.notifyDataSetChanged();

        for (int i = 0; i < historyDateList.size(); i++)
            historyLv.expandGroup(i);

        recentList.clear();
        recentList.addAll(db.getHistoryItemsByTeam(sp.getCurrentTeamId()));
        recentAdapter.notifyDataSetChanged();
        recentLayout.setVisibility(recentList.size() > 0 ? View.VISIBLE : View.GONE);
        emptyLayout.setVisibility(recentList.size() > 0 ? View.GONE : View.VISIBLE);
    }

    private void refreshBrowserList() {
        if (browserLayout == null)
            return;

        browserList.clear();
        teamBrowserList.clear();
        browserFragments.clear();
        browserList.addAll(db.getBrowserItems());
        teamBrowserList.addAll(db.getBrowserItemsByTeam(sp.getCurrentTeamId()));

        if (browserList == null || browserList.size() <= 0 ||
                teamBrowserList == null || teamBrowserList.size() <= 0) {
            BrowserItem item = new BrowserItem();
            item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setBrowserTitle("HOME");
            item.setBrowserUrl("HOME");
            item.setBrowserPrivacy(false);
            item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
            item.setBrowserAt(System.currentTimeMillis());
            if (currentTeam != null) {
                item.setBrowserTeamId(currentTeam.getTeamId());
                item.setBrowserTeamName(currentTeam.getName());

                currentTeam.setBrowserIdx(item.getBrowserId());
                app.setMyTeamBrowserIdx(currentTeam.getTeamId(), item.getBrowserId());
            }
            browserList.add(item);

            currentBrowserFragment = new BrowserFragment();
            currentBrowserFragment.setListener(this);
            currentBrowserFragment.setBrowserItem(item);

            browserFragments.add(currentBrowserFragment);
            sp.setCurrentBrowserIndex(browserList.size() - 1);

            db.insertOrUpdateBrowserItem(item);
        } else {
            int index = -1;
            for (int i = 0; i < browserList.size(); i++) {
                BrowserItem item = browserList.get(i);
                BrowserFragment bFragment = new BrowserFragment();
                bFragment.setBrowserItem(item);
                bFragment.setListener(this);
                browserFragments.add(bFragment);

                if (item.getBrowserId().equalsIgnoreCase(sp.getCurrentTeamId())) {
                    index = i;
                }
            }

            if (index < 0 || index >= browserList.size()) {
                sp.setCurrentBrowserIndex(0);
            } else
                sp.setCurrentBrowserIndex(index);

            currentBrowserFragment = browserFragments.get(sp.getCurrentBrowserIndex());
        }

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.layout_fragment_browser, currentBrowserFragment, browserList.get(sp.getCurrentBrowserIndex()).getBrowserId());
        transaction.commit();
        if (browserAdapter != null)
            browserAdapter.setSelectedIndex(sp.getCurrentBrowserIndex());
//        browserAdapter.notifyDataSetChanged();
//        pager.setCurrentItem(sp.getCurrentBrowserIndex());
        sp.setBrowserTabCount(browserList.size());
        tabText.setText("" + browserList.size());
        tabsText.setText("" + browserList.size());
//        bottomNewText.setText("" + browserList.size());
//        newText.setText("" + browserList.size());
//        returnText.setText("" + browserList.size());
    }

//    public void setTeamTitle(TeamItem item) {
//        this.currentTeam = item;
//
//        if (titleText != null) {
//            titleText.setVisibility(View.VISIBLE);
//            titleText.setText(item.getName());
//        }
//
//        if (teamCountText != null)
//            teamCountText.setText(String.format(r.s(R.string.text_members), currentTeam.getMemberCount()));
//
//        if (api != null) {
//            api.getBookmarkList("", currentTeam.getTeamId());
//            api.getTodayMemo();
//        }
//    }

    @Override
    public void visibleUrlLayout(boolean show, String url) {
        visibleUrl(show, url);
    }

    private void deleteBrowserTabItem(int position) {

    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                deleteBrowserTabItem(position);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;

                    LogUtil.log("" + Math.abs(1.0 - (Math.abs(dX) / itemView.getWidth())));
                    itemView.setAlpha(Math.abs(1f - (Math.abs(dX) / itemView.getWidth())));
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        };

        return simpleCallback;
    }
}
