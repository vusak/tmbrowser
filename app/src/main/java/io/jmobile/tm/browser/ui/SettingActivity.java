package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import io.jmobile.tm.browser.ui.dialog.SettingsDialog;

public class SettingActivity extends BaseActivity {

    ImageButton backButton;
    TextView titleText;

    LinearLayout searchEngineLayout, moreLayout, suggestLayout, termsLayout, privacyLayout;
    Switch savePwdSwitch;
    TextView textEngine, textVersion;
    Button delRecordButton;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings);

        initView();
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> onBackPressed());
        backButton.setVisibility(View.VISIBLE);

        titleText = fv(R.id.text_title);
        titleText.setVisibility(View.VISIBLE);
        titleText.setText(r.s(R.string.menu_setting));

        textEngine = fv(R.id.text_search_engine);
        textEngine.setText(r.sa(R.array.settings_search_engine)[sp.getSearchEngine()]);

        searchEngineLayout = fv(R.id.layout_search_engine);
        searchEngineLayout.setOnClickListener(v -> {
            if (fdf(SettingsDialog.TAG) == null) {
                final ArrayList<String> list = new ArrayList<>();
                list.addAll(Arrays.asList(r.sa(R.array.settings_search_engine)));
                SettingsDialog dlg = SettingsDialog.newInstance(SettingsDialog.TAG, r.s(R.string.setting_search_engine), list, sp.getSearchEngine());
                dlg.setPositiveListener((dialog, tag) -> {
                    int index = list.indexOf(tag);
                    if (index > -1)
                        sp.setSearchEngine(index);
                    textEngine.setText(tag);
                });
                sdf(dlg);
            }
        });

        savePwdSwitch = fv(R.id.switch_save_password);
        savePwdSwitch.setChecked(sp.isSavePasswords());
        savePwdSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> sp.setSetUseSavePasswords(isChecked));

        delRecordButton = fv(R.id.btn_del_records);
        delRecordButton.setOnClickListener(v -> {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG)
                        .setMessage(r.s(R.string.message_delete_records))
                        .setTitle(r.s(R.string.dlg_delete_title))
                        .setNegativeLabel(r.s(R.string.cancel))
                        .setPositiveLabel(r.s(R.string.delete));
                dlg.setPositiveListener((dialog, tag) -> new ClearRecordTask().execute());
                sdf(dlg);
            }
        });

        termsLayout = fv(R.id.layout_terms);
        termsLayout.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Common.URL_JMOBILE_TERMS));
            startActivity(intent);
        });

        privacyLayout = fv(R.id.layout_privacy);
        privacyLayout.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Common.URL_JMOBILE_PRIVACY));
            startActivity(intent);
        });

        textVersion = fv(R.id.version);
        String version;
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pi.versionName;
            textVersion.setText("TM Browser Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
        }
    }

    private void clearApplicationCache(File dir) {
        if (dir == null)
            dir = app.getCacheDir();

        if (dir == null)
            return;

        File[] child = dir.listFiles();
        try {
            for (int i = 0; i < child.length; i++) {
                if (child[i].isDirectory())
                    clearApplicationCache(child[i]);
                else
                    child[i].delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class ClearRecordTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            clearApplicationCache(null);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Util.showToast(SettingActivity.this, r.s(R.string.message_delete_complete), false);
            super.onPostExecute(aVoid);

        }
    }

}
