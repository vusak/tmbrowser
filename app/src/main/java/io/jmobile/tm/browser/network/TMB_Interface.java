package io.jmobile.tm.browser.network;


import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;

public interface TMB_Interface {

    /**
     * 회원가입 이메일 코드발송
     *
     * @param email
     * @param email_code
     * @return
     */
    @FormUrlEncoded
    @POST("user/join")
    rx.Observable<JsonObject> JoinAndSendEmailCode(@Field("email") String email, @Field("email_code") String email_code);

    /**
     * 회원가입
     *
     * @param email_code
     * @param email
     * @param password
     * @param repassword
     * @param name
     * @return
     */
    @FormUrlEncoded
    @PUT("user/join")
    rx.Observable<JsonObject> Join(@Field("email_code") String email_code, @Field("email") String email, @Field("password") String password, @Field("repassword") String repassword, @Field("name") String name);

    /**
     * 인증메일 재발송
     *
     * @param email
     * @return
     */
    @FormUrlEncoded
    @POST("user/join/resend")
    rx.Observable<JsonObject> ResendEmail(@Field("email") String email);

    /**
     * 회원정보 조회
     *
     * @return
     */
    @GET("user")
    rx.Observable<JsonObject> GetUserInfo();

    /**
     * 회원정보 수정
     *
     * @param name
     * @return
     */
    @FormUrlEncoded
    @PUT("user")
    rx.Observable<JsonObject> ModifyUserInfo(@Field("name") String name);

    /**
     * 회원 탈퇴
     *
     * @return
     */
    @DELETE("user")
    rx.Observable<JsonObject> LeaveMember();

    /**
     * 비밀번호 찾기
     *
     * @param email
     * @param name
     * @return
     */
    @POST("user/repassword")
    rx.Observable<JsonObject> FindPassword(@Field("email") String email, @Field("name") String name);

    /**
     * 비밀번호 변경
     *
     * @param current_password
     * @param new_password
     * @param new_password_match
     * @return
     */
    @FormUrlEncoded
    @PUT("user/password")
    rx.Observable<JsonObject> ChangePassword(@Field("current_password") String current_password, @Field("new_password") String new_password, @Field("new_password_match") String new_password_match);

    /**
     * 프로필수정
     *
     * @param filedata
     * @return
     */
    @Multipart
    @PUT("user/profile")
    rx.Observable<JsonObject> UploadProfile(@Part List<MultipartBody.Part> filedata);


	/*
		Team API
	 */

    /**
     * 나의 팀 목록
     *
     * @return
     */
    @GET("team")
    rx.Observable<JsonObject> GetTeamList();

    /**
     * 팀 생성
     *
     * @param name
     * @return
     */
    @FormUrlEncoded
    @POST("team")
    rx.Observable<JsonObject> CreateTeam(@Field("name") String name);

    /**
     * 팀 정보 확인
     *
     * @param team_id
     * @return
     */
    @GET("team/{team_id}")
    rx.Observable<JsonObject> GetTeamInfo(@Path("team_id") String team_id);

    /**
     * 팀 설정 변경
     *
     * @param team_id
     * @param options1
     * @param options2
     * @return
     */
    @FormUrlEncoded
    @PUT("team/{team_id}")
    rx.Observable<JsonObject> ModifyTeamInfo(@Path("team_id") String team_id, @FieldMap Map<String, Integer> options1, @FieldMap Map<String, String> options2);
    //Observable<JsonObject> ModifyTeamInfo(@Path("team_id") String team_id, @Field("volume") String volume, @Field("join_rule") String join_rule, @Field("join_protect_domain") String join_protect_domain, @Field("flag_push") String flag_push, @Field("trash_option") String trash_option);

    /**
     * 팀 삭제
     *
     * @param team_id
     * @return
     */
    @DELETE("team/{team_id}")
    rx.Observable<JsonObject> DeleteTeam(@Path("team_id") String team_id);

    /**
     * 가입할 팀 찾기
     *
     * @param name
     * @return
     */
    @FormUrlEncoded
    @POST("team/join")
    rx.Observable<JsonObject> FindTeam(@Field("name") String name);

    /**
     * 팀 가입
     *
     * @param team_id
     * @return
     */
    @POST("team/join/{team_id}")
    rx.Observable<JsonObject> JoinTeam(@Path("team_id") String team_id);

    /**
     * 팀 가입신청 메일발송
     *
     * @param team_id
     * @param memo
     * @return
     */
    @FormUrlEncoded
    @POST("team/join/{team_id}/request")
    rx.Observable<JsonObject> JoinTeamSendEmail(@Path("team_id") String team_id, @Field("memo") String memo);

    /**
     * 팀원 목록 조회
     *
     * @param team_id
     * @return
     */
    @GET("team/member/{team_id}")
    rx.Observable<JsonObject> GetTeamMemberList(@Path("team_id") String team_id);

    /**
     * 팀원 추가
     *
     * @param team_id
     * @param email
     * @return
     */
    @FormUrlEncoded
    @POST("team/member/{team_id}")
    rx.Observable<JsonObject> AddTeamMember(@Path("team_id") String team_id, @Field("email") String email);

//	/**
//	 * 팀원 탈퇴
//	 *
//	 * @param team_id
//	 * @param team_member_id
//	 * @return
//	 */
//
//	@DELETE("team/member/{team_id}")
//	Observable<JsonObject> LeaveTeamMember(@Path("team_id") String team_id, @Field("team_member_id") String team_member_id);

    /**
     * 팀원 탈퇴
     *
     * @param team_id
     * @param team_member_id
     * @return
     */
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "team/member/{team_id}", hasBody = true)
    rx.Observable<JsonObject> LeaveTeamMember(@Path("team_id") String team_id, @Field("team_member_id") String team_member_id);

    /**
     * 팀원정보 확인
     *
     * @param team_id
     * @param team_member_id
     * @return
     */
    @GET("team/member/{team_id}/{team_member_id}")
    rx.Observable<JsonObject> GetTeamMemberList(@Path("team_id") String team_id, @Path("team_member_id") String team_member_id);

    /**
     * 팀원 강제 탈퇴
     *
     * @param team_id
     * @param team_member_id
     * @return
     */

    @DELETE("team/member/{team_id}/{team_member_id}")
    rx.Observable<JsonObject> DeleteTeamMember(@Path("team_id") String team_id, @Path("team_member_id") String team_member_id);

    /**
     * 팀 초대메일 재전송
     *
     * @param team_id
     * @param team_member_id
     * @return
     */
    @POST("team/member/{team_id}/{team_member_id}")
    rx.Observable<JsonObject> ReSendJoinMail(@Path("team_id") String team_id, @Path("team_member_id") String team_member_id);

    /**
     * 팀원 가입 승인
     *
     * @param team_id
     * @param team_member_id
     * @return
     */
    @FormUrlEncoded
    @PUT("team/member/{team_id}")
    rx.Observable<JsonObject> WaitMemberConfirm(@Path("team_id") String team_id, @Field("team_member_id") String team_member_id);

	/*
		Login API
	 */

    /**
     * Token 발급
     *
     * @param email
     * @param password
     * @return
     */
    @FormUrlEncoded
    @POST("auth/token")
    Observable<JsonObject> TokenLogin(@Field("email") String email, @Field("password") String password);

    /**
     * Token 확인
     *
     * @param Authorization
     * @return
     */
    @GET("auth/resource")
    Observable<JsonObject> CheckToken(@Header("Authorization") String Authorization);

    /**
     * Token 확인
     *
     * @return
     */
    @GET("auth/resource")
    Observable<JsonObject> CheckToken();

    /**
     * Token 재발급
     *
     * @return
     */
    @GET("auth/refresh")
    Observable<JsonObject> RefreshToken();


    /**
     * 로그인
     *
     * @param email
     * @param password
     * @return
     */
    @FormUrlEncoded
    @POST("auth/login")
    Observable<JsonObject> Login(@Field("email") String email, @Field("password") String password);

    /**
     * 로그아웃
     *
     * @return
     */
    @GET("auth/logout")
    Observable<JsonObject> Logout();


    /**
     * 회원 대쉬보드
     *
     * @return
     */
    @GET("./")
    Observable<JsonObject> GetHomeData();
}