package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import io.jmobile.tm.browser.BaseApplication;
import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.data.TeamItem;

public class TeamAdapter extends ReSelectableAdapter<TeamItem, TeamAdapter.TeamHolder> {

    private static final int MAX_PORT = 5;
    private static final int MAX_LAND = 8;
    private Context context;
    private ArrayList<TeamItem> items = new ArrayList<>();
    private int index = -1;
    private int checkedIndex = -1;
    private boolean isPortrate;
    private String email;
    private String selectedId;

    private int[] layoutIds = {
            R.id.layout_team_1,
            R.id.layout_team_2,
            R.id.layout_team_3,
            R.id.layout_team_4,
            R.id.layout_team_5,
            R.id.layout_team_6,
            R.id.layout_team_7,
            R.id.layout_team_8
    };
    private int[] profileIds = {
            R.id.image_team_1,
            R.id.image_team_2,
            R.id.image_team_3,
            R.id.image_team_4,
            R.id.image_team_5,
            R.id.image_team_6,
            R.id.image_team_7,
            R.id.image_team_8
    };

    private int[] frameIds = {
            R.id.image_frame_1,
            R.id.image_frame_2,
            R.id.image_frame_3,
            R.id.image_frame_4,
            R.id.image_frame_5,
            R.id.image_frame_6,
            R.id.image_frame_7,
            R.id.image_frame_8
    };

    private int[] nameIds = {
            R.id.text_team_1,
            R.id.text_team_2,
            R.id.text_team_3,
            R.id.text_team_4,
            R.id.text_team_5,
            R.id.text_team_6,
            R.id.text_team_7,
            R.id.text_team_8
    };

    public TeamAdapter(Context context, int layoutId, ArrayList<TeamItem> items, TeamAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);

        this.isPortrate = context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(TeamHolder holder, int position) {

        TeamItem item = items.get(position);
        String selTeamId = ((BaseApplication) context.getApplicationContext()).getSPController().getCurrentTeamId();

        holder.title.setText(item.getName());
        holder.title.setTextColor(ContextCompat.getColor(context, item.isTeamLeader() ? R.color.main : R.color.gray5_a100));

        if (holder.countText != null)
            holder.countText.setText(item.getMemberCount() + " " + context.getString(item.getMemberCount() > 1 ? R.string.text_members : R.string.text_member));

        if (holder.icon != null)
            holder.icon.setImageResource(item.isTeamLeader() ? R.drawable.circle_main : R.drawable.circle_gray);
        if (holder.infoButton != null) {
            holder.infoButton.setVisibility(View.VISIBLE);
            holder.infoButton.setImageResource(item.isTeamLeader() ? R.drawable.ic_round_setting_gray_4_24 : R.drawable.ic_round_info_gray_4_24);
        }
//            holder.infoButton.setVisibility(item.isTeamLeader() ? View.VISIBLE : View.GONE);
//            holder.deleteButton.setVisibility(item.isTeamLeader() ? View.GONE : View.VISIBLE);

        if (holder.email != null) {
            holder.email.setVisibility(item.getMemberItems() == null ? View.GONE : View.VISIBLE);

            if (item.getMemberItems() != null)
                for (MemberItem mItem : item.getMemberItems()) {
                    if (mItem.getRole().equalsIgnoreCase("leader")) {
                        holder.email.setText(mItem.getEmail());
                        break;
                    }
                }
        }

        if (holder.line != null)
            holder.line.setVisibility(position == items.size() - 1 ? View.GONE : View.VISIBLE);

        if (holder.root != null && !TextUtils.isEmpty(selectedId)) {
            boolean select = item.getTeamId().equalsIgnoreCase(selectedId);
            holder.root.setBackgroundResource(select ? R.drawable.ripple_effect_main_gray : R.drawable.ripple_effect_main_transparent);
            if (holder.selectedImage != null)
                holder.selectedImage.setVisibility(select ? View.VISIBLE : View.GONE);
            holder.title.setTextColor(ContextCompat.getColor(context, select ? R.color.main : R.color.black_a100));
        }

        if (holder.drawerRoot != null) {
            holder.drawerRoot.setBackgroundResource(item.getTeamId().equalsIgnoreCase(selTeamId) ? R.drawable.ripple_effect_main_alpha : R.drawable.ripple_effect_main_trans_round);
            holder.title.setTextColor(ContextCompat.getColor(context, item.getTeamId().equalsIgnoreCase(selTeamId) ? R.color.main : R.color.black_a80));
        }

        if (holder.dialogRoot != null) {
            boolean select = item.getTeamId().equalsIgnoreCase(selectedId);
            holder.dialogRoot.setBackgroundResource(select ? R.drawable.ripple_effect_main_alpha : R.drawable.ripple_effect_main_transparent);
            holder.radio.setImageResource(select ? R.drawable.ic_radio_button_checked_gray_4_24 : R.drawable.ic_radio_button_unchecked_gray_4_24);
            holder.title.setTextColor(ContextCompat.getColor(context, select ? R.color.main : R.color.black_a100));
        }

        if (holder.layoutDash != null) {
            int max = isPortrate ? MAX_PORT : MAX_LAND;
            if (max > item.getMemberCount())
                max = item.getMemberCount();

            for (int i = MAX_LAND - 1; i >= 0; i--) {
                if (max <= i) {
                    boolean gone = isPortrate && i >= MAX_PORT;
//                    holder.layouts[i].setVisibility(gone ? View.GONE : View.INVISIBLE);
                    holder.layouts[i].setVisibility(View.GONE);
                } else {
                    holder.layouts[i].setVisibility(View.VISIBLE);
                    MemberItem member = item.getMemberItems().get(i);
                    if (member != null) {
                        boolean mine = !TextUtils.isEmpty(email) && email.equalsIgnoreCase(member.getEmail());
                        holder.layouts[i].setBackgroundResource(mine ? R.drawable.circle_main : R.drawable.circle_gray);
                        holder.frames[i].setImageResource(mine ? R.drawable.circle_stroke_main : R.color.transparent);
                        String name = member.getName();
                        if (name.length() > 1)
                            holder.names[i].setText(member.getName().substring(0, 2));
                        else
                            holder.names[i].setText(name);

                        final int index = i;
                        if (!TextUtils.isEmpty(member.getProfileUrl())) {
                            Glide.with(context).load(member.getProfileUrl()).asBitmap().centerCrop().placeholder(R.drawable.ic_tm_avator_color_48).into(new BitmapImageViewTarget(holder.profiles[i]) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    if (resource == null) {
                                        holder.profiles[index].setImageResource(mine ? R.drawable.circle_main : R.drawable.circle_gray);
                                        holder.frames[index].setVisibility(View.GONE);
                                        holder.names[index].setVisibility(View.VISIBLE);
                                    } else {
                                        RoundedBitmapDrawable circularBitmapDrawable =
                                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                                        circularBitmapDrawable.setCircular(true);
                                        holder.profiles[index].setImageDrawable(circularBitmapDrawable);
                                        holder.frames[index].setVisibility(View.VISIBLE);
                                        holder.names[index].setVisibility(View.GONE);
                                    }
                                }
                            });
                        } else {
                            holder.profiles[i].setImageResource(mine ? R.drawable.circle_main : R.drawable.circle_gray);
                            holder.frames[i].setVisibility(View.GONE);
                            holder.names[i].setVisibility(View.VISIBLE);
                        }

                        int limit = isPortrate ? MAX_PORT : MAX_LAND;
                        boolean more = limit < item.getMemberCount() && i == (limit - 1);
                        if (more) {
                            holder.frames[i].setVisibility(View.VISIBLE);
                            holder.frames[i].setImageResource(mine ? R.drawable.circle_stroke_main : R.drawable.circle_gray_dim);
                            holder.moreImage1.setVisibility(isPortrate ? View.VISIBLE : View.GONE);
                            holder.moreImage2.setVisibility(isPortrate ? View.GONE : View.VISIBLE);
                        }
                    }
                }
            }
        }
    }

    @Override
    public TeamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TeamHolder holder = new TeamHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.icon = holder.fv(R.id.image_icon);
        holder.title = holder.fv(R.id.text_title);
        holder.infoButton = holder.fv(R.id.button_info);

        holder.root = holder.fv(R.id.layout_root);
        holder.drawerRoot = holder.fv(R.id.layout_root_drawer);
        holder.email = holder.fv(R.id.text_email);
        holder.selectedImage = holder.fv(R.id.image_select);
        holder.line = holder.fv(R.id.view_line);

        holder.layoutDash = holder.fv(R.id.layout_team_dash);
        holder.layouts = new FrameLayout[MAX_LAND];
        holder.profiles = new ImageView[MAX_LAND];
        holder.frames = new ImageView[MAX_LAND];
        holder.names = new TextView[MAX_LAND];
        for (int i = 0; i < MAX_LAND; i++) {
            holder.layouts[i] = holder.fv(layoutIds[i]);
            holder.profiles[i] = holder.fv(profileIds[i]);
            holder.frames[i] = holder.fv(frameIds[i]);
            holder.names[i] = holder.fv(nameIds[i]);
        }
        holder.moreText1 = holder.fv(R.id.text_more1);
        holder.moreText2 = holder.fv(R.id.text_more2);
        holder.moreImage1 = holder.fv(R.id.image_more1);
        holder.moreImage2 = holder.fv(R.id.image_more2);
        holder.countText = holder.fv(R.id.text_team_member_count);
        holder.dialogRoot = holder.fv(R.id.layout_root_dialog);
        holder.radio = holder.fv(R.id.image_radio);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnInfoButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    ((TeamAdapterListener) listener).onInfoButtonClicked(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        return holder;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSelectedId() {
        return this.selectedId;
    }

    public void setSelectedId(String id) {
        this.selectedId = id;
        notifyDataSetChanged();
    }

    public static interface TeamAdapterListener extends ReOnItemClickListener<TeamItem> {
        public void onInfoButtonClicked(int position, TeamItem item);
    }

    public class TeamHolder extends ReAbstractViewHolder {
        LinearLayout root, drawerRoot, dialogRoot;
        ImageView icon, selectedImage, radio;
        TextView title, email;
        View line;
        ImageButton infoButton;
        FrameLayout layouts[];
        LinearLayout layoutDash;
        ImageView profiles[], frames[];
        TextView names[];
        TextView moreText1, moreText2;
        ImageView moreImage1, moreImage2;
        TextView countText;
        private OnItemViewClickListener infoButtonClickListener;

        public TeamHolder(View itemView) {
            super(itemView);
        }

        public void setOnInfoButtonClickListener(OnItemViewClickListener listener) {
            infoButtonClickListener = listener;
            if (listener != null && infoButton != null) {
                infoButton.setOnClickListener(v -> infoButtonClickListener.onItemViewClick(getPosition(), TeamHolder.this));
            }
        }
    }

}
