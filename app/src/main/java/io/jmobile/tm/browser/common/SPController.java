package io.jmobile.tm.browser.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.util.Set;

public class SPController {
    public static final String LOGIN_TOKEN = "login_token";
    public static final String DEFAULT_LOGIN_TOKEN = "";
    public static final String LOGIN_EMAIL = "login_email";
    public static final String DEFAULT_LOGIN_EMAIL = "";
    public static final String REMEMBER_LOGIN_EMAIL = "remember_login_email";
    public static final boolean DEFAULT_REMEMBER_LOGIN_EMAIL = true;
    public static final String LOGIN_NAME = "login_name";
    public static final String DEFAULT_LOGIN_NAME = "";

    private static final String CURRENT_TEAM_ID = "current_team_id";
    private static final String DEFAULT_CURRENT_TEAM_ID = Common.TMBR_PRIVATE_TEAM_ID;

    private static final String SET_USE_SAVE_PASSWORDS = "set_use_save_passwords";
    private static final boolean DEFAULT_USE_SAVE_PASSWORDS = true;
    private static final String BROWSER_URL = "browser_url";
    private static final String DEFAULT_BROWSER_URL = "";
    private static final String SET_SEARCH_ENGINE = "set_search_engine";
    private static final int DEFAULT_SEARCH_ENGINE = 0;
    private static final String CURRENT_BROWSER_INDEX = "current_browser_index";
    private static final int DEFAULT_CURRENT_BROWSER_INDEX = -1;
    private static final String BROWSER_TAB_COUNT = "browser_tab_count";
    private static final int DEFAULT_BROWSER_TAB_COUNT = 1;

    private static final String DELAY_SHOW_AD_TMFM = "delay_show_ad_tmfm";
    private static final int DEFAULT_DELAY_SHOW_AD_TMFM = -1;

    protected SharedPreferences sp;

    public SPController(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void clearSP() {
        sp.edit().clear().commit();
    }

    public void clearLoginUser(boolean all) {
        setLoginName("");
        setLoginToken("");
        if (all || !isRememberLoginEmail())
            setLoginEmail("");
        setCurrentTeamId(Common.TMBR_PRIVATE_TEAM_ID);
    }

    public String getLoginToken() {
        return sp.getString(LOGIN_TOKEN, DEFAULT_LOGIN_TOKEN);
    }

    public void setLoginToken(String val) {
        put(LOGIN_TOKEN, val);
    }

    public boolean isLogined() {
        return !TextUtils.isEmpty(getLoginToken());
    }

    public String getLoginEmail() {
        return sp.getString(LOGIN_EMAIL, DEFAULT_LOGIN_EMAIL);
    }

    public void setLoginEmail(String val) {
        put(LOGIN_EMAIL, val);
    }

    public boolean isRememberLoginEmail() {
        return sp.getBoolean(REMEMBER_LOGIN_EMAIL, DEFAULT_REMEMBER_LOGIN_EMAIL);
    }

    public void setRememberLoginEmail(boolean val) {
        put(REMEMBER_LOGIN_EMAIL, val);
    }

    public String getLoginName() {
        return sp.getString(LOGIN_NAME, DEFAULT_LOGIN_NAME);
    }

    public void setLoginName(String val) {
        put(LOGIN_NAME, val);
    }

    public String getCurrentTeamId() {
        return sp.getString(CURRENT_TEAM_ID, DEFAULT_CURRENT_TEAM_ID);
    }

    public void setCurrentTeamId(String val) {
        put(CURRENT_TEAM_ID, val);
    }

    public boolean isPrivateMode() {
        String teamId = getCurrentTeamId();
        return TextUtils.isEmpty(teamId) || teamId.equalsIgnoreCase(Common.TMBR_PRIVATE_TEAM_ID);
    }

    public boolean isSavePasswords() {
        return sp.getBoolean(SET_USE_SAVE_PASSWORDS, DEFAULT_USE_SAVE_PASSWORDS);
    }

    public void setSetUseSavePasswords(boolean val) {
        put(SET_USE_SAVE_PASSWORDS, val);
    }

    public String getBrowserUrl() {
        return sp.getString(BROWSER_URL, DEFAULT_BROWSER_URL);
    }

    public void setBrowserUrl(String val) {
        put(BROWSER_URL, val);
    }

    public int getSearchEngine() {
        return sp.getInt(SET_SEARCH_ENGINE, DEFAULT_SEARCH_ENGINE);
    }

    public void setSearchEngine(int val) {
        put(SET_SEARCH_ENGINE, val);
    }

    public int getCurrentBrowserIndex() {
        return sp.getInt(CURRENT_BROWSER_INDEX, DEFAULT_CURRENT_BROWSER_INDEX);
    }

    public void setCurrentBrowserIndex(int val) {
        put(CURRENT_BROWSER_INDEX, val);
    }

    public int getBrowserTabCount() {
        return sp.getInt(BROWSER_TAB_COUNT, DEFAULT_BROWSER_TAB_COUNT);
    }

    public void setBrowserTabCount(int val) {
        put(BROWSER_TAB_COUNT, val);
    }

    public void setDelayShowAdTMFileManager() {
        put(DELAY_SHOW_AD_TMFM, System.currentTimeMillis());
    }

    public boolean isDelayedShowADTMFileManager() {
        long delay = sp.getLong(DELAY_SHOW_AD_TMFM, DEFAULT_DELAY_SHOW_AD_TMFM);
        long curr = System.currentTimeMillis();

        return delay == -1 || Math.abs(curr - delay) > (1000 * 60 * 60 * 24);
//        return delay == -1 || Math.abs(curr - delay) > (1000 * 30);
    }

    protected void put(String key, boolean value) {
        sp.edit().putBoolean(key, value).commit();
    }

    protected void put(String key, int value) {
        sp.edit().putInt(key, value).commit();
    }

    protected void put(String key, float value) {
        sp.edit().putFloat(key, value).commit();
    }

    protected void put(String key, long value) {
        sp.edit().putLong(key, value).commit();
    }

    protected void put(String key, String value) {
        sp.edit().putString(key, value).commit();
    }

    protected void put(String key, Set<String> value) {
        sp.edit().putStringSet(key, value).commit();
    }
}
