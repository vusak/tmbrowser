package io.jmobile.tm.browser.ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.base.BaseDialogFragment;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.network.TMB_network;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.showToast;

public class AccountActivity extends BaseActivity {
    static final int STEP_1 = 1;
    static final int STEP_2 = 2;
    static final int STEP_3 = 3;

    ImageButton backButton;
    TextView titleText;

    TextView step1Text, step2Text, step3Text;
    View line1, line2;
    TextView stepMessageText;

    FrameLayout emailLayout, verifyLayout, passwordLayout, passwordConfirmLayout, nameLayout;
    TextView emailText, verifyText, passwordText, passwordConfirmText, nameText;
    EditText emailEdit, verifyEdit, passwordEdit, passwordConfirmEdit, nameEdit;
    ImageButton emailDelButton, verifyDelButton, passwordDelButton, passwordConfirmDelButton, nameDelButton;
    View emailLine, verifyLine, passwordLine, passwordConfirmLine, nameLine;

    LinearLayout askLyout;
    TextView askText;
    Button loginButton, reSendButton, nextButton;

    Dialog progress;
    int currentStep = STEP_1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_account);

        initView();
    }

    @Override
    public void onBackPressed() {
        if (fdf(MessageDialog.TAG) == null) {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            dlg.setTitle(r.s(R.string.notice));
            dlg.setMessage(r.s(R.string.message_quit));
            dlg.setPositiveLabel(r.s(R.string.quit));
            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                @Override
                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                    Intent intent = new Intent(AccountActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            });
            dlg.setNegativeLabel(r.s(R.string.cancel));
            sdf(dlg);
        }
    }

    private void initView() {
        titleText = fv(R.id.text_title);
        titleText.setVisibility(View.VISIBLE);
        titleText.setText(r.s(R.string.title_register));

        backButton = fv(R.id.button_back);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(view -> onBackPressed());
        step1Text = fv(R.id.text_step_1);
        step2Text = fv(R.id.text_step_2);
        step3Text = fv(R.id.text_step_3);

        line1 = fv(R.id.line1);
        line2 = fv(R.id.line2);

        stepMessageText = fv(R.id.text_step_message);

        emailLayout = fv(R.id.layout_email);
        emailText = fv(R.id.text_email);
        emailLine = fv(R.id.view_email_line);
        emailEdit = fv(R.id.edit_email);
        emailEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                emailText.setTextColor(r.c(AccountActivity.this, hasFocus ? R.color.main : R.color.main_tm));
                emailLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
                emailDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(emailEdit.getText().toString()) ? View.GONE : View.VISIBLE);
            }
        });

        emailEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                emailDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        emailDelButton = fv(R.id.button_email_del);
        emailDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emailEdit != null)
                    emailEdit.setText("");
            }
        });

        verifyLayout = fv(R.id.layout_verify);
        verifyText = fv(R.id.text_verify);
        verifyLine = fv(R.id.view_verify_line);
        verifyEdit = fv(R.id.edit_verify);
        verifyEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                verifyText.setTextColor(r.c(AccountActivity.this, hasFocus ? R.color.main : R.color.main_tm));
                verifyLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
                verifyDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(verifyEdit.getText().toString()) ? View.GONE : View.VISIBLE);
            }
        });

        verifyEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                verifyDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        verifyDelButton = fv(R.id.button_verify_del);
        verifyDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verifyEdit != null)
                    verifyEdit.setText("");
            }
        });

        passwordLayout = fv(R.id.layout_password);
        passwordText = fv(R.id.text_password);
        passwordLine = fv(R.id.view_password_line);
        passwordEdit = fv(R.id.edit_password);
        passwordEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                passwordText.setTextColor(r.c(AccountActivity.this, hasFocus ? R.color.main : R.color.main_tm));
                passwordLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
                passwordDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(passwordEdit.getText().toString()) ? View.GONE : View.VISIBLE);
            }
        });

        passwordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                passwordDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        passwordDelButton = fv(R.id.button_password_del);
        passwordDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordEdit != null)
                    passwordEdit.setText("");
            }
        });

        passwordConfirmLayout = fv(R.id.layout_password_confirm);
        passwordConfirmText = fv(R.id.text_password_confirm);
        passwordConfirmLine = fv(R.id.view_password_confirm_line);
        passwordConfirmEdit = fv(R.id.edit_password_confirm);
        passwordConfirmEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                passwordConfirmText.setTextColor(r.c(AccountActivity.this, hasFocus ? R.color.main : R.color.main_tm));
                passwordConfirmLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
                passwordConfirmDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(passwordConfirmEdit.getText().toString()) ? View.GONE : View.VISIBLE);
            }
        });

        passwordConfirmEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                passwordConfirmDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        passwordConfirmDelButton = fv(R.id.button_password_confirm_del);
        passwordConfirmDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordConfirmEdit != null)
                    passwordConfirmEdit.setText("");
            }
        });

        nameLayout = fv(R.id.layout_name);
        nameText = fv(R.id.text_name);
        nameLine = fv(R.id.view_name_line);
        nameEdit = fv(R.id.edit_name);
        nameEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                nameText.setTextColor(r.c(AccountActivity.this, hasFocus ? R.color.main : R.color.main_tm));
                nameLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
                nameDelButton.setVisibility(!hasFocus || TextUtils.isEmpty(nameEdit.getText().toString()) ? View.GONE : View.VISIBLE);
            }
        });

        nameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                nameDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        nameDelButton = fv(R.id.button_name_del);
        nameDelButton.setOnClickListener(v -> {
            if (nameEdit != null)
                nameEdit.setText("");
        });

        askLyout = fv(R.id.layout_ask);
        askText = fv(R.id.text_ask);
        loginButton = fv(R.id.button_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        reSendButton = fv(R.id.button_resend);
        reSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    TMB_network.resendEmail(emailEdit.getText().toString().trim())
                            .flatMap(tmBrowserPair -> {
                                return Observable.just(tmBrowserPair);
                            })
                            .doOnError(throwable -> {

                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(tmBrowserPair -> {
                                        if (tmBrowserPair.first) {
                                            if (fdf(MessageDialog.TAG) == null) {
                                                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                                dlg.setTitle(r.s(R.string.notice));
                                                dlg.setMessage(r.s(R.string.message_re_send));
                                                dlg.setPositiveLabel(r.s(R.string.ok));
                                                sdf(dlg);
                                            }
                                        } else {
                                            showToast(AccountActivity.this, (String) tmBrowserPair.second, true);
                                        }
                                    },
                                    throwable -> {
                                        throwable.printStackTrace();
                                    });
                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                }
            }

        });

        nextButton = fv(R.id.button_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkEmpty())
                    return;

                Util.hideKeyBoard(AccountActivity.this);
                switch (currentStep) {
                    case STEP_1:
                        doStep1();
                        break;

                    case STEP_2:
                        doStep2();
                        break;

                    case STEP_3:
                        doStep3();
                        break;
                }
            }
        });

        setStepLayout();
    }

    private boolean checkEmpty() {
        switch (currentStep) {
            case STEP_1:
                if (TextUtils.isEmpty(emailEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_empty_email), true);
                    return true;
                }

                if (!Util.emailvalidate(emailEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_error_email_pattern), true);
                    return true;
                }
                break;

            case STEP_2:
                if (TextUtils.isEmpty(verifyEdit.getText().toString())
                        || verifyEdit.getText().toString().length() < 4) {
                    showToast(this, r.s(R.string.message_empty_verify), true);
                    return true;
                }
                break;

            case STEP_3:
                if (TextUtils.isEmpty(passwordEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_empty_password), true);
                    return true;
                }

                if (passwordEdit.getText().toString().length() < 6) {
                    showToast(this, r.s(R.string.hint_password), true);
                    return true;
                }

                if (TextUtils.isEmpty(passwordConfirmEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_empty_confirm_password), true);
                    return true;
                }

                if (!passwordEdit.getText().toString().equalsIgnoreCase(passwordConfirmEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_wrong_confirm_password), true);
                    return true;
                }

                if (TextUtils.isEmpty(nameEdit.getText().toString())) {
                    showToast(this, r.s(R.string.message_empty_name), true);
                    return true;
                }
                break;
        }

        return false;
    }

    private void setStepLayout() {
        emailEdit.setEnabled(currentStep == STEP_1);
        emailEdit.setFocusable(currentStep == STEP_1);
        switch (currentStep) {
            case STEP_1:
                step1Text.setBackgroundResource(R.drawable.text_circle_main);
                step2Text.setBackgroundResource(R.drawable.text_circle_gray);
                step3Text.setBackgroundResource(R.drawable.text_circle_gray);
                step1Text.setTextColor(r.c(this, R.color.white));
                step2Text.setTextColor(r.c(this, R.color.gray4_a100));
                step3Text.setTextColor(r.c(this, R.color.gray4_a100));
                line1.setBackgroundColor(r.c(this, R.color.gray3_a100));
                line2.setBackgroundColor(r.c(this, R.color.gray3_a100));
                stepMessageText.setText(r.s(R.string.message_reg_step_1));
                askText.setText(r.s(R.string.question_already_sign_up));
                emailEdit.requestFocus();
                break;
            case STEP_2:
                step1Text.setBackgroundResource(R.drawable.text_circle_main);
                step2Text.setBackgroundResource(R.drawable.text_circle_main);
                step3Text.setBackgroundResource(R.drawable.text_circle_gray);
                step1Text.setTextColor(r.c(this, R.color.white));
                step2Text.setTextColor(r.c(this, R.color.white));
                step3Text.setTextColor(r.c(this, R.color.gray4_a100));
                line1.setBackgroundColor(r.c(this, R.color.main));
                line2.setBackgroundColor(r.c(this, R.color.gray3_a100));
                stepMessageText.setText(r.s(R.string.message_reg_step_2));
                askText.setText(r.s(R.string.question_verification_code));
                break;
            case STEP_3:
                step1Text.setBackgroundResource(R.drawable.text_circle_main);
                step2Text.setBackgroundResource(R.drawable.text_circle_main);
                step3Text.setBackgroundResource(R.drawable.text_circle_main);
                step1Text.setTextColor(r.c(this, R.color.white));
                step2Text.setTextColor(r.c(this, R.color.white));
                step3Text.setTextColor(r.c(this, R.color.white));
                line1.setBackgroundColor(r.c(this, R.color.main));
                line2.setBackgroundColor(r.c(this, R.color.main));
                stepMessageText.setText(r.s(R.string.message_reg_step_3));
                break;
        }


        verifyLayout.setVisibility(currentStep == STEP_2 ? View.VISIBLE : View.GONE);
        passwordLayout.setVisibility(currentStep == STEP_3 ? View.VISIBLE : View.GONE);
        passwordConfirmLayout.setVisibility(currentStep == STEP_3 ? View.VISIBLE : View.GONE);
        nameLayout.setVisibility(currentStep == STEP_3 ? View.VISIBLE : View.GONE);

        loginButton.setVisibility(currentStep == STEP_1 ? View.VISIBLE : View.GONE);
        reSendButton.setVisibility(currentStep == STEP_2 ? View.VISIBLE : View.GONE);
        askLyout.setVisibility(currentStep == STEP_3 ? View.GONE : View.VISIBLE);
        nextButton.setText(r.s(currentStep == STEP_3 ? R.string.button_finish : R.string.button_next));
    }

    private void doStep1() {
        try {
            showProgress();
            TMB_network.joinAndSendEmailCode(emailEdit.getText().toString().trim(), "")
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    currentStep = STEP_2;
                                    setStepLayout();
                                } else {
                                    showToast(AccountActivity.this, r.s(R.string.message_already_signed_up), true);
                                }

                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void doStep2() {
        try {
            showProgress();
            TMB_network.joinAndSendEmailCode(emailEdit.getText().toString().trim(), verifyEdit.getText().toString().trim())
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    currentStep = STEP_3;
                                    setStepLayout();
                                } else {
                                    showToast(AccountActivity.this, r.s(R.string.message_wrong_verify), true);
                                }

                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void doStep3() {
        try {
            showProgress();
            TMB_network.join(verifyEdit.getText().toString().trim(), emailEdit.getText().toString().trim(),
                    passwordEdit.getText().toString().trim(), passwordConfirmEdit.getText().toString().trim(),
                    nameEdit.getText().toString().trim())
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    if (fdf(MessageDialog.TAG) == null) {
                                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                        dlg.setTitle(r.s(R.string.notice));
                                        dlg.setMessage(r.s(R.string.message_sign_up_completed));
                                        dlg.setPositiveLabel(r.s(R.string.ok));
                                        dlg.setPositiveListener((dialog, tag) -> startLogin());
                                        sdf(dlg);
                                    }
                                } else {
                                    showToast(AccountActivity.this, (String) tmBrowserPair.second, false);
                                }

                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void startLogin() {
        try {
            showProgress();
            TMB_network.tokenLogin(emailEdit.getText().toString().trim(), passwordEdit.getText().toString().trim())
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
                                    String token = "Bearer " + result.get("access_token").getAsString();
                                    sp.setLoginToken(token);
                                    sp.setLoginEmail(emailEdit.getText().toString().trim());
                                    getUserInfo();
                                } else {
                                    showToast(AccountActivity.this, (String) tmBrowserPair.second, true
                                    );
                                    Intent intent = new Intent(AccountActivity.this, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }

                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void getUserInfo() {
        try {
            showProgress();
            TMB_network.getUserInfo()
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                        sp.setLoginToken("");
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
                                    String name = result.get("name").getAsString();
                                    sp.setLoginName(name);
                                    MemberItem item = new MemberItem(result, false);
                                    if (item == null) {
                                        sp.setLoginToken("");
                                    } else {
                                        item.setMaster(true);
                                        db.insertOrUpdateMemberItem(item);
                                        Intent intent = new Intent(AccountActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                } else {
                                    sp.setLoginToken("");
                                    showToast(AccountActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                sp.setLoginToken("");
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.title_register), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }
}
