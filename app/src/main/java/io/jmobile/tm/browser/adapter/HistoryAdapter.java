package io.jmobile.tm.browser.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.common.ImageUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.HistoryItem;

public class HistoryAdapter extends BaseExpandableListAdapter {

    private Context context;
    private Map<String, List<HistoryItem>> map;
    private List<String> groupNameList;
    private LayoutInflater li;

    public HistoryAdapter(Activity context, Map<String, List<HistoryItem>> map, List<String> groupNameList) {

        this.context = context;
        this.map = map;
        this.groupNameList = groupNameList;
        li = context.getLayoutInflater();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        String name = groupNameList.get(groupPosition);
        return map.get(name).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = li.inflate(R.layout.item_history, parent, false);

        long current = System.currentTimeMillis();

        HistoryItem item = (HistoryItem) getChild(groupPosition, childPosition);
        TextView url = (TextView) convertView.findViewById(R.id.text_url);
        url.setText(item.getHistoryUrl());

        TextView title = (TextView) convertView.findViewById(R.id.text_title);
        title.setText(item.getHistoryTitle());

        TextView date = (TextView) convertView.findViewById(R.id.text_date2);
        date.setText(Util.getTimeString(item.getHistoryAt()));

        TextView initial = (TextView) convertView.findViewById(R.id.text_initial);
        ImageView icon = (ImageView) convertView.findViewById(R.id.image_icon);
        if (item.getHistoryIcon() != null) {
            RoundedBitmapDrawable circleBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), ImageUtil.getImage(item.getHistoryIcon()));
            circleBitmapDrawable.setCircular(true);
            icon.setImageDrawable(circleBitmapDrawable);

            icon.setBackgroundResource(R.color.transparent);
            initial.setVisibility(View.GONE);
        } else {
            icon.setImageResource(R.color.transparent);
            icon.setBackgroundResource(R.drawable.circle_main);
            initial.setVisibility(View.VISIBLE);
            initial.setText(item.getHistoryInitial());
        }

        View line = (View) convertView.findViewById(R.id.line_history);
        line.setVisibility(isLastChild ? View.INVISIBLE : View.VISIBLE);
        View finishLine = (View) convertView.findViewById(R.id.line);
        finishLine.setVisibility(isLastChild ? View.VISIBLE : View.GONE);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        String name = groupNameList.get(groupPosition);
        return map.get(name).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupNameList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groupNameList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = li.inflate(R.layout.group_history, parent, false);

        TextView date = (TextView) convertView.findViewById(R.id.text_group);
        String name = (String) getGroup(groupPosition);
        if (Util.isToday(name))
            name = context.getString(R.string.today) + " " + name;
        if (Util.isYesterday(name))
            name = context.getString(R.string.yesterday) + " " + name;
        date.setText(name);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
