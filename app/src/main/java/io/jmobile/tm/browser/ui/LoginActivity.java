package io.jmobile.tm.browser.ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.JsonObject;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.network.TMB_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.showToast;

public class LoginActivity extends BaseActivity {

    TextView emailText, passwordText;
    EditText emailEdit, passwordEdit;
    View emailLine, passwordLine;
    ImageButton emailDelButton, passwordDelButton;
    Button loginButton, signupButton;
    CheckBox rememberCheck;

    Dialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        initView();

        // test
        if (LogUtil.DEBUG_MODE) {
//            emailEdit.setText("cecilia@jmobile.io");
//            passwordEdit.setText("12341234");
//            emailEdit.setText("lsonk7@gmail.com");
//            passwordEdit.setText("12341234");
//            emailEdit.setText("devnamoand@gmail.com");
//            passwordEdit.setText("1234567890");
        }

        if (sp.isRememberLoginEmail())
            emailEdit.setText(sp.getLoginEmail());
    }

    private void initView() {
        emailText = fv(R.id.text_email);
        emailLine = fv(R.id.view_email_line);
        emailEdit = fv(R.id.edit_email);
        emailEdit.setOnFocusChangeListener((v, hasFocus) -> {
            emailText.setTextColor(r.c(LoginActivity.this, hasFocus ? R.color.main : R.color.main_tm));
            emailLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            emailDelButton.setVisibility(TextUtils.isEmpty(emailEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });

        emailEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                emailDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }

        });

        emailDelButton = fv(R.id.button_email_del);
        emailDelButton.setOnClickListener(view -> {
            if (emailEdit != null)
                emailEdit.setText("");
        });

        rememberCheck = fv(R.id.check_remember);
        rememberCheck.setChecked(sp.isRememberLoginEmail());
        rememberCheck.setOnCheckedChangeListener((buttonView, isChecked) -> sp.setRememberLoginEmail(isChecked));

        passwordText = fv(R.id.text_password);
        passwordLine = fv(R.id.view_password_line);
        passwordEdit = fv(R.id.edit_password);
        passwordEdit.setOnFocusChangeListener((v, hasFocus) -> {
            passwordText.setTextColor(r.c(LoginActivity.this, hasFocus ? R.color.main : R.color.main_tm));
            passwordLine.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            passwordDelButton.setVisibility(TextUtils.isEmpty(passwordEdit.getText().toString()) ? View.GONE : View.VISIBLE);
        });

        passwordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                passwordDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });


        passwordDelButton = fv(R.id.button_password_del);
        passwordDelButton.setOnClickListener(view -> {
            if (passwordEdit != null)
                passwordEdit.setText("");
        });

        loginButton = fv(R.id.button_login);
        loginButton.setOnClickListener(view ->
        {
            if (checkEmpty())
                return;

            startLogin();
        });

        signupButton = fv(R.id.button_sign_up);
        signupButton.setOnClickListener(view -> {
            Intent intent = new Intent(LoginActivity.this, AccountActivity.class);
            startActivity(intent);
            finish();
        });

    }

    private boolean checkEmpty() {
        if (TextUtils.isEmpty(emailEdit.getText().toString())) {
            showToast(this, r.s(R.string.message_empty_email), true);
            return true;
        }

        if (!Util.emailvalidate(emailEdit.getText().toString())) {
            showToast(this, r.s(R.string.message_error_email_pattern), true);
            return true;
        }

        if (TextUtils.isEmpty(passwordEdit.getText().toString())) {
            showToast(this, r.s(R.string.message_empty_password), true);
            return true;
        }

        if (passwordEdit.getText().toString().length() < 6) {
            showToast(this, r.s(R.string.hint_password), true);
            return true;
        }
        return false;
    }

    private void startLogin() {
        try {
            showProgress();
            TMB_network.tokenLogin(emailEdit.getText().toString().trim(), passwordEdit.getText().toString().trim())
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
                                    String token = "Bearer " + result.get("access_token").getAsString();
                                    sp.setLoginToken(token);
                                    sp.setLoginEmail(emailEdit.getText().toString().trim());
                                    getUserInfo();
                                } else {
                                    showToast(LoginActivity.this, (String) tmBrowserPair.second, false);
                                }

                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
            dismissProgress();
        }
    }

    private void getUserInfo() {
        try {
            showProgress();
            TMB_network.getUserInfo()
                    .flatMap(tmBrowserPair -> Observable.just(tmBrowserPair))
                    .doOnError(throwable -> {
                        dismissProgress();
                        sp.setLoginToken("");
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
                                    String name = result.get("name").getAsString();
                                    sp.setLoginName(name);
                                    MemberItem item = new MemberItem(result, false);
                                    if (item == null) {
                                        sp.setLoginToken("");
                                    } else {
                                        item.setMaster(true);
                                        db.insertOrUpdateMemberItem(item);
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                } else {
                                    sp.setLoginToken("");
                                    showToast(LoginActivity.this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                sp.setLoginToken("");
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            LogUtil.log(e.getMessage());
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.button_login), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

}
