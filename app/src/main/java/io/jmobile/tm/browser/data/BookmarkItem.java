package io.jmobile.tm.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import io.jmobile.tm.browser.base.BaseComparator;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;

public class BookmarkItem extends TableObject {
    public static final String TABLE_NAME = "bookmarks";

    public static final String ROW_ID = "_id";
    public static final String BOOKMARK_ID = "bookmark_id";
    public static final String BOOKMARK_NAME = "bookmark_name";
    public static final String BOOKMARK_PARENT_ID = "bookmark_parent_id";
    public static final String BOOKMARK_PATH = "bookmark_path";
    public static final String BOOKMARK_URL = "bookmark_url";
    public static final String BOOKMARK_THUMBNAIL_URL = "bookmark_thumbnail_url";
    public static final String BOOKMARK_POSITION = "bookmark_position";
    public static final String BOOKMARK_REG_DATE = "bookmark_reg_date";
    public static final String BOOKMARK_CHARACTER = "bookmark_character";
    public static final String BOOKMARK_FOLDER = "bookmark_folder";
    public static final String BOOKMARK_DEPTH = "bookmark_depth";
    public static final String BOOKMARK_INITIAL = "bookmark_initial";
    public static final String BOOKMARK_QUICK = "bookmark_quick";

    public static final Creator<BookmarkItem> CREATOR = new Creator<BookmarkItem>() {
        @Override
        public BookmarkItem createFromParcel(Parcel source) {
            return new BookmarkItem(source);
        }

        @Override
        public BookmarkItem[] newArray(int size) {
            return new BookmarkItem[size];
        }
    };

    public static BookmarkComparator BOOKMARK_PRIVATE_ASC = new BookmarkPComparator(true);
    public static BookmarkComparator BOOKMARK_PRIVATE_DESC = new BookmarkPComparator(false);
    public static BookmarkComparator BOOKMARK_TEAM_ASC = new BookmarkTComparator(true);
    public static BookmarkComparator BOOKMARK_TEAM_DESC = new BookmarkTComparator(false);

    private long id;
    private String bookmarkId;
    private String name;
    private String parentIdx;
    private String path;
    private String url;
    private String thumbnailUrl;
    private String teamId;
    private String teamName;
    private String userName;
    private String userId;
    private int position;
    private String regDate;
    private String character;
    private boolean folder;
    private int depth;
    private String initial;
    private boolean quick;

    public BookmarkItem() {
        super();
    }

    public BookmarkItem(JSONObject o) {
        this.bookmarkId = Util.optString(o, Common.TAG_IDX);
        this.name = Util.optString(o, Common.TAG_NAME);

        this.teamId = Util.optString(o, Common.TAG_GROUP_IDX);
        this.teamName = Util.optString(o, Common.TAG_GROUP_NAME);

        this.regDate = Util.optString(o, Common.TAG_CREATE_DATE);
        this.userId = Util.optString(o, Common.TAG_CREATOR_ID);
        this.userName = Util.optString(o, Common.TAG_CREATOR_NAME);

        String quick = Util.optString(o, Common.TAG_QUICK_YN);
        if (TextUtils.isEmpty(quick))
            this.quick = false;
        else
            this.quick = quick.equalsIgnoreCase("Y");

        String folder = Util.optString(o, Common.TAG_FOLDER_YN);
        if (TextUtils.isEmpty(folder))
            this.folder = false;
        else
            this.folder = folder.equalsIgnoreCase("Y");
        if (this.folder) {

        } else {
            this.url = Util.optString(o, Common.TAG_TARGET_URL);
            String temp = this.url.replace("www.", "");
            temp = temp.replace("http://", "");
            temp = temp.replace("https://", "");
            this.initial = temp.substring(0, 1);

            this.thumbnailUrl = Util.optString(o, Common.TAG_THUMBNAIL_URL);
            if (thumbnailUrl.contains("null"))
                this.thumbnailUrl = "";
            else {
                if (!thumbnailUrl.contains("http://") && !thumbnailUrl.contains("https://"))
                    thumbnailUrl = "http://" + thumbnailUrl;
            }
        }
        String temp = Util.optString(o, Common.TAG_DEPTH);
        this.depth = TextUtils.isEmpty(temp) ? 0 : Integer.parseInt(temp);
        temp = Util.optString(o, Common.TAG_POSITION);
        this.position = TextUtils.isEmpty(temp) ? 0 : Integer.parseInt(temp);
        temp = Util.optString(o, Common.TAG_PARENT_IDX);
        this.parentIdx = TextUtils.isEmpty(temp) ? "0" : temp;
    }

    public BookmarkItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.bookmarkId = in.readString();
        this.name = in.readString();
        this.url = in.readString();
        this.thumbnailUrl = in.readString();
        this.path = in.readString();
        this.teamId = in.readString();
        this.teamName = in.readString();
        this.userId = in.readString();
        this.userName = in.readString();
        this.position = in.readInt();
        this.regDate = in.readString();
        this.character = in.readString();
        this.folder = in.readInt() == 1;
        this.depth = in.readInt();
        this.parentIdx = in.readString();
        this.initial = in.readString();
        this.quick = in.readInt() == 1;
    }

    public static void createTableBookmarks(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(BOOKMARK_ID + " text not null unique, ")
                .append(BOOKMARK_NAME + " text, ")
                .append(BOOKMARK_PARENT_ID + " text, ")
                .append(BOOKMARK_PATH + " text, ")
                .append(BOOKMARK_URL + " text, ")
                .append(BOOKMARK_THUMBNAIL_URL + " text, ")
                .append(BOOKMARK_POSITION + " integer, ")
                .append(BOOKMARK_REG_DATE + " text, ")
                .append(BOOKMARK_CHARACTER + " text, ")
                .append(BOOKMARK_FOLDER + " integer not null, ")
                .append(BOOKMARK_DEPTH + " integer, ")
                .append(BOOKMARK_QUICK + " integer, ")
                .append(BOOKMARK_INITIAL + " text) ")
                .toString());
    }

    public static BookmarkItem getBookmarkItem(SQLiteDatabase db, String id) {
        List<BookmarkItem> list = getBookmarkItems(db, null, BOOKMARK_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<BookmarkItem> getBookmarkItemsByFolders(SQLiteDatabase db, String parentIdx) {
        List<BookmarkItem> list = getBookmarkItems(db, null, BOOKMARK_PARENT_ID + " = ?", new String[]{parentIdx}, null, null, null, null);

        return list;
    }

    public static List<BookmarkItem> getQuickmarkItems(SQLiteDatabase db) {
        List<BookmarkItem> list = getBookmarkItems(db, null, BOOKMARK_QUICK + " = ?", new String[]{"1"}, null, null, null, null);

        return list;
    }

    public static List<BookmarkItem> getBookmarkItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<BookmarkItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                BookmarkItem item = new BookmarkItem()
                        .setRowId(c)
                        .setBookmarkId(c)
                        .setName(c)
                        .setParentIdx(c)
                        .setPath(c)
                        .setUrl(c)
                        .setThumbnailUrl(c)
                        .setPosition(c)
                        .setRegDate(c)
                        .setCharacter(c)
                        .setFolder(c)
                        .setDepth(c)
                        .setQuick(c)
                        .setInitial(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateBookmarkItem(SQLiteDatabase db, BookmarkItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBookmarkId(v)
                    .putName(v)
                    .putParentIdx(v)
                    .putPath(v)
                    .putUrl(v)
                    .putThumbnailUrl(v)
                    .putPosition(v)
                    .putRegDate(v)
                    .putCharacter(v)
                    .putFolder(v)
                    .putDepth(v)
                    .putQuick(v)
                    .putInitial(v);

            int rowAffected = db.update(TABLE_NAME, v, BOOKMARK_ID + " =? ", new String[]{"" + item.getBookmarkId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getName());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteBookmarkItem(SQLiteDatabase db, BookmarkItem item) {
        try {
            db.delete(TABLE_NAME, BOOKMARK_ID + " = ? ", new String[]{"" + item.getBookmarkId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllBookmarkItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(bookmarkId);
        out.writeString(name);
        out.writeString(url);
        out.writeString(thumbnailUrl);
        out.writeString(path);
        out.writeString(teamId);
        out.writeString(teamName);
        out.writeString(userId);
        out.writeString(userName);
        out.writeInt(position);
        out.writeString(regDate);
        out.writeString(character);
        out.writeInt(folder ? 1 : 0);
        out.writeInt(depth);
        out.writeString(parentIdx);
        out.writeString(initial);
        out.writeInt(quick ? 1 : 0);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BookmarkItem && bookmarkId == ((BookmarkItem) obj).bookmarkId;
    }

    /**
     * Row ID
     **/
    public long getRowId() {
        return this.id;
    }

    public BookmarkItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public BookmarkItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public BookmarkItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    /**
     * Bookmark Id
     **/
    public String getBookmarkId() {
        return this.bookmarkId;
    }

    public BookmarkItem setBookmarkId(String id) {
        this.bookmarkId = id;

        return this;
    }

    public BookmarkItem setBookmarkId(Cursor c) {
        this.bookmarkId = s(c, BOOKMARK_ID);

        return this;
    }

    public BookmarkItem putBookmarkId(ContentValues v) {
        v.put(BOOKMARK_ID, bookmarkId);

        return this;
    }

    /**
     * Bookmark name
     **/
    public String getName() {
        return this.name;
    }

    public BookmarkItem setName(String name) {
        this.name = name;

        return this;
    }

    public BookmarkItem setName(Cursor c) {
        this.name = s(c, BOOKMARK_NAME);

        return this;
    }

    public BookmarkItem putName(ContentValues v) {
        v.put(BOOKMARK_NAME, name);

        return this;
    }

    /**
     * Bookmark Path
     **/
    public String getPath() {
        return this.path;
    }

    public BookmarkItem setPath(String path) {
        this.path = path;

        return this;
    }

    public BookmarkItem setPath(Cursor c) {
        this.path = s(c, BOOKMARK_PATH);

        return this;
    }

    public BookmarkItem putPath(ContentValues v) {
        v.put(BOOKMARK_PATH, path);

        return this;
    }

    /**
     * Bookmark URL
     **/
    public String getUrl() {
        return this.url;
    }

    public BookmarkItem setUrl(String url) {
        this.url = url;

        return this;
    }

    public BookmarkItem setUrl(Cursor c) {
        this.url = s(c, BOOKMARK_URL);

        return this;
    }

    public BookmarkItem putUrl(ContentValues v) {
        v.put(BOOKMARK_URL, url);

        return this;
    }

    /**
     * Bookmark thumbnail(icon) url
     **/
    public String getThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public BookmarkItem setThumbnailUrl(String url) {
        this.thumbnailUrl = url;

        return this;
    }

    public BookmarkItem setThumbnailUrl(Cursor c) {
        this.thumbnailUrl = s(c, BOOKMARK_THUMBNAIL_URL);

        return this;
    }

    public BookmarkItem putThumbnailUrl(ContentValues v) {
        v.put(BOOKMARK_THUMBNAIL_URL, this.thumbnailUrl);

        return this;
    }

    /**
     * Bookmark Position
     **/
    public int getPosition() {
        return this.position;
    }

    public BookmarkItem setPosition(int position) {
        this.position = position;

        return this;
    }

    public BookmarkItem setPosition(Cursor c) {
        this.position = i(c, BOOKMARK_POSITION);

        return this;
    }

    public BookmarkItem putPosition(ContentValues v) {
        v.put(BOOKMARK_POSITION, this.position);

        return this;
    }

    /**
     * Bookmark TeamId (for team)
     **/
    public String getTeamId() {
        return this.teamId;
    }

    public BookmarkItem setTeamId(String teamId) {
        this.teamId = teamId;

        return this;
    }

    /**
     * Bookmark Team name (for team)
     **/
    public String getTeamName() {
        return this.teamName;
    }

    public BookmarkItem setTeamName(String name) {
        this.teamName = name;

        return this;
    }

    /**
     * Bookmark User id (for team)
     **/
    public String getUserId() {
        return this.userId;
    }

    public BookmarkItem setUserId(String userId) {
        this.userId = userId;

        return this;
    }

    /**
     * Bookmark User name (for team)
     **/
    public String getUserName() {
        return this.userName;
    }

    public BookmarkItem setUserName(String name) {
        this.userName = name;

        return this;
    }

    /**
     * Bookmark regdate
     **/
    public String getRegDate() {
        return this.regDate;
    }

    public BookmarkItem setRegDate(String regDate) {
        this.regDate = regDate;

        return this;
    }

    public BookmarkItem setRegDate(Cursor c) {
        this.regDate = s(c, BOOKMARK_REG_DATE);

        return this;
    }

    public BookmarkItem putRegDate(ContentValues v) {
        v.put(BOOKMARK_REG_DATE, this.regDate);

        return this;
    }

    /**
     * Bookmark Character
     **/
    public String getCharacter() {
        return this.character;
    }

    public BookmarkItem setCharacter(String character) {
        this.character = character;

        return this;
    }

    public BookmarkItem setCharacter(Cursor c) {
        this.character = s(c, BOOKMARK_CHARACTER);

        return this;
    }

    public BookmarkItem putCharacter(ContentValues v) {
        v.put(BOOKMARK_CHARACTER, this.character);

        return this;
    }

    /**
     * Bookmark Folder
     **/
    public boolean isFolder() {
        return this.folder;
    }

    public BookmarkItem setFolder(boolean folder) {
        this.folder = folder;

        return this;
    }

    public BookmarkItem setFolder(Cursor c) {
        this.folder = i(c, BOOKMARK_FOLDER) == 1;

        return this;
    }

    public BookmarkItem putFolder(ContentValues v) {
        v.put(BOOKMARK_FOLDER, this.folder ? 1 : 0);

        return this;
    }

    /**
     * Bookmark Folder Depth
     **/
    public int getDepth() {
        return this.depth;
    }

    public BookmarkItem setDepth(int depth) {
        this.depth = depth;

        return this;
    }

    public BookmarkItem setDepth(Cursor c) {
        this.depth = i(c, BOOKMARK_DEPTH);

        return this;
    }

    public BookmarkItem putDepth(ContentValues v) {
        v.put(BOOKMARK_DEPTH, this.depth);

        return this;
    }

    /**
     * Bookmark Parent id
     **/
    public String getParentIdx() {
        return this.parentIdx;
    }

    public BookmarkItem setParentIdx(String idx) {
        this.parentIdx = idx;

        return this;
    }

    public BookmarkItem setParentIdx(Cursor c) {
        this.parentIdx = s(c, BOOKMARK_PARENT_ID);

        return this;
    }

    public BookmarkItem putParentIdx(ContentValues v) {
        v.put(BOOKMARK_PARENT_ID, this.parentIdx);

        return this;
    }

    /**
     * Bookmark initial
     **/
    public String getInitial() {
        if (TextUtils.isEmpty(initial)) {
            return this.name.substring(0, 1);
        } else
            return this.initial;
    }

    public BookmarkItem setInitial(String initial) {
        this.initial = initial;

        return this;
    }

    public BookmarkItem setInitial(Cursor c) {
        this.initial = s(c, BOOKMARK_INITIAL);

        return this;
    }

    public BookmarkItem putInitial(ContentValues v) {
        v.put(BOOKMARK_INITIAL, this.initial);

        return this;
    }

    /**
     * Bookmark showing quick dial
     **/
    public boolean isQuick() {
        return this.quick;
    }

    public BookmarkItem setQuick(boolean quick) {
        this.quick = quick;

        return this;
    }

    public BookmarkItem setQuick(Cursor c) {
        this.quick = i(c, BOOKMARK_QUICK) == 1;

        return this;
    }

    public BookmarkItem putQuick(ContentValues v) {
        v.put(BOOKMARK_QUICK, this.quick ? 1 : 0);

        return this;
    }

    private static class BookmarkComparator extends BaseComparator implements Comparator<BookmarkItem> {
        public BookmarkComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(BookmarkItem lhs, BookmarkItem rhs) {
            if (lhs == null && rhs != null)
                return 1;
            else if (rhs == null && lhs != null)
                return -1;
            else
                return 0;
        }

        protected final int comparePId(BookmarkItem lhs, BookmarkItem rhs) {
            return compareLong(lhs.getRowId(), rhs.getRowId(), false);
        }

        protected final int compareTId(BookmarkItem lhs, BookmarkItem rhs) {
            return compareInt(Integer.parseInt(lhs.getBookmarkId()), Integer.parseInt(rhs.getBookmarkId()), false);
        }
    }

    private static class BookmarkPComparator extends BookmarkComparator {
        public BookmarkPComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(BookmarkItem lhs, BookmarkItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            result = compareInt(lhs.isFolder() ? 1 : 0, rhs.isFolder() ? 1 : 0, false);
            if (result != 0)
                return result;
            else {
                return comparePId(lhs, rhs);
            }
        }
    }

    private static class BookmarkTComparator extends BookmarkComparator {
        public BookmarkTComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(BookmarkItem lhs, BookmarkItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            result = compareInt(lhs.isFolder() ? 1 : 0, rhs.isFolder() ? 1 : 0, false);
            if (result != 0)
                return result;
            else {
                return compareTId(lhs, rhs);
            }
        }
    }
}
