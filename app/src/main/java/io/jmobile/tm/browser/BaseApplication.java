package io.jmobile.tm.browser;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;

import java.util.ArrayList;

import io.jmobile.tm.browser.common.DBController;
import io.jmobile.tm.browser.common.SPController;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.BrowserItem;
import io.jmobile.tm.browser.data.MemberItem;
import io.jmobile.tm.browser.data.TeamItem;

public class BaseApplication extends Application {
    private static BaseApplication sInstance;
    private SPController sp;
    private DBController db;
    private ResourceWrapper r;
    private String version;
    private String os = "Android " + Build.VERSION.RELEASE;
    private String deviceName;
    private String locale;
    private String modelName = Build.MODEL;
    private String deviceIdentifier;
    private String contry;
    private String simContry;
    private TelephonyManager tManager;
    private ArrayList<TeamItem> myTeams;
    private MemberItem myMember;
    private TeamItem myTeam;

    public BaseApplication() {
        sInstance = this;
    }

    public static synchronized BaseApplication getInstance() {
        return sInstance;
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate() {
        super.onCreate();

        sp = new SPController(getApplicationContext());
        db = new DBController(getApplicationContext());
        r = new ResourceWrapper(getResources());

        version = Util.getVersion(getApplicationContext());
        deviceName = Build.MODEL;
        if (Build.VERSION.SDK_INT >= 24) {
            locale = getResources().getConfiguration().getLocales().get(0).getLanguage();
            contry = getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            locale = getResources().getConfiguration().locale.getLanguage();
            contry = getResources().getConfiguration().locale.getCountry();
        }
        tManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        simContry = tManager.getSimCountryIso();

        deviceIdentifier = Util.sha256(Util.getMACAddress());
    }

    public SPController getSPController() {
        return this.sp;
    }

    public DBController getDBController() {
        return this.db;
    }

    public ResourceWrapper getResourceWrapper() {
        return r;
    }

    public String getOS() {
        return this.os;
    }

    public String getOSName() {
        return "Android";
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getVersion() {
        return this.version;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public String getLocale() {
        return this.locale;
    }

    public void setConfiguration(String locale, String contry) {
        this.locale = locale;
        this.contry = contry;
    }

    public String getContry() {
        return this.contry;
    }

    public String getSimContry() {
        return this.simContry;
    }

    public void setSimContry(String val) {
        this.simContry = val;
    }

    public String getModelName() {
        return this.modelName;
    }

    public String getDeviceIdentifier() {
        return this.deviceIdentifier;
    }

    public ArrayList<TeamItem> getMyTeams() {
        return myTeams;
    }

    public void setMyTeams(ArrayList<TeamItem> items) {
        if (myTeams == null)
            myTeams = new ArrayList<>();
        else
            myTeams.clear();
        myTeams.addAll(items);

        ArrayList<BrowserItem> bItems = new ArrayList<>();
        for (int i = 0; i < myTeams.size(); i++) {
            bItems.clear();
            bItems.addAll(db.getBrowserItemsByTeam(myTeams.get(i).getTeamId()));
            String idx = "";
            if (bItems.size() > 0) {
                long at = -1;
                for (BrowserItem item : bItems) {
                    if (at < item.getBrowserAt()) {
                        at = item.getBrowserAt();
                        idx = item.getBrowserId();
                    }
                }
            }
            myTeams.get(i).setBrowserIdx(idx);
        }
    }

    public TeamItem getMyTeam(String id) {
        if (myTeams != null && myTeams.size() > 0) {
            for (TeamItem item : myTeams) {
                if (item.getTeamId().equalsIgnoreCase(id)) {
                    return item;
                }
            }
        }

        return null;
    }

//    public MemberItem getMember(String teamId, String memberId) {
//        TeamItem teamItem = null;
//        if (myTeams != null && myTeams.size() > 0) {
//            for (TeamItem item : myTeams) {
//                if (item.getTeamId().equalsIgnoreCase(teamId)) {
//                    teamItem = item;
//                    break;
//                }
//            }
//        }
//
//        if (teamItem != null && teamItem.getMemberItems() != null && teamItem.getMemberItems().size() > 0) {
//            for (MemberItem item : teamItem.getMemberItems()) {
//                if (item.getMemberId().equalsIgnoreCase(memberId)) {
//                    return item;
//                }
//            }
//        }
//
//        return null;
//    }

    public void setMyTeamBrowserIdx(String teamId, String browserIdx) {
        for (int i = 0; i < myTeams.size(); i++) {
            if (myTeams.get(i).getTeamId().equalsIgnoreCase(teamId)) {
                myTeams.get(i).setBrowserIdx(browserIdx);
            }
        }
    }

//    public MemberItem getMyMember() {
//        return myMember;
//    }
//
//    public void setMyMember(MemberItem item) {
//        this.myMember = item;
//    }
//
//    public TeamItem getMyTeam() {
//        return this.myTeam;
//    }
//
//    public void setMyTeam(TeamItem item) {
//        this.myTeam = item;
//    }

    public static class ResourceWrapper {
        private Resources r;

        public ResourceWrapper(Resources r) {
            this.r = r;
        }

        public int px(int id) {
            return r.getDimensionPixelSize(id);
        }

        public String s(int id) {
            return r.getString(id);
        }

        public String[] sa(int id) {
            return r.getStringArray(id);
        }

        public int c(Context context, int id) {
            if (context == null)
                return 0;
            else
                return ContextCompat.getColor(context, id);
        }

        public Drawable d(Context context, int id) {
            if (context == null)
                return null;
            else
                return ContextCompat.getDrawable(context, id);
        }

        public boolean b(int id) {
            return r.getBoolean(id);
        }

        public int i(int id) {
            return r.getInteger(id);
        }

        public long l(int id) {
            return Long.parseLong(s(id));
        }
    }
}
