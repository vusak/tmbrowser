package io.jmobile.tm.browser.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.ReAdapter;
import io.jmobile.tm.browser.adapter.UrlAdapter;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.data.HistoryItem;

public class UrlFragment extends BaseFragment {

    private static UrlFragment fragment;

    ImageButton urlBackButton, urlSearchButton;
    EditText urlEdit;

    RecyclerView urlLv, recentLv;
    ArrayList<HistoryItem> historyList, urlList, recentList;
    LinearLayoutManager urlManager, recentManager;
    UrlAdapter urlAdapter, recentAdapter;
    LinearLayout emptyLayout, recentLayout, urlListLayout;

    UrlFragmentListener listener;
    private Handler urlSearchHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                if (urlList == null)
                    urlList = new ArrayList<>();
                urlList.clear();
                String keyword = urlEdit.getText().toString();
                for (HistoryItem item : historyList) {
                    if (item.getHistoryUrl().contains(keyword)
                            || item.getHistoryTitle().contains(keyword))
                        urlList.add(item);
                }

                urlAdapter.setKeyword(keyword);
            }
        }
    };

    public static UrlFragment newInstance() {
        if (fragment == null)
            fragment = new UrlFragment();
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_url;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void handleApiMessage(Message m) {
    }

    private void initView() {
        urlBackButton = fv(R.id.button_url_back);
        urlBackButton.setOnClickListener(v -> {
            if (listener != null)
                listener.visibleUrlLayout(false, "");
        });

        urlEdit = fv(R.id.edit_url);
        urlEdit.setOnFocusChangeListener((v, hasFocus) -> urlEdit.selectAll());
        urlEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                urlSearchHandler.removeCallbacksAndMessages(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                urlListLayout.setVisibility(empty ? View.GONE : View.VISIBLE);
                if (!empty)
                    urlSearchHandler.sendEmptyMessage(0);
            }
        });
        urlEdit.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Util.hideKeyBoard(getActivity());

                String url = urlEdit.getText().toString().trim();
                String SCHEME = "http://";
                if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
                    sp.setBrowserUrl(getSearchEnging() + url);
                } else if (url.matches("http.*")) { // http로 시작
                    sp.setBrowserUrl(url);
                } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
                        || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
                        || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
                        || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
                        || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
                    sp.setBrowserUrl(SCHEME + url);
                } else {
                    sp.setBrowserUrl(getSearchEnging() + url);
                }
                Util.hideKeyBoard(urlEdit);
                if (listener != null)
                    listener.goToBrowser(sp.getBrowserUrl());
            }
            return false;
        });

        urlSearchButton = fv(R.id.button_url_search);
        urlSearchButton.setOnClickListener(v -> {
            if (!urlEdit.getText().toString().isEmpty()) {
                Util.hideKeyBoard(getActivity());

                String url = urlEdit.getText().toString().trim();
                String SCHEME = "http://";
                if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
                    sp.setBrowserUrl(getSearchEnging() + url);
                } else if (url.matches("http.*")) { // http로 시작
                    sp.setBrowserUrl(url);
                } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
                        || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
                        || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
                        || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
                        || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
                    sp.setBrowserUrl(SCHEME + url);
                } else {
                    sp.setBrowserUrl(getSearchEnging() + url);
                }
                Util.hideKeyBoard(urlEdit);
                if (listener != null)
                    listener.goToBrowser(sp.getBrowserUrl());
            }
        });

        emptyLayout = fv(R.id.layout_url_empty);
        recentLayout = fv(R.id.layout_url_history);
        urlListLayout = fv(R.id.layout_url_list);
        urlListLayout.setVisibility(View.GONE);

        recentLv = fv(R.id.lv_url_history);
        recentManager = new GridLayoutManager(getActivity(), 1);
        recentLv.setLayoutManager(recentManager);
        recentList = new ArrayList<>();
        recentAdapter = new UrlAdapter(getActivity(), R.layout.item_url, recentList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                Util.hideKeyBoard(urlEdit);
                if (listener != null)
                    listener.goToBrowser(((HistoryItem) item).getHistoryUrl());
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        recentLv.setAdapter(recentAdapter);

        urlLv = fv(R.id.lv_url);
        urlManager = new GridLayoutManager(getActivity(), 1);
        urlLv.setLayoutManager(urlManager);
        urlList = new ArrayList<>();
        urlAdapter = new UrlAdapter(getActivity(), R.layout.item_url, urlList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                Util.hideKeyBoard(urlEdit);
                if (listener != null)
                    listener.goToBrowser(((HistoryItem) item).getHistoryUrl());
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        urlLv.setAdapter(urlAdapter);
        refreshHistoryList();
    }

    private void refreshHistoryList() {
        if (historyList == null)
            historyList = new ArrayList<>();

        historyList.clear();
        historyList.addAll(db.getHistoryItems());

        recentList.clear();
        recentList.addAll(db.getHistoryItemsByTeam(sp.getCurrentTeamId()));
        recentAdapter.notifyDataSetChanged();
        recentLayout.setVisibility(recentList.size() > 0 ? View.VISIBLE : View.GONE);
        emptyLayout.setVisibility(recentList.size() > 0 ? View.GONE : View.VISIBLE);
    }

    private String getSearchEnging() {
        String engine = "http://www.google.com/m?q=";
        switch (sp.getSearchEngine()) {
            case 0: // Google
                engine = "http://www.google.com/m?q=";
                break;

            case 1: // Naver
                engine = "http://m.search.naver.com/search.naver?query=";
                break;

            case 2: // bing
                engine = "http://www.bing.com/search?q=";
                break;
        }

        return engine;
    }

    public void visible(boolean show, String url) {
        if (show) {
            if (url != null && !TextUtils.isEmpty(url)) {
                urlEdit.setText(url);
                urlEdit.setSelection(0, url.length());
            }
            if (urlEdit != null)
                Util.showKeyBoard(urlEdit);
        } else
            Util.hideKeyBoard(getActivity());
    }

    public String getCurrentUrl() {
        String url = "";
        if (urlEdit != null)
            url = urlEdit.getText().toString();

        return url;
    }

    public void setListener(UrlFragmentListener listener) {
        this.listener = listener;
    }

    public static interface UrlFragmentListener {
        public void goToBrowser(String url);

        public void visibleUrlLayout(boolean show, String url);
    }

}
