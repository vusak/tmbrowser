package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.data.AppItem;

public class AppAdapter extends ReSelectableAdapter<AppItem, AppAdapter.AppHolder> {

    private Context context;
    private ArrayList<AppItem> items = new ArrayList<>();
    private int selectedIndex = -1;

    public AppAdapter(Context context, int layoutId, ArrayList<AppItem> items, AppAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(AppHolder holder, int position) {

        AppItem item = items.get(position);

        holder.name.setText(item.getName());
        if (TextUtils.isEmpty(item.getName())) {
            holder.name.setText(item.getPackagename());
        }
        holder.initial.setText(item.getInitial());
        holder.initial.setVisibility(View.GONE);
        holder.thumbnail.setBackgroundResource(R.color.transparent);
        try {
            Drawable drawable = context.getPackageManager().getApplicationIcon(item.getPackagename());
            holder.thumbnail.setImageDrawable(drawable);
        } catch (PackageManager.NameNotFoundException e) {
            LogUtil.log(e.getMessage());
            holder.thumbnail.setBackgroundResource(R.color.main_alpha);
            holder.initial.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(item.getInitial())) {
                holder.initial.setText(holder.name.getText().subSequence(0, 1));
            }
        }

        if (holder.root != null)
            holder.root.setBackgroundResource(position == selectedIndex ? R.drawable.background_team : R.drawable.background_team_gray);
//        holder.thumbnail.setVisibility(folder ? View.GONE : View.VISIBLE);
//        holder.initial.setVisibility(folder ? View.GONE : View.VISIBLE);


    }

    @Override
    public AppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AppHolder holder = new AppHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.thumbnail = holder.fv(R.id.image_thumbnail);
        holder.name = holder.fv(R.id.text_name);
        holder.initial = holder.fv(R.id.text_initial);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

//        holder.setOnInfoButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
//            @Override
//            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
//                if (listener != null && items.size() > position && position >= 0)
//                    ((AppAdapterListener) listener).onInfoButtonClicked(position, items.get(position));
//            }
//
//            @Override
//            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
//                return false;
//            }
//        });

//        holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
//            @Override
//            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
//                if (listener != null && items.size() > position && position >= 0)
//                    ((AppAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
//            }
//
//            @Override
//            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
//                return false;
//            }
//        });
        return holder;
    }

    public void setSelectedIndex(int index) {
        selectedIndex = index;

        notifyDataSetChanged();
    }

    public static interface AppAdapterListener extends ReOnItemClickListener<AppItem> {
//        public void onInfoButtonClicked(int position, TeamItem item);
//
//        public void onDeleteButtonClick(int positon, TeamItem item);
    }

    public class AppHolder extends ReAbstractViewHolder {
        LinearLayout root;
        ImageView thumbnail;
        TextView name, initial;
        View line;
//        private OnItemViewClickListener settingButtonClickListener, deleteButtonClickListener;

        public AppHolder(View itemView) {
            super(itemView);
        }

//        public void setOnInfoButtonClickListener(OnItemViewClickListener listener) {
//            settingButtonClickListener = listener;
//            if (listener != null && infoButton != null) {
//                infoButton.setOnClickListener(v -> settingButtonClickListener.onItemViewClick(getPosition(), AppHolder.this));
//            }
//        }
//
//        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
//            deleteButtonClickListener = listener;
//            if (listener != null && deleteButton != null) {
//                deleteButton.setOnClickListener(v ->deleteButtonClickListener.onItemViewClick(getAdapterPosition(), AppHolder.this));
//            }
//        }
    }
}
