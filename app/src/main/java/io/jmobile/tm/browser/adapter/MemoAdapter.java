package io.jmobile.tm.browser.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.data.MemoItem;

public class MemoAdapter extends ReSelectableAdapter<MemoItem, MemoAdapter.MemoHolder> {

    private Context context;
    private ArrayList<MemoItem> items = new ArrayList<>();
    private String loginId;
    private boolean deleteMode = false;

    public MemoAdapter(Context context, int layoutId, String loginId, ArrayList<MemoItem> items, MemoAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.loginId = loginId;
        this.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(MemoHolder holder, int position) {

        MemoItem item = items.get(position);

        boolean mine = loginId.equalsIgnoreCase(item.getUserId());

        holder.header.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
        holder.footer.setVisibility(position == items.size() - 1 ? View.VISIBLE : View.GONE);

        if (holder.deleteCheck != null) {
            holder.deleteCheck.setChecked(item.isSelected());
            holder.deleteCheck.setVisibility(deleteMode ? View.VISIBLE : View.GONE);
        }

        holder.name.setText(item.getUserName());
        holder.name.setTextColor(ContextCompat.getColor(context, mine ? R.color.main : R.color.gray5_a100));
        holder.name.setGravity(mine ? Gravity.RIGHT : Gravity.LEFT);
        holder.memo.setText(item.getMemo());
        holder.memo.setGravity(mine ? Gravity.RIGHT : Gravity.LEFT);
        holder.date2.setText(mine ? "" : item.getRegDate());
        holder.date2.setVisibility(mine ? View.GONE : View.VISIBLE);
        holder.date1.setText(mine ? item.getRegDate() : "");
        holder.date1.setVisibility(mine ? View.VISIBLE : View.GONE);
        if (holder.team1 != null) {
            holder.team1.setText(mine ? "" : item.getTeamName());
            holder.team1.setVisibility(mine || TextUtils.isEmpty(item.getTeamName()) ? View.GONE : View.VISIBLE);
        }
        if (holder.team2 != null) {
            holder.team2.setText(mine ? item.getTeamName() : "");
            holder.team2.setVisibility(mine && !TextUtils.isEmpty(item.getTeamName()) ? View.VISIBLE : View.GONE);
        }
        holder.profile.setVisibility(mine ? View.GONE : View.VISIBLE);
        holder.myProfile.setVisibility(mine ? View.VISIBLE : View.GONE);
        holder.memoBackground.setBackgroundResource(mine ? R.drawable.background_memo_mine : R.drawable.background_memo);

        if (!TextUtils.isEmpty(item.getUserProfile())) {
            Glide.with(context).load(item.getUserProfile()).asBitmap().centerCrop().placeholder(R.drawable.ic_tm_avator_color_48).into(new BitmapImageViewTarget(mine ? holder.myProfile : holder.profile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    if (mine)
                        holder.myProfile.setImageDrawable(circularBitmapDrawable);
                    else
                        holder.profile.setImageDrawable(circularBitmapDrawable);
                }
            });
        } else {
            holder.myProfile.setImageResource(R.drawable.ic_tm_avator_color_48);
            holder.profile.setImageResource(R.drawable.ic_tm_avator_color_48);
        }
//        holder.initial.setText(item.getCharacter());
//        boolean folder = item.isFolder();
//        holder.thumbnail.setVisibility(folder ? View.GONE : View.VISIBLE);
    }

    @Override
    public MemoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MemoHolder holder = new MemoHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.memoBackground = holder.fv(R.id.layout_memo_bg);
        holder.profile = holder.fv(R.id.image_profile);
        holder.myProfile = holder.fv(R.id.image_my_profile);
        holder.name = holder.fv(R.id.text_name);
        holder.date2 = holder.fv(R.id.text_date2);
        holder.date1 = holder.fv(R.id.text_date1);
        holder.team1 = holder.fv(R.id.text_team1);
        holder.team2 = holder.fv(R.id.text_team2);
        holder.memo = holder.fv(R.id.text_memo);
        holder.header = holder.fv(R.id.view_header);
        holder.footer = holder.fv(R.id.view_footer);
        holder.deleteCheck = holder.fv(R.id.check_delete);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnMemoClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    ((MemoAdapterListener) listener).onMemoClicked(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        return holder;
    }

    public boolean isDeleteMode() {
        return this.deleteMode;
    }

    public void setDeleteMode(boolean mode) {
        this.deleteMode = mode;
        clearSelection();
        notifyDataSetChanged();
    }

    public static interface MemoAdapterListener extends ReOnItemClickListener<MemoItem> {
        public void onMemoClicked(int position, MemoItem item);
//        public void onInfoButtonClicked(int position, TeamItem item);
//
//        public void onDeleteButtonClick(int positon, TeamItem item);
    }

    public class MemoHolder extends ReAbstractViewHolder {
        ImageView profile, myProfile;
        LinearLayout memoBackground;
        TextView name, date1, date2, memo, team1, team2;
        View header, footer;
        CheckBox deleteCheck;
        private OnItemViewClickListener memoClickListener;
//        private OnItemViewClickListener settingButtonClickListener, deleteButtonClickListener;

        public MemoHolder(View itemView) {
            super(itemView);
        }

        public void setOnMemoClickListener(OnItemViewClickListener listener) {
            memoClickListener = listener;
            if (listener != null && memo != null) {
                memo.setOnClickListener(v -> memoClickListener.onItemViewClick(getPosition(), MemoHolder.this));
            }
        }
//
//        public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
//            deleteButtonClickListener = listener;
//            if (listener != null && deleteButton != null) {
//                deleteButton.setOnClickListener(v ->deleteButtonClickListener.onItemViewClick(getAdapterPosition(), AppHolder.this));
//            }
//        }
    }
}
