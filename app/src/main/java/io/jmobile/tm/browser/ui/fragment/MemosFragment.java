package io.jmobile.tm.browser.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.adapter.MemoAdapter;
import io.jmobile.tm.browser.base.BaseFragment;
import io.jmobile.tm.browser.common.Common;
import io.jmobile.tm.browser.data.MemoItem;
import io.jmobile.tm.browser.network.Api;

public class MemosFragment extends BaseFragment {

    LinearLayout emptyLayout;
    RecyclerView lv;
    LinearLayoutManager manager;
    MemoAdapter adapter;
    ArrayList<MemoItem> list;

    Api api;
    MemoFragmentListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_memos;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();
        api.getMemoList();
        if (listener != null)
            listener.onShowProgress();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser)
            refreshList();
    }

    @Override
    public void handleApiMessage(Message m) {
        super.handleApiMessage(m);
        if (listener != null)
            listener.onDismissProgress();
        if (m.what == Common.API_CODE_MEMO_SELECT || m.what == Common.API_CODE_MEMO_DELETE) {
            boolean result = m.getData().getBoolean(Common.TAG_RESULT);
            if (result) {
                list.clear();
                ArrayList<MemoItem> temp = m.getData().getParcelableArrayList(Common.TAG_MEMOS);
                list.addAll(temp);
                Collections.sort(list, MemoItem.COMPARATOR_DATE_DESC);
                adapter.notifyDataSetChanged();
                emptyLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
            }
        }
    }

    private void initView() {
        emptyLayout = fv(R.id.layout_empty);
        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new MemoAdapter(getActivity(), R.layout.item_memo, sp.getLoginEmail(), list, new MemoAdapter.MemoAdapterListener() {
            @Override
            public void onMemoClicked(int position, MemoItem item) {

            }

            @Override
            public void OnItemClick(int position, MemoItem item) {
                if (adapter.isDeleteMode()) {
                    adapter.select(position);
                    if (listener != null)
                        listener.onCheckChanged(adapter.getSelectedList().size(), adapter.isSelectedAll());
                }
            }

            @Override
            public void OnItemLongClick(int position, MemoItem item) {

            }
        });
//        adapter = new MemoAdapter(getActivity(), R.layout.item_memo, sp.getLoginEmail(), list, new MemoAdapter.BookmarkAdapterListener() {
//            @Override
//            public void OnItemClick(int position, MemoItem item) {
//                if (adapter.isDeleteMode()) {
//                    adapter.select(position);
//                    if (listener != null)
//                        listener.onCheckChanged(adapter.getSelectedList().size(), adapter.isSelectedAll());
//                }
//
//            }
//
//            @Override
//            public void OnItemLongClick(int position, MemoItem item) {
//
//            }
//        });
        lv.setAdapter(adapter);
    }

    public void refreshList() {
        if (api == null)
            return;

        api.getMemoList();
        if (listener != null)
            listener.onShowProgress();
    }

    public void setDeleteMode(boolean mode) {
        if (getActivity() == null || adapter == null)
            return;
        adapter.setDeleteMode(mode);
    }

    public void setDeleteAllSelected(boolean all) {
        if (getActivity() == null || adapter == null || !adapter.isDeleteMode())
            return;

        if (all)
            adapter.setAllSelection();
        else
            adapter.clearSelection();

        if (listener != null)
            listener.onCheckChanged(adapter.getSelectedList().size(), adapter.isSelectedAll());
    }

    public void deleteSelectedItem() {
        if (getActivity() == null || adapter == null || !adapter.isDeleteMode())
            return;

        for (MemoItem item : adapter.getSelectedList()) {
            if (listener != null)
                listener.onShowProgress();
            api.deleteMemo(item);

        }
    }

    public void setListener(MemoFragmentListener listener) {
        this.listener = listener;
    }

    public static interface MemoFragmentListener {
        public void onCheckChanged(int count, boolean all);

        public void onShowProgress();

        public void onDismissProgress();
    }
}
