package io.jmobile.tm.browser.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import io.jmobile.tm.browser.R;
import io.jmobile.tm.browser.base.BaseActivity;
import io.jmobile.tm.browser.common.LogUtil;
import io.jmobile.tm.browser.common.Util;
import io.jmobile.tm.browser.network.TMB_network;
import io.jmobile.tm.browser.ui.dialog.JoinTeamDialog;
import io.jmobile.tm.browser.ui.dialog.MessageDialog;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static io.jmobile.tm.browser.common.Util.Progress;
import static io.jmobile.tm.browser.common.Util.hideKeyBoard;
import static io.jmobile.tm.browser.common.Util.showToast;

public class TeamSearchActivity extends BaseActivity {

    ImageButton backButton, searchButton, deleteButton;
    EditText searchEdit;
    ProgressBar progressBar;
    TextView messageText;
    LinearLayout messageLayout, resultLayout;

    TextView nameText, emailText, membersText, joinedText;
    Button joinButton;

    String teamId;
    String teamName;
    Dialog progress;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_team_search);

        initView();
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> onBackPressed());

        searchButton = fv(R.id.button_search);
        searchButton.setOnClickListener(v -> startSearchTeam());
        searchEdit = fv(R.id.edit_search);
        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                deleteButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });
        searchEdit.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                startSearchTeam();
                return true;
            } else
                return false;
        });

        deleteButton = fv(R.id.button_delete);
        deleteButton.setOnClickListener(v -> {
            if (searchEdit != null)
                searchEdit.setText("");
        });

        progressBar = fv(R.id.progress);
        progressBar.setVisibility(View.GONE);

        messageLayout = fv(R.id.layout_message);
        messageLayout.setVisibility(View.GONE);
        messageText = fv(R.id.text_message);

        resultLayout = fv(R.id.layout_result);
        resultLayout.setVisibility(View.GONE);

        nameText = fv(R.id.text_team_name);
        emailText = fv(R.id.text_team_email);
        membersText = fv(R.id.text_team_members);
        joinedText = fv(R.id.text_team_joined);

        joinButton = fv(R.id.button_team_join);
        joinButton.setOnClickListener(v -> {
            if (joinButton.getText().toString().equalsIgnoreCase(r.s(R.string.message_search_request_join_send))) {
                if (fdf(JoinTeamDialog.TAG) == null) {
                    JoinTeamDialog dlg = JoinTeamDialog.newInstance(JoinTeamDialog.TAG, teamId);
                    dlg.setPositiveListener((dialog, tag) -> {
                        if (fdf(MessageDialog.TAG) == null) {
                            MessageDialog mDlg = MessageDialog.newInstance(MessageDialog.TAG);
                            mDlg.setMessage(r.s(R.string.message_search_request_join_complete));
                            mDlg.setPositiveLabel(r.s(R.string.ok));
                            mDlg.setPositiveListener((mDialog, mTag) -> {
                                finish();
                            });
                            sdf(mDlg);
                        }

                    });
                    sdf(dlg);
                }
            } else {
                requestJoin();
            }
        });
    }

    private boolean checkEmpty() {
        if (TextUtils.isEmpty(searchEdit.getText().toString())) {
            showToast(this, r.s(R.string.message_empty_team_search), true);
            return true;
        }

        return false;
    }

    private void setSearchResult(JsonObject object) {
        resultLayout.setVisibility(View.GONE);

        try {
            JSONObject o = new JSONObject(object.toString());

            JSONObject team = o.getJSONObject("team");
            teamId = Util.optString(team, "team_id");
            teamName = Util.optString(team, "name");
            String join_rule = Util.optString(team, "join_rule");
            String join_protect_domain = Util.optString(team, "join_protect_domain");

            JSONObject leader = o.getJSONObject("leader");
            String role = Util.optString(leader, "role");

            JSONObject user = leader.getJSONObject("User");
            String userName = Util.optString(user, "name");
            String email = Util.optString(user, "email");
            boolean joinable = o.getBoolean("joinable");
            boolean is_member = o.getBoolean("is_member");
            int count = o.getInt("count");

            if (join_rule.equals("protect") || join_rule.equals("public")) {
                resultLayout.setVisibility(View.VISIBLE);
                nameText.setText(teamName);
                emailText.setText(userName + "(" + email + ")");
                if (count > 0) {
                    membersText.setText(String.format(r.s(R.string.format_members), count));
                    membersText.setVisibility(View.VISIBLE);
                } else
                    membersText.setVisibility(View.GONE);

                if (joinable) {
                    joinButton.setVisibility(View.VISIBLE);
                    joinedText.setVisibility(View.GONE);
                    joinButton.setText(r.s(join_rule.equals("public") ? R.string.message_search_request_join_send : R.string.join));
                } else {
                    joinButton.setVisibility(View.GONE);
                    joinedText.setVisibility(View.VISIBLE);
                    joinedText.setText(r.s(is_member ? R.string.joined : R.string.pending));
                }
            }
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

    private void startSearchTeam() {
        resultLayout.setVisibility(View.GONE);

        if (checkEmpty())
            return;

        if (searchEdit != null) {
            hideKeyBoard(searchEdit);
        }
        deleteButton.setVisibility(View.GONE);

        messageText.setText(r.s(R.string.message_searching));
        messageLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        try {
            TMB_network.findTeam(searchEdit.getText().toString().trim())
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        progressBar.setVisibility(View.GONE);
                        messageLayout.setVisibility(View.GONE);
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                progressBar.setVisibility(View.GONE);
                                messageLayout.setVisibility(View.GONE);
                                if (tmBrowserPair.first) {
                                    JsonObject result = ((JsonObject) tmBrowserPair.second).getAsJsonObject("result");
                                    setSearchResult(result);
                                } else {
                                    String error = (String) tmBrowserPair.second;
                                    if (error != null && error.equals("403"))
                                        messageText.setText(r.s(R.string.message_search_team_private));
                                    else
                                        messageText.setText(r.s(R.string.message_search_no_team));
                                    messageLayout.setVisibility(View.VISIBLE);
                                }
                            },
                            throwable -> {
                                progressBar.setVisibility(View.GONE);
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            LogUtil.log(e.getMessage());
        }
    }

    private void requestJoin() {
        showProgress();
        try {
            TMB_network.joinTeam(teamId)
                    .flatMap(tmBrowserPair -> {
                        return Observable.just(tmBrowserPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tmBrowserPair -> {
                                dismissProgress();
                                if (tmBrowserPair.first) {
                                    if (fdf(MessageDialog.TAG) == null) {
                                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                                        dlg.setMessage(teamName + " " + r.s(R.string.message_search_join_complete));
                                        dlg.setPositiveLabel(r.s(R.string.ok));
                                        dlg.setPositiveListener((dialog, tag) -> {
                                            Intent intent = new Intent();
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        });
                                        sdf(dlg);
                                    }
                                } else {
                                    showToast(this, (String) tmBrowserPair.second, false);
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
        }
    }

    private void showProgress() {
        if (progress == null) {
            progress = Progress(this, r.s(R.string.title_request_join_team), false);
            progress.show();
        }
    }

    private void dismissProgress() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }
}
